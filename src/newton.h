/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef NEWTON_H
	#define NEWTON_H



#include "daten.h"


//! Newton-Verfahren-Klasse
/*!
Beinhaltet die Methoden des Newton-Verfahrens.
\author Oliver Borm
*/
class newton
{
	private:
		daten			*datenObjekt;
		v3Knoten		*knoten;
		v2Quasiorthogonale	*quasiorthogonale;

	public:
		newton();

		
		//! Bisektionsverfahren zur Bestimmung des Drucks
		/*!
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return Druck \f$ p \f$
		    \author Oliver Borm
		*/
		double newtonVerfahren_p ( int j, int k );

		
		//! Bisektionsverfahren für Verschiebung der Stromlinien
		/*!
		    Verändert mit Hilfe des Massenstromes die Lage der Stromlinien
		    \param m_ziel der zu erreichende Massenstrom
		    \param m_akt aktueller Massenstrom
		    \param ny_neu aktuelle Lage der Stromlinie
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return Lage \f$ \nu \f$
		    \author Oliver Borm
		*/
		double newtonVerfahren ( double m_ziel,double m_akt,double ny_neu, int i, int j, int k );
};

#endif
