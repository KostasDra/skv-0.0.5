/***************************************************************************
 *   Copyright (C) 2009-2010 Andrea Bader (bader.andrea@gmx.de)            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Andrea Bader
*/



#ifndef TEMPERATUR_DENTON_H
	#define TEMPERATUR_DENTON_H

#include<cmath>

#include "daten.h"
#include "temperatur.h"


class temperatur_denton
{
	private:
		daten 			*datenObjekt;
		v3Knoten		*knoten;
		v2Quasiorthogonale	*quasiorthogonale;
	public:
		temperatur_denton();


		//! Berechnung der absoluten Totaltemperatur
		/*!
		    Berechnet den Wert der absoluten Totaltemperatur aus der Totalenthalpie und cp.

		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter

		    \author Andrea Bader
		*/
		double berechne_T_tot_abs ( int i, int j, int k );


		//! Berechnung der statischen Temperatur
		/*! Die statische Temperatur wird aus der eingelesenen oder berechneten Totaltemperatur berechnet.

		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter

		    \author Andrea Bader
		*/
		double berechne_T ( int i, int j, int k );

};

#endif
