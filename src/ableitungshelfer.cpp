/***************************************************************************
 *   Copyright (C)  2009-2010 by Andrea Bader (bader.andrea@gmx.de)        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author  Andrea Bader
*/

#include "ableitungshelfer.h"

daten* ableitungshelfer::bekommeDaten()
{
	return daten::getInstance();
}


double ableitungshelfer::bildeErsteAbleitung_dq(int k, int j, int i, EKnotenEigenschaft eigen)
{
	daten* datenObjekt = ableitungshelfer::bekommeDaten();
	v3Knoten* knoten   = &datenObjekt->knoten;

	double ableitungsResult = 0.0;
	double F_i = 		(*knoten)[k][i][j].bekommeKnotenWert(eigen);

	//! Für die 1. Stromlinie (Vorwärtsableitung):
	if (i == 0)  //erste -> Vorwärtsableitung
	{
		double F_iPlus2 = (*knoten)[k][i+2][j].bekommeKnotenWert(eigen);
		double F_iPlus1 = (*knoten)[k][i+1][j].bekommeKnotenWert(eigen);
		//! - \f[ q^+ = \sqrt{\left( r(i+1)-r(i)\right)^2 + \left(z(i+1)-z(i)\right)^2} \f]
		double qPlus 	= sqrt ( pow2((*knoten)[k][i+1][j].r - (*knoten)[k][i][j].r)   + pow2((*knoten)[k][i+1][j].z-(*knoten)[k][i][j].z)) ;
		//! - \f[ q^{++} = \sqrt{\left(r(i+2)-r(i+1)\right)^2 + \left(z(i+2)-z(i+1)\right)^2} \f]
		double qPlusplus= sqrt ( pow2((*knoten)[k][i+2][j].r - (*knoten)[k][i+1][j].r) + pow2((*knoten)[k][i+2][j].z-(*knoten)[k][i+1][j].z));

		//! - \f[ \frac{\partial F}{\partial q} = \frac{\left(F(i+1)-F(i)\right)\cdot(1+\frac{q^{++}}{q^{+}})^2-\left(F(i+2)-F(i)\right)}{q^{++}\cdot(1+\frac{q^{++}}{q^+})}\f]
		ableitungsResult = 	( (F_iPlus1 - F_i)* pow2( 1 + qPlusplus / qPlus ) - ( F_iPlus2 - F_i )) /
							( qPlusplus * ( 1 + qPlusplus / qPlus ) ) ;
	}
	//! Für die letzte Stromlinie (Rückwärtsableitung):
	else if ( i == datenObjekt->anzahlStromlinien-1 ) 	// letzte -> Rückwärtsableitung
	{
		double F_iMinus2 = (*knoten)[k][i-2][j].bekommeKnotenWert(eigen);
		double F_iMinus1 = (*knoten)[k][i-1][j].bekommeKnotenWert(eigen);
		//! - \f[ q^- = \sqrt{\left( r(i)-r(i-1)\right)^2 + \left(z(i)-z(i-1)\right)^2} \f]
		double qMinus 	 = sqrt ( pow2((*knoten)[k][i][j].r-	(*knoten)[k][i-1][j].r) + pow2((*knoten)[k][i][j].z-	(*knoten)[k][i-1][j].z));
		//! - \f[ q^{--} = \sqrt{\left(r(i-1)-r(i-2)\right)^2 + \left(z(i-1)-z(i-2)\right)^2} \f]
		double qMinusminus= sqrt ( pow2((*knoten)[k][i-1][j].r-	(*knoten)[k][i-2][j].r) + pow2((*knoten)[k][i-1][j].z-(*knoten)[k][i-2][j].z));

		//! - \f[ \frac{\partial F}{\partial q} = \frac{\left(F(i)-F(i-1)\right)\cdot(1+\frac{q^{--}}{q^{-}})^2-\left(F(i)-F(i-2)\right)}{q^{--}\cdot(1+\frac{q^{--}}{q^-})}\f]
		ableitungsResult = 	( ( F_i - F_iMinus1 ) * pow2( 1 + qMinusminus/qMinus ) - ( F_i - F_iMinus2 ) ) /
				(  qMinusminus  * ( 1 + qMinusminus/qMinus ) ) ;
	}
	//! Für dazwischenliegende Stromlinien (Zentralableitung):
	else if ( i != datenObjekt->anzahlStromlinien-1 && i != 0 ) //mitte -> Zentralableitung
	{
		double F_iMinus1 = (*knoten)[k][i-1][j].bekommeKnotenWert(eigen);
		double F_iPlus1  = (*knoten)[k][i+1][j].bekommeKnotenWert(eigen);
		//! - \f[ q^+ = \sqrt{\left( r(i+1)-r(i)\right)^2 + \left(z(i+1)-z(i)\right)^2} \f]
		double qPlus     = sqrt ( pow2((*knoten)[k][i+1][j].r-	(*knoten)[k][i][j].r) 	+ pow2((*knoten)[k][i+1][j].z-(*knoten)[k][i][j].z)) ;
		//! - \f[ q^- = \sqrt{\left( r(i)-r(i-1)\right)^2 + \left(z(i)-z(i-1)\right)^2} \f]
		double qMinus    = sqrt ( pow2((*knoten)[k][i][j].r-	(*knoten)[k][i-1][j].r) + pow2((*knoten)[k][i][j].z-	(*knoten)[k][i-1][j].z));

		//! - \f[ \frac{\partial F}{\partial q} = \frac{\left(F(i+1)-F(i)\right) \cdot (q^-)^2+\left(F(i)-F(i-1)\right)\cdot (q^+)^2}{q^+\cdot(q^-)^2+q^-\cdot(q^+)^2}\f]
		ableitungsResult = 	(( F_iPlus1 - F_i)  * pow2(qMinus) + ( F_i -  F_iMinus1) * pow2(qPlus)) /
				( qPlus * pow2(qMinus) + qMinus * pow2 (qPlus) );
	}
	return ableitungsResult;
}


double ableitungshelfer::bildeErsteAbleitung_dm(int k, int j, int i, EKnotenEigenschaft eigen)
{
	daten* datenObjekt = ableitungshelfer::bekommeDaten();
	v3Knoten* knoten   = &datenObjekt->knoten;

	double ableitungsResult = 0.0;
	double F_j = 		(*knoten)[k][i][j].bekommeKnotenWert(eigen);

	//! Für die 1. Stromlinie (Vorwärtsableitung):
	if (j == 0)  //erste -> Vorwärtsableitung
	{
		//! - \f[ m^{++} = \sqrt{\left(r(j+2)-r(j+1)\right)^2 + \left( z(j+2)-z(j+1) \right)^2} \f]
		double mPlusplus = sqrt ( pow2((*knoten)[k][i][j+2].r-	(*knoten)[k][i][j+1].r) + 	pow2((*knoten)[k][i][j+2].z-(*knoten)[k][i][j+1].z));
		//! - \f[ m^{+} = \sqrt{\left(r(j+1)-r(j)\right)^2 + \left( z(j+1)-z(j) \right)^2} \f]
		double mPlus     = sqrt ( pow2((*knoten)[k][i][j+1].r-	(*knoten)[k][i][j].r) 	+ 	pow2((*knoten)[k][i][j+1].z-(*knoten)[k][i][j].z)) ;
		double F_jPlus2  = (*knoten)[k][i][j+2].bekommeKnotenWert(eigen);
		double F_jPlus1  = (*knoten)[k][i][j+1].bekommeKnotenWert(eigen);

		//! - \f[ \frac{\partial F}{\partial m} = \frac{\left(F(j+1)-F(j)\right) \cdot \left( 1+\frac{m^{++}}{m^+} \right)^2-(F(j+2)-F(j))}{m^{++}\cdot \left( 1+\frac{m^{++}}{m^+} \right)}\f]
		ableitungsResult = 	( (F_jPlus1 - F_j)* pow2( 1 + mPlusplus / mPlus ) - ( F_jPlus2 - F_j )) /
							( mPlusplus * ( 1 + mPlusplus / mPlus ) ) ;
	}
	//! Für die letzte Stromlinie (Rückwärtsableitung):
	else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 ) 	// letzte -> Rückwärtsableitung
	{
		//! - \f[ m^{--} = \sqrt{\left(r(j-1)-r(j-2)\right)^2 + \left( z(j-1)-z(j-2) \right)^2} \f]
		double mMinusminus = sqrt ( pow2((*knoten)[k][i][j-1].r-	(*knoten)[k][i][j-2].r) + 	pow2((*knoten)[k][i][j-1].z-(*knoten)[k][i][j-2].z));
		//! - \f[ m^{-} = \sqrt{\left(r(j)-r(j-1)\right)^2 + \left( z(j)-z(j-1) \right)^2} \f]
		double mMinus      = sqrt ( pow2((*knoten)[k][i][j].r-	(*knoten)[k][i][j-1].r) + 	pow2((*knoten)[k][i][j].z-	(*knoten)[k][i][j-1].z));
		double F_jMinus1   = (*knoten)[k][i][j-1].bekommeKnotenWert(eigen);
		double F_jMinus2   = (*knoten)[k][i][j-2].bekommeKnotenWert(eigen);

		//! - \f[ \frac{\partial F}{\partial m} = \frac{\left(F(j)-F(j-1)\right) \cdot \left( 1+\frac{m^{--}}{m^-} \right)^2-(F(j)-F(j-2))}{m^{--}\cdot \left( 1+\frac{m^{--}}{m^-} \right)}\f]
		ableitungsResult = 	( ( F_j - F_jMinus1 ) * pow2( 1 + mMinusminus/mMinus ) - ( F_j - F_jMinus2 ) ) /
							(  mMinusminus  * ( 1 + mMinusminus/mMinus ) ) ;
	}
	//! Für dazwischenliegende Stromlinien (Zentralableitung):
	else if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 ) //mitte -> Zentralableitung
	{
		//! - \f[ m^{-} = \sqrt{\left(r(j)-r(j-1)\right)^2 + \left( z(j)-z(j-1) \right)^2} \f]
		double mMinus    = sqrt ( pow2((*knoten)[k][i][j].r-	(*knoten)[k][i][j-1].r) + 	pow2((*knoten)[k][i][j].z-	(*knoten)[k][i][j-1].z));
		//! - \f[ m^{+} = \sqrt{\left(r(j+1)-r(j)\right)^2 + \left( z(j+1)-z(j) \right)^2} \f]
		double mPlus     = sqrt ( pow2((*knoten)[k][i][j+1].r-	(*knoten)[k][i][j].r) 	+ 	pow2((*knoten)[k][i][j+1].z-(*knoten)[k][i][j].z)) ;
		double F_jMinus1 = (*knoten)[k][i][j-1].bekommeKnotenWert(eigen);
		double F_jPlus1  = (*knoten)[k][i][j+1].bekommeKnotenWert(eigen);

		//! - \f[ \frac{\partial F}{\partial m} = \frac{\left(F(j+1)-F(j)\right)(m^-)^2+\left( F(j)-F(j-1) \right)}{m^+\cdot (m^-)^2 +m^- \cdot (m^+)^2}\f]
		ableitungsResult = 	(( F_jPlus1 - F_j)  * pow2(mMinus) + ( F_j -  F_jMinus1) * pow2(mPlus)) /
							( mPlus * pow2(mMinus) + mMinus * pow2 (mPlus) );
	}
	return ableitungsResult;
}


double ableitungshelfer::bildeZweiteAbleitung_dm (int k, int j, int i, EKnotenEigenschaft eigen)
{
	daten* datenObjekt = ableitungshelfer::bekommeDaten();
	v3Knoten* knoten   = &datenObjekt->knoten;

	double ableitungsResult = 0.0;
	double F_j = 		(*knoten)[k][i][j].bekommeKnotenWert(eigen);

	//! Für die 1. Stromlinie (Vorwärtsableitung):
	if (j == 0)
	{
		double F_jPlus2 = (*knoten)[k][i][j+2].bekommeKnotenWert(eigen);
		//! - \f[ m^+ = \sqrt{\left( r(j+1)-r(j)\right)^2 + \left(z(j+1)-z(j)\right)^2} \f]
		double mPlus 	= sqrt ( pow2((*knoten)[k][i][j+1].r-	(*knoten)[k][i][j].r) 	+ 	pow2((*knoten)[k][i][j+1].z-(*knoten)[k][i][j].z)) ;
		//! - \f[ m^{++} = \sqrt{\left(r(j+2)-r(j+1)\right)^2 + \left(z(j+2)-z(j+1)\right)^2} \f]
		double mPlusplus= sqrt ( pow2((*knoten)[k][i][j+2].r-	(*knoten)[k][i][j+1].r) + 	pow2((*knoten)[k][i][j+2].z-(*knoten)[k][i][j+1].z));
		double F_jPlus1 = (*knoten)[k][i][j+1].bekommeKnotenWert(eigen);

		//! - \f[ \frac{\partial ^2F}{\partial m^2} = \frac{2\left((F(j+2)-F(j))-(F(j+1)-F(j)(1+\frac{m^{++}}{m^{+}}))\right)}{(m^{++})^2\left( 1+\frac{m^{+}}{m^{++}} \right)}\f]
		ableitungsResult = 	2 * ( (F_jPlus2 - F_j) - ( F_jPlus1 - F_j ) * ( 1 + mPlusplus / mPlus ) ) /
								( pow2( mPlusplus)  * ( 1 + mPlus / mPlusplus ) ) ;
	}
	//! Für die letzte Stromlinie (Rückwärtsableitung):
	else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 )
	{
		double F_jMinus2 = 	(*knoten)[k][i][j-2].bekommeKnotenWert(eigen);
		//! - \f[ m^- = \sqrt{\left( r(j)-r(j-1)\right)^2 + \left(z(j)-z(j-1)\right)^2} \f]
		double mMinus = 		sqrt ( pow2((*knoten)[k][i][j].r-	(*knoten)[k][i][j-1].r) + 	pow2((*knoten)[k][i][j].z-	(*knoten)[k][i][j-1].z));
		//! - \f[ m^{--} = \sqrt{\left(r(j-1)-r(j-2)\right)^2 + \left(z(j-1)-z(j-2)\right)^2} \f]
		double mMinusminus = 	sqrt ( pow2((*knoten)[k][i][j-1].r-	(*knoten)[k][i][j-2].r) + 	pow2((*knoten)[k][i][j-1].z-(*knoten)[k][i][j-2].z));
		double F_jMinus1 = 	(*knoten)[k][i][j-1].bekommeKnotenWert(eigen);

		//! - \f[ \frac{\partial ^2F}{\partial m^2} = \frac{2\left((F(j)-F(j-1))(1+\frac{m^{--}}{m^{-}})-(F(j)-F(j-2))\right)}{(m^{--})^2(1+\frac{m^{-}}{m^{--}})}\f]
		ableitungsResult = 	2 * ( ( F_j - F_jMinus1 ) * ( 1 + mMinusminus / mMinus ) - ( F_j - F_jMinus2 ) ) /
								( pow2( mMinusminus ) * ( 1 +  mMinus / mMinusminus ) ) ;
	}
	//! Für dazwischenliegende Stromlinien (Zentralableitung):
	else if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 )
	{
		//! - \f[ m^+ = \sqrt{\left( r(j+1)-r(j)\right)^2 + \left(z(j+1)-z(j)\right)^2} \f]
		double mPlus = 			sqrt ( pow2((*knoten)[k][i][j+1].r-	(*knoten)[k][i][j].r) 	+ 	pow2((*knoten)[k][i][j+1].z-(*knoten)[k][i][j].z)) ;
		//! - \f[ m^- = \sqrt{\left( r(j)-r(j-1)\right)^2 + \left(z(j)-z(j-1)\right)^2} \f]
		double mMinus = 		sqrt ( pow2((*knoten)[k][i][j].r-	(*knoten)[k][i][j-1].r) + 	pow2((*knoten)[k][i][j].z-	(*knoten)[k][i][j-1].z));
		double F_jPlus1 = 	(*knoten)[k][i][j+1].bekommeKnotenWert(eigen);
		double F_jMinus1 = 	(*knoten)[k][i][j-1].bekommeKnotenWert(eigen);

		//! - \f[ \frac{\partial ^2F}{\partial m^2} = \frac{2\left( (F(j+1)\cdot m^{-})-F(j)(m^{-}+m^{+}+F(j-1)\cdot m^{+}) \right)}{(m^{-}\cdot (m^{+})^2+m^{+}\cdot(m^{-})^2)}\f]
		ableitungsResult = 	2 * (( F_jPlus1 * mMinus ) - F_j * ( mMinus + mPlus ) + F_jMinus1*mPlus ) /
								( mMinus * pow2(mPlus) + mPlus * pow2 (mMinus) );
	}
	return ableitungsResult;
}


double ableitungshelfer::bildeEinfachVorwaertsAbleitung_dq(int k, int j, int i, EKnotenEigenschaft eigen)
{
	daten* datenObjekt = ableitungshelfer::bekommeDaten();
	v3Knoten* knoten   = &datenObjekt->knoten;

	//! Die Vorwärtsableitung einer beliebigen Knoteneigenschaft \f$ \frac{\partial F}{\partial q} \f$ wird folgendermaßen berechnet:
	double ableitungsResult = 0.0;
	double F_i 		= (*knoten)[k][i][j].bekommeKnotenWert(eigen);
	double F_iPlus1		= (*knoten)[k][i+1][j].bekommeKnotenWert(eigen);

	//! - \f[	\Delta q = \sqrt{\left(z(i+1)-z(i)\right)^2+\left(r(i+1)-r(i)\right)^2}	\f]
	double delta_q = sqrt ( pow2(  (*knoten)[k][i+1][j].z-(*knoten)[k][i][j].z )
						   +pow2(  (*knoten)[k][i+1][j].r-(*knoten)[k][i][j].r ) );
	//! - \f[	\frac{\partial F}{\partial q} = \frac{F(i+1)-F(i)}{\Delta q}	\f]
	ableitungsResult = (F_iPlus1 - F_i) / delta_q;
	return ableitungsResult;
}


double ableitungshelfer::bildeEinfachRueckwaertsAbleitung_dm(int k, int j, int i, EKnotenEigenschaft eigen)
{
	daten* datenObjekt = ableitungshelfer::bekommeDaten();
	v3Knoten* knoten   = &datenObjekt->knoten;
	double ableitungsResult = 0.0;
	//! Die Rückwärtsableitung einer beliebigen Knoteneigenschaft \f$ \frac{\partial F}{\partial m} \f$ wird folgendermaßen berechnet:
	double F_j = 	   (*knoten)[k][i][j].bekommeKnotenWert(eigen);

	//! Für die 1. Quasiorthogonale:
	if (j == 0) //einfach Vörwärts auf der 1. QO
	{
		double F_jPlus1 = (*knoten)[k][i][j+1].bekommeKnotenWert(eigen);
		//! - \f[	\Delta m = \sqrt{\left(z(j+1)-z(j)\right)^2+\left(r(j+1)-r(j)\right)^2}	\f]
		double delta_m = sqrt ( pow2(  (*knoten)[k][i][j+1].z-(*knoten)[k][i][j].z )
						      + pow2(  (*knoten)[k][i][j+1].r-(*knoten)[k][i][j].r ) );
		//! - \f[	\frac{\partial F}{\partial m} = \frac{F(j+1)-F(j)}{\Delta m}	\f]
		ableitungsResult = (F_jPlus1 - F_j) / delta_m ;
	}
	//! Für alle anderen Quasiorthogonalen:
	else
	{
		double F_jMinus1 = (*knoten)[k][i][j-1].bekommeKnotenWert(eigen);
		//! - \f[	\Delta m = \sqrt{\left(z(j)-z(j-1)\right)^2+\left(r(j)-r(j-1)\right)^2}	\f]
		double delta_m = sqrt ( pow2(  (*knoten)[k][i][j].z-(*knoten)[k][i][j-1].z )
				               +pow2(  (*knoten)[k][i][j].r-(*knoten)[k][i][j-1].r ) );
		//! - \f[	\frac{\partial F}{\partial m} = \frac{F(j)-F(j-1)}{\Delta m}	\f]
		ableitungsResult = (F_j - F_jMinus1) / delta_m ;
	}
	return ableitungsResult;
}

