/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm
*/

#include "ausgabe.h"





ausgabe::ausgabe()
{
	datenObjekt 		= daten::getInstance();
	quasiorthogonale 	= &datenObjekt->quasiorthogonale;
	stromlinie		= &datenObjekt->stromlinie;
	
}





void ausgabe::ausgabeDaten ()
{
	//! Ablauf:
	
	//! - Daten in die CGNS-Datei schreiben CGNSformat.writeCGNS().
	data.writeCGNS (datenObjekt->getworkdir());
	
	//! - Schreibe Massenstrom gemittelte Wirkungsgrade (aus datenObjekt).
	ofstream eta ( ( datenObjekt->getworkdir() + "Wirkungsgrade" ).c_str() );					//fm
	eta.setf ( ios::scientific );
	eta.precision ( 10 );

	eta << "Massenstrom gemittelter isentroper Wirkungsgrad: " <<
	datenObjekt->eta_isentrop_m << endl;
	eta << endl;

	eta << "Massenstrom gemittelter polytroper Wirkungsgrad: " <<
	datenObjekt->eta_polytrop_m << endl;
	eta << endl;

	//! - Schreibe Massenstrom gemitteltes Totaldruckverhältnis.
	eta << "Massenstrom gemitteltes Totaldruckverhältnis: " <<
	(*quasiorthogonale)[datenObjekt->anzahlGitter-1]
				     [datenObjekt->anzahlQuasiOrthogonale[datenObjekt->anzahlGitter-1]-1].Pi_tot_abs_m << endl;
	eta << endl;

	//! - Schreibe Wirkungsgrade jeder Stromlinie
	eta << "Isentroper Wirkungsgrad auf jeder Stromlinie: " << endl;

	for ( int i=datenObjekt->anzahlStromlinien-1;i>=0;i-- )
	{
		eta << (*stromlinie)[i].eta_isentrop << endl;
	}
	eta << endl;

	eta << "Polytroper Wirkungsgrad auf jeder Stromlinie: " << endl;

	for ( int i=datenObjekt->anzahlStromlinien-1;i>=0;i-- )
	{
		eta << (*stromlinie)[i].eta_polytrop << endl;
	}
	eta << endl;

	//! - Schreibe Druckverhältnis auf jeder Stromlinie.
	eta << "Druckverhältnis auf jeder Stromlinie: " << endl;

	for ( int i=datenObjekt->anzahlStromlinien-1;i>=0;i-- )
	{
		eta << (*stromlinie)[i].Pi_tot_abs << endl;
	}
	eta << endl;

	eta.close();
}
