/***************************************************************************
 *   Copyright (C) 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                 2009 by Oliver Borm (oli.borm@web.de)                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef STROMLINIENDATEN_H
	#define STROMLINIENDATEN_H



#include <nurbs++/nurbs.h>



using namespace PLib;


//! Stromlinien-Datenklasse
/*!
Beinhaltet die NURBS-Kurven der Stromlinien mit ihren gespeicherten Werten.
\author Oliver Borm, Florian Mayer
*/
struct Stromliniendaten
{
	double h_rot;		//!< Rothalpie
	double deHaller;	//!< DeHaller Zahl
 	double Pi_tot_rel;	//!< Relatives Totaldruckverhältnis
	double Pi_tot_abs;	//!< Absolutes totaldruckverhältnis
	double Pi_abs;		//!< Absolutes Druckverhältnis
	double eta_isentrop;	//!< Isentroper Wirkungsgrad \f[	\eta_{isentrop}	\f]
	double eta_polytrop;	//!< Polytroper Wirkungsgrad \f[	\eta_{polytrop}	\f]

	NurbsCurve_2Dd nurbsStromlinie;
	NurbsCurve_2Dd nurbsEpsilon;
	NurbsCurve_2Dd nurbsEntropieSL;
	NurbsCurve_2Dd nurbsSigmaSL;
	NurbsCurve_2Dd nurbsCosEpsilon;
	NurbsCurve_2Dd nurbsCotBetaSL;

	NurbsCurve_2Dd nurbsGitterStromlinie;
	NurbsCurve_2Dd nurbsEpsilonGitter;
	NurbsCurve_2Dd nurbsEntropieGitterSL;
	NurbsCurve_2Dd nurbsSigmaGitterSL;
	NurbsCurve_2Dd nurbsCosEpsilonGitter;
	NurbsCurve_2Dd nurbsCotBetaGitterSL;

	//ab: nicht mehr im Einsatz, aber belassen zu Testzwecken
	NurbsCurve_2Dd nurbsWmSL;					//geschwindigkeit_denton::aktualisiere_nurbsWmSL ( )
	NurbsCurve_2Dd nurbsWmGitterSL;				//geschwindigkeit_denton::aktualisiere_nurbsWmSL ( )
	NurbsCurve_2Dd nurbsDrallSL;				//geschwindigkeit_denton::aktualisiere_nurbsDrallSL ( )
	NurbsCurve_2Dd nurbsDrallGitterSL;			//geschwindigkeit_denton::aktualisiere_nurbsDrallSL ( )
	NurbsCurve_2Dd nurbsDrallQuadratSL; 		//geschwindigkeit_denton::aktualisiere_nurbsQuadratDrallSL ( )
	NurbsCurve_2Dd nurbsDrallQuadratGitterSL;  	//geschwindigkeit_denton::aktualisiere_nurbsQuadratDrallSL ( )
};

#endif
