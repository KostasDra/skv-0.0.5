/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm  (oli.borm@web.de)             *
 *                 2008-2009 by Florian Mayer (ilumine@gmx.de)             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm, Florian Mayer, Balint Balassa
*/



#include "loeser_expeu.h"



loeser_expeu::loeser_expeu( geometrie *geometrieObjektEingabe )
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	quasiorthogonale	= &datenObjekt->quasiorthogonale;
	stromlinieGitter 	= &datenObjekt->stromlinieGitter;
	geometrieObjekt 	= geometrieObjektEingabe;
}



double loeser_expeu::dgl ( int i, int j, int k, double dir) {
	double dgl;

	double wmaq = pow2(  (*knoten)[k][i][j].w_m /(*knoten)[k][i][j].a );
// 	double cotbeta = (*knoten)[k][i][j].cot_beta;
	double cotbetaq = 1+pow2( (*knoten)[k][i][j].cot_beta );
	double wmq = pow2(  (*knoten)[k][i][j].w_m );
	double singe = sin ( (*knoten)[k][i][j].gamma-(*knoten)[k][i][j].epsilon );
	double cosge = cos ( (*knoten)[k][i][j].gamma-(*knoten)[k][i][j].epsilon );
// 	double coseps = (*knoten)[k][i][j].cos_epsilon;
// 	double sineps = (*knoten)[k][i][j].sin_epsilon;
	double vierko = singe* ( (*knoten)[k][i][j].cos_epsilon * cot ( (*knoten)[k][i][j].beta_ru ) - (*knoten)[k][i][j].sin_epsilon * cot ( (*knoten)[k][i][j].beta_zu ) );
// 	double r = (*knoten)[k][i][j].r;
// 	double omega = datenObjekt->omega[k];
// 	double wm = (*knoten)[k][i][j].w_m;

	if ( (*knoten)[k][i][j].unbeschaufelt )		//! Falls Gitter unbeschaufelt:
	{
		/*!	\f{eqnarray*}
		\frac{\mathrm{d}p}{\mathrm{d}\nu} &=& \frac{\rho}{1-\left(\frac{w_m}{a}\right)^2} \cdot 
		\Bigg[ -w_m^2\cdot{dir}\cdot\varepsilon ' \cdot \sin(\gamma-\varepsilon) \cdot \bigg( 1-\bigg( \frac{w_m}{a} \bigg)^2 \bigg) \\
		& & + \frac{(w_u+\omega r)^2}{r} \cdot
		\bigg( \big( \sin\varepsilon \cos(\gamma-\varepsilon)+\cos\varepsilon\sin(\gamma-\varepsilon)\cdot{dir} \big) -{dir}\cdot
		\bigg( \frac{w_m}{a} \bigg)^2 \sin(\gamma-\varepsilon)\cos\varepsilon \bigg) \\
		& & + w_m^2 \cdot \bigg( -\frac{S'}{c_p} + \frac{\sin\varepsilon}{r} +  \frac{\partial \sin \varepsilon}{\partial r} +
		\frac{\partial \cos \varepsilon}{\partial z} \bigg) \cdot \cos(\gamma-\varepsilon) \Bigg]
		\f}	*/
	dgl = 	(*knoten)[k][i][j].dichte / ( 1.0 - wmaq ) *
		( -(wmq*dir) * epsilonObjekt.berechne_kruemmungsRadius ( i,j,k ) * singe * ( 1.0 - wmaq ) +
		pow2( (*knoten)[k][i][j].w_u + datenObjekt->omega[k] * (*knoten)[k][i][j].r ) /
		(*knoten)[k][i][j].r * (( (*knoten)[k][i][j].sin_epsilon * cosge + (*knoten)[k][i][j].cos_epsilon * singe * dir) - dir*wmaq * singe * (*knoten)[k][i][j].cos_epsilon ) +
		wmq * ( -ableitungObjekt.ersteAbleitung_entropie( i,j,k ) / (*knoten)[k][i][j].cp +
		(*knoten)[k][i][j].sin_epsilon / (*knoten)[k][i][j].r +
		ableitungObjekt.ersteAbleitung_r_sin_epsilon( i,j,k ) + ableitungObjekt.ersteAbleitung_z_cos_epsilon( i,j,k ) ) *
		cosge );

		return dgl;
	}
	else	//! Falls Gitter beschaufelt:
	{
		/*! \f{eqnarray*}
		\frac{\mathrm{d}p}{\mathrm{d}\nu} &=& \frac{\rho}{1-\big( \frac{w_m}{a} \big)^2 \cdot \big( 1+\cot^2\beta \big)}
		\Bigg[ -w_m^2\cdot{dir}\cdot\varepsilon ' \cdot \sin(\gamma-\varepsilon) \cdot \bigg( 1-\bigg( \frac{w_m}{a} \bigg)^2 \cdot \big( 1+\cot^2\beta \big)\bigg) \\
		& & + \frac{\big( w_m\cot\beta+\omega r \big)^2}{r} \cdot
		\bigg\{ \big(\sin\varepsilon \cos(\gamma-\varepsilon)+\cos\varepsilon\sin(\gamma-\varepsilon)\cdot{dir}\big) \\
		& & -{dir}\cdot\bigg(\frac{w_m}{a}\bigg)^2\sin(\gamma-\varepsilon) \big( \cos\varepsilon+\cot\beta\cot\beta_{zu} \big) \bigg\} \\
		& & -{dir}\cdot w_m \cdot \bigg( w_m \big(\frac{\partial\cot \beta}{\partial r}\sin\varepsilon + \frac{\partial \cot \beta}{\partial z}\cos\varepsilon\big)
		+ \big( \frac{w_m\cot\beta}{r}+2\omega \big)\cdot \sin\varepsilon \bigg) \\
		& & \cdot \bigg( \cos(\gamma-\varepsilon)\cot\beta +
		{dir}\cdot \sin(\gamma-\varepsilon) \big( \cos\varepsilon\cot\beta_{ru}-\sin\varepsilon\cot\beta_{zu} \big) \bigg(1-\bigg(\frac{w_m}{a}\bigg)^2 \bigg)\bigg) \\
		& & + w_m^2\cdot \bigg( \frac{\sigma'}{\sigma} - \frac{S'}{c_p} + \frac{\sin\varepsilon}{r} +
		\frac{\partial \sin \varepsilon}{\partial r} + \frac{\partial \cos \varepsilon}{\partial z} \bigg) \\
		& & \cdot \bigg( \cos(\gamma-\varepsilon)\big(1+\cot^2\beta\big) + {dir}\cdot \cot\beta \sin(\gamma-\varepsilon) \big( \cos\varepsilon\cot\beta_{ru}-\sin\varepsilon\cot\beta_{zu} \big) \bigg)
		\Bigg]
		\f} */
	dgl = 	(*knoten)[k][i][j].dichte / ( 1.0 - wmaq * cotbetaq ) *
		( -(wmq*dir) * epsilonObjekt.berechne_kruemmungsRadius ( i,j,k ) * singe * ( 1.0 - wmaq * cotbetaq ) +
		pow2(  (*knoten)[k][i][j].w_m * (*knoten)[k][i][j].cot_beta + datenObjekt->omega[k] * (*knoten)[k][i][j].r ) /
		(*knoten)[k][i][j].r * ( ((*knoten)[k][i][j].sin_epsilon * cosge + (*knoten)[k][i][j].cos_epsilon * singe * dir) - dir*wmaq * singe *
		( (*knoten)[k][i][j].cos_epsilon + (*knoten)[k][i][j].cot_beta * cot ((*knoten)[k][i][j].beta_zu) ) ) -
		dir*(*knoten)[k][i][j].w_m * ( (*knoten)[k][i][j].w_m * ( ableitungObjekt.ersteAbleitung_r_cot_beta(i, j, k) *
		(*knoten)[k][i][j].sin_epsilon + ableitungObjekt.ersteAbleitung_z_cot_beta(i,j,k) *
		(*knoten)[k][i][j].cos_epsilon ) + ((*knoten)[k][i][j].w_m * (*knoten)[k][i][j].cot_beta / (*knoten)[k][i][j].r + 2.0 * datenObjekt->omega[k] ) * (*knoten)[k][i][j].sin_epsilon ) * (cosge * (*knoten)[k][i][j].cot_beta + dir*vierko * (1.0 - wmaq)) +
		wmq * (ableitungObjekt.ersteAbleitung_sigma( i,j,k ) / (*knoten)[k][i][j].sigma -
		ableitungObjekt.ersteAbleitung_entropie( i,j,k ) / (*knoten)[k][i][j].cp + (*knoten)[k][i][j].sin_epsilon / (*knoten)[k][i][j].r +
		ableitungObjekt.ersteAbleitung_r_sin_epsilon( i,j,k ) + ableitungObjekt.ersteAbleitung_z_cos_epsilon( i,j,k )) *
		(cosge * cotbetaq + dir*(*knoten)[k][i][j].cot_beta * vierko));

		return dgl;
	}

	if ( datenObjekt->debug > 1 )
	{
		cout << "beschaufelter Raum:"<<endl;
// 	}
//
// 	if ( datenObjekt->debug > 1)
// 	{
		cout<<"i/j/k => "<<i<<"/"<<j<<"/"<<k<<endl;
		cout<<"Teil_A: "<< -wmq*epsilonObjekt.berechne_kruemmungsRadius ( i,j,k ) *singe* ( 1-wmaq*cotbetaq ) <<endl;
		cout<<"\twmq: "<< wmq<<endl;
		cout<<"\t1/kruemmungsRadius: "<< epsilonObjekt.berechne_kruemmungsRadius ( i,j,k ) <<endl;
		cout<<"\tsinge: "<< singe<<endl;
		cout<<"\t(1-wmaq*cotbetaq): "<< ( 1-wmaq*cotbetaq ) <<endl<<endl;

// 		cout<<"Teil_B: "<< +pow2( wm*cotbeta+omega*r ) /r* ( ( sineps*cosge+coseps*singe )-wmaq*singe* ( coseps + cotbeta *cot ( (*knoten)[k][i][j].beta_zu ) ) ) <<endl;
// 		cout<<"\tpow(wm*cotbeta+omega*r,2)/r: "<<pow2( wm*cotbeta+omega*r ) /r<<endl;
// 		cout<<"\t\twm: "<<wm<<endl;
// 		cout<<"\t\tcotbeta: "<<cotbeta<<endl;
// 		cout<<"\t\tomega*r: "<<omega*r<<endl;
// 		cout<<"\t\tr: "<<r<<endl;
// 		cout<<"\tsineps*cosge+coseps*singe: "<<sineps*cosge+coseps*singe<<endl;
// 		cout<<"\t\tsineps: "<<sineps<<endl;
		cout<<"\t\tcosge: "<<cosge<<endl;
// 		cout<<"\t\tcoseps: "<<coseps<<endl;
		cout<<"\t\tsinge: "<<singe<<endl;
		cout<<"\twmaq*singe: "<<wmaq*singe<<endl;
// 		cout<<"\tcoseps+cotbeta*cot_beta: "<<coseps+cotbeta*cot ( (*knoten)[k][i][j].beta_zu ) <<endl;
// 		cout<<"\t\tcoseps: "<<coseps<<endl;
		cout<<"\t\tcotbeta: "<<cosge<<endl;
		cout<<"\t\ttan_beta: "<<tan ( (*knoten)[k][i][j].beta_zu ) <<endl;

// 		cout<<"Teil_C: "<< -wm* ( ableitungObjekt.ersteAbleitung_r_cot_beta ( i,j,k ) *sineps+ableitungObjekt.ersteAbleitung_z_cot_beta ( i,j,k ) *coseps+ ( wm*cotbeta/r+2*omega ) *sineps ) * ( cosge*cotbeta+vierko+ ( 1-wmaq ) ) <<endl;
// 		cout<<"\t-wm*(d_cotbeta/dr*sineps+d_cotbeta/dz*coseps)+(wm*cotbeta/r+2*omega)*sineps): "<<-wm* ( ableitungObjekt.ersteAbleitung_r_cot_beta ( i,j,k ) *sineps+ableitungObjekt.ersteAbleitung_z_cot_beta ( i,j,k ) *coseps+ ( wm*cotbeta/r+2*omega ) *sineps ) <<endl;
// 		cout<<"\t\twm: "<<wm<<endl;
		cout<<"\t\td_cotbeta/dr: "<<ableitungObjekt.ersteAbleitung_r_cot_beta ( i,j,k ) <<endl;
// 		cout<<"\t\tsineps: "<<sineps<<endl;
		cout<<"\t\td_cotbeta/dz: "<<ableitungObjekt.ersteAbleitung_z_cot_beta ( i,j,k ) <<endl;
// 		cout<<"\t\tcoseps: "<<coseps<<endl;
// 		cout<<"\t\twm*cotbeta/r: "<<wm*cotbeta/r<<endl;
// 		cout<<"\t\t2*omega: "<<2*omega<<endl;
// 		cout<<"\tcosge*cotbeta+vierko+(1-wmaq): "<<cosge*cotbeta+vierko+ ( 1-wmaq ) <<endl;
		cout<<"\t\tcosge: "<<cosge<<endl;
// 		cout<<"\t\tcotbeta: "<<cotbeta<<endl;
		cout<<"\t\tvierko: "<<vierko<<endl;
		cout<<"\t\t(1-wmaq): "<< ( 1-wmaq ) <<endl;

// 		cout<<"Teil_D: "<< +wmq* ( ableitungObjekt.ersteAbleitung_sigma ( i,j,k ) /(*knoten)[k][i][j].sigma-ableitungObjekt.ersteAbleitung_entropie ( i,j,k ) /(*knoten)[k][i][j].cp +sineps/r+ableitungObjekt.ersteAbleitung_r_sin_epsilon ( i,j,k ) +ableitungObjekt.ersteAbleitung_z_cos_epsilon ( i,j,k ) ) * ( cosge*cotbetaq+cotbeta*vierko ) <<endl;
// 		cout<<"\twmq*(1/sigma*d_sigma/dm-1/cp*ds/dm+sineps/r+d_sineps/dr+d_coseps/dz): "<<wmq* ( ableitungObjekt.ersteAbleitung_sigma ( i,j,k ) /(*knoten)[k][i][j].sigma-ableitungObjekt.ersteAbleitung_entropie ( i,j,k ) /(*knoten)[k][i][j].cp +sineps/r+ableitungObjekt.ersteAbleitung_r_sin_epsilon ( i,j,k ) +ableitungObjekt.ersteAbleitung_z_cos_epsilon ( i,j,k ) ) <<endl;
		cout<<"\t\twmq: "<<wmq<<endl;
		cout<<"\t\t1/sigma: "<<1/(*knoten)[k][i][j].sigma <<endl;
		cout<<"\t\td_sigma/dm: "<<ableitungObjekt.ersteAbleitung_sigma ( i,j,k ) <<endl;
		cout<<"\t\t1/cp: "<<1/(*knoten)[k][i][j].cp <<endl;
		cout<<"\t\tds/dm: "<<ableitungObjekt.ersteAbleitung_entropie ( i,j,k ) <<endl;
// 		cout<<"\t\tsineps/r: "<<sineps/r<<endl;
		cout<<"\t\td_sineps/dr: "<<ableitungObjekt.ersteAbleitung_r_sin_epsilon ( i,j,k ) <<endl;
		cout<<"\t\td_coseps/dz: "<<ableitungObjekt.ersteAbleitung_z_cos_epsilon ( i,j,k ) <<endl;
// 		cout<<"\t(cosge*cotbetaq+cotbeta*vierko): "<< ( cosge*cotbetaq+cotbeta*vierko ) <<endl;
		cout<<"\t\tcosge: "<<cosge<<endl;
		cout<<"\t\tcotbetaq: "<<cotbetaq<<endl;
// 		cout<<"\t\tcotbeta: "<<cotbeta<<endl;
		cout<<"\t\tvierko: "<<vierko<<endl;
	}



/*	dgl = 	(*knoten)[k][i][j].dichte / ( 1 - wmaq * cotbetaq ) *
		( -(wmq*dir) * epsilonObjekt.berechne_kruemmungsRadius ( i,j,k ) * singe * ( 1 - wmaq * cotbetaq ) +
		pow2(  wm * cotbeta + omega * r ) /
		r * ((sineps * cosge + coseps * singe * dir) - dir*wmaq * singe *
		(coseps + cotbeta * cot((*knoten)[k][i][j].beta_zu))) -
		dir*wm * (wm*(ableitungObjekt.ersteAbleitung_r_cot_beta(i, j, k) *
		sineps + ableitungObjekt.ersteAbleitung_z_cot_beta(i,j,k) *
		coseps) + (wm * cotbeta / r + 2 * omega) * sineps) * (cosge * cotbeta + dir*vierko * (1 - wmaq)) +
		wmq * (ableitungObjekt.ersteAbleitung_sigma( i,j,k ) / (*knoten)[k][i][j].sigma -
		ableitungObjekt.ersteAbleitung_entropie( i,j,k ) / (*knoten)[k][i][j].cp + sineps / r +
		ableitungObjekt.ersteAbleitung_r_sin_epsilon( i,j,k ) + ableitungObjekt.ersteAbleitung_z_cos_epsilon( i,j,k )) *
		(cosge * cotbetaq + dir*cotbeta * vierko));
*/






/*
dgl = 	(*knoten)[k][i][j].dichte / ( 1 - (*knoten)[k][i][j].machzahl ) *
		( -(wmq*dir) * epsilonObjekt.berechne_kruemmungsRadius ( i,j,k ) * singe * ( 1 - (*knoten)[k][i][j].machzahl ) +
		pow2(  wm * cotbeta + omega * r ) /
		r * ((sineps * cosge + coseps * singe * dir) - dir*wmaq * singe *
		(coseps + cotbeta * cot((*knoten)[k][i][j].beta_zu))) -
		dir*wm * (wm*(ableitungObjekt.ersteAbleitung_r_cot_beta(i, j, k) *
		sineps + ableitungObjekt.ersteAbleitung_z_cot_beta(i,j,k) *
		coseps) + (wm * cotbeta / r + 2 * omega) * sineps) * (cosge * cotbeta + dir*vierko * (1 - wmaq)) +
		wmq * (ableitungObjekt.ersteAbleitung_sigma( i,j,k ) / (*knoten)[k][i][j].sigma -
		ableitungObjekt.ersteAbleitung_entropie( i,j,k ) / (*knoten)[k][i][j].cp + sineps / r +
		ableitungObjekt.ersteAbleitung_r_sin_epsilon( i,j,k ) + ableitungObjekt.ersteAbleitung_z_cos_epsilon( i,j,k )) *
		(cosge * cotbetaq + dir*cotbeta * vierko));

*/
/*	 cout << "Vorfaktor = "<< (*knoten)[k][i][j].dichte / ( 1 - (*knoten)[k][i][j].machzahl ) << endl;
	 cout << "Nenner Vorfaktor = " << ( 1 - (*knoten)[k][i][j].machzahl ) << endl;
	 cout << "Teil A = " << -(wmq*dir) * epsilonObjekt.berechne_kruemmungsRadius ( i,j,k ) * singe * ( 1 - wmaq * cotbetaq ) << endl;
	 cout << "Teil B = " << pow2(  wm * cotbeta + omega * r ) /
		r * ((sineps * cosge + coseps * singe * dir) - dir*wmaq * singe *
		(coseps + cotbeta *cot ((*knoten)[k][i][j].beta_zu))) << endl;

	cout << "Teil C = " << -
		dir*wm * (wm*(ableitungObjekt.ersteAbleitung_r_cot_beta(i, j, k) *
		sineps + ableitungObjekt.ersteAbleitung_z_cot_beta(i,j,k) *
		coseps) + (wm * cotbeta / r + 2 * omega) * sineps) * (cosge * cotbeta + dir*vierko * (1 - wmaq)) << endl;

	cout<<"\t\td_cotbeta/dr: "<<ableitungObjekt.ersteAbleitung_r_cot_beta ( i,j,k ) <<endl;

	cout << "Teil D = " << wmq * (ableitungObjekt.ersteAbleitung_sigma( i,j,k ) / (*knoten)[k][i][j].sigma -
		ableitungObjekt.ersteAbleitung_entropie( i,j,k ) / (*knoten)[k][i][j].cp + sineps / r +
		ableitungObjekt.ersteAbleitung_r_sin_epsilon( i,j,k ) + ableitungObjekt.ersteAbleitung_z_cos_epsilon( i,j,k )) *
		(cosge * cotbetaq + dir*cotbeta * vierko) << endl;

*/



}





void loeser_expeu::massenStromLoeser ( int j, int k )
{
	int Mitte = 0;

	if ( datenObjekt->startit )
	{
		Mitte = datenObjekt->anzahlStromlinien /2; //Startwert in Kanalmitte
	}
	//! Berechnet den Massenstrom mit Hilfe von massenStrom.berechneMassenStrom() .
	massenStromObjekt.berechneMassenStrom ( j,k );

	//! Falls der Massenstrom nicht der Vorgabe entspricht, wird das Newton-Verfahren aufgerufen newton.newtonVerfahren_p() .
// 	cout << "MS: " << (*quasiorthogonale)[k][j].massenStrom << endl;
	double mp_ratio = (*quasiorthogonale)[k][j].massenStrom /datenObjekt->massenStromVorgabe;
	if ((mp_ratio <= ( 1-datenObjekt->toleranz )) || (mp_ratio >= ( 1+datenObjekt->toleranz )) )
	{
		(*knoten)[k][Mitte][j].p = newtonObjekt.newtonVerfahren_p ( j,k ); // Aktualisieren von p
	}
}





//SW: negative Richtung muß noch getestet werden
void loeser_expeu::loeser(int i, int j, int k, int dir) {
	double p_neu=0.0;
	double ddir = (double)dir;

	//! Berechnet Entfernung zum nächsten Knotenpunkt.
	double schritt = sqrt ( pow2(  ddir * (*knoten)[k][i+dir][j].r - ddir * (*knoten)[k][i][j].r  ) +
				pow2(  ddir * (*knoten)[k][i+dir][j].z - ddir * (*knoten)[k][i][j].z ) );

	//! Ruft dgl() auf.
	double ableitung = dgl ( i,j,k, ddir ) /*Einfügen der DGL */;
	(*knoten)[k][i][j].gradient = ableitung;

	if ( datenObjekt->debug >1 )
	{
		cout << "   Schrittweite("<<i<<"->"<<i+dir<<") = " << schritt << endl;
		cout << "   Gradient("<<i<<"->"<<i+dir<<") = " << ableitung << endl;
	}

	//! Berechnung des Druckwertes: \f[	p(i+dir,j,k) = p(i,j,k)+\Delta \nu \cdot \frac{\mathrm{d}p}{\mathrm{d}\nu}	\f]
	p_neu = schritt*ableitung+(*knoten)[k][i][j].p;

/*	cout << "Schrittweite("<<i<<"->"<<i+dir<<") = " << schritt << endl;
	cout << "Gradient("<<i<<"->"<<i+dir<<") = " << ableitung << endl;
	cout << "Setze p[" <<k<<"]["<<i+dir<<"]["<<j<<"] = " << p_neu << endl;
	cout << "____________________________________________" << endl;
*/
	p_neu = max ( p_neu, datenObjekt->p_mindest ); //sollte p_neu < 0 werden

	(*knoten)[k][i+dir][j].p = p_neu;

	if ( datenObjekt->debug >0 )
	{
		cout << "Setze p[" <<i+dir<<"]["<<j<<"]["<<k<<"] = " << (*knoten)[k][i+dir][j].p << "\t" << schritt*ableitung << "\t"<<(*knoten)[k][i][j].p <<endl;
	}

}





void loeser_expeu::werte ( int i,int j,int k )
{
	//! Berechne Temperatur \f$ T \f$ nach temperatur.berechne_T_0() für allererste QO.
	//SW: Für den Fall, dass j == 0 und k == 0 wird T anders berechnet
	if( j == 0 && k == 0 )
		(*knoten)[k][i][j].T = tempObjekt.berechne_T_0 ( i,j,k );
	else	//! Oder berechne Temperatur \f$ T \f$ nach temperatur.berechne_T() für andere QO.
		(*knoten)[k][i][j].T = tempObjekt.berechne_T ( i,j,k );

	//! Berechne Schallgeschwindigkeit \f$ a \f$ nach geschwindigkeit.berechne_schallGeschwindigkeit().
	(*knoten)[k][i][j].a = geschwindigkeitObjekt.berechne_schallGeschwindigkeit ( i,j,k );

	//! Berechne Dichte \f$ \rho \f$ nach dichte.berechne_dichte().
	(*knoten)[k][i][j].dichte = dichteObjekt.berechne_dichte ( i,j,k );

	// Berechne Geschwindigkeitskomponenten (und \f$ \beta,\cot\beta \f$). Unterscheidung zwischen beschaufelt und unbeschaufelt.
	//SW: Für den Fall das j == 0, wird w_m anders berechnet
	/* o.b: Da die Rothalpie jetzt bereits mit berechne_rothalpie_0 in der ersten Quasiorthogonalen initialisiert wurde, kann diese Unterscheidung wegfallen! 2009-11-02
	// Die Rothalpie wird bereits in der äußeren Schleife berechnet.
	if(j == 0)
		(*knoten)[k][i][j].w_m = geschwindigkeitObjekt.berechne_w_m_0 ( i,j,k );
	else*/
// 		(*knoten)[k][i][j].w_m = geschwindigkeitObjekt.berechne_w_m ( i,j,k );

	//! Berechnung von relativer Meridiangeschwindigkeit \f$ w_m \f$ nach geschwindigkeit.setze_w_m().
	geschwindigkeitObjekt.setze_w_m ( i,j,k );

	//Berechne Machzahl
//	(*knoten)[k][i][j].machzahl = sqrt ( pow2(  (*knoten)[k][i][j].w_m / (*knoten)[k][i][j].a ) * ( 1+pow2(  (*knoten)[k][i][j].cot_beta ) ) );

	//o.b.: Kann diese Unterscheidung wegfallen, wenn beta korrekt berechnet wird? 2009-11-02
// 	if ( (*knoten)[k][i][j].unbeschaufelt )
// 	{
// 		(*knoten)[k][i][j].machzahl = ( (*knoten)[k][i][j].w_m / (*knoten)[k][i][j].a );
// 	}
// 	else
// 	{
// 	(*knoten)[k][i][j].machzahl = (*knoten)[k][i][j].w_m / (*knoten)[k][i][j].a * sqrt ( 1+pow2(  (*knoten)[k][i][j].cot_beta ) );

	// TODO Wird die relative Machzahl überhaupt benötigt?
// 	(*knoten)[k][i][j].machzahl = (*knoten)[k][i][j].w / (*knoten)[k][i][j].a;

// 	}

	//! Berechne relativen Totaldruck \f$ p_{tot,rel} \f$ nach druck.berechne_p_tot_rel(). Wird für den kritischen Druck benötigt.
	(*knoten)[k][i][j].p_tot_rel = druckObjekt.berechne_p_tot_rel ( i,j,k );

	//! Berechne kritischen Druck \f$ p_{krit} \f$ nach druck.berechne_p_krit().
	(*knoten)[k][i][j].p_krit = druckObjekt.berechne_p_krit ( i,j,k );

	//SW: Falls j == 0 wird dieser Wert nicht berechnet
	//o.b.: Wüsste nicht warum dies nicht auch für j=0 geschehen sollte, da sich Algorithmus geändert hat
	// TODO Andererseits müsste man auch noch überprüfen in wieweit T_tot_rel überhaupt noch gebraucht wird
// 	if(j != 0) {
		// Berechne relative Totaltemperatur
// 		(*knoten)[k][i][j].T_tot_rel = tempObjekt.berechne_T_tot_rel ( i,j,k );
// 	}

	if ( datenObjekt->debug > 1 )
	{
		cout << "Setze T[" <<i<<"]["<< j <<"]["<<k<<"] = " << (*knoten)[k][i][j].T << endl;
		cout << "Setze a[" <<i<<"]["<< j <<"]["<<k<<"] = " << (*knoten)[k][i][j].a << endl;
		cout << "Setze Dichte[" <<i<<"]["<< j <<"]["<<k<<"] = " << (*knoten)[k][i][j].dichte << endl;
		cout << "Setze w_m[" <<i<<"]["<< j <<"]["<<k<<"] = " << (*knoten)[k][i][j].w_m << endl;
// 		cout << "Setze M[" <<i<<"]["<< j <<"]["<<k<<"] = " << (*knoten)[k][i][j].machzahl << endl;
		cout << "Setze p_tot[" <<i<<"]["<< j <<"]["<<k<<"] = " << (*knoten)[k][i][j].p_tot_rel << endl<<endl;
	}

	if ( datenObjekt->debug >1 && i==0 )
	{
		cout << "p_alt_pos = " << (*quasiorthogonale)[k][j].p_alt_pos << endl;
		cout << "p_alt_neg = " << (*quasiorthogonale)[k][j].p_alt_neg << endl;
	}
}



/*
// TODO überprüfen ob diese Werte noch benötigt werden.
void loeser_expeu::absolut_werte ( int j,int k )
{
	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
	{
		// Absolutgeschwindigkeit 'c'
		(*knoten)[k][i][j].c = geschwindigkeitObjekt.berechne_c ( i,j,k );
//		cout << "wm = " << (*knoten)[k][i][j].w_m << endl;

		// Strömungswinkel im Absolutsystem 'alpha'
		epsilonObjekt.berechne_alpha ( i,j,k );

		if ( k==0 && j==0 )
		{
			// berechnet die relativen Zustände im ersten Gitter auf der ersten Quasiorthogonalen
			tempObjekt.umrechnen ( );
			druckObjekt.umrechnen ( );
		}
		else
		{
			// Totaltemperatur im Absolutsystem 'T_tot_abs'
			(*knoten)[k][i][j].T_tot_abs = tempObjekt.berechne_T_tot_abs ( i,j,k );
			// Totaldruck im Absolutsystem 'p_tot_abs'
			(*knoten)[k][i][j].p_tot_abs = druckObjekt.berechne_p_tot_abs ( i,j,k );
		}

		if ( datenObjekt->omega[k] != 0.0 && datenObjekt->debug >1 )
		{
			//SW: Hier stand früher mal noch zu Debugzwecken ein absolutwert, der war aber immer
			//SW: 0.0 und wurde nirgends gesetzt
			cout<<"Geschwindigkeit:\trelativ = "<<(*knoten)[k][i][j].w_m /sin ( (*knoten)[k][i][j].beta ) <<endl;
			cout<<"Totaldruck:\trelativ = "<<(*knoten)[k][i][j].p_tot_rel <<endl;
// 			cout<<"Totaltemperatur:\trelativ = "<<(*knoten)[k][i][j].T_tot_rel <<endl;
			cout<<"Winkel:\talpha = "<<(*knoten)[k][i][j].alpha <<"\tbeta = "<<(*knoten)[k][i][j].beta <<endl;
			cout<<endl;
		}
	}
}
*/




void loeser_expeu::schleife ( )
{

	//Stoppuhr
	time_t start,end;
	double dif;
	time (&start);
	
	bool rezirkulation = false;

	jdiffvk = 0;
	jdiffhk = 0;

	datenObjekt->m_alt_pos = 0.0;
	datenObjekt->m_alt_neg = 0.0;
	datenObjekt->mfailed = datenObjekt->massenStromVorgabe;

	int zaehler_schleife02 = 0;
	int zaehler_schleife01 = 0;

	double mp_Verhaeltnis;

	int i_rez, j_rez, k_rez;

	//SW: Schleifenabbruchsbedingungen
	bool bNeustart = false;
	bool bZumAnfang = false;

	int Mitte = 0;
	//Nur im Sonderfall den Wert ändern
	if ( datenObjekt->startit )
	{
		Mitte = datenObjekt->anzahlStromlinien /2; //Startwert in Kanalmitte
	}

	// Initialisierung von beta mit 90°
	// nur wegen erstem Aufruf von setze_cotBeta
// 	epsilonObjekt.initialisiereBeta();

	do	/*! Durchlaufe die Schleife, bis sich die Lage der Stromlinien nicht mehr ändert,
		oder die maximale Iteration erreicht ist:*/
	{
		if ( datenObjekt->debug >0 )
		{
			cout << "Zählschleife 2: " << zaehler_schleife02 << endl;
		}

		//! - Aktualisieren von \f$ \varepsilon \f$.
		epsilonObjekt.setzeEpsilon ( );

		//! - Aktualisierung von \f$ \cos\varepsilon, \sin\varepsilon \f$.
		epsilonObjekt.setze_cosEpsilon ( );
		epsilonObjekt.setze_sinEpsilon ( );

		//! - Aktualisieren von \f$ \gamma \f$.
		epsilonObjekt.setzeGamma ();

		//! - Aktualisieren von  \f$ \sigma \f$.
		geometrieObjekt->aktualisiereGeometrie_H_sigma ();

		//! - Aktualisieren von  \f$ \beta_{zu} \f$.
		geometrieObjekt->aktualisiereGeometrie_H_beta_zu ();

		//! - Aktualisieren von  \f$ \beta_{ru} \f$.
		geometrieObjekt->aktualisiereGeometrie_H_beta_ru ();

		//! - Aktualisieren von der Entropie \f$ s \f$.
		geometrieObjekt->aktualisiereGeometrie_H_entropie ();

		//! - Aktualisieren von  \f$ \beta \f$.
		//Nach Aktualisierung von beta_ru und beta_zu !!!
		// Setzt nur Werte im beschaufeltem Raum.
		epsilonObjekt.setzeBeta ();
		//! - Aktualisierung von \f$ \cot\beta \f$.
		epsilonObjekt.setze_cotBeta ( );


		for ( int k=0;k<datenObjekt->anzahlGitter; k++ )
		{
			druckObjekt.zuruecksetzen_p_alt ();

			if ( k == 0 )
			{
				//! - Setzen der Rothalpie \f$ h_{rot} \f$ für alle Stromlinien und Gitter nach rothalpie.
				for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
				{
					(*stromlinieGitter)[k][i].h_rot = rothalpieObjekt.berechne_rothalpie_0 ( i,k );
				}

				if ( (*knoten)[k][Mitte][0].p < datenObjekt->p_mindest )
				{
					(*knoten)[k][Mitte][0].p = (*knoten)[0][Mitte][0].p_tot_abs *0.9;
				}
				
				zaehler_schleife01 = 0;

				do /*! - berechne_Knoten() und werte() für die allererste Quasiorthogonale solange,
					bis massenStromLoeser() die Vorgabe für \f$ \dot{m} \f$ erreicht.*/
				{
					if ( datenObjekt->startit )
					{
						
						for ( int i=Mitte; i>0; i-- ) //Schleife von Mitte bis Nabe
						{
							berechne_Knoten( i,0,k,-1 );
						}
						werte (0, 0, k);

						for ( int i=Mitte; i<datenObjekt->anzahlStromlinien-1; i++ ) // Schleife von Mitte bis Gehäuse
						{
							berechne_Knoten( i,0,k,1 );
						}
						werte ( datenObjekt->anzahlStromlinien-1, 0, k);
					}
					else
					//##################
					{
						for ( int i=0; i<datenObjekt->anzahlStromlinien-1; i++ )
						{
							berechne_Knoten( i,0,k,1 );
						}//i_ende
						werte ( datenObjekt->anzahlStromlinien-1, 0, k);
					}//else_ende //##################
					// Berechnung an der Stelle i=anzahlStromlinien

// 					(*quasiorthogonale)[k][0].machzahl_gemittelt = geschwindigkeitObjekt.machzahl_gemittelt ( 0,k );
					massenStromObjekt.initialisiere_nurbsMassenStromDichte ( 0, k);
					massenStromLoeser ( 0, k);
					mp_Verhaeltnis = (*quasiorthogonale)[k][0].massenStrom /datenObjekt->massenStromVorgabe;

				
					zaehler_schleife01++;
					if (zaehler_schleife01 == datenObjekt->anzahlDurchlaeufe && datenObjekt->debug > -1)
					{
						cout << "'-> WARNUNG: Massenstromvorgabe nicht erreicht!" << endl;
					}
				}
				while ( ( mp_Verhaeltnis< ( 1-datenObjekt->toleranz ) ||
				          mp_Verhaeltnis> ( 1+datenObjekt->toleranz ) ) &&
				        zaehler_schleife01 < datenObjekt->anzahlDurchlaeufe );

				
				
				//cout << "p_tot_rel*0.9 Mitte fuer k==0 " << (*knoten)[k][Mitte][0].p_tot_rel*0.9 << endl;

				//absolut_werte ( 0, k);

				if (datenObjekt->debug >-1 )
				{
					cout << "Massenstrom [!0]["<<k<<"] = " << (*quasiorthogonale)[k][0].massenStrom << endl;
				}

			}
			
			
			else	/*! - Für alle Gitter (bis auf erstes) werden an der ersten Quasiorthogonalen die Werte für \f$ p,T,\rho \f$
				vom vorangehenden Gitter übernommen.
				\f$ w_m \f$ wird nach geschwindigkeit.setze_w_m() berechnet, danach \f$ a \f$ vom vorangehenden Gitter übernommen.
				\f$ p_{tot,rel}\f$ wird nach druck.berechne_p_tot_rel() berechnet und \f$ p_{krit}\f$ nach druck.berechne_p_krit().
				Der Massenstrom \f$ \dot{m} \f$ wird vom vorangehenden Gitter übernommen und danach \f$ h_{rot}\f$ nach rothalpie.berechne_rothalpie().  */
			{
				
				for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
				{
					(*knoten)[k][i][0].p = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].p;
					(*knoten)[k][i][0].T = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].T;
					(*knoten)[k][i][0].dichte = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].dichte;

					//cout << "p bei i fuer k-1 " << (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].p << endl;
					//cout << "p bei i uebergeben " << (*knoten)[k][i][0].p << endl;
					
					// Berechnung von neuem w, w_u, w_m und falls unbeschaufelt auch beta
					//geschwindigkeitObjekt.setze_w_m(k,i,0);
					geschwindigkeitObjekt.setze_w_m(i,0,k);
// 					(*knoten)[k][i][0].w_m = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].w_m;

					(*knoten)[k][i][0].a = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].a;

// 					(*knoten)[k][i][0].alpha = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].alpha;
// 					(*knoten)[k][i][0].cot_alpha = cot ( (*knoten)[k][i][0].alpha );

					// ! ACHTUNG korrektes beta oder w notwendig
// 					(*knoten)[k][i][0].machzahl =  (*knoten)[k][i][0].w_m / (*knoten)[k][i][0].a * sqrt ( 1+pow2(  (*knoten)[k][i][0].cot_beta ) );

// 					(*quasiorthogonale)[k][0].machzahl_gemittelt = geschwindigkeitObjekt.machzahl_gemittelt ( 0,k );

					// ! ACHTUNG korrektes p, w und notwendig
					(*knoten)[k][i][0].p_tot_rel = druckObjekt.berechne_p_tot_rel ( i,0,k );

					

					//Berechne kritischen Druck
					(*knoten)[k][i][0].p_krit = druckObjekt.berechne_p_krit ( i,0,k );

					(*quasiorthogonale)[k][0]. massenStrom = (*quasiorthogonale)[k-1][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].massenStrom;
					massenStromObjekt.initialisiere_nurbsMassenStromDichte ( 0, k);

					// ! ACHTUNG korrektes beta oder w notwendig
					(*stromlinieGitter)[k][i].h_rot = rothalpieObjekt.berechne_rothalpie ( i,k );

					if ( datenObjekt->debug > 1 )
					{
						cout << "Setze T[" <<i<<"]["<< 0 <<"]["<<k<<"] = " << (*knoten)[k][i][0].T << endl;
						cout << "Setze a[" <<i<<"]["<< 0 <<"]["<<k<<"] = " << (*knoten)[k][i][0].a << endl;
						cout << "Setze Dichte[" <<i<<"]["<< 0 <<"]["<<k<<"] = " << (*knoten)[k][i][0].dichte << endl;
						cout << "Setze w_m[" <<i<<"]["<< 0 <<"]["<<k<<"] = " << (*knoten)[k][i][0].w_m << endl;
// 						cout << "Setze M[" <<i<<"]["<< 0 <<"]["<<k<<"] = " << (*knoten)[k][i][0].machzahl << endl;
						cout << "Setze p_tot[" <<i<<"]["<< 0 <<"]["<<k<<"] = " << (*knoten)[k][i][0].p_tot_rel << endl<<endl;
					}
				//cout << "p Mitte fuer k>0 " << (*knoten)[k][Mitte][0].p << endl;
				//cout << "p_tot_rel*0.9 Mitte fuer k>0 " << (*knoten)[k][Mitte][0].p_tot_rel*0.9 << endl;
				}// i Ende
			}// else Ende

// 			// Setzen der Rothalpie
// 			for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
// 			{
// 				(*stromlinieGitter)[k][i].h_rot = rothalpieObjekt.berechne_rothalpie ( i,k );
//
// 				if ( datenObjekt->debug >0 )
// 				{
// 					cout << "Rothalpie ["<<i<<"][" <<k<<"]" << (*stromlinieGitter)[k][i].h_rot << endl;
// 				}
// 			}

			//Beginn Bereich "Anpassen des Stromflächenwinkels nach dem Rotor"
			//finde Vorderkante Diffusor -> j=jdiffvk
			// o.b. was passiert, wenn es nach dem rotor noch unbeschaufelte QO's gibt, die rotieren???
// 			behandle_Stromflaechenwinkel(zaehler_schleife02, k);

			//! - Für alle Quasiorthogonalen (außer erste) des aktuellen Gitters:<ol>
			for ( int j=1; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				//! <li> Für alle Gitter Zurücksetzen der Drücke \f$ p_{alt,+} \f$ und \f$ p_{alt,-} \f$ auf 0.
				druckObjekt.zuruecksetzen_p_alt ();

				//Suche nach Überschalllösung
				// o.b.: klar, nur bei k == 2, macht wohl überhaupt keinen Sinn
// 				if ( k==2 && j<=jdiffvk
// // 					&& geschwindigkeitObjekt.machzahl_gemittelt ( 0,k ) > 1.0
// 					)
// 				{
// 					(*knoten)[k][Mitte][j].p = (*knoten)[k][Mitte][j-1].p;
// 				}
// 				else //Suche nach Unterschalllösung

				/*! <li> Startwerte für Druck setzen auf \f$ p(i,j,k) = p_{tot,rel}(i,j-1,k)\cdot \f$ daten.pFaktor bzw. auf den Mindestdruck,
				falls der Mindestdruck unterschritten wird.*/
				if ( (*knoten)[k][Mitte][j].p < datenObjekt->p_mindest )
				{
					(*knoten)[k][Mitte][j].p = max ( (*knoten)[k][Mitte][j-1].p_tot_rel * datenObjekt->pFaktor, datenObjekt->p_mindest );
				}

				// Startwerte setzen
				// kann so nicht funktionieren, da in der ersten Iteration noch kein p_tot_rel bekannt ist
// 				if ( (*knoten)[k][Mitte][j].p > (*knoten)[k][Mitte][j].p_tot_rel )
// 				{
// 					(*knoten)[k][Mitte][j].p = (*knoten)[k][Mitte][j-1].p;
// 				}
				if ( datenObjekt->debug > -1 && k==0 && j==1 && zaehler_schleife02==0)
					{
						cout << endl << "Hinweis: Erste QO  der Gitter (QO 0) werden nicht aufgefuehrt!" << endl;
					}
				if ( datenObjekt->debug > -1 && j==1)
					{
						cout << endl;
					}
				if ( datenObjekt->debug > -1 )
				{
					cout << "aktueller StartWert p für QO " << j <<": \t" << setiosflags(ios::right) << setw(10) << setiosflags(ios::fixed) << setprecision(2) << (*knoten)[k][Mitte][j].p << endl;
				}

				zaehler_schleife01 = 0;
				do //! <li> berechne_Knoten() und werte() für Quasiorthogonalen solange, bis massenStromLoeser() die Vorgabe erreicht.
				{
					if ( datenObjekt->startit )
					{
						for ( int i=Mitte; i>0; i-- ) //Schleife von Mitte bis Nabe
						{
							berechne_Knoten( i,j,k,-1 );
						}
						werte ( 0,j,k );

						for ( int i=Mitte; i<datenObjekt->anzahlStromlinien-1; i++ ) // Schleife von Mitte bis Gehäuse
						{
							berechne_Knoten( i,j,k,1 );
						}
						werte ( datenObjekt->anzahlStromlinien-1,j,k );
					}
					else
					//##################
					{
						for ( int i=0; i<datenObjekt->anzahlStromlinien-1; i++ )
						{
							berechne_Knoten( i,j,k,1 );
						}//i_ende
						werte ( datenObjekt->anzahlStromlinien-1,j,k );
					}//else_ende //##################

					// Berechnung an der Stelle i=anzahlStromlinien
// 					(*quasiorthogonale)[k][j].machzahl_gemittelt = geschwindigkeitObjekt.machzahl_gemittelt ( j,k );
					massenStromObjekt.initialisiere_nurbsMassenStromDichte ( j, k);
					//Berechne Massenstrom
					massenStromLoeser ( j, k);

					mp_Verhaeltnis = (*quasiorthogonale)[k][j].massenStrom /datenObjekt->massenStromVorgabe;

					// Berechnung der absoluten Werte
// 					absolut_werte ( j,k );

					//Änderung der Inzidenz des Impellers, wenn Anströmung an Diffusor mangelhaft
					// setzt voraus, dass der Impeller im 2. Gitter ist (auf Grund der Indizierung)
/*					if(behandle_MassenStrom(zaehler_schleife01, mp_Verhaeltnis, j,k)) {
						//SW: Erneut über alle Gitter iterieren
						bZumAnfang = true;
						//SW: Zum Neustarten die Zählervariable resetten
						k = 0;
						break;
					}
*/
/*					//Änderung der Gehäusekontur, wenn Rezirkulation vorliegt
					for ( int i=0; i<datenObjekt->anzahlStromlinien-1; i++ )
					{
						if ( ( mp_Verhaeltnis > ( 1-datenObjekt->toleranz ) && mp_Verhaeltnis < ( 1 + datenObjekt->toleranz ) ) && (*knoten)[k][i][j].w_m < datenObjekt->toleranz && zaehler_schleife02 > 4 )
						{
							rezirkulation = true;
							i_rez = i;
							j_rez = j;
							k_rez = k;
						}
						else
						{
							rezirkulation = false;
						}
					}

					//SW: Rezirkulation behandeln.
					if(behandle_Rezirkulation(rezirkulation, i_rez, j_rez, k_rez)) {
						//SW: Berechnung von Anfang an Neustarten
						bNeustart = true;
						break;
					}
*/
				
					zaehler_schleife01++;
					
					if (zaehler_schleife01 == datenObjekt->anzahlDurchlaeufe && datenObjekt->debug > -1)
					{
						cout << "'-> WARNUNG: Massenstromvorgabe nicht erreicht!" << endl;
					}
				}
				while ( ( mp_Verhaeltnis< ( 1-datenObjekt->toleranz ) ||
				          mp_Verhaeltnis> ( 1+datenObjekt->toleranz ) ) &&
				        zaehler_schleife01 < datenObjekt->anzahlDurchlaeufe );

			//SW: Zum Anfang zurück oder Neustart?
			if(bNeustart || bZumAnfang)
				//SW: hier abbrechen
				break;

			}//for(j...) ende
			//! </ol>

			//SW: Falls ZumAnfang gesetzt
			if(bZumAnfang)
				//SW: Entschärfen, da die Schleife jetzt wieder startet
				bZumAnfang = false;

			//SW: Falls zum Start zurück, auch aus dieser Schleife ausbrechen
			if(bNeustart)
				break;
		}

		//SW: Falls Neustart
		if(bNeustart) {
			//SW: Erneuten Neustart deaktivieren
			bNeustart = false;
			//SW: Schleife neu beginnen
			continue;
		}


		//! - Neue Lage der Stromlinien berechnen (neue \f$ \nu \f$-Werte).
		//SW: Es existiert nur eine neueStromlinien-Funktion ohne Parameter, ich nehm den hier mal raus
		stromlinienObjekt.neueStromlinien ();

// 		geometrieObjekt->aktualisiereGeometrie ();// o.b.: Auskommentiert da doppelt

		//Relaxation
		geometrieObjekt->bekomme_ny_neu ();

// 		approximieren(datenObjekt->loeser_approx);

		//! - Aktualisieren der Geometrie.
		geometrieObjekt->aktualisiereGeometrie (); // Aktualisierung der r,z-Werte; nach Relaxation
		geometrieObjekt->aktualisiereGeometrie_nurbsSL ();
// 		geometrieObjekt->aktualisiereGeometrie_approx_nurbsGitterSL (); // o.b.: Auskommentiert da doppelt
// 		geometrieObjekt->aktualisiereGeometrie_approx_nurbsSL (); // o.b.: Auskommentiert da doppelt

		zaehler_schleife02++;

		time (&end);
		if (difftime (end,start) > datenObjekt->maxTime)
		{
			cout << "Achtung -> Abbruch da maximale Zeit überschritten wurde!" << endl;
			break;
		}

		// Überprüfung auf Lageänderung
	}
	while ( ( stromlinienObjekt.ueberpruefeStromlinien () ) &&
	        zaehler_schleife02 < datenObjekt->anzahlDurchlaeufe );

	//Stopuhrausgabe
	time (&end);
	dif = difftime (end,start);
	if ( datenObjekt->debug >=0 && zaehler_schleife02 < datenObjekt->anzahlDurchlaeufe )
	{
		
		cout << "\n\nBerechnung beendet nach " << zaehler_schleife02+1 << " Iterationen bzw " << dif << " Sekunden." << endl;
	}
	else if ( datenObjekt->debug >=0 )
	{
		cout << "\n\nDie maximale Anzahl an Iterationen wurde nach "<<dif<<" Sekunden erreicht!" << endl;
	}

}





void loeser_expeu::integrale_Daten ( )
{
	//! Setzt absoluten Totaldruck \f$ p_{tot,abs} \f$.
	druckObjekt.setze_p_tot_abs();
	//! Setzt massengemittelten absoluten Totaldruck \f$ p_{tot,abs,m} \f$.
	druckObjekt.setze_p_tot_abs_m ();
	//! Setzt absolutes Totaldruckverhältnis \f$ \Pi_{tot,abs} \f$.
	druckObjekt.setze_Pi_tot_abs ();
	//! Setzt massengemittelten absolutes Totaldruckverhältnis \f$ \Pi_{tot,abs,m} \f$.
	druckObjekt.setze_Pi_tot_abs_m ();
	//! Setzt absolute Totaltemperatur \f$ T_{tot,abs} \f$.
	tempObjekt.setze_T_tot_abs ();
	//! Setzt massengemittelte absolute Totaltemeratur \f$ T_{tot,abs,m} \f$.
	tempObjekt.setze_T_tot_abs_m ();
	//! Setzt isentropen Wirkungsgrad \f$ \eta_{is} \f$.
	wirkungsgradObjekt.wirkungsgrad_isentrop ();
	//! Setzt polytropen Wirkungsgrad \f$ \eta_{pol} \f$.
	wirkungsgradObjekt.wirkungsgrad_polytrop ();
	//! Setzt massengemittelten isentropen Wirkungsgrad \f$ \eta_{is,m} \f$.
	wirkungsgradObjekt.wirkungsgrad_isentrop_m ();
	//! Setzt massengemittelten polytropen Wirkungsgrad \f$ \eta_{pol,m} \f$.
	wirkungsgradObjekt.wirkungsgrad_polytrop_m ();
}





void loeser_expeu::reset_grid ( )
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].r = (*knoten)[k][i][j].r_org;
				(*knoten)[k][i][j].z = (*knoten)[k][i][j].z_org;
				(*knoten)[k][i][j].beta_ru = (*knoten)[k][i][j].betaru_org;
				(*knoten)[k][i][j].beta_zu = (*knoten)[k][i][j].betazu_org;
				(*knoten)[k][i][j].sigma = (*knoten)[k][i][j].sigma_org;
				(*knoten)[k][i][j].entropie = (*knoten)[k][i][j].entropie_org;
			}
			(*knoten)[k][0][j].p = 0.0;
		}
	}

	geometrieObjekt->initialisiereGeometrie_nurbsQO ();
	geometrieObjekt->initialisiereGeometrie_nurbsSL ();
	geometrieObjekt->initialisiereGeometrie_H_entropie ();
	geometrieObjekt->initialisiereGeometrie_H_sigma ();
	geometrieObjekt->initialisiereGeometrie_H_beta_ru ();
	geometrieObjekt->initialisiereGeometrie_H_beta_zu ();
	geometrieObjekt->bekomme_ny ();
	geometrieObjekt->bekomme_my ();
	epsilonObjekt.setzeEpsilon ();
	epsilonObjekt.setzeGamma ();
	epsilonObjekt.setze_cosEpsilon ();
	epsilonObjekt.setze_sinEpsilon ();
	druckObjekt.zuruecksetzen_p_alt ();
}





void loeser_expeu::berechne_Knoten(int i, int j, int k, int dir) {

	werte ( i,j,k );
	//SW: Falls j == 0 NurbsMasseStromDichte initialisieren
	if(j == 0)
	{
		massenStromObjekt.initialisiere_nurbsMassenStromDichte ( 0, k);
	}
// 	else
// 		//SW: absolut_werte setzen(?)
// 		absolut_werte(j, k);

	double Mkrit=0.0;
	if ( (*knoten)[k][i][j].unbeschaufelt )		//! Falls Gitter unbeschaufelt:
	{
		//!	\f[	Ma_{krit} = \frac{w_m}{a}	\f]
		Mkrit = (*knoten)[k][i][j].w_m / (*knoten)[k][i][j].a;
	}
	else	//! Falls Gitter beschaufelt:
	{
		//!	\f[	Ma_{krit} = \frac{w_m}{a}\cdot \sqrt{1+(\cot \beta)^2}	\f]
		Mkrit = (*knoten)[k][i][j].w_m / (*knoten)[k][i][j].a  *sqrt( 1+ pow2(  (*knoten)[k][i][j].cot_beta ) );

		//SW: Debugausgabe für j != 0
		if ((datenObjekt->debug >1) && (j != 0))
		{
			cout << "(w_m/a)^2 = " << pow2(  (*knoten)[k][i][j].w_m / (*knoten)[k][i][j].a ) << endl;
			cout << "\tw_m = " << (*knoten)[k][i][j].w_m << endl;
			cout << "\ta = " << (*knoten)[k][i][j].a << endl;
			cout << "1+cotbeta^2 = " << ( 1+pow2(  (*knoten)[k][i][j].cot_beta ) ) << endl;
			cout << "\tcot_beta = " << (*knoten)[k][i][j].cot_beta << endl;
		}
	}

//	cout << "Mkrit = " << Mkrit << endl;
	if ( datenObjekt->debug >0 )
	{
		cout << "Mkrit["<<i<<"][!0]["<<k<<"] = "<<Mkrit<<endl;
	}

	if ( Mkrit > datenObjekt->MkritMin && Mkrit < datenObjekt->MkritMax )	//! Falls  daten.MkritMin \f$ < Ma_{krit} < \f$ daten.MkritMax:
	{
		// Berechnung an der Stelle i-1
		//SW: Die Berechnung für j == 0 verläuft anders als für j != 0
		if(j == 0)	//! - Falls erste Quasiorthogonale:
		{
			//! -	\f$	p = p_{krit}	\f$ nach druck.berechne_p_krit().
			(*knoten)[k][i+dir][j].p = druckObjekt.berechne_p_krit ( i+dir,j,k );
		}
		else	//! - Falls nicht erste Quasiorthogonale:
		{
			//!	\f[	p(i+{dir},j,k) = p_{krit}(i+{dir},j-1,k)	\f] nach druck.berechne_p_krit().
			//!	\f[	p_{tot,rel}(i+{dir},j,k) = p_{tot,rel}(i+{dir},j-1,k)	\f]
			// Berechnung an der Stelle i-1
			(*knoten)[k][i+dir][j].p = druckObjekt.berechne_p_krit ( i+dir,j-1,k );
			(*knoten)[k][i+dir][j].p_tot_rel = (*knoten)[k][i+dir][j-1].p_tot_rel;
		}


		if ( datenObjekt->debug >0 )
		{
			cout << "Setze kritischen Druckwert: p[" <<i+dir<<"]["<<j<<"]["<<k<<"] = " << (*knoten)[k][i+dir][0].p << endl;
			//SW: Debugmessage für j != 0
			if(j != 0)
				cout << "Setze p_tot[" <<i+dir<<"]["<< j-1 <<"]["<<k<<"] = " << (*knoten)[k][i+dir][j].p_tot_rel << endl;
		}

	}
	else	//! Falls \f$ Ma_{krit} < \f$ daten.MkritMin oder \f$ Ma_{krit} > \f$ daten.MkritMax: Aufruf von loeser()
	{
		loeser ( i,j,k,dir );
	}
}



//TODO: Momentan wird diese Methode nicht gebraucht
/*
bool loeser_expeu::behandle_Rezirkulation(bool rezirkulation, int i_rez, int j_rez, int k_rez){
	if (!rezirkulation)
		return false;

	if(datenObjekt->debug>1)
		cout<<"Rezirkulationsgebiet bei ["<<i_rez<<"]["<<j_rez<<"]["<<k_rez<<"]"<<endl;

	double manfac;
	double manfac1 = (*knoten)[k_rez][i_rez][j_rez].ny *0.95;
	double delta_manfac = ( 1.0 - manfac1 ) *0.25;

	if ( datenObjekt->debug >1 )
		cout<<"Rezirkulation! -> Änderung der Geometrie um den Faktor = "<<manfac1<<endl;

	reset_grid ();

	int k_ende=0;
	int jk1=0;
	int jk2=0;
	if ( j_rez+3>=datenObjekt->anzahlQuasiOrthogonale[k_rez] )	//! Falls aktuelle QO eine der letzten drei QO im Gitter ist,
	{
		//! endet das Rezirkulationsgebiet im darauffolgenden Gitter.
		jk1= ( datenObjekt->anzahlQuasiOrthogonale[k_rez]-1 )-j_rez;
		jk2=3-jk1;
		k_ende=min ( k_rez+1,datenObjekt->anzahlGitter-1 );
	}
	else
	{
		//! Ansonsten endet das Rezirkulationsgebiet im aktuellen Gitter.
		k_ende=k_rez;
	}

	for ( int k=k_rez;k<=k_ende;k++ )	//! Schleife durch alle Knotenpunkte die Teil des Rezirkulationsgebietes sind:
	{
		int j_start=0;
		int j_ende=0;

		if ( k==k_rez )
		{
			if ( j_rez-3>0 )
				{j_start = j_rez-3;}
			if ( j_rez+3<datenObjekt->anzahlQuasiOrthogonale[k] )
				{j_ende = j_rez+3;}
			if ( j_rez+3>=datenObjekt->anzahlQuasiOrthogonale[k] )
				{j_ende = datenObjekt->anzahlQuasiOrthogonale[k]-1;}
		}
		if ( k==k_rez+1 )
		{
			if ( jk2>0 )
			{
				j_start = 0;
				j_ende = jk2;
			}
		}

		for ( int j=j_start; j<=j_ende; j++ )	// Schleife durch alle QO im Rezirkulationsgebiet.
		{
			if ( k==k_rez )
			{
				if ( j==j_rez-3 || ( jk2==0 && j==j_rez+3 ) )
					{manfac = manfac1 + 3*delta_manfac;}
				else if ( j==j_rez-2 || ( ( jk2==1||jk2==0 ) && j==j_rez+2 ) )
					{manfac = manfac1 + 2*delta_manfac;}
				else if ( j==j_rez-1 || ( ( jk2==2||jk2==1||jk2==0 ) && j==j_rez+1 ) )
					{manfac = manfac1 + delta_manfac;}
				else
					{manfac = manfac1;}
			}
			else if ( k==k_rez+1 )
			{
				if ( ( jk2==3||jk2==2||jk2==1 ) && j==jk2 )
					{manfac = manfac1 + 3*delta_manfac;}
				else if ( ( jk2==3||jk2==2 ) && j==jk2-1 )
					{manfac = manfac1 + 2*delta_manfac;}
				else if ( jk2==3 && j==jk2-2 )
					{manfac = manfac1 + delta_manfac;}
				else
					{manfac = manfac1;}
			}

			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )	// Schleife durch alle Stromlinien im Rezirkulationsgebiet.
			{
				if ( k==k_rez+1 && j==0 )	//! - Falls im letzten Gitter des Rezirkulationsgebietes und auf erster QO:
				{
					//!	\f[	F(i,j,k) = F(,i,j_{max},k-1)	\f]
					(*knoten)[k][i][j].r = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].r;
					(*knoten)[k][i][j].z = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].z;
					(*knoten)[k][i][j].sigma = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].sigma;
					(*knoten)[k][i][j].beta_zu = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].beta_zu;
					(*knoten)[k][i][j].beta_ru = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].beta_ru;
					(*knoten)[k][i][j].entropie = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].entropie;
				}
				else	//! - Ansonsten wird mit Hilfe der NURBS zwischen den Knotenpunkten interpoliert für \f$ F \f$.

					//! - Mit \f$ F \in \{r,z,\sigma,\beta_{zu},\beta_{ru},S\}\f$
				{
					double newny = (*knoten)[k][i][j].ny *manfac;
					(*knoten)[k][i][j].r = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( newny ).y();
					(*knoten)[k][i][j].z = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( newny ).x();
					(*knoten)[k][i][j].sigma = (*quasiorthogonale)[k][j].nurbsSigma.hpointAt ( newny ).w();
					(*knoten)[k][i][j].beta_zu = (*quasiorthogonale)[k][j].nurbsBeta_zu.hpointAt ( newny ).w();
					(*knoten)[k][i][j].beta_ru = (*quasiorthogonale)[k][j].nurbsBeta_ru.hpointAt ( newny ).w();
					(*knoten)[k][i][j].entropie = (*quasiorthogonale)[k][j].nurbsEntropie.hpointAt ( newny ).w();
				}
			}//i_ende
		}//j_ende
	}//k_ende

	datenObjekt->origingrid();

	//! Anschließend wird die Geometrie aktualiesiert, und die neuen \f$ \nu,\mu \f$ Werte berechnet.
	rezirkulation = false;
	geometrieObjekt->initialisiereGeometrie_nurbsQO ();
	geometrieObjekt->initialisiereGeometrie_nurbsSL ();
	geometrieObjekt->initialisiereGeometrie_H_entropie ();
	geometrieObjekt->initialisiereGeometrie_H_sigma ();
	geometrieObjekt->initialisiereGeometrie_H_beta_ru ();
	geometrieObjekt->initialisiereGeometrie_H_beta_zu ();
	geometrieObjekt->bekomme_ny ();
	geometrieObjekt->bekomme_my ();

	// ACHTUNG: Hier break
	return true;
}
*/


//TODO: Momentan wird diese Methode nicht gebraucht
/*
bool loeser_expeu::behandle_MassenStrom(int zaehler_schleife01, double mp_Verhaeltnis, int j, int k) {

	if (!( zaehler_schleife01 == datenObjekt->anzahlDurchlaeufe-1
	&& ( mp_Verhaeltnis< ( 1-datenObjekt->toleranz ) ||
		mp_Verhaeltnis> ( 1+datenObjekt->toleranz ) )
	&& k>1 && (*quasiorthogonale)[k][j].massenStrom <datenObjekt->massenStromVorgabe
	&& j>=jdiffvk ))
		return false;

	cout << "Massenstromfehler bei ["<<j<<"]["<<k<<"] = "<<(*quasiorthogonale)[k][j].massenStrom <<endl;

	//! \f[	\Delta \beta = (\pi-\beta)\cdot \left( 1-\frac{\dot{m}}{\dot{m}_{Vorgabe}} \right)	\f]
	double delta_beta = 1.0* ( M_PI-(*knoten)[1][0][1].beta ) * ( 1- ( (*quasiorthogonale)[k][j].massenStrom /datenObjekt->massenStromVorgabe ) );

	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )	//! Schleife über alle Stromlinien:
	{
		//! - \f$	\beta = \beta + \Delta\beta	\f$
		(*knoten)[1][i][1].beta = (*knoten)[1][i][1].beta +delta_beta; //Vorderkante Impeller
		//! - Es werden die Temperatur \f$ T \f$, Dichte \f$ \rho \f$ und relative Meridiangeschwindigkeit \f$ w_m \f$ aktualisiert.
		(*knoten)[1][i][1].cot_beta = cot ( (*knoten)[1][i][1].beta );
		(*knoten)[1][i][1].T = tempObjekt.berechne_T_0 ( i,1,1 );
		(*knoten)[1][i][1].dichte = dichteObjekt.berechne_dichte ( i,1,1 );
		(*knoten)[1][i][1].w_m = geschwindigkeitObjekt.berechne_w_m_0 ( i,1,1 );

		if ( i<datenObjekt->anzahlStromlinien-1 )
			loeser ( i,1,1,1 );
	}
	massenStromObjekt.initialisiere_nurbsMassenStromDichte ( 1, 1);
	massenStromObjekt.berechneMassenStrom ( 1, 1);		//! Neuer Massenstrom wird berechnet.
	datenObjekt->massenStromVorgabe = (*quasiorthogonale)[1][1].massenStrom;	//! Neue Massenstromvorgabe wird gesetzt.

	cout << "Neue Massenstromvorgabe = " << datenObjekt->massenStromVorgabe <<endl;

	//! Ursprüngliches \f$ \beta \f$ wird auf allen Stromlinien wiederhergestellt.
	for ( int i=0; i<datenObjekt->anzahlStromlinien-1; i++ )
	{
		(*knoten)[1][i][1].beta = (*knoten)[1][i][1].beta-delta_beta;
		(*knoten)[1][i][1].cot_beta = cot ( (*knoten)[1][i][1].beta );
	}

	reset_grid ();
	// ACHTUNG: Hier break
	return true;

}
*/



/*
void loeser_expeu::approximieren(bool approx) {
	//SW: Falls nicht gewollt, dann lassen wir es eben
	if (!approx)
		return;

	// Mittelung des ny-Wertes bei Gitterwechseln
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
		{
			if ( k<datenObjekt->anzahlGitter-1 )
			{
				(*knoten)[k][i][datenObjekt->anzahlQuasiOrthogonale[k]-1].ny = ( (*knoten)[k][i][datenObjekt->anzahlQuasiOrthogonale[k]-2].ny +(*knoten)[k+1][i][1].ny ) /2;
				(*knoten)[k+1][i][0].ny = (*knoten)[k][i][datenObjekt->anzahlQuasiOrthogonale[k]-1].ny;
			}
		}
	}
}
*/


/*
//"Anpassen des Stromflächenwinkels nach dem Rotor"
void loeser_expeu::behandle_Stromflaechenwinkel(int zaehler_schleife02, int k) {

	if ( zaehler_schleife02 == 0 && k > 0 && datenObjekt->omega[k] == 0.0 )
	{
		int j = 0;
		jdiffvk = 0;
		jdiffhk = 0;
		do
		{
			j++;
			jdiffvk++;
		}
		while ( ( ( (*knoten)[k][0][j-1].unbeschaufelt && (*knoten)[k][0][j].unbeschaufelt ) || ( !(*knoten)[k][0][j-1].unbeschaufelt && (*knoten)[k][0][j].unbeschaufelt ) ) && jdiffvk<datenObjekt->anzahlQuasiOrthogonale[k]-1 );

		//finde Hinterkante Diffusor -> j=jdiffhk-1
		j = jdiffvk;
		jdiffhk = jdiffvk;
		if ( jdiffvk<datenObjekt->anzahlQuasiOrthogonale[k]-1 ) //->es gibt eine Beschaufelung nach Rotor
		{
			do
			{
				j++;
				jdiffhk++;
			}
			while ( !(*knoten)[k][0][j-1].unbeschaufelt && !(*knoten)[k][0][j].unbeschaufelt );
		}
		else //-> keine Beschaufelung nach Rotor
		{
			jdiffhk=1;
			// o.b. toleranz fest einkodiert!!!
			if ( k>0 && datenObjekt->omega[k] == 0.0 && fabs ( (*knoten)[k][0][1].z-(*knoten)[k][0][0].z ) < 0.0001 )
			{
				for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
				{
					// o.b. atan nur zwischen -pi/2 und pi/2 gültig!!!
					(*knoten)[k][i][0].beta_ru = atan ( tan ( (*knoten)[k][i][0].alpha ) * (*knoten)[k][i][0].sin_epsilon );
					for ( int j=jdiffhk; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
					{
						(*knoten)[k][i][j].beta_ru = (*knoten)[k][i][j-1].beta_ru;
						(*knoten)[k][i][j].beta_zu = (*knoten)[k][i][j-1].beta_zu;
					}
				}
			}
			else if ( k>0 && datenObjekt->omega[k] == 0.0 && fabs ( (*knoten)[k][0][1].r-(*knoten)[k][0][0].r ) < 0.0001 )
			{
				for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
				{
					(*knoten)[k][i][0].beta_zu = atan ( tan ( (*knoten)[k][i][0].alpha ) *(*knoten)[k][i][0].cos_epsilon );
					for ( int j=jdiffhk; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
					{
						(*knoten)[k][i][j].beta_ru = (*knoten)[k][i][j-1].beta_ru;
						(*knoten)[k][i][j].beta_zu = (*knoten)[k][i][j-1].beta_zu;
					}
				}
			}
			if ( datenObjekt->debug >0 )
			{
				cout << "Es existiert keine Beschaufelung nach dem Rotor!"<<endl;
			}
		}
	}//if_ende

	//anpassen beta_ru, wenn radial am austritt
	if ( k>0 && datenObjekt->omega[k] ==0.0 && fabs ( (*knoten)[k][0][1].z-(*knoten)[k][0][0].z ) < 0.0001 && jdiffvk < datenObjekt->anzahlQuasiOrthogonale[k]-1 )
	{
		//interpoliere beta_ru zwischen hinterkante impeller und vorderkante diffusor
		// bitte was???? o.b.
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			// o.b. verwendung von lokalen variablen
			// atan nur zwischen -pi/2 und pi/2 gültig!!!
			double alpha_ru0 = atan ( tan ( (*knoten)[k][i][0].alpha ) *(*knoten)[k][i][0].sin_epsilon );
			double delta_alpha1 = ( (*knoten)[k][i][jdiffvk].beta_ru- alpha_ru0 ) / ( jdiffvk );

			for ( int j=0; j<jdiffvk; j++ )
			{
				(*knoten)[k][i][j].beta_ru = alpha_ru0+j*delta_alpha1;
			}//j_ende
		}//i_ende
	}

	//anpassen beta_zu, wenn axial am austritt
	else if ( k>0 && datenObjekt->omega[k] == 0.0
			//&& fabs ( (*knoten)[k][0][1].r-(*knoten)[k][0][0].r ) < 0.0001
			&& jdiffvk<datenObjekt->anzahlQuasiOrthogonale[k]-1 )
	{


		//interpoliere beta_zu zwischen hinterkante rotot und vorderkante stator
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			// atan nur zwischen -pi/2 und pi/2 gültig!!!
			double alpha_zu0 = atan ( tan ( (*knoten)[k][i][0].alpha ) *(*knoten)[k][i][0].cos_epsilon );
			double delta_alpha3 = 0.0;
			delta_alpha3 = ( (*knoten)[k][i][jdiffvk].beta_zu - alpha_zu0 ) / ( jdiffvk );

			for ( int j=0; j<jdiffvk; j++ )
			{
				(*knoten)[k][i][j].beta_zu = alpha_zu0+j*delta_alpha3;
			//	cout << "(*knoten)[" << k << "][" << i << "][" << j << "].beta_zu: " << (*knoten)[k][i][j].beta_zu << endl;
			}//j_ende
		}//i_ende
	}


	//setze beta_ru & beta_zu für austritt wie hinterkante diffusor
	if ( k>0 && datenObjekt->omega[k] == 0.0 && jdiffhk < datenObjekt->anzahlQuasiOrthogonale[k] )
	{
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			for ( int j=jdiffhk; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				(*knoten)[k][i][j].beta_ru = (*knoten)[k][i][j-1].beta_ru;
				(*knoten)[k][i][j].beta_zu = (*knoten)[k][i][j-1].beta_zu;
			}
		}
		geometrieObjekt->initialisiereGeometrie_H_beta_ru ();
		geometrieObjekt->initialisiereGeometrie_H_beta_zu ();
		epsilonObjekt.setzeBeta ();
		epsilonObjekt.setze_cotBeta ();
	}

}
*/
