/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DICHTE_H
	#define DICHTE_H



#include "daten.h"
#include "realFluid.h"


//! Klasse für die Dichteberechnung.
/*!
Diese Klasse ist für die Berechnung der Dichte zuständig.
\author Oliver Borm
*/
class dichte
{
	private:
		v3Knoten	*knoten;
		daten		*datenObjekt;
		
	public:

		dichte();

		//! Berechnung der Dichte
		/*!
		    Berechnet mit Hilfe der idealen Gasgleichung die Dichte in einem Knotenpunkt.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Oliver Borm
		*/
		inline double berechne_dichte ( int i, int j, int k );

		//! Berechnung der Totaldichte
		/*!
		    Berechnet mit Hilfe der idealen Gasgleichung die Totaldichte.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Andrea Bader
		*/
		inline double berechne_TotalDichte ( int i, int j, int k );


		//! Setzen der Totaldichte im Eintritt
		/*!
		    Berechnen und Setzen der massenstromgemittelten Eintrittstotaldichte.
		    Notwendig für die Berechnung der reduzierten Kennzahlen.
		    \author Andrea Bader
		*/
		inline void setze_eintrittstotaldichte ();
};



inline double dichte::berechne_dichte ( int i, int j, int k )
{
	//!	\f[	\rho = \frac{p}{RT}	\f]
    if (!daten::getInstance()->ideal and !realFluid::getFluid()->is_initialising){
        realFluid::getFluid()->update(0,(*knoten)[k][i][j].p,(*knoten)[k][i][j].T);
	return realFluid::getFluid()->myFluid->rhomass();
	/* Gleichung 8 */
    }
    else{
        return (*knoten)[k][i][j].p/(daten::getInstance()->R*(*knoten)[k][i][j].T);
    }
}


inline double dichte::berechne_TotalDichte ( int i, int j, int k )
{
	//!	\f[	\rho_{tot,abs} = \frac{p_{tot,abs}}{RT_{tot,abs}}	\f]
    if (!daten::getInstance()->ideal and !realFluid::getFluid()->is_initialising){
        realFluid::getFluid()->update(0,(*knoten)[k][i][j].p_tot_abs,(*knoten)[k][i][j].T_tot_abs);
	return realFluid::getFluid()->myFluid->rhomass();
	/* Gleichung 8 */
    }
    else{
	return (*knoten)[k][i][j].p_tot_abs/(daten::getInstance()->R*(*knoten)[k][i][j].T_tot_abs);
    }
}


inline void dichte::setze_eintrittstotaldichte ()
{
	double eintrittstotaldichte = 0.0;
	for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
	{
		double dichteUnten = berechne_TotalDichte(i,0,0);
		double dichteOben = berechne_TotalDichte((i+1),0,0);
		eintrittstotaldichte = eintrittstotaldichte + datenObjekt->faktorMassenStromRoehre[i]
		                                                                         * (dichteUnten + dichteOben)/2;
	}
	//! Summiert die Massenstrom gewichtete Eintrittstotaldichte an allen Stromröhren
	datenObjekt->eintrittstotaldichte = eintrittstotaldichte;
}

#endif
