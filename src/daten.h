/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de )             *
 *                 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DATEN_H
	#define DATEN_H



#include <vector>
#include <cmath>		// für M_PI
#include <string>

#include "CoolProp.h"
#include "AbstractState.h"
#include "crossplatform_shared_ptr.h"
#include "Knoten.h"
#include "Stromliniendaten.h"
#include "Quasiorthogonalendaten.h"

// --------------------
// debugging makros, für kürzeren und besser lesbaren Aufruf.
#define DEBUG_PRINT(LEVEL, NAME) if(daten::getInstance()->debug > LEVEL) { cout << NAME << endl; }
#define DEBUG_IMMER(NAME) DEBUG_PRINT(-1, NAME);
#define DEBUG_0(NAME) DEBUG_PRINT(0, NAME);
#define DEBUG_1(NAME) DEBUG_PRINT(1, NAME);
#define DEBUG_2(NAME) DEBUG_PRINT(2, NAME);
#define DEBUG_3(NAME) DEBUG_PRINT(3, NAME);

#include "cassert"
#define DEBUG 1

#if DEBUG
  #define NAN_ASSERT(V) datenObjekt->loeser_resultCode = LRESULT_error; assert(std::isnan(V) != 1)
#else
  #define NAN_ASSERT(V) ;
#endif
// --------------------

//SW: Quadratmakro
#define pow2(p) (p)*(p)
#define pow3(p) (p)*(p)*(p)
#define pow4(p) (p)*(p)*(p)*(p)

#define SMALL 1.0/HUGE
// #define cot(p) 1.0/tan(p)
// #define cot(p) ( tan(p) != 0.0 ) ? (1.0/tan(p)) : (HUGE_VAL)
#define cot(p) ( ( abs(tan(p)) > (SMALL) ) ? (1.0/tan(p)) : (HUGE) )
#define acot(p) atan2(1.0,p)

using namespace std;
using namespace CoolProp;


//SW: Typedef für spätere Verwendung
typedef vector<int> 					vInt;
typedef vector<double>					vDouble;
typedef vector< vector< vector< Knoten> > >		v3Knoten;
typedef vector< vector< Stromliniendaten> > 		v2StromlinieGitter;
typedef vector<Stromliniendaten>			vStromlinie;
typedef vector< vector< Quasiorthogonalendaten> >	v2Quasiorthogonale;

enum loeser_type {LSR_denton, LSR_expeu};

enum dgl_type {DGL_denton, DGL_benetschik};

enum loeser_result_code {
	LRESULT_success     = 0,
	LRESULT_unknown     = 1,
	LRESULT_error       = 2
};


//! Klasse für Datenspeicherung
/*!
Die Datenklasse ist der zentrale Zugriffsort
für alle eingelesenen und berechneten Grössen.
\author Oliver Borm, Florian Mayer, Balint Balassa
*/
class daten
{
	private:
		daten();
		string workdir;

	public:
		//! Workdir-Methoden
		string getworkdir();
		void setworkdir(string _workdir);

		//loeser_type loeserAuswahl;
		//int solver;	//!< Art des Solvers. 0 für expliziten Euler, 1 für Denton.

		double 	Rm;	//!< allgemeine Gaskonstante \f$ R_m=8.314472\frac{J}{mol K}\f$.
		double 	mol;	//!< molare Masse für Luft \f$ M=0.028964\frac{kg}{mol}\f$.
		double 	R;	//!< spezifische Gaskonstante für Luft \f$ R_{Luft}=\frac{R_m}{M}=287.0028305\frac{J}{kg K}\f$.

		//! Schalter ob \f$ c_p \f$ eine Funktion der Temperatur ist.
		bool cpFt;	//!< Falls True, wird \f$ c_p \f$ mit Hilfe der NASA Polynom Koeffizienten berechnet. Siehe Knoten.update_cp().

		double a11;	//!< NASA Polynom Koeffizient für \f$ c_p \f$ Wert von Luft unter \f$ 1000\mathrm{K} \f$.
		double a21;	//!< NASA Polynom Koeffizient für \f$ c_p \f$ Wert von Luft unter \f$ 1000\mathrm{K} \f$.
		double a31;	//!< NASA Polynom Koeffizient für \f$ c_p \f$ Wert von Luft unter \f$ 1000\mathrm{K} \f$.
		double a41;	//!< NASA Polynom Koeffizient für \f$ c_p \f$ Wert von Luft unter \f$ 1000\mathrm{K} \f$.
		double a51;	//!< NASA Polynom Koeffizient für \f$ c_p \f$ Wert von Luft unter \f$ 1000\mathrm{K} \f$.
		double a12;	//!< NASA Polynom Koeffizient für \f$ c_p \f$ Wert von Luft über \f$ 1000\mathrm{K} \f$.
		double a22;	//!< NASA Polynom Koeffizient für \f$ c_p \f$ Wert von Luft über \f$ 1000\mathrm{K} \f$.
		double a32;	//!< NASA Polynom Koeffizient für \f$ c_p \f$ Wert von Luft über \f$ 1000\mathrm{K} \f$.
		double a42;	//!< NASA Polynom Koeffizient für \f$ c_p \f$ Wert von Luft über \f$ 1000\mathrm{K} \f$.
		double a52;	//!< NASA Polynom Koeffizient für \f$ c_p \f$ Wert von Luft über \f$ 1000\mathrm{K} \f$.
                
                // Set fluid to real or ideal
                bool ideal;
                bool err_cout;

		int inputFormat;			//!< Input mode 0->nicht spezifiziert, 1->cgns, 2->text.

		int		anzahlStromlinien;	//!< Anzahl der Stromlinien.
		int		anzahlGitter;		//!< Anzahl der Stufen.
		vInt		anzahlQuasiOrthogonale; //!< Anzahl der Quasiorthogonalen je Gitter.
		int		MAXanzahlQO;		//!< Maximale Anzahl der Quasiorthogonalen.
		vDouble 	omega;			//!< Winkelgeschwindigkeit \f$ \omega \f$ je Gitter.
		int		anzahlUniqueQO;
		Vector_Point3Dd punkte_SL;
		
		//! Debugmodus.
		int 		debug;

		//Ausgabeparameter.
		string		zonename;
		string		eingabeDatei;
		string		ausgabeDatei;
		bool		gitterMod;		//!< True für ein Gitter pro Zone, False für alle Gitter in einer Zone.
		int		anzahlAusgabeGitter;

		//Eingabeparameter
		string		globalVarDatei;
		string		gitterDatei;

		//! 3D-Container für Knoten.
		v3Knoten		knoten;
		//! Container für Stromlinien(daten).
		v2StromlinieGitter	stromlinieGitter;
		vStromlinie		stromlinie;
		//! Container für QuasiOrthogonalen(daten).
		v2Quasiorthogonale	quasiorthogonale;

		

		//GeometrieDaten
		double 		minDistError;		//!< Fehlerschranke; in der minDist2 Methode.
		double 		minDistsizeSearch;	//!< The size of the search in the parametric space; in der minDist2 Methode.
		int		minDistsep;		//!< The number of points initially looked at to find a minimal distance; in der minDist2 Methode.
		int 		minDistmaxiter;		//!< The maximal number of iterations; in der minDist2 Methode.
		double 		minDistuMin;		//!< The minimal parametric value; in der minDist2 Methode.
		double 		minDistuMax;		//!< The maximal parametric value; in der minDist2 Methode.
		double 		relaxfac;		//!< Relaxationsfaktor für neue Stromlinienlage.
		bool		wil;

		//LoeserDaten
		double		pFaktor;		//!< Faktor mit dem \f$ p_{tot,rel} \f$ multipliziert wird, um einen Startwert für den Druck \f$ p \f$ zu erhalten.
		double		MkritMin;		//!< Unterer Grenzwert für die kritische Machzahl.
		double		MkritMax;		//!< Oberer Grenzwert für die kritische Machzahl.


		// Numerische Variablen
		int		gradNurbsKurven;	//!< Grad der Nurbskurven.
		int		anzahlZwischenPunkte;
		double		errorMaxToleranz;	//! in %
		double		toleranz;		//!< Toleranz.
		int		anzahlDurchlaeufe;	//!< Anzahl der maximalen Iterationen des SKV.

		

		// Massenstromdaten
		double		massenStromVorgabe;	//!< Vorgabe des Gesamtmassenstromes.
// 		double		massenStromVorgabe2;
		vDouble 	faktorMassenStromRoehre;//!< Aufteilung des Massenstromes.
		double		mfailed;
		double		m_alt_pos;
		double		m_alt_neg;

		vInt	stoss1;

		int		maxTime;
                
		// Lösermodus
		loeser_type loeserAuswahl;
		
//******************* nur Löser Expeu *********************************************

		double		gammaToleranz;

		int		approxError;	//! Akzeptierter Fehler bei der Interpolation von Nurbskurven
		bool		approx;		//! Wenn 'true', dann Approximation mit 'globalApproxErrBnd'
		bool		approxSL;	//! Wenn 'true', dann Approximation mit 'globalApproxErrBnd'
		bool		approxQO;	//! Wenn 'true', dann Approximation mit 'globalApproxErrBnd'
		bool		loeser_approx;	//! Wenn 'true', dann Approximation mit 'globalApproxErrBnd'
		
		bool 		interpSL;	//!< Interpolation zusätzlicher Stromlinien
		int 		numberSL;	//!< Interpolation zusätzlicher Stromlinien

		int 		ablmod;		//!< Ableitungsmodus
		bool		startit;	//!< Iterationsstart. True für Kanalmitte, False für Nabe.

		//LöserDaten
		double		p_mindest;	//!< Mindestdruck, der nicht unterschritten werden darf.
		double minimalVelocity;		//!< Minimale Geschwindigkeit, die nicht unterschritten werden darf.

		//Skelettflaechen Daten
		double 		maxFehler;	//!< Maximaler Fehler bei Skelettfläche
		int 		MaxIt;		//!< Maximale Anzahl der Itertationen der NURBS-Funktionen.

		//Stromlinien Daten
		double 		stromlinieToleranz;//!< Toleranz bei Stromlinien

		double		eta_isentrop_m;	//!< Isentroper Wirkungsgrad (gemittelt zwischen zwei benachbarten Stromröhren).
		double		eta_polytrop_m;	//!< Polytroper Wirkungsgrad (gemittelt zwischen zwei benachbarten Stromröhren).

//******************* nur Löser Denton *********************************************

		int		anzahlMassDurchlaufe; 		//!< Durchläufe des Massenstromlösers
		bool		berechneMassenstromVorgabe;	//!< Berechne Massenstromvorgabe

		double		damp_wm	;  			//!< Dämpfung der Anpassung der Meridiangeschwinigkeit

		//DGL Optionen
		dgl_type 	dglFormulierung;		//!< Formulierung der DGL (Denton, Benetschik)
		bool		dglAusgabe;			//!< Ausgabe der DGL Werte.

		//Schaufelanzahl Rotor und Stator
		int		Z_rotorVK;			//!< Schaufelanzahl Rotor
		int		Z_statorVK;			//!< Schaufelanzahl Stator

		double 		schluckziffer_wm;		//!< Schluckziffer zum Initialisieren der Meridiangeschwindigkeit

		double 		eta_polytrop_vorgabe;		//!< Vorgabe des Polytropen Wirkungsgrades

		loeser_result_code loeser_resultCode;		//!< Berechnungserfolg

		//--------------------------------------------------------------
		// Start: Kennfeldberechung Steuervariablen
		//--------------------------------------------------------------
		bool 		rechneKennfeld;			//!< Berechne Kennfeld oder nicht

		double 		massenstromSchrittweite;	//!< Schrittweite bei Massenstromiteration
		bool		pumpgrenzeSuchen;		//!< Suchen der Pumpgrenze
		unsigned int 	kennlinieGrenzenSuchZaehler;
		vector<double> 	vUmfangsGeschwindigkeiten;

//**********************************************************************************


// ----  Laufzeitvariablen Löser Denton	----- berechnet, nicht gesetzt --------------------------
		double u_ref;

		int Z_rotorHK;
		int Z_statorHK;
		int rotorVK_j;
		int rotorVK_k;
		int rotorHK_j;
		int rotorHK_k;

		int statorVK_j;
		int statorVK_k;
		int statorHK_j;
		int statorHK_k;

		double deviationHKrotor;
		double deviationHKstator;

		double v_red;
		double Pi_tot;
		double Pi_stat;

		double eta_opt;
		double schluckziffer_opt;
		double schluckziffer_aktuell;
		double eintrittstotaldichte;

		//Fehlerverlauf
		double		error_history[5];
		
//----------------------------------------------------------------------------------------------------

		//! Singleton-Zugriffsfunktion
		static daten* getInstance();


		//! Backup des Ursprungsnetzes.
		/*!
		    Mit Hilfe dieser Funktion wird eine
		    Sicherungskopie des "Ursprungsnetzes"
		    erstellt.
		    Verwendet hauptsächlich für Rezirkulationsroutine.
		    \author Florian Mayer
		 */
		void origingrid();

		
		//! Backup des Ursprungsnetzes + original Sigma zum feststellen des beschaufelten Raumes.
		/*!
		    Mit Hilfe dieser Funktion wird eine
		    Sicherungskopie des "Ursprungsnetzes"
		    erstellt.
		    Verwendet hauptsächlich für Rezirkulationsroutine.
		    \author Oliver Borm
		 */
		void origingrid2();

		
		//! Initialisierung des Containers "Anzahl der Quasiorthogonalen".
		inline void initialisiere_anzahlQuasiOrthogonale() {anzahlQuasiOrthogonale.resize ( anzahlGitter );}


		//! Initialisierung der übrigen benötigten Vektor-Container.
		void initialisiere();

		
//*********** Methoden für Löser Denton ***********************************************

		//! Initialisierung der Laufzeit Variablen
		/*!
		    Mit dieser Methode werden die Laufzeit Variablen des Denton-Lösers, die weder eingelesen, noch
		    direkt eingegeben werden,  entsprechend initialisiert.

		    \warning 	Diese Initalisierung ist wichtig, da teilweise abgefragt wird,
						ob der Wert (Beispiel Deviationswinkel) noch der Initalisierung
						entspricht, oder bereits angepasst wurde.

		    \author Andrea Bader
		 */
		void initialisiereLaufzeitDaten();


		//! Zurücksetzen der Laufzeit Variablen
		/*!
		    Für die Kennfeldberechnung wird bei jedem neuen Betriebspunkt der
			Berechnungserfolg (loeser_resultCode) wieder zurückgesetzt.

		    \author Andrea Bader
		 */
		void zuruecksetzenLaufzeitDaten();


		//! Belegt die bool Quasiorthogonal-Eigenschaften beim Einlesen mit Type
		/*! bool kanal
			bool rotor;
			bool stator;
			bool eintritt;
			bool austritt;
			bool vorderkante;
			bool hinterkante;
			speichert außerdem die Indizes der VK bzw. HK
			Verwendet zur Berechnung der Totalenthalpie und Dralls nach Casey!

			\author Andrea Bader
		*/
		void setzeQOTyp();


		//! Identifizieren der Konvergenzgefahr!
		/*!
		    Die Konvergenzgefahr für die Kennfeldberechnung wird identifiziert und
		    der eventuelle Fehler für den Kennfeldmanager gespeichert.

		    \author Andrea Bader
		 */
		bool konvergenz_Gefahr(double error_max);


		//! Berechnet Umfangsgeschwindigkeit oder Omega, abhängig von eingelsen Werten (u, oder omega)
		/*!
			\warning int rotorHK_k muss belegt sein!

			\author Andrea Bader
		*/
		void setzeUOderOmega();

//*********** Ende: Methoden für Löser Denton *****************************************
};

inline string daten::getworkdir()
{

    return workdir;

}

inline void daten::setworkdir(string _workdir)
{
      workdir = _workdir;

}

#endif
