/***************************************************************************
 *   Copyright (C) 2008-2009 by Florian Mayer (ilumine@gmx.de)             *
 *                 2009 by Oliver Borm (oli.borm@web.de)                   *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm, Florian Mayer
*/



#ifndef KNOTEN_H
	#define KNOTEN_H



#include<cmath>

// für AbleitungsHelfer!
enum EKnotenEigenschaft
{
	KE_r,
	KE_z,
	KE_totalenthalpie,
	KE_entropie,
	KE_w_m,
	KE_rc_u,
	KE_rc_u_2
};

//! Knotenklasse
/*!
Speichert die Informationen zu einem Knotenpunkt.
\author Oliver Borm, Florian Mayer
*/
struct Knoten
{
		//! Aktualisierung des \f$ \kappa \f$ Wertes in Abhängigkeit von \f$ c_p \f$.
		void update_kappa();

		
		//! Aktualisierung des \f$ c_p \f$ Wertes für neue Temperatur.
		/*!
		    \f$ c_p \f$ wird mit Hilfe eines Polynoms 4. Grades für die gegebene Temperatur neu berechnet.
		    Dabei können verschiedene Koeffizienten für Temperaturen unterhalb und oberhalb von \f$ 1000\mathrm{K} \f$ benutzt werden.
		    \param T Temperatur für die \f$ c_p \f$ neu berechnet werden soll.
		*/
		void update_cp(double T);

		
		//! Zuordnen der Knoteneigenschaften für Ableitungshelfer.
		/*!
		Dynamisches Auslesen einer beliebigen Knoteneigenschaft	(durch EKnotenEigenschaft Enum Type).
		\author Andrea Bader
		*/
		double bekommeKnotenWert(EKnotenEigenschaft ke);

		
		double kappa;		//!< \f$ \kappa [-]\f$
		double cp;		//!< \f$ c_p [\frac{\mathrm{J}}{\mathrm{kg K}}]\f$.

		double gradient;	//!< Speicher für DGL-Berechnung.

		// Strömungsmechanische Daten
		double p;		//!< Druck \f$ p \f$.
                double ptemp;           //!< Pressure for real fluid solver \f$ p \f$.
		double T;		//!< Temperatur \f$ T \f$.
                double Ttemp;           //!< Temperature for real fluid solver \f$ p \f$.
		double a;		//!< Schallgeschwindigkeit \f$ a \f$.
		double dichte;		//!< Dichte \f$ \rho \f$.
		double entropie;	//!< Spezifische Entropie \f$ s \f$.
		double entropie_org;
		double totalenthalpie;	//!< Absolute Totalenthalpie \f$ h_{tot, abs} \f$
		double enthalpie;	//!< Enthalpie \f$ h \f$

		//Speicher der berechneten Ableitungen
		double wmdwm_dm;
		double rcu_dm;
		double rcu_2_dm; 	//nur für dglFormulierung = DGL_benetschik;

		// relative Werte
// 		double machzahl;
		double w_m;		//!< Relative Meridiangeschwindigkeit \f$ w_m \f$.
		double w_m_neu;
		double w_u;		//!< Relative Umfangsgeschwindigkeit \f$ w_u \f$.
		double w;		//!< Relative Geschwindigkeit \f$ w \f$.
		double p_tot_rel;	//!< Relativer Totaldruck \f$ p_{tot,rel} \f$.
// 		double T_tot_rel;

		// absolute Werte
		// Notwendig für erste Quasiorthogonale
		double c_u;		//!< Absolute Umfangsgeschwindigkeit \f$ c_u \f$.
		double c;
		double rc_u;		//!< Drall: Produkt aus r*c_u -> direkt definiert, vereinfacht Zugriff.
		// Notwendig für erste Quasiorthogonale
		double p_tot_abs;	//!< Absoluter Totaldruck \f$ p_{tot,abs} \f$.
		// Notwendig für erste Quasiorthogonale
		double T_tot_abs;	//!< Absolute Totaltemperatur \f$ T_{tot,abs} \f$.

		double massenStromRoehre; //!< Massenstrom für jede Röhre an jeder Quasiorthogonalen.

		double p_krit;		//!< Kritischer Druck \f$ p_{krit} \f$.

		//Geometrie Daten
		double r;		//!< Radius. \f$ r \f$-Koordinate.
		double r_org;
		double z;		//!< \f$ z \f$-Koordinate.
		double z_org;
		double my;		//!< Stromlinienparameter \f$ \mu \f$.
		double myG;		//!< Wie "my", nur auf ein Gitter begrenzt.
		double mySchaufel;	//!< Wie "my" nur auf beschaufelten Bereich beschränkt! Benutzt für Schaufeltransparenzfunktionen.
		double ny;		//!< Quasiorthogonaleparameter \f$ \nu \f$.
		double ny_alt;
		double ny_alt_pos;
		double ny_alt_neg;

		double faktorVK;	//!< Schaufeltransparenzgewichtung auf der Vorderkante.
		double faktorHK;	//!< Schaufeltransparenzgewichtung auf der Hinterkante.
		
		double sigma;		//!< Schaufelversperrung \f$ \sigma \f$.
		double sigma_org;
		double sigma_original;

// 		double beta_alt_pos;
// 		double beta_alt_neg;

		bool unbeschaufelt;	//!< Flag ob Gitter unbeschaufelt ist.

		double epsilon;		//!< Winkel \f$ \epsilon \f$ zwischen Stromlinie und \f$ z \f$-Achse.
		double gamma;		//!< Winkel \f$ \gamma \f$ zwischen Quasiorthogonale und \f$ z \f$-Achse.

// 		double alpha;
// 		double alpha_ru;
// 		double alpha_zu;
// 		double cot_alpha;

		double beta;		//!< Winkel \f$ \beta \f$ zwischen relativem Umfangsgeschwindigkeitsvektor und relativem Geschwindigkeitsvektor.
		double beta_ru;		//!< Winkel \f$ \beta_{ru} \f$ zwischen relativem Umfangsgeschwindigkeitsvektor und Stromflächentangente in \f$ ru \f$-Ebene.
		double beta_zu;		//!< Winkel \f$ \beta_{zu} \f$ zwischen relativem Umfangsgeschwindigkeitsvektor und Stromflächentangente in \f$ zu \f$-Ebene.
		double betaru_org;
		double betazu_org;
		double beta_St;
		double alpha_St;

		double cot_beta;	//!< \f$ \cot \beta \f$
		double cos_epsilon;	//!< \f$ \cos \epsilon \f$
		double sin_epsilon;	//!< \f$ \sin \epsilon \f$

		//zusätzliche Winkelfunktionen, um Arcusfunktion zu vermeiden Winkel nicht direkt gespeichert.
		double cos_gamma;
		double sin_psi;
		double cos_psi;
		double sin_gamma;
		double tan_alpha;
		double kruemmung;

		double error;			// Fehlerausgabe für CGNS-Datei

		Knoten();
};

#endif
