%% ==============================================================
%% SKV Manual von Oliver Borm, Andrea Bader, Balint Balassa, Florian Mayer  
%% steht unter einer Creative Commons Namensnennung - Weitergabe 
%% unter gleichen Bedingungen 3.0 Deutschland Lizenz.
%% http://creativecommons.org/licenses/by-sa/3.0/de/
%% ==============================================================

%% ====================================================
%%  **********           Kapitel 3          **********
%% ====================================================

\chapter{Ein- und Ausgabedateien}
\label{kap03}

In diesem Kapitel wird die Struktur und die Inhalte der Ein- und Ausgabedateien erläutert.
Desweiteren werden zwei Testrechnungen mit den entsprechenden Ein- und Ausgabedateien vorgestellt.

\section{Eingabe}
Die Art der Eingabedatei kann direkt beim Aufruf des Programms bestimmt werden (siehe \kapref{kap02}). Ansonsten wird der aktuelle Ordner nach Eingabedateien durchsucht. Falls beide, Text und CGNS Formate im Ordner vorhanden sind, wird die CGNS Datei bevorzugt.

\subsection{Text Format}

Die Eingabe kann einerseits über Textdateien erfolgen. Diese Textdateien enthalten alle notwendigen Daten um die Berechnung durchzuführen und müssen entsprechend \textbf{formatiert} sein.

Es gibt zwei unterschiedliche Arten von Eingabedateien im  Textformat. 
In der Datei \verb|Globale_Variablen| sind alle Variablen gespeichert, welche für den gesamten Fall gültig sind.
Alle Variablen, die für jedes Gitter unterschiedlich sind, werden in den jeweiligen \verb|Gitter_Variablen-xy| Dateien gespeichert.
Dabei steht \verb|xy| für eine fortlaufende Nummer von $00$ bis $99$. Aus diesem Grund können auch maximal 100 Gitter gleichzeitig berechnet werden.

Die Einlesemethode erkennt im allgemeinen nicht, 
welche Ausdrücke vor den Zahlenwerten stehen, sondern sie kennt nur die Anzahl der Ausdrücke.
Deshalb ist auf eine saubere Formatierung zu achten.
Jeder Ausdruck kann maximal 25 ASCII Zeichen lang sein und wird durch mindestens ein Leerzeichen oder Tabulator getrennt.

Es ist besonders darauf zu achten, dass die Anzahl der unterschiedlichen Variablen, exakt aufeinander abgestimmt sind.
Dies ist eine häufige Fehlerquelle.

Es ist darauf zu achten, dass jedes Gitter mindestens vier Stromlinien und Quasiorthogonale besitzen muss, da die \nurbs Kurven für die Geometriebeschreibung den Grad 3 haben.
Die gewählte Interpolationsmethode benötigt dadurch 4 Punkte zur Lösung des Gleichungssystems.

% Beispiele für die jeweiligen Eingabedateien sind in \kapref{testfall01} und \kapref{testfall02} zu finden.

\subsection{Globale\_Variablen}
Am Anfang des Dokumentes stehen die drei integralen Variablen:
\begin{itemize}
\item Massenstrom
\item Anzahl der Stromlinien 
\item Anzahl der Gitter
\end{itemize}

Es ist stets auf die richtige Reihenfolge zu achten.
Danach wird die Anzahl der Quasiorthogonalen in jedem Gitter eingelesen:
\begin{itemize}
\item Anzahl der Quasiorthogonalen 
\end{itemize}

Wahlweise kann jetzt die Aufteilung des Gesamtmassenstromes je Stromröhre eingelesen werden.
Falls dies geschehen soll, muss der erste eingelesene Ausdruck mit einem großen \verb|A| beginnen.
Ansonsten ist der Massenstrom je Stromröhre identisch.

\begin{itemize}
\item prozentualer Anteil des Massenstromes je Stromröhre
\end{itemize}

Als Randbedingung wird für die erste Quasiorthogonale im ersten Gitter auf jeder Stromlinie jeweils zuerst  der Totaldruck und danach die Totaltemperatur eingelesen: 

\begin{itemize}
\item Absoluter Totaldruck $p_{tot,abs}$ für jede Stromlinie an der Stelle (i,0,0) 
\item Absolute Totaltemperatur $T_{tot,abs}$ für jede Stromlinie an der Stelle (i,0,0) 
\end{itemize}

Wahlweise kann noch die Geschwindigkeit $c_u$ an der ersten Quasiorthogonalen angegeben werden,
je nach dem welche Angaben im Gitter gemacht werden sollen.

\subsection{Gitter\_Variablen-xy}

Wie in den anderen Eingabedateien, werden auch hier zuerst die integralen Werte eingelesen:
\begin{itemize}
\item Winkelgeschwindigkeit $\omega$ 
\end{itemize}

Danach kann der Druck an der Nabe wahlweise eingelesen werden.
Falls dies geschehen soll, muss der erste eingelesene Ausdruck mit einem kleinen \verb|s| beginnen.
Dies ist zwingend erforderlich, falls man auf der Quasiorthogonalen eine Überschalllösung sucht.
Wenn der Druck nicht eingelesen wird, dürfen aber die entsprechenden Werte auch nicht in der Eingabedatei stehen.
Werden geeignete Werte für den Druck eingelesen, konvergiert die Berechnung im allgemeinen schneller. 
%Das vorliegende Programm ist zwar in der Lage eine Überschalllösung zu finden, ob diese physikalisch sinnvoll ist, muss aber vom Benutzer verifiziert werden.
%Desweiteren ist dieses Programm \underline{\textbf{nicht}} in der Lage unstetige Übergange von Über- nach Unterschall (Verdichtungsstöße) darzustellen.

\begin{itemize}
\item Druck $p$ für jede Quasiorthogonale an der Stelle (0,j,k), falls gegeben
\end{itemize}


Für jeden Punkt auf der Quasiorthogonalen, werden die sechs nachfolgenden Variablen benötigt, falls keine Geschwindigkeit $c_u$ vorgegeben wurde.
Beginnend mit der ersten Quasiorthogonalen im ersten Gitter, werden die Variablen entlang dieser für alle Stromlinien eingelesen.

\begin{itemize}
\item r - Koordinate
\item z - Koordinate
\item $\beta_{ru}$
\item $\beta_{zu}$ 
\item Versperrung  $\sigma$ 
\item Spezifische Entropie  $s$
\end{itemize}

Falls die Geschwindigkeit $c_u$ vorgegeben wurde, lauten die benötigten Werte:

\begin{itemize}
\item r - Koordinate
\item p - Druck
\item z - Koordinate
\item Versperrung  $\sigma$ 
\item Spezifische Entropie  $s$
\end{itemize}


\subsection{CGNS Format}
In der ursprünglichen Variante des Programms werden die benötigten Daten aus Textdateien eingelesen.
Die Lösung der Berechnung wird wiederum in entsprechenden Textdateien abgespeichert.
Diese Form der Datenverarbeitung wird für größere Rechennetze schnell unübersichtlich und damit fehleranfällig. 
Aus diesem Grund können auch CGNS-Datei eingelesen werden.
CGNS steht für CFD General Notation System.
In der Numerischen Strömungssimulation ist das CGNS ein Standard in der Datenverwaltung.
Ein weiterer Vorteil des CGNS ist die Visualisierung der Ergebnisse mit Hilfe entsprechender Programme, wie zum Beispiel ADFViewer, oder ParaView.

Eine CGNS-Datei weist eine Reihe von Knoten auf, die in einer Baumstruktur angeordnet sind.
Diese Struktur ist dem Verzeichnisbaum von UNIX-Systemen sehr ähnlich.
Der oberste Knoten wird als Ursprungsknoten bezeichnet.
Jeder Knoten unterhalb des Ursprungsknoten ist definiert durch Namen und Label und kann Daten enthalten.
Der Name darf beliebig gewählt werden, das Label nicht.

Jede CGNS Eingabedatei heißt standardmäßig \textbf{scm0.cgns} (die entsprechende Ausgabedatei \textbf{scm1.cgns}), und das Programm sucht nach genau diesem Namen.
Die Standard CGNS Eingabedatei hat einen Ursprungsknoten und einen Basisknoten namens TurboMachine.
Desweiteren hat sie Unterknoten für jedes Gitter der Geometrie, und ggf. Unterknoten für vorgegebene Randbedingungen wie z.B. Massenstrom usw.
Die Unterknoten der Gitter haben selbst Unterknoten, die Drehgeschwindigkeit, Koordinaten und andere Informationen speichern.
Es sei angemerkt, dass solche CGNS Eingabedateien vom \textit{BladeDesigner} automatisch in korrekter Weise erstellt werden.





\section{Ausgabe}

\subsection{Ausgabedateien}
\label{kap03:2:1}

Alle berechneten und einige abgeleitete Strömungsgrößen werden als CGNS Datei ausgegeben.
Desweiteren werden einige integrale Größen, wie der Wirkungsgrad und das Druckverhältnis, in die Datei \verb|Wirkungsgrad| geschrieben. 
%Die Stromlinien und Quasiorthogonalen werden im \PSi und VRML Format in \anref{ausgabe:2} ausgegeben. 

%\underline{Hinweis:} Für die Ausgabe wird in \anref{ausgabe:2} ein \verb|NurbsCurveArrayd| der NURBS++ Programmbibliothek \cite{nurbs:2002} initialisiert. 
%Dabei kommt es beim Löschen dieses Objektes im Destruktor zu einem Speicherzugriffsfehler. 
%Es konnte nicht genau herausgefunden werden woran das liegt,
%aber als Zwischenlösung wird im Konstruktor und in der Methode \verb|.init()| in der Datei \verb|nurbsArray.cpp| nicht die Adresse des übergebenen Zeigers, sondern der Wert des übergebenen Zeigers zugewiesen. 

Folgende Größen werden aktuell ausgegeben:
\begin{itemize}
\item Koordinaten der Rechenpunkte (x,y,z)
\item Netz der Stromlinien und Quasiorthogonalen
\item Druck $p$
%\item Totaldruck $p_{tot}$ 
\item Temperatur $T$
%\item Totaltemperatur $T_{tot}$ 
\item Dichte $\varrho$ 
\item Absolutgeschwindigkeitskomponente $c_z = w_m \cdot \cos \varepsilon$
\item Absolutgeschwindigkeitskomponente $c_r = w_m \cdot \sin\varepsilon$
\item Absolutgeschwindigkeitskomponente $c_u = w_u + \omega r$
\item Relativgeschwindigkeit $w$
\item Machzahl $Ma = \frac{c}{a}$ 
\item Meridiangeschwindigkeit $w_m$  
\item Relativgeschwindigkeitskomponente $w_z = w_m \cdot \cos\varepsilon$
\item Relativgeschwindigkeitskomponente $w_r = w_m \cdot \sin\varepsilon$
\item relative Machzahl $Ma_{rel}=\frac{w_m}{a}\sqrt{1+\cot^2\beta}$
\item Massenstrom $\dot{m}$
\item Rothalpie $h_{rot}$
\item Spezifische Entropie  $s$
\item Versperrung  $\sigma$ 
\item Winkel $\beta$
\item Winkel $\beta_{ru}$
\item Winkel $\beta_{zu}$
\item Winkel $\gamma$
\item Winkel $\varepsilon$


\item Wirkungsgrade $\eta_{is}$ und $\eta_{pol}$
\item Druckverhältnisse $\Pi$
\end{itemize}




%% ====================================================
%%  **********       ENDE  Kapitel 3        **********
%% ====================================================

