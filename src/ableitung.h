/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de )             *
 *                 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ABLEITUNG_H
	#define ABLEITUNG_H

#include "daten.h"

//! Ableitungsklasse
/*!
    In der Ableitungsklasse werden alle Ableitungen mit Hilfe der
    nurbs++-Bibliothek berechnet. Dazu werden die Methoden
    '.deriveAt()' und '.deriveAtH()' verwendet. Alternativ kann auch
    ein Differenzenverfahren benutzt werden. Der entsprechende Schalt-
    parameter befindet sich in 'daten.cpp' mit der Bezeichnung
    'ablmod'.

    Für den loeser_denton wurden die Hilfsmethoden mit Übergabeparameter
    zur Berechnung der Differenzenquotienten geschaffen (ableitungshelfer).
    Diese Methoden werden teilweise von Methoden der Ableitungsklasse aufgerufen.
    Neue Methoden befinden sich im unteren Abschnitt. Die Berechnung mit Nurbs
    Ableitungsmethoden wurde auf Grund von Stabilitätsproblemen wieder entfernt bzw.
    nicht weiter implementiert.
    \author Oliver Borm, Florian Mayer, Andrea Bader
*/

class ableitung
{
	private:
		daten 			*datenObjekt;
		v3Knoten		*knoten;
		v2Quasiorthogonale	*quasiorthogonale;
		v2StromlinieGitter	*stromlinieGitter;

	public:

		ableitung();

		//================ ABLEITUNGSMETHODEN VON BORM, MAYER ==========================
		
		//! Ableitung des Winkels  \f$ \varepsilon \f$
		/*!
		    Berechnet die erste Ableitung entlang einer Stromlinie und gibt diese zurück.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \varepsilon' \f$
		    \author Florian Mayer
		*/
		double ersteAbleitung_epsilon ( int i, int j, int k );


		//! Ableitung der spezifischen Entropie
		/*!
		    Berechnet die erste Ableitung entlang einer Stromlinie und gibt diese zurück.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ s' \f$
		    \author Florian Mayer
		*/
		double ersteAbleitung_entropie ( int i, int j, int k );


		//! Ableitung \f$ \frac{\partial \sin \varepsilon}{\partial r} \f$ an der Stelle i,j,k.
		/*!
		    Berechnet die erste Ableitung entlang der r-Achse und gibt diese zurück.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \frac{\partial \sin \varepsilon}{\partial r} \f$
		    \author Florian Mayer
		*/
		double ersteAbleitung_r_sin_epsilon ( int i, int j, int k );


		//! Ableitung \f$ \frac{\partial \cos \varepsilon}{\partial z} \f$ an der Stelle i,j,k.
		/*!
		    Berechnet die erste Ableitung entlang der z-Achse und gibt diese zurück.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \frac{\partial \cos \varepsilon}{\partial z} \f$
		    \author Florian Mayer
		*/
		double ersteAbleitung_z_cos_epsilon ( int i, int j, int k );


		//! Ableitung \f$ \frac{\partial \cot \beta}{\partial z} \f$ an der Stelle i,j,k.
		/*!
		    Berechnet die erste Ableitung entlang der z-Achse und gibt diese zurück.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \frac{\partial \cot \beta}{\partial z} \f$
		    \author Florian Mayer
		*/
		double ersteAbleitung_z_cot_beta ( int i, int j, int k );


		//! Ableitung \f$ \frac{\partial \cot \beta}{\partial r} \f$ an der Stelle i,j,k.
		/*!
		    Berechnet die erste Ableitung entlang der r-Achse und gibt diese zurück.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \frac{\partial \cot \beta}{\partial r} \f$
		    \author Florian Mayer
		*/
		double ersteAbleitung_r_cot_beta ( int i, int j, int k );


		//! Ableitung der Schaufelversperrung
		/*!
		    Berechnet die erste Ableitung entlang einer Stromlinie und gibt diese zurück.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \sigma' \f$
		    \author Florian Mayer
		*/
		double ersteAbleitung_sigma ( int i, int j, int k );



		//#################################
		// Ableitungen in krummlinigen Koordinaten
		//#################################

		//! Berechnet die krummlinige Ableitung \f$ \frac{\partial r}{\partial \xi}\f$ an der Stelle i,j,k.
		/*!
		   \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \frac{\partial r}{\partial \xi}\f$
		*/
		double dr_dxi ( int i, int j, int k );


		//! Berechnet die krummlinige Ableitung \f$ \frac{\partial r}{\partial \eta}\f$ an der Stelle i,j,k.
		/*!
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \frac{\partial r}{\partial \eta}\f$
		*/
		double dr_deta ( int i, int j, int k );


		//! Berechnet die krummlinige Ableitung \f$ \frac{\partial z}{\partial \xi}\f$ an der Stelle i,j,k.
		/*!
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \frac{\partial z}{\partial \xi}\f$
		*/
		double dz_dxi ( int i, int j, int k );


		//! Berechnet die krummlinige Ableitung \f$ \frac{\partial z}{\partial \eta}\f$ an der Stelle i,j,k.
		/*!
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \frac{\partial z}{\partial \eta}\f$
		*/
		double dz_deta ( int i, int j, int k );

		
//================ ABLEITUNGSMETHODEN VON BADER ==========================

		//#################################
		// Ableitungen für Denton Löser
		//#################################


		//! Setzen aller Geometrieableitungen für Denton Löser (Winkel und Krümmung)
		/*!
		    Folgenden Knoteneigenschaften werden durch Aufruf der entsprechenden Ableitungsmethoden direkt gesetzt:
			\f$ \sin \varepsilon,\ \cos \varepsilon,\ \sin \gamma,\ \cos \gamma,\ \sin \psi,\ \cos \psi, \f$ Krümmung,
			\f$ \tan \alpha,\ \cot \beta,\ \beta \f$
			
		    \author Andrea Bader
		*/
		void setze_Geometrieableitungen ( );


		//! Ableitungen in Meridianrichtung werden für Denton Löser gesetzt!
		/*!
		In dieser Methode werden die Ableitungen \f$ w_m\frac{\partial w_m}{\partial m} \f$,
		\f$ r\frac{\partial c_u}{\partial m} \f$ und für Benetschik \f$ r\frac{\partial c_{u,2}}{\partial m} \f$
		durch Aufruf der entsprechenden Methoden berechnet und in Knoteneigenschaften gespeichert.
		\author Andrea Bader
		*/
		void setze_dm_Ableitung();




		//! Ableitung der Meridiangeschwindigkeit für Denton Löser
		/*!
		Berechnet \f$ w_m\frac{\partial w_m}{\partial m} \f$ erste Ableitung wm entlang einer Stromlinie und gibt diese zurück.
		\param i aktuelle Stromlinie
		\param j aktuelle Quasiorthogonale
		\param k aktuelles Gitter
		\author Andrea Bader
		*/
		double ersteAbleitung_dwm_dm ( int i, int j, int k );
};

#endif
