/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef TEMPERATUR_H
	#define TEMPERATUR_H



#include<cmath>



#include "daten.h"


//! Klasse für die Temperaturberechnung.
/*!
Diese Klasse ist für die Berechnung der verschiedenen Temperaturen zuständig.
\author Oliver Borm, Florian Mayer
*/
//class
class temperatur
{
	private:
		daten 			*datenObjekt;
		v3Knoten		*knoten;
		v2Quasiorthogonale	*quasiorthogonale;
	public:
		temperatur();

		
		//! Berechnung der relativen Totaltemperatur
		/*!
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ T_{tot,rel} \f$
		    \author Oliver Borm
		*/
		inline double berechne_T_tot_rel ( int i, int j, int k );


		//! Berechnung der statischen Temperatur (1)
		/*!
		    Berechnung der statischen Temperatur mit Hilfe der Isentropenbeziehung.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ T \f$
		    \author Oliver Borm
		*/
		inline double berechne_T_0 ( int i, int j, int k );


		//! Berechnung der statischen Temperatur (2)
		/*!
		    Berechnung der statischen Temperatur mit Hilfe
		    des zweiten Hauptsatzes der Thermodynamik für perfekte Gase.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ T \f$
		    \author Oliver Borm
		*/
		double berechne_T ( int i, int j, int k );

		
		//! Setzt absolute Totaltemperatur
		/*! Berechnet und setzt \f$ T_{tot,abs} \f$
		    \author Oliver Borm
		*/
		void setze_T_tot_abs ( );


		//! Berechnung der massengemittelten absoluten Totaltemperatur
		/*!
		    \warning Die absolute Totaltemperatur muss zuerst berechnet werden!
		    \author Oliver Borm
		*/
		void setze_T_tot_abs_m ( );

		
		// ! Bestimmung der relativen Totaltemperatur aus der absoluten Totaltemperatur
		/* !
		    Sollte das allerste Gitter des Rechennetzes bereits rotieren,
		    berechnet diese Funktion die relative Totaltemperatur aus der angegebenen
		    absoluten Totaltemperatur.
		    \warning Gültig nur an der ersten Quasiorthogonalen des ersten Gitters, bei drallfreier Anströmung!!!
		    \author Florian Mayer
		*/
// 		void umrechnen ( );


		//! Berechnung der absoluten Totaltemperatur
		/*!
		    Berechnet den Wert der absoluten Totaltemperatur.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Florian Mayer
		*/
		inline double berechne_T_tot_abs ( int i, int j, int k );
};




//SW: Auslagerung der inline-Funktionen
inline double temperatur::berechne_T_tot_abs ( int i, int j, int k )
{
	//! \f[	T_{tot,abs} = T + \frac{w_m^2+(w_u+\omega r)^2}{2c_p}	\f]
	return (*knoten)[k][i][j].T + 0.5*( pow2( (*knoten)[k][i][j].w_m ) + pow2( (*knoten)[k][i][j].w_u + datenObjekt->omega[k] * (*knoten)[k][i][j].r ) )/((*knoten)[k][i][j].cp) ;
}


inline double temperatur::berechne_T_tot_rel ( int i, int j, int k )
{
	//! \f[	T_{tot,rel} = T + \frac{(w_m\sqrt{1+\cot^2\beta})^2}{2c_p}	\f]
	return (*knoten)[k][i][j].T + 0.5*pow2(  (*knoten)[k][i][j].w_m * sqrt( 1.0 + pow2( cot ( (*knoten)[k][i][j].beta ) ) ) ) / ( (*knoten)[k][i][j].cp );
}



inline double temperatur::berechne_T_0 ( int i, int j, int k )
{
	//! \f[	T = T_{tot,abs} \cdot \left(\frac{p}{p_{tot,abs}}\right)^\frac{\kappa-1}{\kappa}	\f]
	return (*knoten)[k][i][j].T_tot_abs *pow ( ( (*knoten)[k][i][j].p /(*knoten)[k][i][j].p_tot_abs ), ( (*knoten)[k][i][j].kappa-1 ) /(*knoten)[k][i][j].kappa );
}

#endif
