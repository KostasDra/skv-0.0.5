/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef STROMLINIE_H
	#define STROMLINIE_H



#include "newton.h"
#include "massenstrom.h"
#include "daten.h"


//! Stromlinie-Klasse
/*!
Beinhaltet die Methoden zur Berechnung der neuen Lage der Stromlinien.
\author Oliver Borm
*/
class stromlinie
{
	private:
		massenStrom 		massenStromObjekt;
		newton			newtonObjekt;
		daten*			datenObjekt;
		v3Knoten*		knoten;
		v2Quasiorthogonale	*quasiorthogonale;

	public:
		stromlinie();

		//! Lageänderung der Stromlinien
		/*!
		    Durch Änderung der \f$ \nu \f$-Werte wird die Lage der Stromlinien geändert.
		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \param geometrieObjekt Kopie des Objektes vom Typ 'geometrie'
		    \author Oliver Borm
		*/
		void neueStromlinien ();


		//! Überprüfung der Stromlinien
		/*!
		    Mit Hilfe dieser Funktion wird das Maß der Lageänderung überprüft.
		    Ist die Änderung gering genug, ist die Berechnung abgeschlossen.
		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \author Oliver Borm
		*/
		bool ueberpruefeStromlinien ( );

//+++++++++++++++++++++++ Methoden von Andrea Bader ++++++++++++++++++++++++++++++++++++++++++

		//! Berechnung des Relaxationsfaktors nach Wilkinson
		/*!
		    Mindestens notwendiger Relaxationsfaktor nach Wilkinson wird
			in Abhängigikeit des Längenverhältnisses = Kanalhöhe / Abstand QO
			berechnet. Sollte der Dämpfungsfaktor im DatenObjekt kleiner sein,
			wird dieser ersetzt.

		    \author Andrea Bader
		*/
		void relaxFaktorWilkinson();


		//! Aufruf der Methode anpassenStromlinienlage (int k, int j)!
		/*!
			Anpassen der Stromlinienlage für gesamtes Rechenengebiet.

		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \param geometrieObjekt Kopie des Objektes vom Typ 'geometrie'

		    \author Andrea Bader
		*/
		void anpassenStromlinienlage();


		//! Anpassen der Stromlinienlage
		/*!
			Durch Änderung der Ny-Werte wird die Lage der Stromlinien geaendert, entsprechend dem Massenstrom geändert.
			Neue Methode für loeser_denton notwendig, da Massenstromberechnung geändert.

		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter

		    \author Oliver Borm, Andrea Bader
		*/
		void anpassenStromlinienlage (int k, int j);
		
};

#endif
