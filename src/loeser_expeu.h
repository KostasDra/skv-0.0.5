/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm   (oli.borm@web.de)            *
 *                 2008-2009 by Florian Mayer (ilumine@gmx.de)             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef LOESER_EXPEU_H
	#define LOESER_EXPEU_H



#include <cmath>


#include "loeser.h"
#include "daten.h"
#include "massenstrom.h"
#include "newton.h"
#include "druck.h"
#include "geometrie.h"
#include "epsilon.h"
#include "temperatur.h"
#include "geschwindigkeit.h"
#include "dichte.h"
#include "rothalpie.h"
#include "stromlinie.h"
#include "ableitung.h"
#include "wirkungsgrad.h"



//SW: Konstanten
//#define MKRITMIN 0.99
//#define MKRITMAX 1.01


//! Löserklasse
/*!
Löst die Differentialgleichung für jeden Knotenpunkt.
\author Oliver Borm, Florian Mayer, Balint Balassa
*/
class loeser_expeu
	: public loeser
{
	private:
		int jdiffvk;
		int jdiffhk;

		massenStrom 	massenStromObjekt;
		newton 		newtonObjekt;
		druck		druckObjekt;
		temperatur	tempObjekt;
		epsilon		epsilonObjekt;
		geschwindigkeit geschwindigkeitObjekt;
		dichte		dichteObjekt;
		rothalpie	rothalpieObjekt;
		stromlinie	stromlinienObjekt;
		ableitung	ableitungObjekt;
		wirkungsgrad	wirkungsgradObjekt;

		geometrie		*geometrieObjekt;
		daten			*datenObjekt;
		v3Knoten		*knoten;
		v2Quasiorthogonale	*quasiorthogonale;
		v2StromlinieGitter	*stromlinieGitter;

		//! Berechnung eines Knotenpunktes
		/*!
		    Berechnet die Werte für einen Knoten
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \param dir Richtung in die weiter berechnet wird
		    		entwerder 1(nach oben) oder -1(nach unten)
		    \author Oliver Borm, Florian Mayer, Steve Walter
		*/
		void berechne_Knoten(int i, int j, int k, int dir);


		//! Berechnung des statischen Druckes auf folgender Stromlinie
		/*!
		    Berechnet Rezirkulation falls nötig
		    \param rezirkulation gibt an ob Rezirkulation vorliegt/durchgeführt werden soll oder nicht
		    \param i_rez aktuelle Stromlinie im Rezirkulationsgebiet
		    \param j_rez aktuelle Quasiorthogonale im Rezirkulationsgebiet
		    \param k_rez aktuelles Gitter im Rezirkulationsgebiet
		    \author Oliver Borm, Steve Walter
		*/
		bool behandle_Rezirkulation(bool rezirkulation, int i_rez, int j_rez, int k_rez);

		
		//! Berechnung des Massenstrom
		/*!
		    Berechnet den Massenstrom
		    \param zaehler_schleife01 aktueller Durchlauf
		    \param mp_Verhaeltnis Massenstromverhältnis
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Oliver Borm, Steve Walter
		*/
		bool behandle_MassenStrom(int zaehler_schleife01, double mp_Verhaeltnis, int j, int k);

		
		// ! Approximierung
		/* !
		    Approximiert bei Bedarf
		    \param approx soll approximiert werden oder nicht
		    \author Oliver Borm, Steve Walter
		*/
		//void approximieren(bool approx);

		
		// ! Anpassung des Stromflächenwinkels
		/* !
		    Passt den Stromflächenwinkel an den Rotor an
		    \param zaehler_schleife01 aktueller Durchlauf
		    \param k aktuelles Gitter
		    \author Oliver Borm, Steve Walter
		*/
		//void behandle_Stromflaechenwinkel(int zeahler_schleife02, int k);

		
	public:
		loeser_expeu(geometrie *geometrieObjektEingabe);

		//! Lösen der Differentialgleichung Gleichung.
		/*!
		    Löst das Differential \f$ \frac{d p}{d \nu} \f$.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \param dir Richtung: 1.0 = positiv = altes dgl(i, j, k)
			   		-1.0 = negativ = altes negdgl(i, j, k)
		    \return \f$ \frac{d p}{d \nu} \f$
		    \author Steve Walter
		*/
		double dgl ( int i, int j, int k, double dir);

		
		// ! TODO Funktion
		/* !
		    TODO Beschreibung
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Oliver Borm, Florian Mayer
		*/
		//double meridiandgl ( int i, int j, int k );

		
		//! Berechnung des statischen Druckes
		/*!
		    Mit Hilfe des Massenstromes wird der neue Startwert des
		    statischen Druckes an der Nabe berechnet.
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Oliver Borm, Florian Mayer
		*/
		void massenStromLoeser ( int j, int k );

		
		//! Berechnung des statischen Druckes auf folgender Stromlinie
		/*!
		    Berechnet mit Hilfe der Differentialgleichung den neuen
		    Druckwert auf der naechsten Stromlinie.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \param dir Richtung: 1 = positiv = altes loeser(i, j, k)
			   		-1 = negativ = altes negloeser(i, j, k)
		    \author Oliver Borm, Florian Mayer
		*/
		void loeser(int i, int j, int k, int dir);

		
		//! Berechnung stömungsmechanischer Größen (2)
		/*!
		    Berechnet alle erforderlichen Stroemungsgrössen
		    auf einer Quasiothogonalen des Gitters.
		    Ausnahme: die Erste.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Oliver Borm
		*/
		void werte ( int i,int j,int k );


		// ! Berechnung strömungsmechanischer Größen (3)
		/* !
		    Berechnet alle erforderlichen Strömungsgrößen
		    auf einer Quasiothogonalen des Gitters im Absolutsystem.
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Florian Mayer
		*/
		//void absolut_werte ( int j,int k );


		//! Zurücksetzen aller Druckwerte
		/*!
		    Setzt alle Druckwerte auf Ihren Ursprungswert zurück.
		    \author Oliver Borm
		*/
		inline void zuruecksetzen ( );

		
		//! Iterationsschleife
		/*!
		    Die Iterationsschleife wird solange durchlaufen, bis sich
		    die Lage der Stromlinien nicht mehr ändert. Von hier werden
		    alle anderen Methoden aufgerufen, die die Strömungs-
		    parameter berechnen.
		    \author Oliver Borm, Florian Mayer
		*/
		void schleife ( );


		//! Berechnung zusaetzlicher Größen
		/*!
		    Berechnet zusätzliche Zustandsgrößen, wie Totaldruck,
		    Totaldruckverhältnis und Wirkungsgrad.
		    \author Oliver Borm
		*/
		void integrale_Daten ( );

		
		//! Setzt alle Eingabewerte auf Ursprungswerte zurück.
		/*!
		    Setzt für alle Knotenpunkte die aktuellen Eingabewerte auf die ursprünglichen Werte, und initialisiert alles neu.
		    \author Oliver Borm, Florian Mayer
		*/
		void reset_grid ( );
};





//SW: Auslagerung der inline-Funktionen
//o.b.: Methode wird in skv.cpp benutzt
void loeser_expeu::zuruecksetzen ( )
{
	druckObjekt.zuruecksetzen_p_alt ( );
}

#endif
