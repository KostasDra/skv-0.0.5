/***************************************************************************
 *    Copyright (C) 2009-2010 by Andrea Bader (bader.andrea@gmx.de)        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Andrea Bader
*/

#include "geschwindigkeit_denton.h"
#include "realFluid.h"

geschwindigkeit_denton::geschwindigkeit_denton()
{
	datenObjekt 		= daten::getInstance();
	knoten 				= &datenObjekt->knoten;
	stromlinie 			= &datenObjekt->stromlinie;
	stromlinieGitter 	= &datenObjekt->stromlinieGitter;
	quasiorthogonale 	= &datenObjekt->quasiorthogonale;
}



void geschwindigkeit_denton::initialisiere_w_m ()
{
	//! Wenn Rotor vorhanden, wm aus Schluckziffer initialisieren
	if (datenObjekt->rotorHK_j != 0)
	{
		wmausSchluckziffer();
	}
}


void geschwindigkeit_denton::wmausSchluckziffer ()
{
	double w_m_temp = 0.0;
	double schluckziffer = datenObjekt->schluckziffer_wm;
	double u_hinterkante = datenObjekt->omega[datenObjekt->rotorHK_k] * (*knoten)[datenObjekt->rotorHK_k][0][datenObjekt->rotorHK_j].r;

	//! \f[ w_{m,temp} = \textrm{Schluckziffer} \cdot u_{HK} \f]
	//! HK = Hinterkante
	w_m_temp = schluckziffer * u_hinterkante;
	if ( datenObjekt->debug >= 0)
	{
		cout << "      u_ref = " << u_hinterkante << endl;
	}

	//initialisieren des gesamten Rechengitters mit gleichem wm
	for ( int k=0; k < datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0; j < datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i < datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].w_m = w_m_temp;
			}
		}
	}
}


double geschwindigkeit_denton::berechne_w_u ( int i, int j, int k )
{
	//! \f[ w_u = w_m \cdot \cot \beta \f]
	return ((*knoten)[k][i][j].w_m * (*knoten)[k][i][j].cot_beta );
}



double geschwindigkeit_denton::berechne_c_u ( int i, int j, int k )
{
	//! \f[ c_u = w_u + \omega \cdot r \f]
	return ((*knoten)[k][i][j].w_u + datenObjekt->omega[k] * (*knoten)[k][i][j].r );
}



double geschwindigkeit_denton::berechne_c ( int i, int j, int k )
{
	//! \f[ c = \sqrt{w_m^2 + c_u^2} \f]
	return sqrt ( pow2 ( (*knoten)[k][i][j].w_m ) + pow2 ( (*knoten)[k][i][j].c_u ) );
}



double geschwindigkeit_denton::berechne_w ( int i, int j, int k )
{
	//! \f[ w = \sqrt{w_m^2 + w_u^2} \f]
	return sqrt ( pow2 ( (*knoten)[k][i][j].w_m ) + pow2 ( (*knoten)[k][i][j].w_u ) );
}



void geschwindigkeit_denton::bestimmeDrall(int k, int j, int i)
{
	double omega = 0.0;
	if ((*quasiorthogonale)[k][j].rotor())
	{
		omega = datenObjekt->omega[k]; //omega nur in Rotor definiert, sonst = 0
	}

	//! Für erste QOs:
	if (j == 0) //1.QO (Eintritt, oder Gitterwechsel
	{
		if ( k == 0 )//Eintritt -> Drall eingelesen, oder = 0 initialisiert
		{
			//! - Allererstes Gitter:
			//! \f[ rc_u = c_u \cdot r \f]
			(*knoten)[k][i][j].rc_u = (*knoten)[k][i][j].c_u * (*knoten)[k][i][j].r ;
		}
		else   //Gitterübertrag
		{
			//! - Gitterübertrag:
			//! \f[ rc_u(i,0,k) = rc_u(i,j_{max},k-1) \f]
			(*knoten)[k][i][j].rc_u = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].rc_u ;
		}
	}
	else if ((*quasiorthogonale)[k][j].kanal() || (*quasiorthogonale)[k][j].vorderkante )
	{
		//! Falls Kanal oder Vorderkante:
		//! \f[ rc_u(j) = rc_u(j-1) \f]
		(*knoten)[k][i][j].rc_u = (*knoten)[k][i][j-1].rc_u; //konstanter Drall
	}
	else //beschaufelter Raum (ohne vorderkante)
	{
		//! Beschaufelter Raum ohne VK:
		double deviationHK; //Deviationswinkel der Hinterkante
		double rwu_VK;		//relativer Eintrittsdrall
		if ((*quasiorthogonale)[k][j].rotor())
		{
			//! - Rotor:
			//! \f[ rw_{u,VK} = rc_{u,VK} - \omega \cdot r^2 \f]
			deviationHK = datenObjekt->deviationHKrotor;
			rwu_VK =   	   (*knoten)[datenObjekt->rotorVK_k][i][datenObjekt->rotorVK_j].rc_u - omega
					* pow2((*knoten)[datenObjekt->rotorVK_k][i][datenObjekt->rotorVK_j].r);
		}
		else if ((*quasiorthogonale)[k][j].stator())  // für Stator -> absoluter Drall
		{
			//! - Stator
			//! \f[ rw_{u,VK} = rc_{u,VK} \f]
			deviationHK = datenObjekt->deviationHKstator;
			rwu_VK =   	   (*knoten)[datenObjekt->statorVK_k][i][datenObjekt->statorVK_j].rc_u;
		}

		//! \f[ \beta_{Str\ddot{o}mung} = \beta + \Delta \cdot \textrm{faktorHK} \f]
		double beta_stroemung = (*knoten)[k][i][j].beta + deviationHK * (*knoten)[k][i][j].faktorHK;
		//! \f[ w_{u,temp} = w_m \cdot \cot \beta_{Str} \f]
		double wu_temp = (*knoten)[k][i][j].w_m * cot ( beta_stroemung  );
		//! \f[ rw_{u,temp} = r \cdot w_{u,temp} \f]
		double rwu_temp = (*knoten)[k][i][j].r * wu_temp;
		//! \f[ rw_{u,temp} = rw_{u,temp} \cdot (1-\textrm{faktorVK} + rw_{u,VK} \cdot \textrm{faktorVK}) \f]
		rwu_temp = rwu_temp * (1.0 - (*knoten)[k][i][j].faktorVK) + rwu_VK * (*knoten)[k][i][j].faktorVK;
		//! \f[ rc_{u,temp} = rw_{u,temp} + \omega r^2 \f]
		double rc_u_temp = rwu_temp + omega * pow2((*knoten)[k][i][j].r);
		//! \f[ rc_u = rc_{u,temp} \f]
		(*knoten)[k][i][j].rc_u  = rc_u_temp;
	}
}


//BB:
// void geschwindigkeit_denton::updateGeschwindigkeitsDreieck(int k, int j, int i)
// {
// 	if (k!=0 && j==0)
// 	{
// 		(*knoten)[k][i][j].c_u =(*knoten)[k-1][i][datenObjekt->AnzahlQuasiorthogonale[k]].c_u;
// 		(*knoten)[k][i][j].w_u = (*knoten)[k][i][j].c_u - datenObjekt->omega[k] * (*knoten)[k][i][j].r ;
// 		(*knoten)[k][i][j].w = ;
// 		(*knoten)[k][i][j].c = ;
// 	}
// 	else
// 	{
// 		double c_u_temp = (*knoten)[k][i][j].rc_u / (*knoten)[k][i][j].r ;
// 		(*knoten)[k][i][j].c_u = c_u_temp ;
// 		(*knoten)[k][i][j].w_u = c_u_temp - datenObjekt->omega[k] * (*knoten)[k][i][j].r ;
// 		(*knoten)[k][i][j].w = berechne_w ( i,j,k );
// 		(*knoten)[k][i][j].c = berechne_c ( i,j,k );
// 	}
// 
// 	if ( datenObjekt->debug > 1)
// 	{
// 		cout << "updateGeschwindigkeitsdreieck: " << endl;
// 		cout << "   rc_u = " << (*knoten)[k][i][j].rc_u<< endl;
// 		cout << "    w_m = " << (*knoten)[k][i][j].w_m << endl;
// 		cout << "    c_u = " << (*knoten)[k][i][j].c_u << endl;
// 		cout << "    w_u = " << (*knoten)[k][i][j].w_u << endl;
// 		cout << "    w   = " << (*knoten)[k][i][j].w << endl;
// 		cout << "    c   = " << (*knoten)[k][i][j].c << endl;
// 	}
// }

void geschwindigkeit_denton::updateGeschwindigkeitsDreieck(int k, int j, int i)
{
	double c_u_temp = (*knoten)[k][i][j].rc_u / (*knoten)[k][i][j].r ;
	(*knoten)[k][i][j].c_u = c_u_temp ;
	(*knoten)[k][i][j].w_u = c_u_temp - datenObjekt->omega[k] * (*knoten)[k][i][j].r ;
	(*knoten)[k][i][j].w = berechne_w ( i,j,k );
	(*knoten)[k][i][j].c = berechne_c ( i,j,k );

	if ( datenObjekt->debug > 1)
	{
		cout << "updateGeschwindigkeitsdreieck: " << endl;
		cout << "   rc_u = " << (*knoten)[k][i][j].rc_u<< endl;
		cout << "    w_m = " << (*knoten)[k][i][j].w_m << endl;
		cout << "    c_u = " << (*knoten)[k][i][j].c_u << endl;
		cout << "    w_u = " << (*knoten)[k][i][j].w_u << endl;
		cout << "    w   = " << (*knoten)[k][i][j].w << endl;
		cout << "    c   = " << (*knoten)[k][i][j].c << endl;
	}
}


void geschwindigkeit_denton::setze_deviation ()
{
	// Rotor vorhanden, dann ist Rotorhinterkante sicher nicht auf j=0!
	if (datenObjekt->rotorHK_j != 0)
	{
		datenObjekt->deviationHKrotor =bestimme_schlupf(datenObjekt->rotorHK_j, datenObjekt->rotorHK_k);
	}
	// Stator vorhanden, dann ist Statorhinterkante sicher nicht auf j=0!, andernsfalls schon, weil so belegt
	if (datenObjekt->statorHK_j != 0)
	{
		datenObjekt->deviationHKstator = 0.0;
		//noch keine Korrelation zur Berechnung der Deviation an der StatorHK vorhanden -> = 0.0
	}
}


double geschwindigkeit_denton::bestimme_schlupf (int HK_j, int HK_k)
{
	int mittlereSL = datenObjekt->anzahlStromlinien / 2;

	//bestimme ein gewichtetes mittleres beta über die austrittsschaufelhöhe
	double betasumme =0.0;
	for (int i=0; i<datenObjekt->anzahlStromlinien; i++)
	{
		double faktor = 1.0;
		if (i==0 || i==datenObjekt->anzahlStromlinien-1)  //obere und untere Stromlinie werden nur zur Hälfe gewichtet.
		{ faktor = 0.5; }
		else
		{ faktor = 1.0; }
		//! betasumme = \f$ \sum \textrm{faktor}\cdot \beta_{HK} \f$;
		//! mit faktor=0,5 für oberste und unterste Stromlinie. Sonst faktor=1.
		betasumme = betasumme + faktor * (*knoten)[HK_k][i][HK_j].beta;
	}
	//! betamittel \f$ = \frac{|\sum \beta|}{\textrm{Stromlinienanzahl}} \f$
	double betamittel = fabs(betasumme)/(datenObjekt->anzahlStromlinien-1);
	

	//! \f$ \beta_{Schlupf} = \beta -90^\circ \f$
	double betaschlupf = betamittel - 0.5*M_PI;

	//! Schlupffaktor nach Wiesner: 
	int schaufelanzahl = datenObjekt->Z_rotorHK;
	double schlupffaktor = 0.0;

	//! \f$ \textrm{Schlupffaktor} = \frac{\sqrt{\cos\beta_{Schlupf}}}{\textrm{Schaufelanzahl}^{0,7}}\f$
	schlupffaktor = sqrt(cos(betaschlupf))/ pow( schaufelanzahl , 0.7);

	//! Berechnung nur auf mittlerer SL:
	//! \f[ w_m = w_m(HK_k, mittlereSL, HK_j) \f]
	double w_m = (*knoten)[HK_k][mittlereSL][HK_j].w_m;
	//! \f[ c_{u,Minderung} = -schlupffaktor \cdot \omega r \f]
	double c_u_minderung = - schlupffaktor * datenObjekt->omega[HK_k] * (*knoten)[HK_k][mittlereSL][HK_j].r;
	//! \f[ w_{u,Schaufel} = w_m \cdot \cot \beta \f]
	double w_u_schaufel =  w_m * (*knoten)[HK_k][mittlereSL][HK_j].cot_beta;
	//! \f[ w_{u,Str\ddot{o}mung} = w_{u,Schaufel} + c_{u,Mindeung} \f]
	double w_u_stroemung = w_u_schaufel + c_u_minderung;
	//! \f[ \beta_{Schaufel} = \beta(HK_k, mittlereSL, HK_j) \f]
	double beta_schaufel = (*knoten)[HK_k][mittlereSL][HK_j].beta;
	//! \f[ \beta_{Str\ddot{o}mung} = \textrm{acot} \frac{ w_{u,Str\ddot{o}mung}}{w_m} \f]
	double beta_stroemung = acot (w_u_stroemung/ w_m);

	//! Deviation auf der Hinterkante \f[ \Delta_{HK} = \beta_{Str\ddot{o}mung} - \beta_{Schaufel} \f]
	double deviation_HK = (beta_stroemung - beta_schaufel);

	// ACHTUNG: Kennfeldberechnung nach Wirkungsgradkorrelation erfordert angepasste Minderleistung
	deviation_HK = deviation_HK;
	return deviation_HK;
}


double geschwindigkeit_denton::berechne_schallGeschwindigkeit ( int i, int j, int k )
{
	//! \f[ a = \sqrt{\kappa RT} \f]
    if (!daten::getInstance()->ideal and realFluid::getFluid()->is_initialising){
        std::cout << (*knoten)[k][i][j].p << "  " << (*knoten)[k][i][j].T << std::endl;
        realFluid::getFluid()->update(0,(*knoten)[k][i][j].p,(*knoten)[k][i][j].T);
        double a = realFluid::getFluid()->myFluid->speed_sound();
        return a;
    }else{
        return sqrt ((*knoten)[k][i][j].kappa*datenObjekt->R*(*knoten)[k][i][j].T);}
}



void geschwindigkeit_denton::setzeSchaufelTransparenz()
{
	//! Schleife über alle Stromlinien:
	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
	{
		//! Falls Rotor vorhanden, dann ist Rotorhinterkante sicher nicht auf j=0!
		if (datenObjekt->rotorHK_j != 0)
		{
			if (datenObjekt->debug >1)
			{
				cout << "faktorVK/HK berechnungen auf rotor i= " << i << endl;
			}
			//! setzeSchaufelTransparenzSL(rotorVK_k, i, rotorVK_j, rotorHK_j, Z_rotorVK, Z_rotorHK)
			setzeSchaufelTransparenzSL(datenObjekt->rotorVK_k, i, datenObjekt->rotorVK_j, datenObjekt->rotorHK_j,
									   datenObjekt->Z_rotorVK, datenObjekt->Z_rotorHK);
		}

		// Stator vorhanden, dann ist Statorhinterkante sicher nicht auf j=0!, andernsfalls schon, weil so belegt
		if (datenObjekt->statorHK_j != 0)
		{
			if (datenObjekt->debug >1)
			{
				cout << "faktorVK/HK berechnungen auf stator i= " << i << endl;
			}
			setzeSchaufelTransparenzSL(datenObjekt->statorVK_k, i, datenObjekt->statorVK_j, datenObjekt->statorHK_j,
									   datenObjekt->Z_statorVK, datenObjekt->Z_statorHK);
		}

		if (datenObjekt->rotorHK_j == 0 && datenObjekt->statorHK_j == 0)
		{
			cout << "kein Rotor, kein Stator vorhanden "<<endl;
		}
	}
}

// privat Methode, nur von setzeSchaufelTransparenz() verwendet!
void geschwindigkeit_denton::setzeSchaufelTransparenzSL(int k, int i, int jVK, int jHK, int ZVK, int ZHK)
{
	int beschaufelteQO = jHK - jVK + 1;

	Vector_Point2Dd punkteSchaufel(beschaufelteQO);
	NurbsCurve_2Dd kurveSchaufel;

	//! Initialisieren einer Nurbskurve von VK bis HK!
	for (int n=0; n < beschaufelteQO ; n++)
	{
		int j   = jVK + n;
		punkteSchaufel[n] = Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ) ;
	}
	kurveSchaufel.globalInterp ( punkteSchaufel,datenObjekt->gradNurbsKurven );

	double schaufelLaenge = kurveSchaufel.length(); //gibt Länge der Nurbskurve zurück
	int Schaufelanzahl_VK = ZVK;
	int Schaufelanzhl_HK  = ZHK;

	//! Wirkungsbereich der Transparenz = Teilungsverhältnis Schaufelteilung/Schaufellänge
	double wbVK = 2*M_PI*(*knoten)[k][i][jVK].r/Schaufelanzahl_VK  / schaufelLaenge;
	double wbHK = 2*M_PI*(*knoten)[k][i][jHK].r/Schaufelanzhl_HK / schaufelLaenge;

	//! Der maximale Wirkungsbereich ist auf 0.5 begrenzt, falls Teilungsverhältnis größer sein sollte.
	wbVK = min(0.5, wbVK);
	wbHK = min(0.5, wbHK);

	if(datenObjekt->debug > 0)
	{
		cout << "Teilungsverhältnis VK : "<< wbVK << " und Teilungsverhältnis HK : " << wbHK << endl;
	}

	//! Beschaufelte QO durchlaufen und die faktorenVK/HK belegen
	for (int j = jVK; j <= jHK; j++)
	{
		double mySchaufel = 0.0;  //VK = 0 -> HK = 1
		if (j != jHK && j != jVK)
		{
			/*double temp = */
			kurveSchaufel.minDist2 (
					Point2Dd ((*knoten)[k][i][j].z,(*knoten)[k][i][j].r ),
					mySchaufel,
					datenObjekt->minDistError, datenObjekt->minDistsizeSearch, datenObjekt->minDistsep,
					datenObjekt->minDistmaxiter, datenObjekt->minDistuMin, datenObjekt->minDistuMax
			);
		}
		else if (j == jHK)	//! Ränder zwangsbelegt-> Genauigkeit minDist2 macht dies erforderlich!
		{
			mySchaufel = 1.0;
		}
		else
		{ // VK
			mySchaufel = 0.0;
		}
		(*knoten)[k][i][j].mySchaufel = mySchaufel;

		//! Berechnen der Faktoren und speichern in Knoteneigenschaft
		if (mySchaufel < wbVK)
		{
			double fVK = 1.0 - mySchaufel / wbVK;
			(*knoten)[k][i][j].faktorVK = fVK * fVK;
		}
		if (mySchaufel > wbHK)
		{
			double fHK = (mySchaufel - wbHK) / (1.0 - wbHK);
			(*knoten)[k][i][j].faktorHK = fHK * fHK;
		}

		if(datenObjekt->debug > 1)
		{
			cout << "  mySchaufel-Knoten: " << (*knoten)[k][i][j].mySchaufel << " ---> ";
			cout << "  faktorVK= " << (*knoten)[k][i][j].faktorVK
				 << "  faktorHK= " << (*knoten)[k][i][j].faktorHK << endl;
		}
	}
}


//todo, neue, keine doku!!!
void geschwindigkeit_denton::setze_deHaller()
{
	double deHaller=0.0;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		deHaller = (*knoten)[datenObjekt->rotorHK_k][i][datenObjekt->rotorHK_j].w
				 / (*knoten)[datenObjekt->rotorVK_k][i][datenObjekt->rotorVK_j].w;

		(*stromlinie)[i].deHaller = deHaller;
	}
}






void geschwindigkeit_denton::setzeRelativGeschwindigkeitnachRotor ()
{
	for ( int k=datenObjekt->rotorHK_k; k < datenObjekt->anzahlGitter;k++ ) //ab dem Gitter des Rotors
	{
		for ( int j=datenObjekt->rotorHK_j+1; j < datenObjekt->anzahlQuasiOrthogonale[k];j++ )  //ein QO nach dem Rotor beginnend
		{
			for ( int i=0; i < datenObjekt->anzahlStromlinien; i++ )
			{
				//! Setzt w=c
				(*knoten)[k][i][j].w = (*knoten)[k][i][j].c;
			}
		}
	}
}








//######################  Nurbs initalisieren ##############################

void geschwindigkeit_denton::aktualisiere_nurbsGeschwindigkeitskurven ( )
{
	aktualisiere_nurbsWmSL ( );
	aktualisiere_nurbsDrallSL ( );
	aktualisiere_nurbsQuadratDrallSL ( );
	aktualisiere_nurbsQuadratDrallQO ();

}

void geschwindigkeit_denton::aktualisiere_nurbsWmSL ( )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve;
	int n;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ((k <= 0) || (j!=0))
				{
					punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].w_m ) ;
					n++;
				}
			}
		}
		kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
		(*stromlinie)[i].nurbsWmSL = kurve;
	}

	//Gitter
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkte.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].w_m ) ;
				n++;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*stromlinieGitter)[k][i].nurbsWmGitterSL = kurve;
		}
	}
}

void geschwindigkeit_denton::aktualisiere_nurbsDrallSL ( )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve;
	int n;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ((k <= 0) || (j!=0))
				{
					punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].rc_u ) ;
					n++;
				}
			}
		}
		kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
		(*stromlinie)[i].nurbsDrallSL = kurve;
	}

	//Gitter
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkte.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].rc_u ) ;
				n++;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*stromlinieGitter)[k][i].nurbsDrallGitterSL = kurve;
		}
	}
}


void geschwindigkeit_denton::aktualisiere_nurbsQuadratDrallSL ( )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve;
	int n;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ((k <= 0) || (j!=0))
				{
					punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r, pow2 ((*knoten)[k][i][j].rc_u )) ;
					n++;
				}
			}
		}
		kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
		(*stromlinie)[i].nurbsDrallQuadratSL = kurve;
	}

	//Gitter
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkte.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r, pow2 ((*knoten)[k][i][j].rc_u )) ;
				n++;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*stromlinieGitter)[k][i].nurbsDrallQuadratGitterSL = kurve;
		}
	}
}

void geschwindigkeit_denton::aktualisiere_nurbsQuadratDrallQO ()
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	NurbsCurve_2Dd kurve;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				punkte[i] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,pow2 ((*knoten)[k][i][j].rc_u )) ;
			}

			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*quasiorthogonale)[k][j].nurbsDrallQuadrat = kurve;
		}
	}
}

//######################  Nurbs initalisieren Ende ##############################












