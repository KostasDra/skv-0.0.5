/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm
*/


#include <limits>
#include "stromlinie.h"



stromlinie::stromlinie()
{
	datenObjekt		= daten::getInstance();
	knoten			= &datenObjekt->knoten;
	quasiorthogonale	= &datenObjekt->quasiorthogonale;
}





void stromlinie::neueStromlinien ()
{
	int zaehler_neueStromlinien;
	double ny_neu;
	double mp_ziel;
	double mp_neu;
	double ny_i;
	double ny_im1;
	double mp_Verhaeltnis;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=1; i<datenObjekt->anzahlStromlinien-1; i++ )
			{
				zaehler_neueStromlinien = 0;
				(*knoten)[k][i][j].ny_alt_pos = 0;
				(*knoten)[k][i][j].ny_alt_neg = 1;

				if ( k>0 && j==0 )
				{
					(*knoten)[k][i][j].ny_alt = (*knoten)[k][i][j].ny;
					(*knoten)[k][i][j].ny = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].ny;
				}
				else
				{
					ny_i = (*knoten)[k][i][j].ny;
					ny_im1 = (*knoten)[k][i-1][j].ny;

					mp_ziel = datenObjekt->massenStromVorgabe *datenObjekt->faktorMassenStromRoehre[i-1];
					mp_neu = massenStromObjekt.berechneMassenStromRoehre ( ny_im1, ny_i, j, k );

					do
					{
						//! Solange der gewünschte Massenstrom nicht ereicht ist, wird das newton.newtonVerfahren() angewendet, bis die Lage der Stromlinein stimmt.
						ny_neu = newtonObjekt.newtonVerfahren ( mp_ziel, mp_neu, ny_i, i, j, k );

						ny_neu = max ( ny_neu,ny_im1 );								//fm
						mp_neu = massenStromObjekt.berechneMassenStromRoehre ( ny_im1,ny_neu,j,k );

						ny_i = ny_neu;

						zaehler_neueStromlinien++;
						mp_Verhaeltnis = mp_neu/mp_ziel;
					}//while-Ende
					while ( ( mp_Verhaeltnis< ( 1-datenObjekt->toleranz ) ||  mp_Verhaeltnis> ( 1+datenObjekt->toleranz ) ) && zaehler_neueStromlinien < datenObjekt->anzahlDurchlaeufe );


					(*knoten)[k][i][j].ny_alt = (*knoten)[k][i][j].ny;
					(*knoten)[k][i][j].ny = ny_neu;

					(*knoten)[k][i][j].massenStromRoehre = mp_neu;
				}//else_ende

			}//i_ende

			ny_im1 = (*knoten)[k][datenObjekt->anzahlStromlinien-2][j].ny;

			//Massenstrom für i-1, oberste Stromröhre
			mp_neu = massenStromObjekt.
			         berechneMassenStromRoehre ( ny_im1,(*knoten)[k][datenObjekt->anzahlStromlinien-1][j].ny,j,k );
			(*knoten)[k][datenObjekt->anzahlStromlinien-1][j].massenStromRoehre = mp_neu; //fm '-2' ersetzt durch '-1'

		}
	}
}


bool stromlinie::ueberpruefeStromlinien ( )
{
	int i=1;
	int j=0;
	int k=0;
	double wert = 0.0;
	double toleranz = datenObjekt->stromlinieToleranz;

	bool abfrage = false;
	do
	{
		j = 0;
		do
		{
			i = 1;
			do
			{
				wert = (*knoten)[k][i][j].ny /(*knoten)[k][i][j].ny_alt;
				if ( wert > ( 1+toleranz ) || wert < ( 1-toleranz ) )
				{
					abfrage = true;
					// TODO wieder entfernen
					if (datenObjekt->debug >-1 )
					{
						cout << "\nny = " << (*knoten)[k][i][j].ny << endl;
						cout << "ny_alt = " << (*knoten)[k][i][j].ny_alt << endl;
						cout << "\t\t========================" << endl;
						cout << "\t\t=>ny/ny_alt = " << wert << "<="<< endl;
						cout << "\t\t========================" << endl;
						cout << "Knoten: [" << k << "][" << i << "][" << j << "]" <<endl;
					}
					return abfrage;
				}
				i++;
			}
			while ( i< ( datenObjekt->anzahlStromlinien-1 ) );
			j++;
		}
		while ( j< ( datenObjekt->anzahlQuasiOrthogonale[k] ) );
		k++;
	}
	while ( k< ( datenObjekt->anzahlGitter ) );

	return abfrage;
}


// ****************** Methoden für loeser_denton ***************************************


void stromlinie::relaxFaktorWilkinson()
{
	double deltaM = 0.0;
	double deltaMmin = std::numeric_limits<double>::max();  //nicht Null!!
	double teilungsverhaeltnis_max = 0.0;
	// der abstand der QO am Gitterwechsel wird hier nicht berücksichtigt.
	//Achtung, dass die DGL nicht zweimal gelöst wird, falls in mehreren Gittern gerechnet wird.

	int j_kleinstes = 0;
	int i_kleinstes = 0;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k]-1;j++ )
		{//Kanalhöhe bestimmen:
			double kanalhoehe = sqrt (pow2((*knoten)[k][0][j].r - (*knoten)[k][datenObjekt->anzahlStromlinien-1][j].r)
					+ pow2((*knoten)[k][0][j].z - (*knoten)[k][datenObjekt->anzahlStromlinien-1][j].z));

			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{	//bestimme kleinstes delta m
				deltaM = sqrt(pow2((*knoten)[k][i][j].r - (*knoten)[k][i][j+1].r) + pow2((*knoten)[k][i][j].z - (*knoten)[k][i][j+1].z));
				deltaMmin = min ( deltaM , deltaMmin );
				if (deltaM == deltaMmin)
				{
					i_kleinstes = i;
				}
			}

			double teilungsverhaeltnis = 0.0;
			//! Teilungsverhältnis  tv = Kanalhöhe / Abstand QO
			teilungsverhaeltnis =	kanalhoehe / deltaMmin ;
			//maximales Seitenverhältnis aus kleinstem deltaMmin
			teilungsverhaeltnis_max = max ( teilungsverhaeltnis , teilungsverhaeltnis_max );
			if (teilungsverhaeltnis == teilungsverhaeltnis_max)
			{
				j_kleinstes = j;
			}
		}
	}
	double relaxfac_wil = 0.0;
	//Dämpfungfaktor  nach Wilkinsion
	//! relaxfac_wil = \f$ \frac{1}{2 (1+20/96 * tvmax)} \f$
	relaxfac_wil = 0.5 / (1.0 + 20.0/96.0 * pow2(teilungsverhaeltnis_max) );

	if (  relaxfac_wil < datenObjekt->relaxfac && datenObjekt->wil==true)
	{
		datenObjekt->relaxfac = relaxfac_wil;
		//BB:
// 		datenObjekt->relaxfac = 0.01;
		if ( datenObjekt->debug >= 0)
		{
			cout <<"-------------------------------------------------------------------------------------------------"<<endl;
			cout << " WARNUNG: Relaxationsfaktor durch Wilkinson Relaxationsfaktor = " << relaxfac_wil << " ersetzt. " << endl;
			cout << " Das maximales Teilungsverhältnis im Rechengebiet ist " << teilungsverhaeltnis_max << endl;
			cout << " Der kleinster Abstand der QO ist  " << deltaMmin <<" bei j = "<< j_kleinstes <<" und bei i = "<< i_kleinstes <<endl;
			cout <<"-------------------------------------------------------------------------------------------------"<<endl;
		}
	}

	// mindest erforderliche Interationen der äusseren Schleife (1.5 als Sicherheit)
	int iter = 0;
	iter = int( 1.5 * log (relaxfac_wil)/ log (1.0-relaxfac_wil) );


	if (  iter > datenObjekt->anzahlDurchlaeufe )
	{
		datenObjekt->anzahlDurchlaeufe = iter;
		if ( datenObjekt->debug >= 0)
		{
			cout << "WARNUNG: Anzahl der Durchläufe reichen nicht, um völlständige neue Lösung zu erhalten!" << endl;
			cout << " Anzahl der Durchläufe wurde auf =  " << iter << " hochgesetzt "<< endl;
		}
	}
}



void stromlinie::anpassenStromlinienlage()
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			massenStromObjekt.initialisiere_nurbsMassenStromDichte_sinPsi( j,k );
			anpassenStromlinienlage(k, j);
		}
	}
}


void stromlinie::anpassenStromlinienlage (int k, int j)
{
	int zaehler_neueStromlinien;
	double ny_neu;
	double mp_ziel;
	double mp_neu;
	double ny_i;
	double ny_im1;
	double mp_Verhaeltnis;


	for ( int i=1; i<datenObjekt->anzahlStromlinien-1; i++ )
	{
		zaehler_neueStromlinien = 0;
		(*knoten)[k][i][j].ny_alt_pos = 0;
		(*knoten)[k][i][j].ny_alt_neg = 1;

		if ( k>0 && j==0 )  //Übertrag auf 1. QO der weiteren Gitter (nicht 1. Gitter)
		{
			(*knoten)[k][i][j].ny_alt = (*knoten)[k][i][j].ny;
			(*knoten)[k][i][j].ny = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].ny;
		}
		else
		{
			ny_i = (*knoten)[k][i][j].ny;
			ny_im1 = (*knoten)[k][i-1][j].ny;

			mp_ziel = datenObjekt->massenStromVorgabe *datenObjekt->faktorMassenStromRoehre[i-1];
			mp_neu = massenStromObjekt.berechneMassenStromRoehre_sinPsi ( ny_im1, ny_i, j, k );

			do
			{
				//Newton Verfahren um Nullstelle zu finden.
				ny_neu = newtonObjekt.newtonVerfahren ( mp_ziel, mp_neu, ny_i, i, j, k );

				ny_neu = max ( ny_neu,ny_im1 );	//damit sich stromlinien nie schneiden!

				mp_neu = massenStromObjekt.berechneMassenStromRoehre_sinPsi(ny_im1,ny_neu,j,k);

				ny_i = ny_neu;

				zaehler_neueStromlinien++;
				mp_Verhaeltnis = mp_neu/mp_ziel;
			}
			while ( ( mp_Verhaeltnis< ( 1-datenObjekt->toleranz )  || mp_Verhaeltnis> ( 1+datenObjekt->toleranz ) )
					&& zaehler_neueStromlinien < datenObjekt->anzahlDurchlaeufe );

			(*knoten)[k][i][j].ny_alt = (*knoten)[k][i][j].ny;
			(*knoten)[k][i][j].ny = ny_neu;

			DEBUG_1(" Stelle [k][i][j] " << "["<<k <<"]["<<i<<"]["<<j<<"] "  << endl <<
					"  ny_alt " << (*knoten)[k][i][j].ny_alt << endl <<
					"  ny_neu " << (*knoten)[k][i][j].ny );

			(*knoten)[k][i][j].massenStromRoehre = mp_neu;
		}
	}

	ny_im1 = (*knoten)[k][datenObjekt->anzahlStromlinien-2][j].ny;
	ny_i = (*knoten)[k][datenObjekt->anzahlStromlinien-1][j].ny;

	//Massenstrom fuer oberste Stromroehre extra berechnen
	mp_neu = massenStromObjekt.berechneMassenStromRoehre_sinPsi ( ny_im1, ny_i, j, k );
	(*knoten)[k][datenObjekt->anzahlStromlinien-1][j].massenStromRoehre = mp_neu;
}