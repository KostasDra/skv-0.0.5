/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm
*/



#include "wirkungsgrad.h"



wirkungsgrad::wirkungsgrad()
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	quasiorthogonale 	= &datenObjekt->quasiorthogonale;
	stromlinie		= &datenObjekt->stromlinie;
}





void wirkungsgrad::wirkungsgrad_isentrop ( )
{
	int k = datenObjekt->anzahlGitter-1;
	int j = datenObjekt->anzahlQuasiOrthogonale[k]-1;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		//! \f[	\eta_{is}(i) = T_{tot,abs}(i,0,0) \cdot \frac{\left( \Pi_{tot,abs}(i) \right)^\frac{\kappa-1}{\kappa}-1}{T_{tot,abs}(i,j,k)-T_{tot,abs}(i,0,0)}	\f]
		(*stromlinie)[i].eta_isentrop = (*knoten)[0][i][0].T_tot_abs * ( pow ( (*stromlinie)[i].Pi_tot_abs,( (*knoten)[k][i][j].kappa-1 ) /(*knoten)[k][i][j].kappa )-1 ) / ( (*knoten)[k][i][j].T_tot_abs - (*knoten)[0][i][0].T_tot_abs );
	}
	//! Mit \f$ \kappa=\kappa(i,j,k) \f$, i der aktuellen Stromlinie und \f$ j=j_{max},k =k_{max} \f$.
}

void wirkungsgrad::wirkungsgrad_polytrop ( )
{
	int k = datenObjekt->anzahlGitter-1;
	int j = datenObjekt->anzahlQuasiOrthogonale[k]-1;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		//! \f[	\eta_{pol}(i) = \frac{R\cdot\ln\left(\Pi_{tot,abs}(i)\right)}{c_p\cdot\ln\frac{T_{tot,abs}(i,j,k)}{T_{tot,abs}(i,0,0)}}	\f]
		(*stromlinie)[i].eta_polytrop = ( datenObjekt->R * log ( (*stromlinie)[i].Pi_tot_abs ) ) / ( log ( (*knoten)[k][i][j].T_tot_abs /(*knoten)[0][i][0].T_tot_abs ) *(*knoten)[k][i][j].cp );
	}
	//! Mit \f$ c_p=c_p(i,j,k) \f$, i der aktuellen Stromlinie und \f$ j=j_{max},k =k_{max} \f$.
}

void wirkungsgrad::wirkungsgrad_isentrop_m ( )
{
	double eta_is_m = 0.0;

	int k = datenObjekt->anzahlGitter-1;
	int j = datenObjekt->anzahlQuasiOrthogonale[k]-1;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		//! \f[	\eta_{is,ges} = \sum^{i_{max}}_{i=0} T_{tot,abs,m}(0,0)\cdot \frac{ \Pi_{tot,abs,m}(j,k)^\frac{\kappa-1}{\kappa}-1}{T_{tot,abs,m}(j,k)-T_{tot,abs,m}(0,0)}	\f]
		//! Mit \f$ \kappa=\kappa(i,j,k) \f$ und \f$j=j_{max},k =k_{max} \f$.
		eta_is_m += ( (*quasiorthogonale)[0][0].T_tot_abs_m * ( pow ( (*quasiorthogonale)[k][j].Pi_tot_abs_m, ( (*knoten)[k][i][j].kappa-1 ) / (*knoten)[k][i][j].kappa )-1 ) / ( (*quasiorthogonale)[k][j].T_tot_abs_m-(*quasiorthogonale)[0][0].T_tot_abs_m ) );
	}
	//! \f[	\eta_{is,m} = \frac{\eta_{is,ges}}{i}	\f]
	//! Mit \f$ i=i_{max} \f$.
	datenObjekt->eta_isentrop_m = eta_is_m/datenObjekt->anzahlStromlinien;
}





void wirkungsgrad::wirkungsgrad_polytrop_m ( )
{
	double eta_poly_m = 0.0;

	int k = datenObjekt->anzahlGitter-1;
	int j = datenObjekt->anzahlQuasiOrthogonale[k]-1;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		//! \f[	\eta_{pol,ges} = \sum^{i_{max}}_{i=0} \frac{R\ln\left(\Pi_{tot,abs,m}(j,k)\right)}{c_p\cdot\ln\frac{T_{tot,abs,m}(j,k)}{T_{tot,abs,m}(0,0)}}	\f]
		//! Mit \f$ c_p=c_p(i,j,k) \f$ und \f$ j=j_{max},k =k_{max} \f$.
		eta_poly_m += ( ( datenObjekt->R *
				log ( (*quasiorthogonale)[k][j].Pi_tot_abs_m ) )
				/ ( log ( (*quasiorthogonale)[k][j].T_tot_abs_m
				/(*quasiorthogonale)[0][0].T_tot_abs_m ) *(*knoten)[k][i][j].cp ) );
	}
	//! \f[	\eta_{pol,m} = \frac{\eta_{pol,ges}}{i}	\f]
	//! Mit \f$ i=i_{max} \f$.
	datenObjekt->eta_polytrop_m = eta_poly_m/datenObjekt->anzahlStromlinien;
}

//+++++++++++++++++++++++++++++++++++++++ Methoden von Andrea Bader +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void wirkungsgrad::setze_entropie_aus_eta_polytrop ( )
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
			{
				if (k!=0 && j==0)	// BB: Gitterübertrag implementiert
				{
					(*knoten)[k][i][j].entropie = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].entropie;
				}
				if (j!=0)
				{
					double entropie_temp = 0.0;
					double delta_entropie = 0.0;
					double eta_polytrop = datenObjekt->eta_polytrop_vorgabe;

					delta_entropie = (*knoten)[k][i][j].cp * log (fabs( (*knoten)[k][i][j].T /(*knoten)[k][i][j-1].T ));
					delta_entropie = fabs (delta_entropie * (1- eta_polytrop));

					entropie_temp = (*knoten)[k][i][j-1].entropie + delta_entropie;
					(*knoten)[k][i][j].entropie = entropie_temp;
				}
			}
		}
	}
}

void wirkungsgrad::setze_optimalwerte_fuer_u ()
{
	int i = datenObjekt->anzahlStromlinien/2;
	double u_ref = (datenObjekt->omega[datenObjekt->rotorHK_k] * (*knoten)[datenObjekt->rotorHK_k][i][datenObjekt->rotorHK_j].r);

	double schluckziffer_opt = -1.2022969108793E-11* pow4(u_ref) + 1.83572754141704E-08* pow3(u_ref) -
			 0.0000102389163677965* pow2(u_ref) + 0.00261103889784513* u_ref - 0.181822216267666;

	//die Berechnung von eta_opt = f(u_ref) wurde aus Geheimhaltungsgründen für diese Programmversion entfernt.
	//deshalb durch die Vorgabe im Datenobjekt ersetzt!
	double eta_opt = datenObjekt->eta_polytrop_vorgabe;

	datenObjekt->eta_opt = eta_opt;
	datenObjekt->schluckziffer_opt = schluckziffer_opt;
}


void wirkungsgrad::setze_eta_betriebspunkt ( )
{
	int i = datenObjekt->anzahlStromlinien/2;
	double u_ref = (datenObjekt->omega[datenObjekt->rotorHK_k] * (*knoten)[datenObjekt->rotorHK_k][i][datenObjekt->rotorHK_j].r);
	double dichte_tot = datenObjekt->eintrittstotaldichte;
	double schluckziffer_aktuell= datenObjekt->massenStromVorgabe /
			(dichte_tot * pow2(2*(*knoten)[datenObjekt->rotorHK_k][i][datenObjekt->rotorHK_j].r)*u_ref);
	double eta_aktuell = 0.0;
	double eta_opt = datenObjekt->eta_opt;


	//ablegen ins datenObjekt zur Ausgabe in Kennfeld.txt
	datenObjekt->schluckziffer_aktuell = schluckziffer_aktuell;

	//eigene Korrelation aus Messwerten eta_aktuell = f(schluckziffer_aktuell, schluckziffer_opt, eta_opt)
	//wurde aus Geheimhaltungsgründen entfernt - > diese Methode ist damit irrelevant
	//der vorgebene Wirkungsgrad wird für alle Betriebspunkte übernommen!

	eta_aktuell = eta_opt;

	// schreiben ins datenobjekt;
	datenObjekt->eta_polytrop_vorgabe = eta_aktuell;
}