/***************************************************************************
 *   Copyright (C) 2008-2009 by Florian Mayer (ilumine@gmx.de)             *
 *                 2009 by Oliver Borm (oli.borm@web.de)                   *
 *                 2010 by Balint Balassa (balint.balassa@gmx.de)          *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef CGNSFORMAT_H
	#define CGNSFORMAT_H

#include <iostream>
#include <cmath>	// für M_PI
#include <string>
#include <cgnslib.h>
//#include <cstdlib>

#include "daten.h"

using namespace std;
//! Klasse für die Bearbeitung von CGNS Dateien.
/*! Die hier verwendeten Funktionen zur Erstellung einer CGNS-Datei
   stammen aus dem Dokument 'CGNS Mid-Level Library' von
   http://www.cgns.org.
   @author Oliver Borm, Florian Mayer, Balint Balassa
*/
class CGNSformat
{
	private:
		daten		*datenObjekt;
		v3Knoten	*knoten;
		vStromlinie	*stromlinie;
		v2StromlinieGitter	*stromlinieGitter;

		int ier;	//!< Error status
		int fn;		//!< file index number
		int B;		//!< Base index number
		int Z;		//!< Zone index number
		int G;		//!< GridCoordinate index number
		int C;		//!< Coordinate index number
		int S;		//!< Solution index number
		int F;		//!< Fieldsolution index number
		int BC;		//!< Boundary Condition index number
		int I;		//!< Interface index number
		int nzones;	//!< Anzahl der Zone_t-Knoten
		int nsolutions;	//!< Anzahl der FlowSolution_t-Knoten
		int nfields;	//!< Anzahl der Loesungsfelder in FlowSolution_t
		int ncoords;	//!< Anzahl der Coordinates_t-Knoten
		int ngrids;	//!< Anzahl GridCoordinates_t-Knoten
		char basename[8];//!< Basename
		char zonename[8];//!< Zonename
		char newzonename1;
		char newzonename[8];
		char coordname[13];//!< Coordinatename
		string szCoordname;
		char solname[33];//!< solutionname
		char fieldname[33];//!< fieldsolutionname
		string szFieldname;
		int size[3][2];	//!< siehe cgns mid level library
		int NVertexR;	//!< Anzahl Punkte in radialer Richtung
		int NVertexPhi;	//!< Anzahl Punkte in Umfangsrichtung
		int NVertexZ;	//!< Anzahl Punkte in z-Richtung
		DataType_t datatype;
		GridLocation_t location;

		int cell_dim; //!< cell dimension
		int phys_dim; //!< physical dimension


	public:
		//! Initialisieren der benötigten Parameter
		/*!
		    Mit Hilfe dieser Funktion werden alle benötigten
		    Paramter initialisiert.
		*/
		CGNSformat();

		
		//! Lesen aus CGNS-Datei
		/*!
		    Mit Hilfe dieser Funktion werden alle benötigten
		    Daten aus der CGNS-Eingabe-Datei eingelesen.
		    \param workDir Verzeichnis in dem die Quelldateien liegen.
		*/
		void readCGNS ( string workDir);


		//! Schreiben in CGNS-Datei
		/*!
		    Mit Hilfe dieser Funktion werden alle angegebenen
		    Daten in eine CGNS-Ausgabe-Datei geschrieben.
		    \param workDir Verzeichnis in das die Ergebnisdateien gelegt werden sollen.
		*/
		void writeCGNS ( string workDir );
};

#endif
