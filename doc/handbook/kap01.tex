%% ==============================================================
%% SKV Manual von Oliver Borm, Andrea Bader, Balint Balassa, Florian Mayer  
%% steht unter einer Creative Commons Namensnennung - Weitergabe 
%% unter gleichen Bedingungen 3.0 Deutschland Lizenz.
%% http://creativecommons.org/licenses/by-sa/3.0/de/
%% ==============================================================

%% ====================================================
%%  **********           Kapitel 1          **********
%% ====================================================

\chapter*{Vorwort}
\addcontentsline{toc}{chapter}{Vorwort}
\label{kap00}

Das Stromlinienkrümmungsverfahren Programm  SKV ist im Laufe der Jahre immer wieder von verschiedenen Studenten im Rahmen von Semester- \cite{borm:2006} oder Diplomarbeiten \cite{mayer:2008} \cite{bader:2010} oder HiWi-Tätigkeiten erweitert worden. 
So entstanden entsprechend viele separate Arbeiten, die das jeweilige Thema der Erweiterung behandelten und dabei die vorangegangenen Themen nur kurz erwähnten, oder vielleicht sogar nur im Literaturverzeichnis aufführten.

Das vorliegende Manual soll als Bedienungsanleitung und Softwaredokumentation für das  SKV 0.0.5 als Ganzes dienen, und dabei alle Teilaspekte behandeln, die mit der Zeit hinzugekommen sind. 
Um den Dokumentationsaufwand zu verringern, wurde dieses Manual in weiten Teilen  aus den Semester- und Diplomarbeiten zusammengestellt. Dabei wurden einige Aspekte ergänzt, die das Verständnis und die Bedienung erleichtern sollen.

\newpage

\section*{Kurze Entwicklungsgeschichte}
\addcontentsline{toc}{section}{Kurze Entwicklungsgeschichte}

\begin{description}
\item[2006] \hfill \\
  Erste Version des SKV mit Druck-DGL veröffentlicht \cite{borm:2006}: Einfache axiale Geometrien rechenbar. Input und Output nur in Form von Textdateien.

\item[2008] \hfill \\
   Zweite Version des SKV \cite{mayer:2008}: Radiale Geometrien werden jetzt auch unterstützt. Input in Form von CGNS-Datei und einer Textdatei, Output in Form von CGNS oder Text möglich. Einfaches System für Verdichtungsstöße implementiert.

\item[2009] \hfill \\
  Dritte Version: Architektur komplett nach Objektorientiertem Programmierparadigma umgestellt. Grundlegende Überarbeitung.
  
  Fehler in DGL korrigiert. $c_ur=\const$ in unbeschaufelten Raum. Rotor-Stator-Geschwindigkeits-Übergangproblem gelöst. Wirkungsgrad und Totaldruckverhältnis implementiert.

\item[2010] \hfill \\
  Vierte Version: Kommandozeilenargumente eingeführt. Input jetzt durch CGNS-Datei alleine möglich. DoxyGen Doku abgeschlossen.

  SKV mit Meridiangeschwindigkeit DGL fertiggestellt \cite{bader:2010}.

\item[2011] \hfill \\
  Fünfte Version: Aktuelles Druck-Löser SKV erweitert um Meridiangeschwindigkeit-Löser. Weitere Kommandozeilenargumente hinzugefügt.
  
  Sechste Version (in Planung): Implementierung von Verlustmodellen. Validierung.
\end{description}

\chapter{Stromlinienkrümmungsverfahren}
\label{kap01}

In diesem Kapitel sowie in \anref{anhang01} soll der grundsätzliche Ablauf eines Stromlinienkrümmungsverfahrens erläutert und die das Problem beschreibenden Gleichungen hergeleitet werden. 
Als Grundlage für die mathematische Herleitung des Druck-Differentialgleichung und das grundlegende Verständnis diente \cite{lichtfuss:2005}. Eine allgemeine Einführung in die Strömungsmechanik ist in \cite{oertel:2004} zu finden.
In \kapref{kap05} und in \kapref{kap07} wird auf die numerischen Methoden eingegangen.
Für die Anwendung des Programmes, wie sie in \kapref{kap02} beschrieben wird, ist, wie bei fast allen Programmen der numerischen Strömungsmechanik, kein vertieftes Verständnis der mathematischen Herleitungen und physikalischen Zusammenhänge notwendig.
Auf die Umsetzung in C++, wird an den entsprechenden Stellen der dazugehörigen Theorie verwiesen.


\section{Hinweise zu Abkürzungen}

Häufig verwendete Begriffe werden manchmal abgekürzt und sollen hier deswegen aufgeführt werden:

\begin{itemize}
\item Stromlinienkrümmungsverfahren $\rightarrow$ SKV
\item Quasiorthogonale $\rightarrow$ QO
\item Stromlinie $\rightarrow$ SL
\item Differentialgleichung $\rightarrow$ DGL
\item Navier-Stokes $\rightarrow$ NS
\item CFD-General-Notation-System $\rightarrow$ CGNS
\item Non-Uniform-Rational-Basis-Spline $\rightarrow$ NURBS
% \item  $\rightarrow$
\end{itemize}

\section{Berechnungsverfahren}

\subsection{Quasi 3D Methode}

Um den Berechnungsaufwand einer 3D Berechnung einer Turbomaschine zu reduzieren, teilt man diese in zwo 2D Berechnungen auf und koppelt beide Berechnungen. Die Berechnung erfolgt auf so genannten S1- und S2-Flächen. 
Die Bezeichnung der Flächen erfolgt nach \cite{naca:tn:2604}.
Die S1-Flächen werden von Schaufel zu Schaufel und der Meridianrichtung aufgespannt. Während die S2-Flächen von Nabe zu Gehäuse und auch in Meridianrichtung aufgespannt werden.  
Man kann die Berechnung natürlich auf unendlich vielen Flächen durchführen. Meistens beschränkt man sich aber auf eine mittlere S2-Fläche und einige S1-Flächen. 
Die Flächen kann man als Stromflächen definieren, dann ist deren Lage Ergebnis der Rechnung und die Geschwindigkeit normal zur Fläche Null. Oder man definiert sie durch geometrische Beziehungen, dann ändert sich die Lage der Flächen während der Rechnung nicht mehr und die Geschwindigkeit, normal zur Fläche, ist i.a. ungleich Null. Die Definition der Flächen als Stromflächen versagt aber bei stark 3D Strömungseffekten, wie z. Bsp. Wirbeln.

S3-Flächen erstrecken sich von Nabe zu Gehäuse und von Schaufel zu Schaufel. Eine S3-Stromfläche steht somit exakt orthogonal zur Strömung.

\begin{figure}[htb]
\makebox[\textwidth]{\includegraphics[width=0.5\textwidth]{Flaechen}}
\caption{S1- und S2-Flächen in einem Schaufelgitter aus \cite{naca:tn:2604}\label{ab01:1a}}
\end{figure}

\subsection{Funktionsweise eines Stromlinienkrümmungsverfahrens}

Die Berechnung durch das Stromlinienkrümmungsverfahren (SKV) erfolgt in den Schnittpunkten von Quasiorthogonale und Stromlinie auf einer in die r-z-Ebene hinein gedrehten S2m-Stromfläche. Die Ergebnisse der Berechnung erhält man aber auf der allgemeinen dreidimensionalen mittleren S2-Fläche.
Die S2m-Stromfläche entspricht in etwa der Skelettfläche einer Schaufel.
Die Schnittkurve einer geometrisch definierten S3-Fläche und der S2m-Stromfläche bezeichnet man als Quasiorthogonale, da diese Kurve quasi orthogonal zur Strömung steht. Als Stromlinie bezeichnet man eine Schnittkurve einer S1-Stromfläche und der S2m-Stromfläche.

\begin{figure}[htb]
\makebox[\textwidth]{\includegraphics[width=0.9\textwidth]{Rechengebiet}}
\caption{Rechengebiet \label{ab01:1}}
\end{figure}

Es gibt zwei unterschiedliche Methoden von SKV's:
\begin{itemize}
\item \textit{Duct Flow}: Quasiorthogonalen nur im schaufelfreien Raum an Vorder- und Hinterkante (grüne Linien in \abref{ab01:1})
\item \textit{Through Flow}: Quasiorthogonalen kontinuierlich verteilt 
\end{itemize}

Die \textit{Through Flow} Methoden sind rechenintensiver. Dies liegt zum einen daran, dass einfach mehr Quasiorthogonale da sind und zum anderen sind die zu lösenden Formeln komplizierter.

Aufgrund der extrem kurzen Rechenzeit des SKV's im Vergleich zu einer dreidimensionalen Navier-Stokes-Rechnung (NS3D), wird dieses Verfahren auch heutzutage noch in der Auslegung von Turbomaschinen in jeder großen Firma oder Institution verwendet, wie eine ausgiebige Literaturrecherche ergeben hat. Diese Programme werden von den Firmen sehr geschützt behandelt, da in diesen die jahrzehntelange Erfahrung der Firmen steckt. Sie sind deshalb nicht frei verfügbar. Mit einem SKV ist es überhaupt erst möglich instationäre Prozesse, wie das Hochfahren einer Gasturbine, zu berechnen. Das vorliegende Programm beschränkt sich aber auf stationäre Vorgänge.

Dem vorliegenden Programm liegen zwei verschiedene Ansätze zu Grunde. Differentialgleichungen entlang der Quasiorthogonalen können hier für folgende Größen gelöst werden:
\begin{itemize}
 \item Druck $p$
 \item Meridiangeschwindigkeit $w_m$
\end{itemize}

Ausgehend von den unterschiedlichen Betrachtungsweisen ergeben sich unterschiedliche Lösungswege, die jeweils Vor- und Nachteile mit sich bringen. Im Folgenden sollen die beiden Ansätze kurz vorgestellt werden.

\subsubsection{Annahmen bei der Druck-DGL}

Ausgehend von den reibungsfreien Erhaltungsgleichungen der Fluidmechanik (Eulergleichungen) erhalten wir die Grundgleichung für das SKV. \glref{gl0001_1_1} ist eine gewöhnliche Differentialgleichung (DGL) für eine gewählte Quasiorthogonale. Die mathematische Herleitung kann im \anref{anhang1:3} nachgelesen werden.

Da man in Turbomaschinen immer einen stehenden und rotierenden Teil hat, ist es einfacher die Grundgleichungen im jeweiligen Relativsystem zu lösen, anstatt im Absolutsystem. Im Stator fällt das Relativsystem mit dem Absolutsystem zusammen, da dort $\omega = 0$ ist. \\
In der dreidimensionalen Impulsgleichung ersetzen wir deshalb die Absolutgeschwindigkeit und -beschleunigung durch die Relativgeschwindigkeit und -beschleunigung, so wie aus der Technischen Mechanik bekannt. Dadurch treten zusätzliche Beschleunigungsterme auf. Dies sind die Führungsbeschleunigung translatorisch, Führungsbeschleunigung rotatorisch normal, Führungsbeschleunigung rotatorisch tangential, Coriolisbeschleunigung und Relativbeschleunigung.

Die Kontinuitätsgleichung formt man durch Definition einer Schichtdicke in $\varphi$-Richtung, welche die Versperrung durch die Schaufeln berücksichtigt, und die Gibb'sche Gleichung um.

Da man in der Herleitung Reibungsfreiheit vorausgesetzt hat, stimmen die Winkel am Ende jeder Schaufelreihe, welche sich aus den berechneten Geschwindigkeiten ergeben, zwischen Rechnung und Wirklichkeit nicht überein. Dies würde zu einer Falschanströmung der jeweils folgenden Schaufelreihe führen. Deshalb gibt man sich die Entropie, welche bei der Durchströmung entsteht, vor. Die Entropie erhält man aus Versuchen, empirischen Korrelationen \cite{nasa:sp:36} oder NS3D Rechnungen. Wobei sich die meisten Korrelationen nur für \textit{Duct Flow} Methoden eignen. Bei den durch NS3D Rechnungen unterstützten Berechnungen nimmt man an, dass sich die Entropieerzeugung im Laufe der Geometrieoptimierung nicht wesentlich ändert.
Durch die Vorgabe der Entropie ist das Gleichungssystem aber überbestimmt, da man bei der Herleitung eine reibungsfreie Strömung vorausgesetzt hat, deshalb bleibt eine DGL, die Impulsgleichung senkrecht zu den Quasiorthogonalen, unberücksichtigt. Das Ergebnis dieser DGL wäre die Entropiedifferenz, diese ist  in diesem Fall natürlich Null.

Mit der Rothalpie, dem zweiten Hauptsatz der Thermodynamik und der verbliebenen Impulsgleichung kann das Gleichungssystem zur Bestimmung des Druckes entlang einer Quasiorthogonalen iterativ gelöst werden.
Prinzipiell kann jede andere Strömungsgröße entlang der Quasiorthogonalen gelöst werden. Speziell in  der englisch sprachigen Literatur wird häufig der Relativgeschwindigkeitsgradient gelöst.


\subsubsection{Annahmen bei der Meridiangeschwindigkeit-DGL}
Die Herleitung nach Denton \cite{denton:1978} 
basiert auf der Annahme, dass die Beschleunigung eines Fluidelements in Richtung der Quasiorthogonalen
mit der Summe der angreifenden Kräfte, bestehend aus dem Druckgradienten, der Reibungskraft und den äußeren Kräften in $q-$Richtung, im Gleichgewicht steht. Der Impulssatz in  Richtung der Quasiorthogonalen lautet damit:

\begin{equation}
\frac{\bar{\partial} c_{q}}{\partial t} = - \frac{1}{\rho} \frac{\bar{\partial} p}{\partial q} + F_{\mathrm{R},q} + F_{q} \ .
 \label{eq:ggw-denton}
 \end{equation}
 
Die Beschleunigung eines Fluidelements entlang der Quasiorthogonalen zerlegt Denton aus der Anschauung in die Komponenten, wie sie in Bild \ref{Fig:bescheluigungterme}  und Gleichung (\ref{eq:ggw-beschleunigungkomponenten}) angegeben sind.
 
% \begin{figure} [htbp]
% \centering
% \includegraphics[angle=0,width=0.4\textwidth,clip]%
%                 {./bilder/beschleunigungterme}
%   \caption{Beschleunigungsterme am Fluidelement}
%  \label{Fig:bescheluigungterme}
% \end{figure} 
 



\begin{equation}
\frac{\bar{\partial} c_{q}}{\partial t} = -\frac{c_u^2}{r} \sin \gamma + \frac{w_m^2}{R_{\mathrm{k}}} \sin \psi + w_m \frac{\bar{\partial} w_m}{\partial m}  \cos \psi
 \label{eq:ggw-beschleunigungkomponenten}
\end{equation}

Der erste Beschleunigungsterm ist die Normalbeschleunigung, die aus der Umfangskomponente der Absolutgeschwindigkeit resultiert. Die Projektion in $q-$Richtung vermittelt $\sin \gamma$. In Radialmaschinen beträgt der Winkel $\gamma$ bis zu $180^{\circ}$, weshalb die daraus resultierende Beschleunigung gering ist.

Dafür nimmt die durch die Stromlinienkrümmung hervorgerufene Normalbeschleunigung $\frac{w_m^2}{R_{\mathrm{k}}}$ durch den $\sin \psi$, der in Radialmaschinen nahe 1 ist, und durch die stark gekrümmten Stromlinien eine vorrangige Stellung ein. Aus dieser Berücksichtigung der Stromlinienkrümmung resultiert die Bezeichnung als "`Stromlinienkrümmungsverfahren"'.

Der letzte Term beschreibt die konvektive Beschleunigung in Richtung der Stromlinie, die durch $\cos \psi $ in die Richtung der Quasiorthogonalen projiziert wird. Da der Winkel $\psi$ zwischen der Stromlinie und der Quasiorthogonalen nahezu $90^{\circ}$ beträgt, wird dieser Term auch bei stark verzögerter Strömung klein sein.

Der Druckgradient in Gleichung (\ref{eq:ggw-denton}) wird üblicherweise durch die Gradienten der Totalenthalpie und Entropie ersetzt, wobei gemäß dem zweiten Hauptsatz der Thermodynamik gilt: 
%
\begin{equation}
T \mathrm{d} s = \mathrm{d} h - v \mathrm{d} p \ .
\label{eq:gibbs}
\end{equation}

Die Aufteilung in den reversiblen und irreversiblen Anteil schließt nach  \cite{benetschik2010} bereits die Reibungskräfte $v \mathrm{d}\: \bar{\bar{\tau}}$ ein, und es ergibt sich daher:

\begin{equation}
 T \mathrm{d} s_{\mathrm{rev}}+ T \mathrm{d} s_{\mathrm{irr}}= \mathrm{d} h - v \mathrm{d} p \ . %_{w\perp w}
\label{eq:gibbs-ben}
\end{equation}

Unter Verwendung  der Gleichungen (\ref{enthalpie-berechnung}), (\ref{bcDef}) und $v = \frac{1}{\rho}$ folgt daraus:

\begin{equation}
T \mathrm{d} s = \mathrm{d} h_{\mathrm{t}}\: - \: w_m \mathrm{d} w_m \:- \:  c_u \mathrm{d} c_u  \:-\:  \frac{1}{\rho} \mathrm{d} p \ + \frac{1}{\rho} \mathrm{d}\:  \bar{\bar{\tau}}  \ .%_{w\perp w}}}} .
\label{eq:gibbs-eingesetzt}
\end{equation}

Da die Reibungskraft $F_{\mathrm{R},q} = \frac{{\partial} \tau_{mq}}{\partial q} $ entspricht, schreibt sich der Druckgradient in Richtung der Quasiorthogonalen folglich zu:
\begin{equation}
\frac{1}{\rho} \frac{\bar{\partial} p}{\partial q} = \frac{\bar{\partial} h_{\mathrm{t}}}{\partial q}\: -\: \frac{T \bar{\partial} s}{\partial q}\: -\: w_m \frac{\bar{\partial} w_m}{\partial q}\: -\:  c_u \frac{\bar{\partial} c_u}{\partial q} + \: F_{q} \ .
\label{eq:druckgradient}
\end{equation}

In Kombination mit den einzelnen Beschleunigungstermen aus (\ref{eq:ggw-beschleunigungkomponenten}) ergibt sich:
\begin{equation}
-\frac{c_u^2}{r} \sin \gamma + \frac{w_m^2}{R_{\mathrm{k}}} \sin \psi + w_m \frac{\bar{\partial} w_m}{\partial m}  \cos \psi = -\frac{\bar{\partial} h_{\mathrm{t}}}{\partial q}\: +\: \frac{T \bar{\partial} s}{\partial q}\: +\: w_m \frac{\bar{\partial} w_m}{\partial q}\: +\:  c_u \frac{\bar{\partial} c_u}{\partial q} \: - \: F_{q} \ .
\label{eq:dgl-anfang}
\end{equation}


Mit Hilfe folgender Umformung 

\begin{align}
\frac{1}{2r^{2}} 	\frac{{\bar{\partial}(rc_u)^2}}{\partial q} \label{eq:zusammenfassenDrall}
	&= \frac{1}{2r^{2}} \ \left( r^2 \  \frac{\bar{\partial} c_u^2}{\partial q} + c_u^2 \ \frac{\bar{\partial} r^2}{\partial q}   \right) \\ \nonumber
	&= \frac{1}{2r^{2}} \ \left(  2\; r^2  c_u \ \frac{\bar{\partial} c_u}{\partial q} + 2 \; c_u^2 \;r \ \frac{\bar{\partial} r}{\partial q}   \right)  \\ \nonumber 
	&= c_u \ \frac{\bar{\partial}c_u}{\partial q} +  \frac{c_u^2}{r} \  \sin \gamma	 \nonumber
\end{align}

lässt sich Gleichung (\ref{eq:dgl-anfang}) zusammenfassen. Nach den Gradienten der Meridiangeschwindigkeit umgestellt, schreibt diese sich zu:
\begin{equation}
\frac{1}{2}  \frac{\bar{\partial}w_m^2}{\partial q}
= w_m \frac{\bar{\partial} w_m}{\partial m}  \cos \psi + \frac{w_m^2}{R_{\mathrm{k}}} \sin \psi + \frac{{\bar{\partial}h_{\mathrm{t}}}}{\partial q}  - T \frac{\bar{\partial}s}{\partial q}  -\frac{1}{2r^{2}} 	\frac{{\bar{\partial}(rc_u)^2}}{\partial q} + \: F_{q} \ .
\label{eq:dgl-hergeleitet}
\end{equation}

Die äußeren Kräfte in Richtung der Quasiorthogonalen $F_{q}$ setzen sich aus der Kraftwirkung der Beschaufelung $F_{\mathrm{S}}$ und einer weiteren Reibungskraft $F_{\mathrm{R},m}$ entlang der Stromlinie zusammen. Die Schaufelwirkung in Umfangsrichtung ist nach Cumpsty (Kapitel 3.4) \cite{cumpsty2004b} durch Gleichung  (\ref{eq:schaufelkraft-cumpsty}) gegeben.

\begin{equation}
F_{\mathrm{u}} = \frac{w_m}{r} \frac{{{\bar{\partial}}(rc_u)}}{\partial m} 
\label{eq:schaufelkraft-cumpsty}
\end{equation}


Die Wirkung der Schaufelkraft in Richtung der Quasiorthogonalen ist nach Bild \ref{Fig:alphabeta} und Gleichung (\ref{alphaDef})
über den Winkel $\alpha $ definiert. 

\begin{equation}
F_{\mathrm{S}} = \tan \alpha \ F_{\mathrm{u}} = \tan \alpha \ \frac{w_m}{r} \frac{{\bar{\partial}(rc_u)}}{\partial m} 
\label{eq:schaufelkraft-tanalpha}
\end{equation}

Die Berücksichtigung von Verlusten verursacht einen Entropiegradienten in Richtung der Stromlinie. Um die Konsistenz zwischen der Verlustkorrelation und der resultierenden Reibungskraft entlang der Stromlinie zu gewährleisten, wird die Reibungskraft nach Gleichung (\ref{eq:reibungskraft}) definiert und mit Hilfe des $\cos \psi$ in Richtung der Quasiorthogonalen projiziert.
\begin{equation}
F_{\mathrm{R},m} = \cos \psi T \frac{\bar{\partial}s}{\partial m} 
\label{eq:reibungskraft}
\end{equation}

Eine analytische Herleitung der Reibungskraft $F_{\mathrm{R},m}$ und Diskussion erfolgt in \cite{casey2008vista} mit der Schlussfolgerung, dass die Berücksichtigung dieser Reibungskraft entlang der Stromlinien keinen signifikanten Einfluss auf die Genauigkeit des Verfahrens hat. Aus diesem Grund wird an dieser Stelle auf eine detaillierte Betrachtung verzichtet.






%% ====================================================
%%  **********       ENDE  Kapitel 1        **********
%% ====================================================

