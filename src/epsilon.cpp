/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm, Florian Mayer
*/

#include "epsilon.h"


epsilon::epsilon()
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	quasiorthogonale 	= &datenObjekt->quasiorthogonale;
	stromlinie		= &datenObjekt->stromlinie;
	stromlinieGitter	= &datenObjekt->stromlinieGitter;
}

void epsilon::setzeEpsilon ( )
{
	Vector_Point2Dd Derivative;
	/* o.b.: auskommentiert 2010-02-12
	double delta_r = 0.0;
	double delta_z = 0.0;
	*/
	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
	{
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
			{
				(*stromlinieGitter)[k][i].nurbsGitterStromlinie.deriveAt ( (*knoten)[k][i][j].myG,1,Derivative );
				/* o.b.: auskommentiert 2010-02-12
				delta_r = Derivative[1].y();
				delta_z = Derivative[1].x();
				
				if ( delta_r>-0.000002 && delta_r<0.000002 )
					{delta_r = fabs ( Derivative[1].y() );}
				if ( delta_z>-0.000002 && delta_z<0.000002 )
					{delta_z = fabs ( Derivative[1].x() );}
				*/
				//!	\f[ 	\varepsilon = \arctan \left( \frac{\mathrm{d} r}{\mathrm{d} z} \right) \f]
				(*knoten)[k][i][j].epsilon = atan2 ( Derivative[1].y(), Derivative[1].x() );

				if ( datenObjekt->debug >1 )
				{
					cout << "i:"<<i<<endl;
					cout << "delta_r: "<<Derivative[1].y() <<"\tdelta_z: "<<Derivative[1].x() <<endl;
					cout << "epsilon: " << (*knoten)[k][i][j].epsilon << " [rad]  " << ( (*knoten)[k][i][j].epsilon /M_PI ) *180 << " [°]"<<endl;
				}
			}
		}
	}
	//! aktualisiere_nurbsEpsilon()
	aktualisiere_nurbsEpsilon ( );
	//! aktualisiere_nurbsEpsilonQO()
	aktualisiere_nurbsEpsilonQO ( );
}


void epsilon::aktualisiere_nurbsEpsilon ( )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve;

	int n;
	//! Schleife über alle Knotenpunkte (außer 1. QOs nachfolgender Gitter):
	for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ( k>0&&j==0 ) {;}
				else
				{
					//! Erstellen der neuen Knotenpunktkoordinaten
					punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].epsilon ) ;
					n++;
				}
			}
		}
		//! NURBS++ Interpolation durch die neuen Knotenpunkte
		kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
		(*stromlinie)[i].nurbsEpsilon = kurve;
	}

	//Gitter
	Vector_HPoint2Dd punkteG;
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkteG.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkteG[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].epsilon ) ;
				n++;
			}
			kurve.globalInterpH ( punkteG,datenObjekt->gradNurbsKurven );
			(*stromlinieGitter)[k][i].nurbsEpsilonGitter = kurve;
		}
	}
}


void epsilon::aktualisiere_nurbsEpsilonQO ( )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	NurbsCurve_2Dd kurve;
	int n;
	//! Schleife über alle Knotenpunkte 
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
		{
			n=0;
			for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
			{
				//! Erstellen der neuen Knotenpunktkoordinaten
				punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].epsilon ) ;
				n++;
			}
			//! NURBS++ Interpolation durch die neuen Knotenpunkte
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*quasiorthogonale)[k][j].nurbsEpsilonQO = kurve;
		}
	}
}


void epsilon::setzeGamma ( )
{
	Vector_Point2Dd Derivative;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.deriveAt ( (*knoten)[k][i][j].ny,1,Derivative );
				/* o.b.: auskommentiert 2010-02-12
				if ( fabs( Derivative[1].y() ) < datenObjekt->gammaToleranz ) Derivative[1].y() = 0.0;
				if ( fabs( Derivative[1].x() ) < datenObjekt->gammaToleranz ) Derivative[1].x() = 0.0;
				*/
				//! \f[ \gamma = \arctan \left( \frac{\mathrm{d} r}{\mathrm{d} z} \right) \f]
				(*knoten)[k][i][j].gamma = atan2 ( Derivative[1].y(), Derivative[1].x() ) ;

				if ( datenObjekt->debug >1 )
				{
					cout << "k:"<<k<<"  j:"<<j<< " i:" << i << endl;
					cout << "delta_r: "<< Derivative[1].y() << "\tdelta_z: " << Derivative[1].x() <<endl;
					cout << "gamma: " << (*knoten)[k][i][j].gamma << " [rad]  " << ( (*knoten)[k][i][j].gamma /M_PI ) *180 << " [°]"<<endl;
				}
			}
		}
	}
}


void epsilon::setzeBeta ( )
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				if ( !(*knoten)[k][i][j].unbeschaufelt )
				{
					//!	\f[ \beta = \mathrm{arccot} \left( \sin \varepsilon \cdot \cot \beta_{ru} + \cos \varepsilon \cdot \cot \beta_{zu} \right) \f]
					(*knoten)[k][i][j].beta = acot( sin ( (*knoten)[k][i][j].epsilon ) * cot ( (*knoten)[k][i][j].beta_ru ) + cos ( (*knoten)[k][i][j].epsilon ) * cot ( (*knoten)[k][i][j].beta_zu ) );

// 					cout << "Setze beta = " << (*knoten)[k][i][j].beta << " Unbeschaufelt = " << (*knoten)[k][i][j].unbeschaufelt << endl;
// 					cout << "epsilon = " << (*knoten)[k][i][j].epsilon << " beta_ru = " << (*knoten)[k][i][j].beta_ru  << " beta_zu = " << (*knoten)[k][i][j].beta_zu << endl;
// 					cout << "sin ( epsilon ) = " << sin((*knoten)[k][i][j].epsilon) << " cot(beta_ru) = " << cot((*knoten)[k][i][j].beta_ru) << " cos ( epsilon ) = " << cos((*knoten)[k][i][j].epsilon)  << " cot(beta_zu) = " << cot((*knoten)[k][i][j].beta_zu) << endl;
				}
// 				else// nur Initialisierung
// 				{
// 					(*knoten)[k][i][j].beta = 0.5*M_PI; // nur Initialisierung
// 				}

				if ( datenObjekt->debug > 1 )
				{
					cout << "beta: " << (*knoten)[k][i][j].beta << " [rad]  " << ( (*knoten)[k][i][j].beta /M_PI ) *180<< " [°]" << endl;
				}

			}//i_ende

		}//j_ende
	}//k_ende
}

// void epsilon::initialisiereBeta ( )
// {
// 	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
// 	{
// 		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
// 		{
// 			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
// 			{
// 				(*knoten)[k][i][j].beta = 0.5*M_PI; // nur Initialisierung
// 			}//i_ende
//
// 		}//j_ende
// 	}//k_ende
// }

/*
void epsilon::berechne_alpha ( int i, int j, int k )
{
// 	double c = (*knoten)[k][i][j].c;
// 	double c_m = (*knoten)[k][i][j].w_m;
// 	double temp = 0.0;

	//temp = atan2 ( (*knoten)[k][i][j].w_m,(*knoten)[k][i][j].w_m /tan ( M_PI-(*knoten)[k][i][j].beta )-datenObjekt->omega[k] *(*knoten)[k][i][j].r );
// 	temp = atan2 ( (*knoten)[k][i][j].w_m,(*knoten)[k][i][j].w_m /tan ( (*knoten)[k][i][j].beta )+datenObjekt->omega[k] *(*knoten)[k][i][j].r );

	//cout << "(*knoten)[" << k << "][" << i << "][" << j << "].w_m =  " << (*knoten)[k][i][j].w_m << endl;
// /*	if ( (*knoten)[k][i][j].beta == M_PI || (*knoten)[k][i][j].beta == -M_PI || (*knoten)[k][i][j].beta == 0.0 )
// 	{
//
// 		(*knoten)[k][i][j].alpha = atan2 ( (*knoten)[k][i][j].w_m, datenObjekt->omega[k] *(*knoten)[k][i][j].r );
// 	}
// // 	else if ( (*knoten)[k][i][j].beta == 0.0)
// // 	{
// //
// // 	(*knoten)[k][i][j].alpha = atan2 ( (*knoten)[k][i][j].w_m, datenObjekt->omega[k] *(*knoten)[k][i][j].r );
// // 	}
// 	else
// 	{
		(*knoten)[k][i][j].alpha = atan2 ( (*knoten)[k][i][j].w_m,(*knoten)[k][i][j].w_m * cot((*knoten)[k][i][j].beta) + datenObjekt->omega[k] * (*knoten)[k][i][j].r );

// 	}
	//cout << "alpha: " << (*knoten)[k][i][j].alpha << " [rad]  " << ( (*knoten)[k][i][j].alpha /M_PI ) *180<< " [°]" << endl;
	setze_cot_alpha ( i,j,k );
}
*/


void epsilon::setze_cotBeta ( )
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].cot_beta = cot( (*knoten)[k][i][j].beta );
			}
		}
	}
}


void epsilon::setze_cosEpsilon ( )
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].cos_epsilon = cos ( (*knoten)[k][i][j].epsilon );
			}
		}
	}
}


void epsilon::setze_sinEpsilon ( )
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].sin_epsilon = sin ( (*knoten)[k][i][j].epsilon );
			}
		}
	}
}

void epsilon::setze_Stroemungswinkel()
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				//Strömungswinkel Beta, nur zur Ausgabe
				//! \f[ \beta_{St} = \cot^{-1} \left( \frac{w_u}{w_m} \right) \f]
				(*knoten)[k][i][j].beta_St = acot ((*knoten)[k][i][j].w_u / (*knoten)[k][i][j].w_m);
				//! \f[ \alpha_{St} = \cot^{-1} \left( \frac{c_u}{w_m} \right) \f]
				(*knoten)[k][i][j].alpha_St = acot ((*knoten)[k][i][j].c_u / (*knoten)[k][i][j].w_m);

				//weitere Winkel, nur zur Ausgabe
				
				if ((*knoten)[k][i][j].sin_epsilon > 1.0 && (*knoten)[k][i][j].sin_epsilon < 1.01){(*knoten)[k][i][j].sin_epsilon=1.0;}// BB: Sonst gibt es gelegentlich numerischen Fehler!
				(*knoten)[k][i][j].epsilon = asin ((*knoten)[k][i][j].sin_epsilon);//nur zur Ausgabe
				
				if ((*knoten)[k][i][j].sin_gamma > 1.0 && (*knoten)[k][i][j].sin_gamma < 1.01){(*knoten)[k][i][j].sin_gamma=1.0;}// BB: Sonst gibt es gelegentlich numerischen Fehler!
				(*knoten)[k][i][j].gamma = asin ((*knoten)[k][i][j].sin_gamma);//nur zur Ausgabe
			}
		}
	}
}
