/* 
 * File:   realFluid.h
 * Author: kostas
 *
 * Created on April 7, 2015, 12:20 AM
 */

#ifndef REALFLUID_H
#define	REALFLUID_H

#include "CoolProp.h"
#include "AbstractState.h"
#include <iostream>
#include "crossplatform_shared_ptr.h"
#include <string>
#include <vector>
#include "daten.h"
#include <fstream>
#include <csignal>
using namespace CoolProp;


class realFluid {
public:
    shared_ptr<AbstractState> myFluid;
    realFluid();
    void readFluid(const string &path);
    void update(const int j, double P, double T);
    //void setFluid(string &mybackend, const std::vector<string> &fluidN);
    static realFluid* getFluid();
    realFluid(const realFluid& orig);
    virtual ~realFluid();
    
    bool is_initialising;
    
private:
    std::string backendName;
    std::vector<std::string> fluidNamev;
    std::vector<double> fractions;
};

#endif	/* REALFLUID_H */

