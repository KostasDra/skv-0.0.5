/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm, Balint Balassa
*/

#include "einlesen.h"


einlesen::einlesen()
{
	datenObjekt = daten::getInstance();
	knoten = &datenObjekt->knoten;
	skalierungsFaktor 	= 1.0;

}





void einlesen::einlesenDaten ()
{
	//! Ablauf:
	// Initialisierung der benötigten Variablen
	druck druckObjekt;
	temperatur tempObjekt;
	// Einlesen aus Dateien und Schreiben in Daten
	char name[25], name1[25];
	char einerChar, zehnerChar;
	double temporaer = 0.0;
	int temporaer_int = 0;
	bool interpSL = datenObjekt->interpSL;
	int numberSL = datenObjekt->numberSL;

	//! - Falls kein Eingabeformat vorgegeben ist, wird der aktuelle Ordner nach möglichen Eingabedateien durchsucht.

	//! - Falls CGNS und Textdateien vorhanden sind, wird CGNS bevorzugt.
	if (datenObjekt->inputFormat==0)
	{
		// Check ob eingabe datei in textformat
		fstream fs;
		string datei;
		datei = datenObjekt->getworkdir() + datenObjekt->globalVarDatei;
		// fs.open((datenObjekt->globalVarDatei).c_str(),ios::in);
		fs.open((datei).c_str(),ios::in);
		if( fs.is_open() )
			{
			if ( datenObjekt->debug >-1 )
			{
				cout << "Text Eingabedatei (Globale Variablen) existiert" << endl;
			}
			datenObjekt->inputFormat = 2;
			}
		fs.close();

		// Check ob eingabe datei in cgns format
		datei = datenObjekt->getworkdir() + datenObjekt->eingabeDatei;
		fs.open((datei).c_str(),ios::in);
		if( fs.is_open() )
			{
			if ( datenObjekt->debug >-1 )
			{
				cout << "CGNS Eingabedatei existiert" << endl;
			}
			datenObjekt->inputFormat = 1;
			}
		fs.close();
	}

	if ( datenObjekt->inputFormat == 0 )
	{
		if ( datenObjekt->debug >-1 )
		{
			cout << "Fehler beim Erkennen des Einleseformats!" << endl;
		}
		exit ( 0 );
	}
	//! - Falls CGNS das Eingabeformat ist, wird die CGNS Lesefunktion CGNSformat.readCGNS() aufgerufen.
	else if ( datenObjekt->inputFormat == 1 )
	{
		if ( datenObjekt->debug >-1 )
		{
			cout << endl << "Reading CGNS..." << endl << endl;
		}
		data.readCGNS (datenObjekt->getworkdir());
	}
	//! - Falls Text das Eingabeformat ist, werden die Textdateien eingelesen:
	//! <ol>
	else if ( datenObjekt->inputFormat == 2 )
	{
		if ( datenObjekt->debug >-1 )
		{
			cout << endl << "Reading text..." << endl << endl;
		}
		ifstream in ( datenObjekt->getworkdir().append(datenObjekt->globalVarDatei).c_str() );
		in >> name >> name1;
		//! <li> Einlesen vom Massenstrom.
		in >> name >> temporaer;
		datenObjekt->massenStromVorgabe = temporaer;
		cout << endl;
		cout << name << "\t" << temporaer << endl;

		//! <li> Einlesen der Anzahl an Stromlinien.
		in >> name >> name1 >> temporaer_int;
		if ( temporaer_int < 4 )
		{
			if ( datenObjekt->debug >-1 )
			{
				cout << "Anzahl Stromlinien muss mindestens 4 sein! ABBRUCH..." << endl;
			}
			exit ( 0 );
		}
		
		if ( interpSL ) datenObjekt->anzahlStromlinien = temporaer_int+ ( temporaer_int-1 ) *numberSL;
		else datenObjekt->anzahlStromlinien = temporaer_int;

		cout << name << "\t" << name1 << "\t" << temporaer_int << endl;

		//! <li> Einlesen der Anzahl an Gittern.
		in >> name >> name1 >> temporaer_int;
		datenObjekt->anzahlGitter = temporaer_int;

		cout << name << "\t" << name1 << "\t" << temporaer_int << endl;
		cout << endl;

		//! <li> Initialisierung von Anzahl der QuasiOrthogonalen.
		datenObjekt->initialisiere_anzahlQuasiOrthogonale();

		in >> name >> name1;
		cout << name << "\t" << name1 << endl;

		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			in >> temporaer_int;
			if ( temporaer_int < 4 )
			{
				if ( datenObjekt->debug >-1 )
				{
					cout << "Anzahl QuasiOrthogonale muss mindestens 4 sein! ABBRUCH..." << endl;
				}
				exit ( 0 );
			}

			datenObjekt->anzahlQuasiOrthogonale[k] = temporaer_int;

			cout << temporaer_int << endl;
		}
		cout << endl;

		//! <li> Initialisierung diverser Arrays.
		datenObjekt->initialisiere();




	//! <li> Einlesen Massenstromverteilung (falls angegeben).
	in >> name >> name1;

	//! <li> Falls Massenstromfaktor nicht gegeben, wird er ausgerechnet.
	if ( name[0] == FORMAT_MASSENSTROM )
	{
		cout << name << "\t" << name1 << endl;

		for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
		{
			in >> temporaer;
			datenObjekt->faktorMassenStromRoehre[i] = temporaer;
			cout << temporaer << endl;
		}

		cout << endl;
		in >> name >> name1;
	}
	else
	{
		if ( datenObjekt->debug >-1 )
		{
			cout << "Anzahl Stromlinien: " << datenObjekt->anzahlStromlinien << endl;
		}

		temporaer = 1.0/ ( datenObjekt->anzahlStromlinien-1 );

		for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
		{
			datenObjekt->faktorMassenStromRoehre[i] = temporaer;
			if ( datenObjekt->debug >-1 )
			{
				cout << "Setze faktorMassenStrom[" << i << "] = " << temporaer << endl;
			}
		}
		cout << endl;
	}


	//! <li> Einlesen von \f$ p_{tot,abs} \f$.
	cout << name << "\t" << name1 << endl;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		in >> temporaer;
		if ( interpSL && i>0 )
		{
			// interpoliert zusätzliche stromlinien
			double dp=0.0;
			dp= ( temporaer-(*knoten)[0][i-1][0].p_tot_abs ) / ( numberSL+1 );
			for ( int ns=0;ns<numberSL;ns++ )
			{
				(*knoten)[0][i+ns][0].p_tot_abs = (*knoten)[0][i-1][0].p_tot_abs + ( 1+ns ) *dp;
			}
			(*knoten)[0][i+numberSL][0].p_tot_abs = temporaer;
			i+=numberSL;
		}
		else
		{
			(*knoten)[0][i][0].p_tot_abs = temporaer;
		}
		cout << temporaer << endl;
	}
	cout << endl;

	//! <li> Einlesen von \f$ T_{tot,abs} \f$
	in >> name >> name1;
	cout << name << "\t" << name1 << endl;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		in >> temporaer;
		if ( interpSL && i>0 )
		{
			// interpoliert zusätzliche stromlinien
			double dT=0.0;
			dT= ( temporaer-(*knoten)[0][i-1][0].T_tot_abs ) / ( numberSL+1 );
			for ( int ns=0;ns<numberSL;ns++ )
			{
				(*knoten)[0][i+ns][0].T_tot_abs = (*knoten)[0][i-1][0].T_tot_abs + ( 1+ns ) *dT;
			}
			(*knoten)[0][i+numberSL][0].T_tot_abs = temporaer;
			i+=numberSL;
		}
		else
		{
			(*knoten)[0][i][0].T_tot_abs = temporaer;
		}
		cout << temporaer << endl;
	}
	cout << endl;

	//! <li> Einlesen von \f$ c_u \f$.
	in >> name;
	if ( name[0] == FORMAT_CU )
	{
		cout << name << endl;

		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			in >> temporaer;
			(*knoten)[0][i][0].c_u = temporaer;
			cout << temporaer << endl;
		}
	}
	cout << endl;
	in.close();	
	}// end else if datenObjekt->inputFormat == 2

	if ( datenObjekt->inputFormat == 2 )
	{
		for(int k = 0; k < datenObjekt->anzahlGitter; k++)
			//! <li> lesenGitter() für alle Gitter.
			lesenGitter(k);
	}	//! </ol>


	//! WARNING Wird der Abschnitt für Denton noch benötigt?!
	// Folgender Abschnitt schreibt vom Denton Löser benötigte Werte in das datenObjekt.
	if ( datenObjekt->inputFormat != 0 && datenObjekt->loeserAuswahl == LSR_denton)
	{
		int Mitte = 0;
		Mitte = datenObjekt->anzahlStromlinien / 2; //Startwert in Kanalmitte
		for(int k = 0; k < datenObjekt->anzahlGitter; k++)
		{
			for (int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++)
			{
				if ((*knoten)[k][Mitte][j].sigma == 1.0)
				{
					datenObjekt->quasiorthogonale[k][j].typ = QOT_Kanal;
				}
				else
				{
					if (datenObjekt->omega[k] == 0)
					{
						datenObjekt->quasiorthogonale[k][j].typ = QOT_Stator;
					}
					else
					{
						datenObjekt->quasiorthogonale[k][j].typ = QOT_Rotor;
					}
				}
				
			}
		}
	}

/*
	//BB: debug func zum Vergleich der eingelesenen Dateien
	int gj = 0;
	int sumOfQO = 0;
	for ( int k=0; k<datenObjekt->anzahlGitter; k++ )
	{
		for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
		{
			gj = sumOfQO;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				cout << "Gitter: " << k << "\tStromlinie: "<< i << "\tQO: " << j << "\tgj: " << gj <<  "\tz,r: " << (*knoten)[k][i][j].z << " " << (*knoten)[k][i][j].r << endl;
				gj++;
			}
		}
		sumOfQO += datenObjekt->anzahlQuasiOrthogonale[k];
	}
*/


// o.b. sollte nicht mehr benötigt werden, bzw. muss umgeschrieben werden damit es funktioniert
// 	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
// 	{
// 		tempObjekt.umrechnen ();
// 		druckObjekt.umrechnen ();
// 	}

	if ( datenObjekt->debug >-1 )
	{
		cout << "Das Einlesen der Eingabedateien war erfolgreich! " << endl;
	}



	if ( datenObjekt->debug >1 )
	{
		char ch;
			cout << "Programm abbrechen? 'q' für Quit, 'c' für Continue: ";
		cin >> ch;
		if ( ch == 'q' )
			{exit ( 0 );}
	}
}





void einlesen::lesenGitter (int k )
{
	//! Ablauf:
	//! - Initialisierung der benötigten Variablen.
	char name[25],name1[25],name2[25],name3[25],name4[25],name5[25];
	double temporaer01,temporaer02,temporaer03,temporaer04,temporaer05,temporaer06;

	//test
	bool interpSL = datenObjekt->interpSL;
	int numberSL = datenObjekt->numberSL;


	char dateiname[20];
	sprintf(dateiname, datenObjekt->gitterDatei.c_str(), k);


	ifstream in ( datenObjekt->getworkdir().append(dateiname).c_str() );



	int Mitte = 0;
	if (datenObjekt->startit)
	{
		Mitte = datenObjekt->anzahlStromlinien / 2; //Startwert in Kanalmitte
	}

	in >> name >> temporaer01;
	datenObjekt->omega[k] = temporaer01;
	cout << name << "\t" << temporaer01 << endl;
	cout << endl;

	in >> name >> name1;

	char ch;
	ch = 's';

	//! - Falls Startwerte für den Druck an den Quasiorthogonalen gegeben sind, werden diese eingelesen.
	if ( name[0] == ch )
	{
		cout << name << "\t" << name1 << endl;

		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			in >> temporaer01;
			(*knoten)[k][Mitte][j].p = temporaer01;
			cout << temporaer01 << endl;
		}
		cout << endl;
		in >> name >> name1;
	}
	//! - Anderfalls wird der Startwert auf 0 gesetzt.
	else
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			(*knoten)[k][Mitte][j].p = 0;
			if ( datenObjekt->debug >-1 )
			{
			cout << "Setze p[0][" << j << "][" << k << "] = 0 "<< endl;
			}
		}
	}
	char c_r = 'r';
	char c_z = 'z';
	char c_p = 'p';
	char c_x = 'x';
	char c_y = 'y';
	// urspruengliche art des dateninputs mit r, z, beta_ru, beta_zu, sigma, entropie
	//! - Es wird unterschieden zwischen der Eingabemöglichkeit:
	//! \f$ r,z,\beta_{ru},\beta_{zu},\sigma \f$ und Entropie,
	if ( name[0]==c_r && name1[0]==c_z )
	{
		in >> name2 >> name3 >> name4 >> name5;
		cout << name << "\t\t" << name1 << "\t\t" << name2 << "\t\t" << name3 << "\t\t" << name4 << "\t\t" << name5 << endl;
		// char nnbn; cin >> nnbn;
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
			{
				in >> temporaer01 >> temporaer02 >> temporaer03 >> temporaer04 >> temporaer05 >> temporaer06;

				// Prüfe sigma auf Gültigkeit (fm)
				if ( temporaer05 <= 0 )
				{
					temporaer05 = 1.0;
					if ( datenObjekt->debug >-1 )
					{
						cout << "### ACHTUNG!! Wert für sigma ist UNGÜLTIG! Setze auf Standardwert sigma = 1 (schaufelfreier Raum) ###" << endl;
					}
				}

				if ( interpSL && i>0 )
				{
					//interpoliert zusaetzliche stromlinien
					double dr=0.0;
					double dz=0.0;
					double dru=0.0;
					double dzu=0.0;
					double dsig=0.0;
					double dent=0.0;
					dr= ( temporaer01-(*knoten)[k][i-1][j].r ) / ( numberSL+1 );
					dz= ( temporaer02-(*knoten)[k][i-1][j].z ) / ( numberSL+1 );
					dru= ( temporaer03-(*knoten)[k][i-1][j].beta_ru ) / ( numberSL+1 );
					dzu= ( temporaer04-(*knoten)[k][i-1][j].beta_zu ) / ( numberSL+1 );
					dsig= ( temporaer05-(*knoten)[k][i-1][j].sigma ) / ( numberSL+1 );
					dent= ( temporaer06-(*knoten)[k][i-1][j].entropie ) / ( numberSL+1 );
					for ( int ns=0;ns<numberSL;ns++ )
					{
						(*knoten)[k][i+ns][j].r = (*knoten)[k][i-1][j].r + ( 1+ns ) *dr;
						(*knoten)[k][i+ns][j].z = (*knoten)[k][i-1][j].z + ( 1+ns ) *dz;
						(*knoten)[k][i+ns][j].beta_ru = (*knoten)[k][i-1][j].beta_ru + ( 1+ns ) *dru;
						(*knoten)[k][i+ns][j].beta_zu = (*knoten)[k][i-1][j].beta_zu + ( 1+ns ) *dzu;
						(*knoten)[k][i+ns][j].sigma = (*knoten)[k][i-1][j].sigma + ( 1+ns ) *dsig;
						(*knoten)[k][i+ns][j].entropie = (*knoten)[k][i-1][j].entropie + ( 1+ns ) *dent;
					}
					(*knoten)[k][i+numberSL][j].r = temporaer01;
					(*knoten)[k][i+numberSL][j].z = temporaer02;
					(*knoten)[k][i+numberSL][j].beta_ru = temporaer03;
					(*knoten)[k][i+numberSL][j].beta_zu = temporaer04;
					(*knoten)[k][i+numberSL][j].sigma = temporaer05;
					(*knoten)[k][i+numberSL][j].entropie = temporaer06;
					i+=numberSL;
				}
				else
				{
					//daten nur einlesen
					(*knoten)[k][i][j].r = temporaer01;
					(*knoten)[k][i][j].z = temporaer02;
					(*knoten)[k][i][j].beta_ru = temporaer03;
					(*knoten)[k][i][j].beta_zu = temporaer04;
					(*knoten)[k][i][j].sigma = temporaer05;
					(*knoten)[k][i][j].entropie = temporaer06;
				}


				// formatierte Ausgabe der Eingabewerte
				cout.setf ( ios::showpoint|ios::fixed );
				cout << temporaer01 << "\t\t" << temporaer02 << "\t\t" << temporaer03 << "\t" << temporaer04 << "\t" << temporaer05 << "\t" << temporaer06 << endl;
			}
			cout << endl;
		}
	}//urspruenglicher input_ende
	//dateninput mit x,y,z,sigma,entropie oder r,p,z,sigma,entropie
	//! - und der Eingabemöglichkeit:
	//! \f$ r,p,z,\sigma \f$ und Entropie.
	else if ( ( name[0]==c_x && name1[0]==c_y ) || ( name[0]==c_r && name1[0]==c_p ) )
	{
		using namespace PLib ;
		// TODO: Nur Punkte im beschaufeltem Raum zur Erzeugung der Skelettflächen benutzen
		Matrix_Point3Dd Punkte_Skelett ( datenObjekt->anzahlStromlinien,datenObjekt->anzahlQuasiOrthogonale[k] ) ;

		in >> name2 >> name4 >> name5;

		//double RadiuZ, ZPos, PhiP, uTemp, vTemp ;
		double* paraTemp ;
		double* betas ;

		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
			{
				in >> temporaer01 >> temporaer02 >> temporaer03 >> temporaer05 >> temporaer06;

				// Prüfe sigma auf Gültigkeit
				if ( temporaer05 <= 0 )
				{
					temporaer05 = 1.0;
					if ( datenObjekt->debug >-1 )
					{
						cout << "### ACHTUNG!! Wert für sigma ist UNGÜLTIG! Setze auf Standardwert sigma = 1 (schaufelfreier Raum) ###" << endl;
					}
				}
				else
				{
					(*knoten)[k][i][j].sigma = temporaer05;
				}

				if ( name1[0]==c_p )
				{
					// Erzeugen der Punktobjekte zum Berechnen der NURBS-Fläche
					Punkte_Skelett ( i,j ) = Point3Dd ( convert_rphi2x ( temporaer01,temporaer02 ),convert_rphi2y ( temporaer01,temporaer02 ),temporaer03 ) ;

					(*knoten)[k][i][j].r = temporaer01;
				}
				else
				{
					// Erzeugen der Punktobjekte zum Berechnen der NURBS-Fläche
					Punkte_Skelett ( i,j ) = Point3Dd ( temporaer01,temporaer02,temporaer03 ) ;

					(*knoten)[k][i][j].r = pow ( pow2 ( temporaer01 ) +pow2( temporaer02 ),0.5 );
				}

				(*knoten)[k][i][j].z = temporaer03;


				(*knoten)[k][i][j].entropie = temporaer06;
			}
		}

		// Deklarieren und Initialisieren der NURBS-Fläche
		SkelettFlaeche3D SkelettFlaeche ( Punkte_Skelett,3,datenObjekt->getworkdir() ) ;

		// Ausgabe der Skelettfläche
		SkelettFlaeche.ausgabe2 (datenObjekt->getworkdir(), k ) ;

		if ( datenObjekt->debug >-1 )
		{
			cout << "r \t\t z \t\t beta_ru \t beta_zu \t sigma \t\t entropie" << endl;
		}

		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			if ( datenObjekt->debug >-1 )
			{
				cout << "Schaufelreihe = " << k << " Quasiorthogonale = " << j << endl;
			}
			for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
			{
				// Berechnen der Parameter des Punktes auf der Fläche, bei denen die übergebenen r und z Koordinaten übereinstimmen
				paraTemp = SkelettFlaeche.berechneSchnittpunkt ( (*knoten)[k][i][j].r,(*knoten)[k][i][j].z ) ;

				// Berechnen von beta_ru und beta_zu
				betas = SkelettFlaeche.berechneBetaRu_BetaZu ( paraTemp[0],paraTemp[1] ) ;

				(*knoten)[k][i][j].beta_ru = betas[0];
				(*knoten)[k][i][j].beta_zu = betas[1];

				// formatierte Ausgabe der Eingabewerte
				cout.setf ( ios::showpoint|ios::fixed );
				cout << (*knoten)[k][i][j].r << "\t" << (*knoten)[k][i][j].z << "\t" << betas[0] << "\t" << betas[1] << "\t" << (*knoten)[k][i][j].sigma << "\t" << (*knoten)[k][i][j].entropie << endl;
			}

		}


	}//elseif_ende

	else
	{
		if ( datenObjekt->debug >-1 )
		{
			cout << "Bezeichnung der Eingabedaten ist inkorrekt!!\nZulässig ist die Angabe von:"<<endl;
			cout << "->\tr, z, beta_ru, beta_zu, sigma, entropie\n->\tr, p, z, sigma, entropie\n->\tx, y, z, sigma, entropie" << endl;
		}
		exit ( 0 );
	}
	in.close();
}

void einlesen::einlesenMitType(ifstream& in, int k)
{
	//! WARNING Wird nicht mehr benötigt?!
	double temporaer01, temporaer02, temporaer03, temporaer04, temporaer05, temporaer06;
	string tempType;
	char typeQO;

	// char nnbn; cin >> nnbn;
	for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++)
	{
		typeQO = '\0';


		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			in >> tempType >> temporaer01 >> temporaer02 >> temporaer03 >> temporaer04 >> temporaer05 >> temporaer06;

			(*knoten)[k][i][j].r 		= temporaer01*skalierungsFaktor;
			(*knoten)[k][i][j].z 		= temporaer02*skalierungsFaktor;
			(*knoten)[k][i][j].beta_ru 	= temporaer03;
			(*knoten)[k][i][j].beta_zu 	= temporaer04;
			(*knoten)[k][i][j].sigma 	= temporaer05;
			(*knoten)[k][i][j].entropie = temporaer06;

			if ( i==0 ) {

				typeQO = tempType.c_str()[0];
			}

			else
			{
				if (typeQO != tempType.c_str()[0]) {
					cerr << "Fehler in Einlesedatei " << endl;
					exit(-1);
				}
			}
			// formatierte Ausgabe der Eingabewerte
			cout.setf ( ios::showpoint|ios::fixed );
			cout << tempType  << "\t\t" <<temporaer01*skalierungsFaktor << "\t\t" << temporaer02*skalierungsFaktor << "\t\t" << temporaer03 << "\t" << temporaer04 << "\t" << temporaer05 << "\t" << temporaer06 << endl;
		}


		switch (toupper(typeQO))
		{
		case 'K':
			datenObjekt->quasiorthogonale[k][j].typ = QOT_Kanal;
			break;

		case 'R':
			datenObjekt->quasiorthogonale[k][j].typ = QOT_Rotor;
			break;

		case 'S':
			datenObjekt->quasiorthogonale[k][j].typ = QOT_Stator;
			break;

		default:
			cerr << "fataler Fehler beim Einlesen: keine/falsche Typendefinition: " << typeQO << endl;
			exit(-1);
		}


	}
}

