/***************************************************************************
 *    Copyright (C) 2009-2010 by Andrea Bader (bader.andrea@gmx.de)        *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/



#ifndef ENTHALPIE_H
	#define ENTHALPIE_H

#include "daten.h"

//! Enthalpieklasse
/*!
In der Enthalpieklasse werden die benötigten Enthalpiewerte berechnet.
\author Andrea Bader
*/
class enthalpie
{
	private:
		daten*					datenObjekt;
		v3Knoten*				knoten;
		v2Quasiorthogonale*		quasiorthogonale;

	public:
		enthalpie();



		//! Berechnung der Totalenthalpie auf der ersten QO aus den Eintrittsbedingungen.
		/*!
			Berechnet die Totalenthalpie auf einer Stromlinie.
			\param i aktuelle Stromlinie

			\author Andrea Bader
		*/
		double berechne_totalenthalpie_0 ( int i );


		//! Berechnung der Totalenthalpie
		/*!
			Berechnet die Totalenthalpie in einem Knoten in Abhängigkeit
			des QO-Typs (Eintritt, Rotor, Kanal)

			\param i aktuelle Stromlinie
			\param j aktuelle Quasiorthogonale
			\param k aktuelles Gitter

			\author Andrea Bader
		*/
		double berechne_totalenthalpie ( int i, int j, int k );


		//! Berechnung der Enthalpie
		/*!
			Berechnet die Enthalpie in einem Knoten aus der Totalenthalpie
			und der Absolutgeschwindigkeit.

			\param i aktuelle Stromlinie
			\param j aktuelle Quasiorthogonale
			\param k aktuelles Gitter

			\author Andrea Bader
		*/
		double berechne_enthalpie ( int i, int j, int k );


		//! Setzen der Totalenthalpie und der Enthalpie
		/*!
			Aufruf der Berechnungsmethoden für Totalenthalpie, Enthalpie
			und abspeichern in Knoteneigenschaften.

			\param i aktuelle Stromlinie
			\param j aktuelle Quasiorthogonale
			\param k aktuelles Gitter

			\author Andrea Bader
		*/
		void setze_enthalpien (int k, int j, int i );

};


#endif
