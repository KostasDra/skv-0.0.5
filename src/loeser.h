/***************************************************************************
 *   Copyright (C) 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Andrea Bader
*/

#ifndef LOESER_H_
#define LOESER_H_

//! Löser-Klasse
/*!
	Diese Klasse ist notwendig um das Erzeugen der verschiedenen LöserObjekte
	zu ermöglichen. Die Schnittstellen zu skv.cpp sind hier definiert.

	\author Andrea Bader
*/

class loeser
{

public:
	virtual void schleife ( ) = 0;
	virtual void integrale_Daten ( ) = 0 ;
	virtual void zuruecksetzen ( ) = 0;
};


#endif
