/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/



#ifndef EPSILON_H
	#define EPSILON_H

#include <cmath>
#include <nurbs++/nurbs.h>

#include "ableitung.h"
#include "daten.h"


//! Epsilonklasse
/*!
In der Epsilonklasse werden die benötigten Winkel berechnet.

    \author Oliver Borm
*/
class epsilon
{
	private:
		ableitung 		ableitungObjekt;
		daten 			*datenObjekt;
		v3Knoten		*knoten;
		v2Quasiorthogonale	*quasiorthogonale;
		v2StromlinieGitter	*stromlinieGitter;
		vStromlinie		*stromlinie;

	public:
		epsilon();

		//! Bestimmung des Winkels \f$ \varepsilon \f$
		/*!
		    Berechnet den Winkel \f$ \varepsilon \f$ in [rad] entlang einer Stromlinie
		    mit Hilfe der NURBS-Ableitung und der cmath-Methode 'atan2'.
		    \author Florian Mayer
		*/
		void setzeEpsilon ( );

		
		//! Bestimmung einer NURBS-Kurve mit den Werten des Winkels \f$ \varepsilon \f$
		/*!
		    Erzeugt eine NURBS-Kurve, die zusätzlich die Werte des Winkels
		    \f$ \varepsilon \f$ entlang einer Stromlinie beeinhaltet.
		    \author Florian Mayer
		*/
		void aktualisiere_nurbsEpsilon ( );

		
		//! Bestimmung einer NURBS-Kurve mit den Werten des Winkels \f$ \varepsilon \f$
		/*!
		    Erzeugt eine NURBS-Kurve, die zusätzlich die Werte des Winkels
		    \f$ \varepsilon \f$ entlang einer Quasiorthogonalen beeinhaltet.
		    \author Florian Mayer
		*/
		void aktualisiere_nurbsEpsilonQO ( );

		
		//! Bestimmung des Winkels \f$ \gamma \f$
		/*!
		    Berechnet den Winkel \f$ \gamma \f$ entlang einer Quasiorthogonale
		    mit Hilfe der NURBS-Ableitung und der cmath-Methode 'atan2'.   
		    \author Florian Mayer
		*/
		void setzeGamma ( );

		
		//! Berechnung des Kehrwertes des Krümmungsradius 1/R
		/*! Berechnung des Kehrwertes des Krümmungsradius 1/R mit Hilfe des ableitung Objekts.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return ableitung.ersteAbleitung_epsilon()
		    \author Oliver Borm
		*/
		inline double berechne_kruemmungsRadius ( int i, int j, int k );

		
		//! Berechnung des Winkels \f$ \beta \f$.
		/*! 
		    \author Oliver Borm
		*/
		void setzeBeta ( );

// 		void initialisiereBeta ();


		//! Berechnet und setzt \f$ \cot \beta \f$.
		/*! \author Oliver Borm
		*/
		void setze_cotBeta ( );


		//! Berechnet und setzt \f$ \cos \varepsilon \f$.
		/*! 
		    \author Oliver Borm
		*/
		void setze_cosEpsilon ( );

		
		//! Berechnet und setzt \f$ \sin \varepsilon \f$.
		/*! 
		    \author Oliver Borm
		*/
		void setze_sinEpsilon ( );

//		//! Berechnung des Winkels Alpha
//		/*!
// 		    Berechnet den Wert des statischen Drucks bei Mach=1.
// 
// 		    \param i aktuelle Stromlinie
// 		    \param j aktuelle Quasiorthogonale
// 		    \param k aktuelles Gitter
// 
// 		    \author Florian Mayer
// 		*/
// 		void berechne_alpha ( int i, int j, int k );

//		//! TODO Funktion
//		/*! TODO Beschreibung
// 		    \param i aktuelle Stromlinie
// 		    \param j aktuelle Quasiorthogonale
// 		    \param k aktuelles Gitter
// 
// 		    \author Florian Mayer
// 		*/
// 		inline void setze_cot_alpha ( int i, int j, int k );


//+++++++++++++++ Methoden von Andrea Bader ++++++++++++++++++++++++++++++++++
// übrigen Methoden nicht von loeser_denton verwendet

		//! Setzen der Strömungswinkel
		/*!
			Zur Ausgabe werden die Strömungswinkel, Epsilon und Gamma gesetzt.

			\author Andrea Bader
		*/
		void setze_Stroemungswinkel( );

//+++++++++++++++ Methoden von Andrea Bader Ende +++++++++++++++++++++++++++++


};

//SW: Auslagerung der inline-Funktionen
inline double epsilon::berechne_kruemmungsRadius ( int i, int j, int k )
{
	return ableitungObjekt.ersteAbleitung_epsilon ( i,j,k );
}

// inline void epsilon::setze_cot_alpha ( int i, int j, int k )
// {
// 	(*knoten)[k][i][j].cot_alpha = cot ( (*knoten)[k][i][j].alpha );
// }

#endif
