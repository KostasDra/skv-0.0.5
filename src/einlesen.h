/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm, Balint Balassa
*/

#ifndef EINLESEN_H
	#define EINLESEN_H



#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>



#include "daten.h"
#include "skelettflaeche3d.h"
#include "druck.h"
#include "temperatur.h"
#include "CGNSformat.h"



//Defines für Dateien einlesen
#define FORMAT_CGNS 'C'
#define FORMAT_TEXT 'T'
#define FORMAT_MASSENSTROM 'A'
#define FORMAT_CU 'C'



using namespace std;


//! Einleseklasse
/*!
In der Einleseklasse werden die benötigten Eingabewerte eingelesen.
\author Oliver Borm
*/
class einlesen
{
	private:
		CGNSformat 	data;
		daten*		datenObjekt;
		v3Knoten*	knoten;
		double 		skalierungsFaktor ;


	public:


		//! Parameterkonstruktor.
		/*!
		    Konstruktor den man das Arbeitsverzeichnis mitgeben kann
		    \param wordirInput Verzeichnis der Quelldateien
		    \author Oliver Borm, Florian Mayer
		*/
		einlesen();

		
		//! Gibt \f$ y \f$-Werte aus für \f$ (r,\varphi) \f$ Koordinaten.
		/*!
		    \param radius Radius des Punktes.
		    \param phi Winkel \f$ \varphi \f$ des Punktes.
		    \return Gibt die \f$ y \f$-Koordinate zurück.
		    \author Oliver Borm, Florian Mayer
		*/
		inline double convert_rphi2y ( double radius, double phi );

		
		//! Gibt \f$ x \f$-Werte aus für \f$ (r,\varphi) \f$ Koordinaten.
		/*!
		    \param radius Radius des Punktes.
		    \param phi Winkel \f$ \varphi \f$ des Punktes.
		    \return Gibt die \f$ x \f$-Koordinate zurück.
		    \author Oliver Borm, Florian Mayer
		*/
		inline double convert_rphi2x ( double radius, double phi );

		
		//! Einlesen der Daten.
		/*!
		    Diese Funktion liest die für die Berechnung benötigten Daten
		    aus den entsprechenden Dateien ein.
		    \author Oliver Borm, Florian Mayer, Balint Balassa
		*/
		void einlesenDaten ( );

		
		//! Einlesen der Gitterparameter.
		/*!
		    Diese Funktion liest die Werte der einzelnen Gitter ein.
		    \param Nummer Dateinummer.
		    \warning Diese Funktion wird automatisch von einlesenDaten() aufgerufen! Eine explizite Anwendung wird nicht empfohlen.
		    \author Oliver Borm
		*/
		void lesenGitter ( int k );

		//! Einlesen des Gitters mit Typ
		/*!
		    Denton-Löser erfordert die Angabe des Typs der Quasiorthogonalen (Rotor, Kanal, Stator).
		    Die Gitter_Variablen Datei für die Anwendung des loeser_denton wird durch diese Methode
		    eingelesen.

		    \warning Diese Funktion wird automatisch von einlesenDaten() aufgerufen! Eine explizite Anwendung wird nicht empfohlen.

		    \author Andrea Bader
		*/
		void einlesenMitType(ifstream& in, int k);
};




inline double einlesen::convert_rphi2y ( double radius, double phi )
{
	return radius*sin ( phi );
}





inline double einlesen::convert_rphi2x ( double radius, double phi )
{
	return radius*cos ( phi );
}

#endif
