/***************************************************************************
 *    Copyright (C) 2009-2010 by Andrea Bader (bader.andrea@gmx.de)        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Andrea Bader
*/



#include "enthalpie.h"



enthalpie::enthalpie()
{
	datenObjekt = daten::getInstance();
	quasiorthogonale = &datenObjekt->quasiorthogonale;
	knoten = &datenObjekt->knoten;
}


double enthalpie::berechne_totalenthalpie_0 ( int i )
{
	//! Nur auf der ersten QO:
	//! \f[ h_{0,tot} = c_p \cdot T_{tot,abs} \f]
	return( (*knoten)[0][i][0].cp * (*knoten)[0][i][0].T_tot_abs );
}


double enthalpie::berechne_totalenthalpie ( int i, int j, int k )
{
	double h_t;

	//! - Falls Eintritt:
	if ( (*quasiorthogonale)[k][j].eintritt )
	{
		//! \f$ h_t = \f$ enthalpie.berechne_totalenthalpie_0()
		h_t = berechne_totalenthalpie_0(i);
	}
	//! - Falls erste QO eines nachfolgenden Gitters:
	else if (j == 0 && k > 0) //Gitterübertrag -> Überträgt Totalenthalpie der letzten QO im vorherigen Gitter
	{
		//! \f[ h_t(0,k) = h_t(j_{max},k-1)\f]
		h_t = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].totalenthalpie;
	}
	//! - Falls QO auf Rotor und nicht Vorderkante:
	else if ((*quasiorthogonale)[k][j].rotor() && !(*quasiorthogonale)[k][j].vorderkante )
	{
		//! Im Rotor wird Totalenthalpieänderung nach Euler berechnet
		//! \f[ h_t(j) = h_t(j-1) + \omega(r(j)c_u(j)-r(j-1)c_u(j-1)) \f]
		h_t = (*knoten)[k][i][j-1].totalenthalpie
				+ datenObjekt->omega[k] * ( (*knoten)[k][i][j].rc_u - (*knoten)[k][i][j-1].rc_u );
	}
	else	//! - Falls Rotor VK oder Stator oder Kanal
	{
		//! \f[ h_t(j) = h_t(j-1) \f]
		h_t = (*knoten)[k][i][j-1].totalenthalpie;
	}

	return h_t;
}

double enthalpie::berechne_enthalpie ( int i, int j, int k )
{
	//! \f$ h = h_t - \frac{c^2}{2} \f$
	double enthalpie = (*knoten)[k][i][j].totalenthalpie  - 0.5 * pow2((*knoten)[k][i][j].c);
	return enthalpie;
}


void enthalpie::setze_enthalpien (int k, int j, int i )
{
	//! \f$ h_{tot,abs} = \f$ enthalpie.berechne_totalenthalpie()
	(*knoten)[k][i][j].totalenthalpie = berechne_totalenthalpie ( i,j,k );
	//! \f$ h = \f$ enthalpie.berechne_enthalpie()
	(*knoten)[k][i][j].enthalpie = berechne_enthalpie(i, j, k);

	if (datenObjekt->debug > 2)
	{
		cout << " h_t = " << (*knoten)[k][i][j].totalenthalpie << endl;
		cout << " h   = " << (*knoten)[k][i][j].enthalpie << endl;
	}
}




