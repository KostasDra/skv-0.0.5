/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm	(oli.borm@web.de)	   *
 *     		   2008-2009 by Florian Mayer (ilumine@gmx.de)		   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GEOMETRIE_H
	#define GEOMETRIE_H



#include <iostream>
#include <nurbs++/nurbs.h>



#include "daten.h"



using namespace PLib;

//! Geometrieklasse
/*!
In der Geometrieklasse werden die benötigten Geometriedaten berechnet.
Siehe NURBS++ Dokumentation für detailiertere Informationen über die NURBS Methoden.
\author Oliver Borm, Florian Mayer
*/
class geometrie
{
	private:
		daten*			datenObjekt;
		v3Knoten*		knoten;
		vStromlinie*		stromlinie;
		v2StromlinieGitter* 	stromlinieGitter;
		v2Quasiorthogonale*	quasiorthogonale;

	public:
		geometrie();

		double minDistError;		//!< Fehlerschranke; in der minDist2 Methode.
		double minDistsizeSearch;	//!< the size of the search in the parametric space; in der minDist2 Methode.
		int minDistsep;			//!< the number of points initially looked at to find a minimal distance; in der minDist2 Methode.
		int minDistmaxiter;		//!< the maximal number of iterations; in der minDist2 Methode.
		double minDistuMin;		//!< the minimal parametric value; in der minDist2 Methode.
		double minDistuMax;		//!< the maximal parametric value; in der minDist2 Methode.


		//! Aktualsierung der Geometrie
		/*!
		    Aktualisiert die r,z-Werte anhand der Schnittpunkte von Quasiorthogonalen
		    und der neuen Lage der Stromlinien.
		    \author Oliver Borm
		*/
		void aktualisiereGeometrie ();

		
		//! Bestimmung der Laufvariable \f$ \nu \f$ entlang einer Quasiorthogonale
		/*!
		    Die Laufvariable \f$ \nu \f$ bezeichnet die Position entlang einer
		    Quasiorthogonale, wobei \f$ \nu=0 \f$ die Nabe und \f$ \nu=1 \f$ das
		    Gehäuse bezeichnet.
		    \author Oliver Borm
		*/
		void bekomme_ny ();

		
		//! Bestimmung der Laufvariable \f$ \mu \f$ entlang einer Stromlinie
		/*!
		    Die Laufvariable \f$ \mu \f$ bezeichnet die Position entlang einer
		    Stromlinie, wobei \f$ \mu=0 \f$ den Eintritt und \f$ \mu=1 \f$ den
		    Austritt bezeichnet.
		    \author Florian Mayer
		*/
		void bekomme_my ();

		
		//! Bestimmung der Laufvariable \f$ \mu \f$ entlang einer Stromlinie (2)
		/*!
		    Die Laufvariable \f$ \mu \f$ bezeichnet die Position entlang einer
		    Stromlinie, wobei \f$ \mu=0 \f$ den Eintritt und \f$ \mu=1 \f$ den
		    Austritt bezeichnet.
		    \author Florian Mayer
		*/
		void bekomme_myG ();


		//! Berechnung von ny unter Berücksichtigung des Relaxationsfaktors.
		/*!
		    \author Florian Mayer
		*/
		void bekomme_ny_neu (); //Relaxation


		//! Bestimmung einer Nurbskurve mit den Werten der Schaufelversperrung entlang einer Quasiorthogonalen
		/*!
		    Erzeugt eine Nurbskurve, die zusätzlich die Werte der
		    Schaufelversperrung entlang einer Quasiorthogonalen beeinhaltet.
		    \author Oliver Borm
		*/
		void initialisiereGeometrie_H_sigma ();

		
		//! Aktualisierung einer Nurbskurve mit den Werten der Schaufelversperrung entlang einer Quasiorthogonalen
		/*!
		    Aktualisiert eine Nurbskurve, die zusätzlich die Werte der
		    Schaufelversperrung entlang einer Quasiorthogonalen beeinhaltet.
		    \author Oliver Borm
		*/
		void aktualisiereGeometrie_H_sigma ();

		
		//! Bestimmung einer Nurbskurve mit den Werten von \f$ \beta_{zu} \f$ entlang einer Quasiorthogonalen
		/*!
		    Erzeugt eine Nurbskurve, die zusätzlich die Werte von
		    \f$ \beta_{zu} \f$ entlang einer Quasiorthogonalen beeinhaltet.
		    \author Oliver Borm
		*/
		void initialisiereGeometrie_H_beta_zu ();

		
		//! Aktualisierung einer Nurbskurve mit den Werten von \f$ \beta_{zu} \f$ entlang einer Quasiorthogonalen
		/*!
		    Aktualisiert eine Nurbskurve, die zusätzlich die Werte von
		    \f$ \beta_{zu} \f$ entlang einer Quasiorthogonalen beeinhaltet.
		    \author Oliver Borm
		*/
		void aktualisiereGeometrie_H_beta_zu ();

		
		//! Bestimmung einer Nurbskurve mit den Werten von \f$ \beta_{ru} \f$ entlang einer Quasiorthogonalen
		/*!
		    Erzeugt eine Nurbskurve, die zusätzlich die Werte von
		    \f$ \beta_{ru} \f$ entlang einer Quasiorthogonalen beeinhaltet.
		    \author Oliver Borm
		*/
		void initialisiereGeometrie_H_beta_ru ();

		
		//! Aktualisierung einer Nurbskurve mit den Werten von \f$ \beta_{ru} \f$ entlang einer Quasiorthogonalen
		/*!
		    Aktualisiert eine Nurbskurve, die zusätzlich die Werte von
		    \f$ \beta_{ru} \f$ entlang einer Quasiorthogonalen beeinhaltet.
		    \author Oliver Borm
		*/
		void aktualisiereGeometrie_H_beta_ru ();

		
		//! Bestimmung einer Nurbskurve mit den Werten der Entropie entlang einer Quasiorthogonalen
		/*!
		    Erzeugt eine Nurbskurve, die zusätzlich die Werte der
		    Entropie entlang einer Quasiorthogonalen beeinhaltet.
		    \author Oliver Borm
		*/
		void initialisiereGeometrie_H_entropie ();

		
		//! Aktualisierung einer Nurbskurve mit den Werten der Entropie entlang einer Quasiorthogonalen
		/*!
		    Aktualisiert eine Nurbskurve, die zusätzlich die Werte der
		    Entropie entlang einer Quasiorthogonalen beeinhaltet.
		    \author Oliver Borm
		*/
		void aktualisiereGeometrie_H_entropie ();

		
		//! Initialisierung der Quasiorthogonalen
		/*!
		    Erzeugt alle Quasiorthogonalen als Nurbskurven.
		    \author Oliver Borm
		*/
		void initialisiereGeometrie_nurbsQO ();

		
		//! Aktualisierung der Quasiorthogonalen
		/*!
		    Aktualisiert alle Quasiorthogonalen als Nurbskurven.
		    \author Florian Mayer
		*/
		void aktualisiereGeometrie_nurbsQO ();

		
		//! Bestimmung einer Nurbskurve mit den Werten von \f$ \sin \epsilon \f$ entlang einer Quasiorthogonalen
		/*!
		    Erzeugt bzw. aktualisiert eine Nurbskurve, die zusaetzlich die Werte von
		    \f$ \sin \epsilon \f$ entlang einer Quasiorthogonalen beeinhaltet.
		    \author Florian Mayer
		*/
		void aktualisiere_nurbsSinEpsilon ();

		
		//! Bestimmung einer Nurbskurve mit den Werten von \f$ \cot \beta \f$ entlang einer Quasiorthogonalen
		/*!
		    Erzeugt bzw. aktualisiert eine Nurbskurve, die zusätzlich die Werte von
		    \f$ \cot \beta \f$ entlang einer Quasiorthogonalen beeinhaltet.
		    \author Florian Mayer
		*/
		void aktualisiere_nurbsCotBetaQO ();

		
		//! Initialisierung der Stromlinien
		/*!
		    Erzeugt alle Stromlinien als Nurbskurven.
		    \author Florian Mayer
		*/
		void initialisiereGeometrie_nurbsSL ();

		
		//! Initialisierung der GitterStromlinien
		/*!
		    Erzeugt alle Stromlinien als Nurbskurven.
		    \author Florian Mayer
		*/
		void initialisiereGeometrie_nurbsGitterSL ();

		
		//! Aktualisierung der Stromlinien (1)
		/*!
		    Alle Stromlinien werden mit den neuen Werten von 'r' und 'z'
		    neu erzeugt. Methode: nurbs++/.globalInterp
		    \author Florian Mayer
		*/
		void aktualisiereGeometrie_nurbsSL ();

		
		//! Aktualisierung der GitterStromlinien (1)
		/*!
		    Alle Stromlinien werden mit den neuen Werten von 'r' und 'z'
		    neu erzeugt. Methode: nurbs++/.globalInterp
		    \author Florian Mayer
		*/
		void aktualisiereGeometrie_nurbsGitterSL ();

		
		//! Aktualisierung der Stromlinien (2)
		/*!
		    Alle Stromlinien werden mit den neuen Werten von 'r' und 'z'
		    neu erzeugt. Methode: nurbs++/.globalApproxErrBnd
		    \author Florian Mayer
		*/
		void aktualisiereGeometrie_approx_nurbsSL ();

		
		//! Aktualisierung der GitterStromlinien (2)
		/*!
		    Alle Stromlinien werden mit den neuen Werten von 'r' und 'z'
		    neu erzeugt. Methode: nurbs++/.globalApproxErrBnd
		    \author Florian Mayer
		*/
		void aktualisiereGeometrie_approx_nurbsGitterSL ();

		
		//! Bestimmung einer Nurbskurve mit den Werten der Entropie entlang einer Stromlinie
		/*!
		    Erzeugt bzw. aktualisiert eine Nurbskurve, die zusätzlich die Werte der
		    Entropie entlang einer Stromlinie beeinhaltet.
		    \author Florian Mayer
		*/
		void aktualisiere_nurbsEntropieSL ();

		
		//! Bestimmung einer Nurbskurve mit den Werten der Schaufelversperrung entlang einer Stromlinie
		/*!
		    Erzeugt bzw. aktualisiert eine Nurbskurve, die zusätzlich die Werte der
		    Schaufelversperrung entlang einer Stromlinie beeinhaltet.
		    \author Florian Mayer
		*/
		void aktualisiere_nurbsSigmaSL ();


		//! Bestimmung einer Nurbskurve mit den Werten von \f$ \cos \epsilon \f$ entlang einer Stromlinie
		/*!
		    Erzeugt bzw. aktualisiert eine Nurbskurve, die zusätzlich die Werte von
		    \f$ \cos \epsilon \f$ entlang einer Stromlinie beeinhaltet.
		    \author Florian Mayer
		*/
		void aktualisiere_nurbsCosEpsilon ();


		//! Bestimmung einer Nurbskurve mit den Werten von \f$ \cot \beta \f$ entlang einer Stromlinie
		/*!
		    Erzeugt bzw. aktualisiert eine Nurbskurve, die zusätzlich die Werte von
		    \f$ \cot \beta \f$ entlang einer Stromlinie beeinhaltet.
		    \author Florian Mayer
		*/
		void aktualisiere_nurbsCotBetaSL ();
};

#endif
