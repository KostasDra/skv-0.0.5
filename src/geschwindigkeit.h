/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/



#ifndef GESCHWINDIGKEIT_H
	#define GESCHWINDIGKEIT_H



#include<cmath>



#include "daten.h"
#include "realFluid.h"

//! Geschwindigkeitklasse
/*!
In der Geschwindigkeitklasse werden die benötigten Geschwindigkeiten berechnet.
\author Oliver Borm
*/
class geschwindigkeit
{
	private:
		daten*		datenObjekt;
		v3Knoten*	knoten;
		v2StromlinieGitter	*stromlinieGitter;

	public:
		geschwindigkeit();

		
//		//! Berechnung der Meridiangeschwindigkeit aus der Rothalpie
//		/* !
// 		    Berechnet den Wert der Meridiangeschwindigkeit aus der Rothalpie.
// 		    \param i aktuelle Stromlinie
// 		    \param j aktuelle Quasiorthogonale
// 		    \param k aktuelles Gitter
// 		    \author Oliver Borm
// 		*/
// 		double berechne_w_m ( int i, int j, int k );


		//! Berechnung der Meridiangeschwindigkeit aus der relativen Totaltemperatur
		/*!
		    Berechnet den Wert der Meridiangeschwindigkeit aus der relativen Totaltemperatur.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ w_{m0} \f$
		    \author Oliver Borm
		*/
		double berechne_w_m_0 ( int i, int j, int k );

		
		//! Berechnung der Absolutgeschwindigkeit
		/*!
		    Berechnet den Wert der Absolutgeschwindigkeit.
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return c
		    \author Florian Mayer
		*/
		inline double berechne_c ( int i, int j, int k );

		
//		//! TODO Funktion
//		/* !
// 		    TODO Beschreibung
// 		    \param i aktuelle Stromlinie
// 		    \param j aktuelle Quasiorthogonale
// 		    \param k aktuelles Gitter
// 		    \author Florian Mayer
// 		*/
// 		inline double berechne_w ( int i, int j, int k );


// 		// ! Berechnung der Relativgeschwindigkeit \f$ w_0 \f$
// 		/* !
// 		    Berechnet den Wert der Relativgeschwindigkeit \f$ w_0 \f$.
// 		    \param i aktuelle Stromlinie
// 		    \param j aktuelle Quasiorthogonale
// 		    \param k aktuelles Gitter
// 		    \return \f$ w_0 \f$
// 		    \author Florian Mayer
// 		*/
// 		inline double berechne_w_0 ( int i, int j, int k );

		
		//! Berechnet und setzt die Relativgeschwindigkeit w.
		/*!
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Florian Mayer
		*/
		inline void setze_w ( int i, int j, int k );

		
		//! Berechnet und setzt die relative Meridionalgeschwindigkeit \f$ w_m \f$.
		/*!
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Florian Mayer
		*/
		void setze_w_m ( int i, int j, int k );


		//! Berechnet und setzt die relative Umfangsgeschwindigkeit \f$ w_u \f$.
		/*!
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Florian Mayer
		*/
		void setze_w_u ( int i, int j, int k );

		
		//! Berechnung der Schallgeschwindigkeit
		/*!
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ a \f$
		    \author Oliver Borm
		*/
		inline double berechne_schallGeschwindigkeit ( int i, int j, int k );


// 		// ! Berechnung das Mittel der Machzahl
// 		/* !
// 		    \param j aktuelle Quasiorthogonale
// 		    \param k aktuelles Gitter
// 		    \author Oliver Borm
// 		*/
// 		double machzahl_gemittelt ( int j, int k );
};

//SW: Ausgelagerte Inlinefunktionen
inline double geschwindigkeit::berechne_c ( int i, int j, int k )
{
	//!	\f[	c = \sqrt{(w_u+\omega r)^2+w_m^2}	\f]
	// Benutzung des Kosinussatzes
// 	return sqrt ( pow2(  datenObjekt->omega[k] *(*knoten)[k][i][j].r ) + pow2(  (*knoten)[k][i][j].w ) + 2.0 * datenObjekt->omega[k] *(*knoten)[k][i][j].r *(*knoten)[k][i][j].w *cos ( (*knoten)[k][i][j].beta ) );

	// c = sqrt( c_m^2 + c_u^2) = sqrt( w_m^2 + (w_u + u )^2)= sqrt( w_m^2 + (w_u + omega*r )^2)
	return sqrt ( pow2(  (*knoten)[k][i][j].w_u + datenObjekt->omega[k] *(*knoten)[k][i][j].r  ) + pow2(  (*knoten)[k][i][j].w_m ) );
}

/*
inline double geschwindigkeit::berechne_w_0 ( int i, int j, int k )
{
// 	return sqrt( max( 2.0*(*knoten)[k][i][j].cp * ( (*knoten)[k][i][j].T_tot_rel-(*knoten)[k][i][j].T ) , datenObjekt->minimalVelocity ) );
	return sqrt( max( 2.0 * ( (*stromlinieGitter)[k][i].h_rot - (*knoten)[k][i][j].cp * (*knoten)[k][i][j].T ) + pow2 ( datenObjekt->omega[k] * (*knoten)[k][i][j].r ) , datenObjekt->minimalVelocity ) );
}
*/

inline void geschwindigkeit::setze_w ( int i, int j, int k )
{
	//!	\f[	w = \sqrt{2\cdot\left( h_{rot}-c_p T \right) + (\omega r)^2}	\f]
// 	double tmpW = 2.0* ( (*stromlinieGitter)[k][i].h_rot - ( (*knoten)[k][i][j].cp * (*knoten)[k][i][j].T ) ) +pow2(  datenObjekt->omega[k] *(*knoten)[k][i][j].r );

	(*knoten)[k][i][j].w = sqrt( max( 2.0* ( (*stromlinieGitter)[k][i].h_rot - (*knoten)[k][i][j].cp * (*knoten)[k][i][j].T ) + pow2(  datenObjekt->omega[k] *(*knoten)[k][i][j].r ) , datenObjekt->minimalVelocity ) );

// 	if ( tmpW < datenObjekt->minimalVelocity ) {cout << "WARNING: setze_w: w < 0.0 " << endl;}
}






inline double geschwindigkeit::berechne_schallGeschwindigkeit ( int i, int j, int k )
{
	//! \f[	a = \sqrt{\kappa R T}	\f]
    if (!daten::getInstance()->ideal and !realFluid::getFluid()->is_initialising){
        realFluid::getFluid()->update(0,(*knoten)[k][i][j].p,(*knoten)[k][i][j].T);
        return realFluid::getFluid()->myFluid->speed_sound();
    }else{
	return sqrt ((*knoten)[k][i][j].kappa*datenObjekt->R*(*knoten)[k][i][j].T);
    }
	/* Gleichung 4 */
}


#endif
