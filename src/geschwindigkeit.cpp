/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm
*/

#include "geschwindigkeit.h"

geschwindigkeit::geschwindigkeit()
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	stromlinieGitter 	= &datenObjekt->stromlinieGitter;
}

/*
double geschwindigkeit::berechne_w_m ( int i, int j, int k )
{
// 	double temp = 2.0* ( (*stromlinieGitter)[k][i].h_rot - ( (*knoten)[k][i][j].cp *(*knoten)[k][i][j].T ) ) +pow2(  datenObjekt->omega[k] *(*knoten)[k][i][j].r );

	if ( datenObjekt->debug >1 )
	{
		cout << "Rothalpie: " << (*stromlinieGitter)[k][i].h_rot <<endl;
		cout << "cp*T: " << (*knoten)[k][i][j].cp *(*knoten)[k][i][j].T << endl;
		cout << "omega*r: " << datenObjekt->omega[k] *(*knoten)[k][i][j].r << endl;
// 		cout << "Ausdruck unter der Wurzel: " << temp << endl;
		cout << "Beta: " << (*knoten)[k][i][j].beta << endl;
		cout << "sin(beta): " << sin ( (*knoten)[k][i][j].beta ) << endl;
		cout << "beta_ru " << (*knoten)[k][i][j].beta_ru << endl;
		cout << "tan(beta_ru) " << tan ( (*knoten)[k][i][j].beta_ru ) << endl;
		cout << "beta_zu " << (*knoten)[k][i][j].beta_zu << endl;
		cout << "tan(beta_zu) " << tan ( (*knoten)[k][i][j].beta_zu ) << endl;
		cout << "epsilon " << tan ( (*knoten)[k][i][j].epsilon ) << endl;
// 		cout << "w_m = " << sqrt ( temp ) *sin ( (*knoten)[k][i][j].beta ) << endl;
	}

/*	if ( std::isinf(temp) || std::isnan(temp))
	{

		return 0.00000000000000000000000001;
	}
*/
// 	if ( temp < 0.01 )
// 	{
// 		if ( datenObjekt->debug >0 )
// 		{
// 			cout << "\t\t### !Achtung! 'w_m' in ["<<i<<"]["<<j<<"]["<<k<<"] = 0 !!! ###" << endl;
// 		}
//
//
// 		return 0.01;
// 	}
// 	else
// 	{
// 		return sqrt ( max ( temp , 0.01) ) *sin ( (*knoten)[k][i][j].beta );
// 		return berechne_w(i,j,k) *sin ( (*knoten)[k][i][j].beta );
// 	}
// }

double geschwindigkeit::berechne_w_m_0 ( int i, int j, int k )
{
	//! Berechnen und Setzen von Relativgeschwindigkeit \f$ w \f$ nach setze_w().
	setze_w(i,j,k);
	//! \f[	w_{m0} = w \cdot \sin \beta	\f]
	return (*knoten)[k][i][j].w * sin ( (*knoten)[k][i][j].beta );
}

void geschwindigkeit::setze_w_u ( int i, int j, int k )
{
	if ( (*knoten)[k][i][j].unbeschaufelt )		//! Für unbeschaufelten Raum wird \f$ w_u \f$ folgendermaßen berechnet:
	{
		if ( j > 0 )	//! - Nicht erste Quasiorthogonale (Berechnung von \f$ w \f$ durch setze_w()):
		{
			setze_w(i,j,k);
			// w_u mit Bedingung konstanter Drall im Absolutsystem,
			//! \f[	w_u(i,j,k) = \frac{w_u(i,j-1,k)\cdot r(i,j-1,k) + \omega(k)\cdot \left( r(i,j-1,k)^2-r(i,j,k)^2 \right) }{r(i,j,k)} 	\f]
			(*knoten)[k][i][j].w_u = ( (*knoten)[k][i][j-1].w_u * (*knoten)[k][i][j-1].r + datenObjekt->omega[k] * ( pow2( (*knoten)[k][i][j-1].r ) - pow2( (*knoten)[k][i][j].r ) )  ) / (*knoten)[k][i][j].r;
		}
		else
		{
			if ( k > 0 )	//! - Erste Quasiorthogonale nicht im ersten Gittern:
			{
				//! \f[	w_u = w_u(i,j_{max},k-1)+ (\omega(k-1)-\omega(k))\cdot r(i,j,k)	\f]
				// Umrechung von w_u,k-1,jmax auf w_u,k,j=0
				(*knoten)[k][i][j].w_u = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].w_u + ( datenObjekt->omega[k-1] - datenObjekt->omega[k] ) * (*knoten)[k][i][j].r;
			}
			else	//! - Erste Quasiorthogonale im ersten Gitter:
			{
				//! \f[	w_u = c_u - \omega\cdot r	\f]
				setze_w(i,j,k);
				// Man geht davon aus, dass c_u in der ersten QO des ersten Gitters als Randbedingung vorhanden ist
				(*knoten)[k][i][j].w_u = (*knoten)[k][i][j].c_u - datenObjekt->omega[k] * (*knoten)[k][i][j].r;
			}
		}
	}
	else	//! Im beschaufelten Raum:
	{
		if ( j == 0 && k > 0 )		//! - Erste Quasiorthogonale nicht im ersten Gittern:
		{
			//! \f[	w_u = w_u(i,j_{max},k-1) + (\omega(k-1)-\omega(k)) \cdot r(i,j,k) 	\f]
			// Umrechung von w_u,k-1,jmax auf w_u,k,j=0
			(*knoten)[k][i][j].w_u = (*knoten)[k-1][i][ datenObjekt->anzahlQuasiOrthogonale[k-1] - 1].w_u + ( datenObjekt->omega[k-1] - datenObjekt->omega[k] ) * (*knoten)[k][i][j].r;
		}
		else	//! - Alle weiteren Quasiorthogonalen (Berechnung von \f$ w \f$ durch setze_w()):
// 		if ( j  > 0 || k == 0 )
		{
			//OB: man geht davon aus, dass man hier niemals bei j==0 und k==0 reinspringt!
			setze_w(i,j,k);
			//! \f[	w_u = w \cdot \cos \beta 	\f]
			(*knoten)[k][i][j].w_u = (*knoten)[k][i][j].w *cos ( (*knoten)[k][i][j].beta );
		}
	}
}

void geschwindigkeit::setze_w_m ( int i, int j, int k )
{
	//! Berechnung von relativer Umfangsgeschwindigkeit \f$ w_u \f$ nach setze_w_u().
	setze_w_u(i,j,k);
	if ( (*knoten)[k][i][j].unbeschaufelt )		//! Für unbeschaufelten Raum wird \f$ w_m \f$ folgendermaßen berechnet:
	{
		if ( j == 0 && k > 0 )			//! - Erste Quasiorthogonalen nicht im ersten Gittern:
		{
			//! \f[	w_m(i,j,k) = w_m(i,j_{max},k-1)	\f]
			//! \f[	w = \sqrt{w_m^2+w_u^2}	\f]
			(*knoten)[k][i][j].w_m = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].w_m;
			(*knoten)[k][i][j].w = sqrt( pow2( (*knoten)[k][i][j].w_m ) + pow2( (*knoten)[k][i][j].w_u ) );
		}
		else	//! - Alle anderen Quasiorthogonalen:
		{
// 			double tmpWm = pow2( (*knoten)[k][i][j].w ) - pow2( (*knoten)[k][i][j].w_u );
			//! \f[	w_m = \sqrt{w^2-w_u^2}	\f]
			(*knoten)[k][i][j].w_m = sqrt( max ( pow2( (*knoten)[k][i][j].w ) - pow2( (*knoten)[k][i][j].w_u ) , datenObjekt->minimalVelocity ) );

// 			if ( ( pow2( (*knoten)[k][i][j].w ) - pow2( (*knoten)[k][i][j].w_u ) ) < datenObjekt->minimalVelocity )
// 			{
// 				cout << "WARNING: setze_w_m: w < w_u " << endl;
// 			}
		}
		// wm ist immer größer Null, damit liegt beta nur zwischen 0 und pi
		//! \f[	\beta = \arctan\left( \frac{w_m}{w_u} \right)	\f]
		(*knoten)[k][i][j].beta = atan2( (*knoten)[k][i][j].w_m , (*knoten)[k][i][j].w_u );
		(*knoten)[k][i][j].cot_beta = cot( (*knoten)[k][i][j].beta );
	}
	else	//! Für beschaufelten Raum wird \f$ w_m \f$ folgendermaßen berechnet:
	{
		if ( j == 0 && k > 0 )		//! - Erste Quasiorthogonalen nicht im ersten Gittern:
		{
			//! \f[	w_m = w_m(i,j_{max},k-1)	\f]
			//! \f[	w = \sqrt{w_m^2+w_u^2}	\f]
			// BB:(*knoten)[k]... umgeaendert nach (*knoten)[k-1]....
			(*knoten)[k][i][j].w_m = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].w_m;
			(*knoten)[k][i][j].w = sqrt( pow2( (*knoten)[k][i][j].w_m ) + pow2( (*knoten)[k][i][j].w_u ) );
		}
		else	//! - Alle weiteren Quasiorthogonalen:
		{
			//! \f[	w_m = w \cdot \sin \beta	\f]
			//o.b.: beta hat einen Definitionsbereich von 0 bis Pi
			// daraus folgt, dass sin(beta) immer größer gleich 0.0 ist
			// außerdem ist w immer größer null
			// und somit auch w_m
			(*knoten)[k][i][j].w_m = (*knoten)[k][i][j].w *sin ( (*knoten)[k][i][j].beta );
		}
	}

}

// double geschwindigkeit::machzahl_gemittelt ( int j, int k )
// {
// 	double Machzahl = 0.0;
//
// 	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
// 	{
// 		Machzahl += (*knoten)[k][i][j].machzahl;
// 	}
// 	Machzahl = Machzahl / datenObjekt->anzahlStromlinien;
// 	return Machzahl;
// }
