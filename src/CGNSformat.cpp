/***************************************************************************
 *   Copyright (C) 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                 2009 by Oliver Borm (oli.borm@web.de)                   *
 *                 2010 by Balint Balassa (balint.balassa@gmx.de)          *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope thied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,           at it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the impl                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm, Florian Mayer, Balint Balassa
*/

#include "CGNSformat.h"



CGNSformat::CGNSformat()
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	stromlinie		= &datenObjekt->stromlinie;
	stromlinieGitter 	= &datenObjekt->stromlinieGitter;

	cell_dim 		= 2; // cell dimension (2D = 2)
	phys_dim 		= 2; // Anzahl der Komponenten, die einen Vektor definieren
}



// Liest cgns Datei ein, das vom BladeDesigner3D erstellt wird
void CGNSformat::readCGNS ( string workDir)
{
	//! Ablauf:
	string eingabeDatei = datenObjekt->eingabeDatei; // "scm0.cgns";
	string eingabePfad = workDir + eingabeDatei ;

	//! - Öffnet Datei im Lesemodus.
	ier = cg_open ( eingabePfad.c_str() , CG_MODE_READ, &fn );
	if (ier != 0){ cg_error_exit();} // exit on error steht nach jeden cg_.... Befehl!
	
	B = 1;	//Momentan wird nur 1 CGNS-Datei (fn=1) und 1 Hauptknoten (B=1) verwendet
	//! - Einlesen der Informationen des Hauptknotens.
	ier = cg_base_read ( fn, B, basename, &cell_dim, &phys_dim );
	if (ier != 0){ cg_error_exit();}
	ier = cg_nzones ( fn, B, &nzones );
	if (ier != 0){ cg_error_exit();}

	int rangeStart[2];	// Indizes ab denen Felder eingelesen werden
	int rangeEnd[2];	// Indizes bis zu denen Felder eingelesen werden
	int numberOfQO[nzones]; // Array für Anzahl der Quasiorthogonalen in einr Zone
	int sumOfQO;		// Summe der bisherigen Quasiorthogonalen
	int numberOfAllQO;
	int numberOfStreamlines;
	int numberOfVertices;
	int ndescriptors;	// Anzahl der decriptoren in einer bestimmten Node
	char name[19];		// Char Array zum einlesen von cgns labels
	string szName;		// Platzhalter für string Konversion des Char Arrays
	char* text;		// Textinhalt des descriptors
	double omega_temp[nzones];
	bool beschaufelt[nzones];
	bool sp = false;	// ob statischer druck gegeben ist
	bool tt = false;	// ob Totaltemperatur am Einlass gegeben ist
	bool tp = false;	// ob Totaldruck am Einlass gegeben ist
	bool c_u = false;	// ob c_u am Einlass gegeben ist
	
	rangeStart[0] = 1;	// Setzen des Startindex in axialer Richtung
	rangeStart[1] = 1;	// Setzen des Startindex in radialer Richtung
	rangeEnd[0] = 0;	// Setzen des Stopindex in axialer Richtung

	numberOfAllQO = 0;
	sumOfQO = 0;

	//! - Herausschreiben der Anzahl an Quasiorthogonalen und der Winkelgeschwindigkeiten der jeweiligen Zone.
	for (int Z=1; Z<=nzones; Z++ )
	{
		// Zähle Quasiorthogonale:
		ier = cg_zone_read ( fn, B, Z, zonename, &size[0][0] );
		if (ier != 0){ cg_error_exit();}
		numberOfQO[Z-1] = size[0][0];
		numberOfAllQO += size[0][0];

		// Suchen nach omega Descriptor:
		ier = cg_goto( fn, B, "Zone_t", Z, "end");
		if (ier != 0){ cg_error_exit();}
		ier = cg_ndescriptors(&ndescriptors);
		if (ier != 0){ cg_error_exit();}
		for (int d=1; d<=ndescriptors; d++)
		{
			ier = cg_descriptor_read(d, name, &text);
			if (ier != 0){ cg_error_exit();}
			szName = name;
			if (szName == "omega")
			{
				omega_temp[Z-1] = strtod(text, NULL);	// Auslesen der Winkelgeschwindigkeit
			}
		}
	}//Z_ende



	numberOfStreamlines = size[0][1];
	numberOfVertices = numberOfStreamlines * numberOfAllQO;
	rangeEnd[1] = numberOfStreamlines;

	//! - Initialisieren der 2D Arrays über die ganze Maschine.
	double CoordZ[numberOfStreamlines][numberOfVertices];
	double CoordX[numberOfStreamlines][numberOfVertices];
	double r_beta_ru[numberOfStreamlines][numberOfVertices];
	double r_beta_zu[numberOfStreamlines][numberOfVertices];
	double r_sigma[numberOfStreamlines][numberOfVertices];
	double r_entropie[numberOfStreamlines][numberOfVertices];
	double r_omega[numberOfStreamlines][numberOfVertices];
	double r_stat_druck[numberOfStreamlines][numberOfVertices];
	double r_druck[numberOfStreamlines][numberOfQO[0]];
	double r_temperature[numberOfStreamlines][numberOfQO[0]];
	double r_c_u[numberOfStreamlines][numberOfQO[0]];


	for (int Z=1; Z<=nzones; Z++)
	{
		//! - Initialisieren der temporären 2D Arrays über die jeweilige Zone.
		double CoordZ_temp[numberOfStreamlines][numberOfQO[Z-1]];
		double CoordX_temp[numberOfStreamlines][numberOfQO[Z-1]];
		double r_beta_ru_temp[numberOfStreamlines][numberOfQO[Z-1]];
		double r_beta_zu_temp[numberOfStreamlines][numberOfQO[Z-1]];
		double r_sigma_temp[numberOfStreamlines][numberOfQO[Z-1]];
		double r_entropie_temp[numberOfStreamlines][numberOfQO[Z-1]];
		double r_omega_temp[numberOfStreamlines][numberOfQO[Z-1]];
		double r_stat_druck_temp[numberOfStreamlines][numberOfQO[Z-1]];

		rangeEnd[0] = numberOfQO[Z-1];	// Setzen des Stopindex in axialer Richtung

		//! - Einlesen der Koordinaten.
		ier = cg_ncoords ( fn, B, Z, &ncoords );
		if (ier != 0){ cg_error_exit();}
		for (  int C=1; C<=ncoords; C++ )
		{
			ier = cg_coord_info ( fn, B, Z, C, &datatype, coordname );
			if (ier != 0){cg_error_exit();}
			szCoordname = coordname; // char* wird in string umgewandelt, weil nur string mit string verglichen werden kann!
			if (szCoordname == "CoordinateZ")
			{
				ier = cg_coord_read ( fn, B, Z, coordname, RealDouble, &rangeStart[0], &rangeEnd[0], &CoordZ_temp[0][0] );
				if (ier != 0){ cg_error_exit();}
				for (int qo=rangeStart[0]-1; qo <rangeEnd[0]; qo++){for (int sl=rangeStart[1]-1; sl<rangeEnd[1]; sl++){CoordZ[sl][qo+sumOfQO] = CoordZ_temp[sl][qo];}}
			}
			if (szCoordname == "CoordinateX")
			{
				ier = cg_coord_read ( fn, B, Z, coordname, RealDouble, &rangeStart[0], &rangeEnd[0], &CoordX_temp[0][0] );
				if (ier != 0){ cg_error_exit();}
				for (int qo=rangeStart[0]-1; qo <rangeEnd[0]; qo++){for (int sl=rangeStart[1]-1; sl<rangeEnd[1]; sl++){CoordX[sl][qo+sumOfQO] = CoordX_temp[sl][qo];}}
			}
		} // C ende

		//! - Lesen der Lösungsfelder in die temporären 2D Arrays der Zone, und Einsetzen der temporären 2D Arrays in das endgültige 2D Array der Maschine.
		ier = cg_nsols ( fn, B, Z, &nsolutions );
		if (ier != 0){ cg_error_exit();}
		for (int S=1; S<=nsolutions; S++ )
		{
			ier = cg_sol_info ( fn, B, Z, S, solname, &location );
			if (ier != 0){ cg_error_exit();}
			ier = cg_nfields ( fn, B, Z, S, &nfields );
			if (ier != 0){ cg_error_exit();}

			for ( int F=1; F<=nfields; F++ )
			{
				ier = cg_field_info ( fn, B, Z, S, F, &datatype, fieldname );
				if (ier != 0){ cg_error_exit();}
				szFieldname = fieldname;
				if (szFieldname == "BetaR")
				{
					ier = cg_field_read ( fn, B, Z, S, fieldname, RealDouble, &rangeStart[0], &rangeEnd[0], &r_beta_ru_temp[0][0] );
					if (ier != 0){ cg_error_exit();}
					// Folgende Zeile schreibt das eingelesene temporäre Feld in das globale Feld an die richtige Stelle:
					for (int qo=rangeStart[0]-1; qo <rangeEnd[0]; qo++){for (int sl=rangeStart[1]-1; sl<rangeEnd[1]; sl++){r_beta_ru[sl][qo+sumOfQO] = r_beta_ru_temp[sl][qo];}}
				}
				else if (szFieldname == "BetaZ")
				{
					ier = cg_field_read ( fn, B, Z, S, fieldname, RealDouble, &rangeStart[0], &rangeEnd[0], &r_beta_zu_temp[0][0] );
					if (ier != 0){ cg_error_exit();}
					for (int qo=rangeStart[0]-1; qo <rangeEnd[0]; qo++){for (int sl=rangeStart[1]-1; sl<rangeEnd[1]; sl++){r_beta_zu[sl][qo+sumOfQO] = r_beta_zu_temp[sl][qo];}}
				}
				else if (szFieldname == "sigma")
				{
					ier = cg_field_read ( fn, B, Z, S, fieldname, RealDouble, &rangeStart[0], &rangeEnd[0], &r_sigma_temp[0][0] );
					if (ier != 0){ cg_error_exit();}
					for (int qo=rangeStart[0]-1; qo <rangeEnd[0]; qo++){for (int sl=rangeStart[1]-1; sl<rangeEnd[1]; sl++){r_sigma[sl][qo+sumOfQO] = r_sigma_temp[sl][qo];}}
				}
				else if (szFieldname == "Entropy")
				{
					ier = cg_field_read ( fn, B, Z, S, fieldname, RealDouble, &rangeStart[0], &rangeEnd[0], &r_entropie_temp[0][0] );
					if (ier != 0){ cg_error_exit();}
					for (int qo=rangeStart[0]-1; qo <rangeEnd[0]; qo++){for (int sl=rangeStart[1]-1; sl<rangeEnd[1]; sl++){r_entropie[sl][qo+sumOfQO] = r_entropie_temp[sl][qo];}}
				}
				else if (szFieldname == "Staticpressure")
				{
					ier = cg_field_read ( fn, B, Z, S, fieldname, RealDouble, &rangeStart[0], &rangeEnd[0], &r_stat_druck_temp[0][0] );
					if (ier != 0){ cg_error_exit();}
					for (int qo=rangeStart[0]-1; qo <rangeEnd[0]; qo++){for (int sl=rangeStart[1]-1; sl<rangeEnd[1]; sl++){r_stat_druck[sl][qo+sumOfQO] = r_stat_druck_temp[sl][qo];}}
					sp = true;
				}
				else if (szFieldname == "Totalpressure")
				{
					ier = cg_field_read ( fn, B, Z, S, fieldname, RealDouble, &rangeStart[0], &rangeEnd[0], &r_druck[0][0] );
					if (ier != 0){ cg_error_exit();}
					tp = true;
				}
				else if (szFieldname == "Totaltemperature")
				{
					ier = cg_field_read ( fn, B, Z, S, fieldname, RealDouble, &rangeStart[0], &rangeEnd[0], &r_temperature[0][0] );
					if (ier != 0){ cg_error_exit();}
					tt = true;
				}
				else if (szFieldname == "Totaltemperature")
				{
					ier = cg_field_read ( fn, B, Z, S, fieldname, RealDouble, &rangeStart[0], &rangeEnd[0], &r_c_u[0][0] );
					if (ier != 0){ cg_error_exit();}
					c_u = true;
				}
			}//F_ende
		}//S_ende
	sumOfQO += numberOfQO[Z-1]; // summe der bisherigen QO
	} // Z ende


	//! - Initialisieren von diversen Hilfswerten.
	datenObjekt->anzahlGitter = nzones;
	datenObjekt->anzahlStromlinien = numberOfStreamlines;
	datenObjekt->initialisiere_anzahlQuasiOrthogonale();
	for (int k=0; k<nzones; k++)
	{
		datenObjekt->anzahlQuasiOrthogonale[k] = numberOfQO[k];
	}
	datenObjekt->initialisiere_anzahlQuasiOrthogonale();
	datenObjekt->initialisiere();

	//! - Beschreiben der Knoten mit den Werten \f$ r,z,\sigma,s,\beta_{ru},\beta_{zu} \f$.
	int gj = 0;
	sumOfQO = 0;
	for ( int k=0; k<datenObjekt->anzahlGitter; k++ )
	{
		datenObjekt->omega[k] = omega_temp[k];
		for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
		{
			gj = sumOfQO;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				(*knoten)[k][i][j].z = CoordZ[i][gj];
				(*knoten)[k][i][j].r = CoordX[i][gj];
				(*knoten)[k][i][j].sigma = r_sigma[i][gj];
				(*knoten)[k][i][j].entropie = r_entropie[i][gj];
				(*knoten)[k][i][j].beta_ru = r_beta_ru[i][gj];
				(*knoten)[k][i][j].beta_zu = r_beta_zu[i][gj];
				if (i==0 && sp==false)	// Falls kein Druck an der Nabe vorgegeben ist
				{
					(*knoten)[k][0][j].p = 0;
				}
				if (i==0 && sp==true)	// Falls doch ein Druck an der Nabe vorgegeben ist
				{
					(*knoten)[k][0][j].p = r_stat_druck[0][gj];
				}
				if (datenObjekt->debug >1)
				{
					cout << "Gitter: " << k << "\tStromlinie: "<< i << "\tQO: " << j << "\tgj: " << gj <<  "\tz,r: " << (*knoten)[k][i][j].z << " " << (*knoten)[k][i][j].r << endl;
				}
				gj++;
			}//j_ende
		}//i_ende
		sumOfQO += numberOfQO[k];
	}//k_ende


	ier = cg_goto( fn, B, "end");	// Geht zurück in den Basisknoten
	if (ier != 0){ cg_error_exit();}

	// Bestimmt die Anzahl an Descriptor Nodes
	ier = cg_ndescriptors(&ndescriptors);
	if (ier != 0){ cg_error_exit();}

	//! - Einlesen aller Descriptoren und Setzen entsprechender Parameter.
	for (int d=1; d<=ndescriptors;d++)
	{
		ier = cg_descriptor_read(d, name, &text);
		if (ier != 0){ cg_error_exit();}
		szName = name;

		if (szName == "pFaktor")
		{
			datenObjekt->pFaktor = strtod(text, NULL);
		}
		if (szName == "MkritMax")
		{
			datenObjekt->MkritMax = strtod(text, NULL);
		}
		if (szName == "MkritMin")
		{
			datenObjekt->MkritMin = strtod(text, NULL);
		}
		if (szName == "Massflow")
		{
			datenObjekt->massenStromVorgabe = strtod(text, NULL);
		}
		if (szName == "Gasconstant")
		{
			datenObjekt->R = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "Gasconstant: " << datenObjekt->R << endl;
			}
		}
		if (szName == "maxScmIterations")
		{
			datenObjekt->anzahlDurchlaeufe = atoi(text);
			if ( datenObjekt->debug >-1 )
			{
				cout << "maxScmIterations: " << datenObjekt->anzahlDurchlaeufe << endl;
			}
		}
		if (szName == "NurbsCurveDegree")
		{
			datenObjekt->gradNurbsKurven = atoi(text);
			if ( datenObjekt->debug >-1 )
			{
				cout << "NurbsCurveDegree: " << datenObjekt->gradNurbsKurven << endl;
			}
		}
		if (szName == "pointsInbetween")
		{
			datenObjekt->anzahlZwischenPunkte = atoi(text);
			cout << "pointsInbetween: " << datenObjekt->anzahlZwischenPunkte << endl;
		}
		if (szName == "tolerance")
		{
			datenObjekt->toleranz = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "tolerance: " << datenObjekt->toleranz << endl;
			}
		}
		if (szName == "gammaTolerance")
		{
			datenObjekt->gammaToleranz = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout  << "gammaTolerance: "<< datenObjekt->gammaToleranz << endl;
			}
		}
		if (szName == "approxError")
		{
			datenObjekt->approxError = atoi(text);
			if ( datenObjekt->debug >-1 )
			{
				cout << "approxError: " << datenObjekt->approxError << endl;
			}
		}
		if (szName == "approxSL")
		{
			if (text == "True"){datenObjekt->approxSL = true;}
			else {datenObjekt->approxSL = false;}
			if ( datenObjekt->debug >-1 )
			{
				cout << "approxSL (0=true, 1=false): " << datenObjekt->approxSL << endl;
			}
		}
		if (szName == "approxQO")
		{
			if (text == "True"){datenObjekt->approxQO = true;}
			else {datenObjekt->approxQO = false;}
			if ( datenObjekt->debug >-1 )
			{
				cout << "approxQO (0=true, 1=false): " << datenObjekt->approxQO << endl;
			}
		}
		if (szName == "interpSL")
		{
			if (text == "True"){datenObjekt->interpSL = true;}
			else {datenObjekt->interpSL = false;}
			if ( datenObjekt->debug >-1 )
			{
				cout << "interpSL (0=true, 1=false): " << datenObjekt->interpSL << endl;
			}
		}
		if (szName == "numberSL")
		{
			datenObjekt->numberSL = atoi(text);
			if ( datenObjekt->debug >-1 )
			{
				cout << "numberSL: " << datenObjekt->numberSL << endl;
			}
		}
		if (szName == "derivativeMode")
		{
			datenObjekt->ablmod = atoi(text);
			if ( datenObjekt->debug >-1 )
			{
				cout << "derivativeMode: " << datenObjekt->ablmod << endl;
			}
		}
		if (szName == "startit")
		{
			if (text == "True"){datenObjekt->startit = true;}
			else {datenObjekt->startit = false;}
			if ( datenObjekt->debug >-1 )
			{
				cout << "startit (0=true, 1=false): " << datenObjekt->startit << endl;
			}
		}
		if (szName == "minDistError")
		{
			datenObjekt->minDistError = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "minDistError: " << datenObjekt->minDistError << endl;
			}
		}
		if (szName == "minDistsizeSearch")
		{
			datenObjekt->minDistsizeSearch = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "minDistsizeSearch: " << datenObjekt->minDistsizeSearch << endl;
			}
		}
		if (szName == "minDistsep")
		{
			datenObjekt->minDistsep = atoi(text);
			if ( datenObjekt->debug >-1 )
			{
				cout << "minDistsep: " << datenObjekt->minDistsep << endl;
			}
		}
		if (szName == "minDistmaxiter")
		{
			datenObjekt->minDistmaxiter = atoi(text);
			if ( datenObjekt->debug >-1 )
			{
				cout << "minDistmaxiter: " << datenObjekt->minDistmaxiter << endl;
			}
		}
		if (szName == "minDistuMin")
		{
			datenObjekt->minDistuMin = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "minDistuMin: " << datenObjekt->minDistuMin << endl;
			}
		}
		if (szName == "minDistuMax")
		{
			datenObjekt->minDistuMax = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "minDistuMax: " << datenObjekt->minDistuMax << endl;
			}
		}
		if (szName == "relaxfac")
		{
			datenObjekt->relaxfac = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "relaxfac: " << datenObjekt->relaxfac << endl;
			}
		}
		if (szName == "p_min")
		{
			datenObjekt->p_mindest = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "p_min: " << datenObjekt->p_mindest << endl;
			}
		}
		if (szName == "minimalVelocity")
		{
			datenObjekt->minimalVelocity = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "minimalVelocity: " << datenObjekt->minimalVelocity << endl;
			}
		}
		if (szName == "solver_approx")
		{
			if (text == "True"){datenObjekt->loeser_approx = true;}
			else {datenObjekt->loeser_approx = false;}
			if ( datenObjekt->debug >-1 )
			{
				cout << "solver_approx (0=true, 1=false): " << datenObjekt->loeser_approx << endl;
			}
		}
		if (szName == "maxError")
		{
			datenObjekt->maxFehler = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "maxError: " << datenObjekt->maxFehler << endl;
			}
		}
		if (szName == "MaxIt")
		{
			datenObjekt->MaxIt = atoi(text);
			if ( datenObjekt->debug >-1 )
			{
				cout << "MaxIt: " << datenObjekt->MaxIt << endl;
			}
		}
		if (szName == "streamlineTolerance")
		{
			datenObjekt->stromlinieToleranz = strtod(text, NULL);
			if ( datenObjekt->debug >-1 )
			{
				cout << "streamlineTolerance: " << datenObjekt->stromlinieToleranz << endl;
			}
		}
		if (szName == "cp-Polynomial")
		{
			
			char * pEnd;
			datenObjekt->cpFt = true;
			datenObjekt->a11 = strtod(text, &pEnd);
			datenObjekt->a21 = strtod(pEnd, &pEnd);
			datenObjekt->a31 = strtod(pEnd, &pEnd);
			datenObjekt->a41 = strtod(pEnd, &pEnd);
			datenObjekt->a51 = strtod(pEnd, &pEnd);
			
			datenObjekt->a12 = strtod(pEnd, &pEnd);
			datenObjekt->a22 = strtod(pEnd, &pEnd);
			datenObjekt->a32 = strtod(pEnd, &pEnd);
			datenObjekt->a42 = strtod(pEnd, &pEnd);
			datenObjekt->a52 = strtod(pEnd, &pEnd);

			if ( datenObjekt->debug >-1 )
			{
				cout << "cp-Polynomial: " << endl;
				cout << datenObjekt->a11 << endl;
				cout << datenObjekt->a21 << endl;
				cout << datenObjekt->a31 << endl;
				cout << datenObjekt->a41 << endl;
				cout << datenObjekt->a51 << endl;

				cout << datenObjekt->a12 << endl;
				cout << datenObjekt->a22 << endl;
				cout << datenObjekt->a32 << endl;
				cout << datenObjekt->a42 << endl;
				cout << datenObjekt->a52 << endl;
			}
		}
	}


	//! - Setze Massenstrom für Strömungskanäle.
	double temp_massflow = 1.0/ ( numberOfStreamlines-1 );
	if ( datenObjekt->debug >-1 )
	{
		cout << "MassenStrom: " << datenObjekt->massenStromVorgabe << endl;
	}
	for ( int i=0; i<numberOfStreamlines-1; i++ )
	{
		datenObjekt->faktorMassenStromRoehre[i] = temp_massflow;
		if ( datenObjekt->debug >-1 )
		{
			cout << "Setze faktorMassenStrom[" << i << "] = " << temp_massflow << endl;
		}
	}


	//! - Setze Absolute Totaltemperatur, Totaldruck und Geschwindigkeit \f$ c_u \f$ am Einlauf.
	for (int i=0; i<numberOfStreamlines; i++ )
	{
		if (tp == true && tt == true)
		{
			if ( datenObjekt->debug >-1 )
			{
				cout << "Druck: " << r_druck[i][0] << "\tTemperatur: " << r_temperature[i][0] << endl;
			}
			(*knoten)[0][i][0].p_tot_abs = r_druck[i][0];
			(*knoten)[0][i][0].T_tot_abs = r_temperature[i][0];
		}
		if (c_u == true)
		{
			(*knoten)[0][i][0].c_u = r_c_u[i][0];
		}
	}

	if ( datenObjekt->debug >-1 )
	{
		cout << "Fertig..." << endl;
	}
	ier = cg_close ( fn );
	if (ier != 0){ cg_error_exit();}
}


void CGNSformat::writeCGNS ( string workDir )
{
	//! Ablauf:
	string zonename = datenObjekt->zonename;
	string ausgabeDatei = datenObjekt->ausgabeDatei;
	string ausgabePfad = workDir + ausgabeDatei ;

	//=======================================================Erstellt CGNS-Datei
	//! - Erstellt Datei im CGNS-Format.
	ier = cg_open ( ausgabePfad.c_str() , CG_MODE_WRITE, &fn );
	if (ier != 0){ cg_error_exit();}

	//! - Erstellt Hauptknoten vom Typ 'CGNSBase_t'.
	phys_dim = 3;
	ier = cg_base_write ( fn, "Base", cell_dim, phys_dim, &B );
	if (ier != 0){ cg_error_exit();}
	ier = cg_close ( fn );
	if (ier != 0){ cg_error_exit();}

	//=======================================================FlowSolution & GridCoordinates
	//! - Schreibt FlowSolution & GridCoordinates in die Datei.
	fn=1;
	B=1;

	ier = cg_open ( ausgabePfad.c_str(), CG_MODE_MODIFY, &fn );
	if (ier != 0){ cg_error_exit();}
	ier = cg_goto ( fn, B, "end" );
	if (ier != 0){ cg_error_exit();}

	NVertexR = datenObjekt->anzahlStromlinien;

	//vertex size
	size[0][0] = NVertexR; //X-Koordinate bzw. R
	//Z-coord wird in k-schleife definiert
	//cell size
	size[1][0] = size[0][0]-1;
	//Z-coord wird in k-schleife definiert
	//boundary vertex size (always zero for structured grids)
	size[2][0] = 0;
	size[2][1] = 0;

	for ( int k=0; k<datenObjekt->anzahlGitter; k++ )
	{
		
		//! - Initialisieren der Lösungsfelder für jedes Gitter.
		NVertexZ = datenObjekt->anzahlQuasiOrthogonale[k];
		//vertex size
		size[0][1] = NVertexZ; //Z-Koordinate
		//cell size
		size[1][1] = size[0][1]-1;

		//arrays to be filled with grid data
		double CoordZ[NVertexZ][NVertexR];
		double CoordR[NVertexZ][NVertexR];
		double CoordF[NVertexZ][NVertexR];

		//arrays to be filled with solution data (compliant and non-compliant)
		double sol_dichte[NVertexZ][NVertexR];
		double sol_p[NVertexZ][NVertexR];
		double sol_T[NVertexZ][NVertexR];
		double sol_entropie[NVertexZ][NVertexR];
		double sol_Cz[NVertexZ][NVertexR];
		double sol_Cr[NVertexZ][NVertexR];
		double sol_Cu[NVertexZ][NVertexR];
		double sol_a[NVertexZ][NVertexR];
		double sol_cp[NVertexZ][NVertexR];

		double sol_Wz[NVertexZ][NVertexR];
		double sol_Wr[NVertexZ][NVertexR];
		double sol_Wu[NVertexZ][NVertexR];
		double sol_Wm[NVertexZ][NVertexR];

	 	double sol_M[NVertexZ][NVertexR];
	 	double sol_Mrel[NVertexZ][NVertexR];
		double sol_sigma[NVertexZ][NVertexR];

		double sol_rothalpie[NVertexZ][NVertexR];

	// 	double sol_p_tot_abs[NVertexZ][NVertexR];
	// 	double sol_T_tot_abs[NVertexZ][NVertexR];

	// 	double sol_p_tot_rel[NVertexZ][NVertexR];
	// 	double sol_T_tot_rel[NVertexZ][NVertexR];

	// 	double sol_eta_is[NVertexZ][NVertexR];
	// 	double sol_eta_poly[NVertexZ][NVertexR];

		//double sol_omega[NVertexZ][NVertexR];
		double sol_mf_roehre[NVertexZ][NVertexR];

		double sol_epsilon[NVertexZ][NVertexR];
		double sol_gamma[NVertexZ][NVertexR];
	// 	double sol_alpha[NVertexZ][NVertexR];

		double sol_beta[NVertexZ][NVertexR];
		double sol_beta_ru[NVertexZ][NVertexR];
		double sol_beta_zu[NVertexZ][NVertexR];

		for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
		{
			int nj = -1;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
					//! - Schreiben der Koordinaten und der Lösungen in die Lösungsfelder.<ol>
					nj++;
					CoordZ[nj][i] = (*knoten)[k][i][j].z;
					CoordR[nj][i] = (*knoten)[k][i][j].r;
					CoordF[nj][i] = 0;

						//FlowSolution SIDS-compliant
						//! <li> Dichte \f$	\rho	\f$
						sol_dichte[nj][i] = (*knoten)[k][i][j].dichte;
						//! <li> Druck \f$	p	\f$
						sol_p[nj][i] = (*knoten)[k][i][j].p;
						//! <li> Temperatur \f$	T	\f$
						sol_T[nj][i] = (*knoten)[k][i][j].T;
						//! <li> Spezifische Entropie \f$	s	\f$
						sol_entropie[nj][i] = (*knoten)[k][i][j].entropie;

						//! <li> Absolutgeschwindigkeitskomponente \f$	c_z = w_m \cdot \cos\varepsilon	\f$
						sol_Cz[nj][i] = (*knoten)[k][i][j].w_m *(*knoten)[k][i][j].cos_epsilon;
						//! <li> Absolutgeschwindigkeitskomponente \f$	c_r = w_m \cdot \sin\varepsilon	\f$
						sol_Cr[nj][i] = (*knoten)[k][i][j].w_m *(*knoten)[k][i][j].sin_epsilon;
//						sol_Cu[nj][i] = (*knoten)[k][i][j].w_m *(*knoten)[k][i][j].cot_beta + (*knoten)[k][i][j].r *datenObjekt->omega[k]; // c_u = w_u + u = w_m * cot(beta) + omega * r
						//! <li> Absolutgeschwindigkeitskomponente \f$	c_u = w_u + \omega r	\f$
						sol_Cu[nj][i] = (*knoten)[k][i][j].w_u + (*knoten)[k][i][j].r *datenObjekt->omega[k]; // c_u = w_u + u = w_m * cot(beta) + omega * r

						//! <li> Schallgeschwindigkeit \f$	a	\f$
						sol_a[nj][i] = (*knoten)[k][i][j].a;

						// FlowSolution SIDS-NONcompliant
						//! <li> Isobare spezifische Wärmekapazität \f$	c_p	\f$
						sol_cp[nj][i] = (*knoten)[k][i][j].cp;

						//! <li> Relativgeschwindigkeitskomponente \f$	w_z = w_m \cdot \cos\varepsilon	\f$
						sol_Wz[nj][i] = (*knoten)[k][i][j].w_m *(*knoten)[k][i][j].cos_epsilon;
						//! <li> Relativgeschwindigkeitskomponente \f$	w_r = w_m \cdot \sin\varepsilon	\f$
						sol_Wr[nj][i] = (*knoten)[k][i][j].w_m *(*knoten)[k][i][j].sin_epsilon;
//							sol_Wu[nj][i] = (*knoten)[k][i][j].w_m *(*knoten)[k][i][j].cot_beta;
						//! <li> Relativgeschwindigkeitskomponente \f$	w_u	\f$
						sol_Wu[nj][i] = (*knoten)[k][i][j].w_u ;
						//! <li> Relative Meridionalgeschwindigkeit \f$	w_m	\f$
						sol_Wm[nj][i] = (*knoten)[k][i][j].w_m;

						//! <li> Machzahl \f$	Ma = \frac{\sqrt{c_z^2+c_r^2+c_u^2}}{a}	\f$
						sol_M[nj][i] = sqrt(pow2(sol_Cz[nj][i]) + pow2(sol_Cr[nj][i]) + pow2(sol_Cu[nj][i])) / (*knoten)[k][i][j].a;
						// <li> Machzahl \f$	Ma = \frac{\sqrt{w_m}}{a\cdot \sin \beta}	\f$
						//sol_M[nj][i] = (*knoten)[k][i][j].w_m / ((*knoten)[k][i][j].a * sin((*knoten)[k][i][j].beta));
						//! <li> Relative Machzahl \f$	Ma_{rel} = \frac{w_m}{a}\sqrt{1+\cot^2\beta}	\f$
						sol_Mrel[nj][i] = (*knoten)[k][i][j].w_m / (*knoten)[k][i][j].a * sqrt ( 1+pow2(  (*knoten)[k][i][j].cot_beta ) );

						//! <li> Rothalpie \f$	h_{rot}	\f$
						sol_rothalpie[nj][i] = (*stromlinieGitter)[k][i].h_rot;

						//! <li> Schaufelversperrung \f$	\sigma	\f$
						sol_sigma[nj][i] = (*knoten)[k][i][j].sigma;

// 							sol_p_tot_abs[nj][i] = (*knoten)[k][i][j].p_tot_abs;
// 							sol_T_tot_abs[nj][i] = (*knoten)[k][i][j].T_tot_abs;
// 							sol_p_tot_rel[nj][i] = (*knoten)[k][i][j].p_tot_rel;
// 							sol_T_tot_rel[nj][i] = (*knoten)[k][i][j].T_tot_rel;

// 							sol_eta_is[nj][i] = (*stromlinie)[i].eta_isentrop;
// 							sol_eta_poly[nj][i] = (*stromlinie)[i].eta_polytrop;
						//sol_omega[nj][i] = datenObjekt->omega[k];

						//! <li> Massenstrom in Röhre \f$	\dot{m}_{R\ddot{o}hre}	\f$
						sol_mf_roehre[nj][i] = (*knoten)[k][i][j].massenStromRoehre;

						//! <li> Winkel \f$	\varepsilon	\f$
						sol_epsilon[nj][i] = (*knoten)[k][i][j].epsilon / M_PI*180.0; //Winkel in Grad
// 						cout << "k: " << k << " j: "<< j << " i: " << i <<" epsilon: " (*knoten)[k][i][j].epsilon<< endl;
						
						//! <li> Winkel \f$	\gamma	\f$
						sol_gamma[nj][i] = (*knoten)[k][i][j].gamma / M_PI*180.0;
						
// 							sol_alpha[nj][i] = (*knoten)[k][i][j].alpha / M_PI*180.0;

						//! <li> Winkel \f$	\beta	\f$
						sol_beta[nj][i] = (*knoten)[k][i][j].beta / M_PI*180.0;

						//! <li> Winkel \f$	\beta_{ru}	\f$
						sol_beta_ru[nj][i] = (*knoten)[k][i][j].beta_ru / M_PI*180.0;

						//! <li> Winkel \f$	\beta_{zu}	\f$
						sol_beta_zu[nj][i] = (*knoten)[k][i][j].beta_zu / M_PI*180.0;

						//! </ol>
			}// j_ende
		}// i_ende

		// Wandelt gitter index k in const char um
		std::stringstream sstr;
		sstr << datenObjekt->zonename << k+1;
		std::string str1 = sstr.str();

		//! - Erstellt Unterknoten vom Typ 'Zone_t' (Kap. 6.2)
		ier = cg_zone_write ( fn, B, (str1).c_str(), &size[0][0], Structured, &Z );
		if (ier != 0){ cg_error_exit();}

		//! - Schreibt Koordinaten in Datei.
		ier = cg_coord_write ( fn, B, Z, RealDouble, "CoordinateZ", CoordZ, &C );
		if (ier != 0){ cg_error_exit();}
		
		ier = cg_coord_write ( fn, B, Z, RealDouble, "CoordinateY", CoordF, &C );
		if (ier != 0){ cg_error_exit();}
		
		ier = cg_coord_write ( fn, B, Z, RealDouble, "CoordinateX", CoordR, &C );
		if (ier != 0){ cg_error_exit();}

		//! - Schreibt FlowSolution in Datei.
		ier = cg_sol_write ( fn, B, Z, "FlowSolution", Vertex, &S ); // (Kap. 12.1)
		if (ier != 0){ cg_error_exit();}
		ier = cg_goto ( fn, B, "Zone_t", Z, "FlowSolution_t", S, "end" );
		if (ier != 0){ cg_error_exit();}

		//! - Schreibt SIDS-compliant FlowSolution in Datei.
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "Density", sol_dichte, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "Pressure", sol_p, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "Temperature", sol_T, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "Entropy", sol_entropie, &F );
		if (ier != 0){ cg_error_exit();}

		ier = cg_field_write ( fn, B, Z, S, RealDouble, "VelocityX", sol_Cr, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "VelocityY", sol_Cu, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "VelocityZ", sol_Cz, &F );
		if (ier != 0){ cg_error_exit();}

	// 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "VelocitySound", sol_a, &F );

		//! - Schreibt SIDS-NONcompliant FlowSolution in Datei.
	// 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "Specific_Heat", sol_cp, &F );

		ier = cg_field_write ( fn, B, Z, S, RealDouble, "RotatingVelocityX", sol_Wr, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "RotatingVelocityY", sol_Wu, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "RotatingVelocityZ", sol_Wz, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "MeridianVelocity", sol_Wm, &F );
		if (ier != 0){ cg_error_exit();}
	 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "Machnumber", sol_M, &F );
		if (ier != 0){ cg_error_exit();}
	 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "Relative_Machnumber", sol_Mrel, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "BladeBlockage", sol_sigma, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "Rothalpie", sol_rothalpie, &F );
		if (ier != 0){ cg_error_exit();}

	// 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "Totalpressure", sol_p_tot_abs, &F );
	//	if (ier != 0){ cg_error_exit();}
	// 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "Totaltemperature", sol_T_tot_abs, &F );
	//	if (ier != 0){ cg_error_exit();}
	// 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "Relative_Totalpressure", sol_p_tot_rel, &F );
	//	if (ier != 0){ cg_error_exit();}
	// 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "Relative_Totaltemperature", sol_T_tot_rel, &F );
	//	if (ier != 0){ cg_error_exit();}

	// 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "Efficiency_(isentrop)", sol_eta_is, &F );
	//	if (ier != 0){ cg_error_exit();}
	// 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "Efficiency_(polytrop)", sol_eta_poly, &F );
	//	if (ier != 0){ cg_error_exit();}

		//ier = cg_field_write ( fn, B, Z, S, RealDouble, "Omega", sol_omega, &F );
		//if (ier != 0){ cg_error_exit();}

		ier = cg_field_write ( fn, B, Z, S, RealDouble, "Epsilon", sol_epsilon, &F );
		if (ier != 0){ cg_error_exit();}


		// Gamma ist der NAN Übeltäter!!!!
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "Gamma", sol_gamma, &F );
		if (ier != 0){ cg_error_exit();}

	// 	ier = cg_field_write ( fn, B, Z, S, RealDouble, "Alpha", sol_alpha, &F );
	//	if (ier != 0){ cg_error_exit();}

		ier = cg_field_write ( fn, B, Z, S, RealDouble, "Beta", sol_beta, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "Beta_ru", sol_beta_ru, &F );
		if (ier != 0){ cg_error_exit();}
		ier = cg_field_write ( fn, B, Z, S, RealDouble, "Beta_zu", sol_beta_zu, &F );
		if (ier != 0){ cg_error_exit();}

		ier = cg_field_write ( fn, B, Z, S, RealDouble, "MassFlow", sol_mf_roehre, &F );
		if (ier != 0){ cg_error_exit();}
	}// k_ende


	//! - Schreibt Reference in Datei.
	ier = cg_goto ( fn, B, "end" );
	if (ier != 0){ cg_error_exit();}
	ier = cg_state_write ( "Dimensional" ); // (Kap. 10.1)
	if (ier != 0){ cg_error_exit();}

	//! - Schließt die erstellte CGNS-Datei
	ier = cg_close ( fn );
	if (ier != 0){ cg_error_exit();}

	//! - Gibt den Daten ihre Einheiten.
	fn=1; B=1;
	ier = cg_open ( ausgabePfad.c_str(), CG_MODE_MODIFY, &fn );
	if (ier != 0){ cg_error_exit();}
	ier = cg_goto ( fn, B, "end" );
	if (ier != 0){ cg_error_exit();}
	ier = cg_dataclass_write ( Dimensional );
	if (ier != 0){ cg_error_exit();}
	ier = cg_units_write ( Kilogram, Meter, Second, Kelvin, Radian );
	if (ier != 0){ cg_error_exit();}

	ier = cg_nzones ( fn, B, &nzones );
	if (ier != 0){ cg_error_exit();}
	for ( int Z=1; Z<=nzones; Z++ )
	{
		ier = cg_nsols ( fn, B, Z, &nsolutions );
		for ( int S=1; S<=nsolutions; S++ )
		{
			ier = cg_nfields ( fn, B, Z, S, &nfields );
			for ( int F=1; F<=nfields; F++ )
			{
				double exp[5];
				ier = cg_field_info ( fn, B, Z, S, F, &datatype, fieldname );
				szFieldname = fieldname;
				
				if ( szFieldname == "Density" )
					{exp[0]=1;exp[1]=-3;exp[2]=0;exp[3]=0;exp[4]=0;}

				else if ( szFieldname == "Pressure" || szFieldname == "Relative_Totalpressure" )
					{exp[0]=1;exp[1]=-1;exp[2]=-2;exp[3]=0;exp[4]=0;}

				else if ( szFieldname == "Temperature" || szFieldname == "Relative_Totaltemperature" )
					{exp[0]=0;exp[1]=0;exp[2]=0;exp[3]=1;exp[4]=0;}

				else if ( szFieldname == "Entropy" )
					{exp[0]=1;exp[1]=2;exp[2]=-2;exp[3]=-1;exp[4]=0;}

				else if ( szFieldname == "VelocityX" || szFieldname == "VelocityY" || szFieldname == "VelocityZ" || szFieldname == "VelocitySound" )
					{exp[0]=0;exp[1]=1;exp[2]=-1;exp[3]=0;exp[4]=0;}

				else if ( szFieldname == "BladeBlockage" || szFieldname == "Efficiency_(isentrop)" || szFieldname == "Efficiency_(polytrop)" )
					{exp[0]=0;exp[1]=0;exp[2]=0;exp[3]=0;exp[4]=0;}

				else if ( szFieldname == "AngularVelocity" )
					{exp[0]=0;exp[1]=0;exp[2]=-1;exp[3]=0;exp[4]=0;}

				else if ( szFieldname == "MassFlow" )
					{exp[0]=1;exp[1]=0;exp[2]=-1;exp[3]=0;exp[4]=0;}

				else if ( szFieldname == "Specific_Heat" )
					{exp[0]=0;exp[1]=2;exp[2]=-2;exp[3]=-1;exp[4]=0;}

				ier = cg_goto ( fn, B, "Zone_t", Z, "FlowSolution_t", S, "DataArray_t", F, "end" );
				ier = cg_exponents_write ( RealDouble, exp );
			}// nfields_ende
		}// nsolutions_ende
	}// nzones_ende
	ier = cg_close ( fn );
	if (ier != 0){ cg_error_exit();}

	if ( datenObjekt->debug >-1 )
	{
		cout << "Die Daten wurden erfolgreich nach 'scm1.cgns' geschrieben!" << endl;
	}
}

