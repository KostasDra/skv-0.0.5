/***************************************************************************
 *   Copyright (C) 2008-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "skelettflaeche3d.h"



SkelettFlaeche3D::SkelettFlaeche3D ( Matrix_Point3Dd Punkte_Skelett, int deg , std::string workdirInput)
{
	datenObjekt = daten::getInstance();
	degree = deg ;
	erzeugeSkelettFlaeche3D ( Punkte_Skelett ) ;
	workdir = workdirInput;
}





double* SkelettFlaeche3D::berechneBetaRu_BetaZu ( double uSF, double vSF )
{
	Point3Dd NormalenVektor, EinheitsVektorR , EinheitsVektorZ ;
	//! - Bilder Normalenvektor \f$ \vec{n} \f$ der Skelettfläche im Punkt mit den Parametern \f$ u,v \f$.
	NormalenVektor = SkelettFlaeche.normal ( uSF,vSF ) ;

	//! - Bildete Einheitsvektoren \f$ \vec{r} \f$ und  \f$ \vec{z} \f$.
	EinheitsVektorR = Point3Dd ( SchnittPunkt.x(),SchnittPunkt.y(),0.0 ) ; // Richtungsvektor in r-Richtung
	EinheitsVektorZ = Point3Dd ( 0.0,0.0,1.0 ) ;

	//! - Berechnet die Winkel:
	//! - \f[	\beta_{ru} = \angle (\vec{r},\vec{n})	\f]
	//! - \f[	\beta_{zu} = \angle (\vec{z},\vec{n})	\f]
	betas[0] = winkel ( EinheitsVektorR,NormalenVektor ) ; // beta_ru
	betas[1] = winkel ( EinheitsVektorZ,NormalenVektor ) ; // beta_zu
	//! Siehe NURBS++ Dokumentation für Details.
	
	return betas ;
}





double* SkelettFlaeche3D::berechneSchnittpunkt ( double radius, double axialePosition )
{
	double uSF, vSF, phi, fehler;
	int n =  0 ;
	fehler = 1.0 ;
	Point3Dd SchnittPunkt_neu ;
	phi = 0.0 ;
	uSF = 0.5 ;
	vSF = 0.5 ;

	while ((n < datenObjekt->MaxIt) && (fehler > datenObjekt->maxFehler) )
	{
		//! Siehe NURBS++ Dokumentation für Details.
		SchnittPunkt = Point3Dd ( radius*cos ( phi ),radius*sin ( phi ),axialePosition ) ;
		fehler = SkelettFlaeche.minDist2 ( SchnittPunkt,uSF,vSF,datenObjekt->maxFehler*0.01 ) ;

		SchnittPunkt_neu = SkelettFlaeche.pointAt ( uSF,vSF ) ;
		phi = atan2 ( SchnittPunkt_neu.y(),SchnittPunkt_neu.x() ) ;
		n++;
	}

	parameter[0] = uSF ;
	parameter[1] = vSF ;

	SchnittPunkt = SchnittPunkt_neu ;

	return parameter;
}




void SkelettFlaeche3D::ausgabe2 ( string workdir, int k )
{
	std::string ausgabeName="skelettflaeche-";
	std::string zusatz=".wrl";
	std::stringstream out;
	out << workdir << ausgabeName << k << zusatz;

	if (datenObjekt->debug >-1)
	{
		cout << "Writing wrl file to " << out.str() << endl;
	}

	SkelettFlaeche.writeVRML97 ( out.str().c_str() ) ;
}
