/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef ROTHALPIE_H
	#define ROTHALPIE_H



#include "daten.h"


//! Rothalpie-Klasse
/*!
Beinhaltet die Methoden zur Berechnung der Rothalpie.
\author Oliver Borm
*/
class rothalpie
{
	private:
		daten*		datenObjekt;
		v3Knoten*	knoten;
	public:
		rothalpie();

		//! Berechnung der Rothalpie
		/*!
		    Berechnet die Rothalpie auf einer Stromlinie mit Hilfe der Temperatur.
		    \param i aktuelle Stromlinie
		    \param k aktuelles Gitter
		    \return Rothalpie \f$ h_{rot} \f$
		    \author Oliver Borm
		*/
		inline double berechne_rothalpie ( int i, int k );


		//! Berechnung der Rothalpie
		/*!
		    Berechnet die Rothalpie auf einer Stromlinie mit Hilfe der absoluten Totaltemperatur.
		    \param i aktuelle Stromlinie
		    \param k aktuelles Gitter
		    \return Rothalpie \f$ h_{rot} \f$
		    \author Oliver Borm
		*/
		inline double berechne_rothalpie_0 ( int i, int k );
};


inline double rothalpie::berechne_rothalpie_0 ( int i, int k )
{
	//! \f[	h_{rot} = c_p \cdot T_{tot,abs} - c_u \cdot \omega r	\f]
	return (*knoten)[k][i][0].cp*(*knoten)[k][i][0].T_tot_abs - (*knoten)[k][i][0].c_u * datenObjekt->omega[k] * (*knoten)[k][i][0].r ;
}


inline double rothalpie::berechne_rothalpie ( int i, int k )
{
	//! \f[	h_{rot} = c_p \cdot T + \frac{w^2-(\omega r)^2}{2}	\f]
	return (*knoten)[k][i][0].cp*(*knoten)[k][i][0].T + 0.5*( pow2( (*knoten)[k][i][0].w ) - pow2 ( datenObjekt->omega[k] * (*knoten)[k][i][0].r ) ) ;
}

#endif
