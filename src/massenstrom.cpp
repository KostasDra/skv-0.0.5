/***************************************************************************
 *   Copyright (C) 2009 by Oliver Borm (oli.borm@web.de)                   *
 *                 2008-2009 by Florian Mayer (ilumine@gmx.de)             *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm
*/



#include "massenstrom.h"



using namespace PLib ;



massenStrom::massenStrom()
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	quasiorthogonale 	= &datenObjekt->quasiorthogonale;
}





void massenStrom::berechneMassenStrom ( int j, int k )
{
	double massenStrom_temp = 0.0;
	if ( k>0 && j==0 )
	{
		(*quasiorthogonale)[k][j].massenStrom = (*quasiorthogonale)[k-1][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].massenStrom;
	}
	else	//! Summiert den Massenstrom auf der Quasiorthogonalen über die Massenstromröhren.
	{
		for ( int i=0; i<datenObjekt->anzahlStromlinien-1; i++ )
		{
			massenStrom_temp = massenStrom_temp + berechneMassenStromRoehre ( (*knoten)[k][i][j].ny,(*knoten)[k][i+1][j].ny,j,k );
			if ( datenObjekt->debug >1 )
			{
				cout << "Massenstromröhre["<<i+1<<"]<=>SL("<<i<<"-"<<i+1<<") = " << berechneMassenStromRoehre ( (*knoten)[k][i][j].ny,(*knoten)[k][i+1][j].ny,j,k ) <<endl;
			}
		}

		(*quasiorthogonale)[k][j].massenStrom = massenStrom_temp;
	}

	if ( datenObjekt->debug >0 )
	{
		cout << "MassenStrom ["<<j<<"]["<<k<<"] = " << (*quasiorthogonale)[k][j].massenStrom << endl;
	}
}





double massenStrom::berechneMassenStromRoehre ( double ny_lo, double ny_hi, int j, int k )
{
	double delta_ny = 0.0;
	//double delta_r = 0.0;
	double phi = 2*M_PI;
	double delta_massenStromDichte = 0.0;
	double massenStrom_Roehre = 0.0;
	double ny_temp = 0.0;
	double sigma = 1.0;
	double flaeche = 0.0;

	massenStrom_Roehre = 0.0;
	
	//! \f[	\Delta \nu = \frac{\nu_{hi}-\nu_{low}}{z_p}	\f]
	delta_ny = ( ny_hi-ny_lo ) /datenObjekt->anzahlZwischenPunkte;

	//! Iteration über Anzahl an Zwischenpunkten \f$ z_p \f$:
	for ( int zp=0; zp<datenObjekt->anzahlZwischenPunkte; zp++ )
	{
		//! - Berechne \f$ \nu_{temp} \f$ für aktuellen Zwischenpunkt \f[	\nu_{temp} = \nu_{low} + \frac{z_p\cdot \Delta \nu + (z_p+1)\cdot \Delta \nu}{2}	\f]
		ny_temp = ny_lo+ ( zp*delta_ny+ ( zp+1 ) *delta_ny ) /2;

		// - \f[	\Delta l = \sqrt{\big(r(\nu_{low}+(z_p+1)\Delta\nu) - r(\nu_{low}+z_p\Delta\nu)\big)^2 + \big(z(\nu_{low}+(z_p+1)\Delta\nu) - z(\nu_{low}+z_p\Delta\nu)\big)^2}	\f]
		//delta_r = sqrt ( pow ( (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+ ( zp+1 ) *delta_ny ).y()-(*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+zp*delta_ny ).y(),2 ) +pow ( (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+ ( zp+1 ) *delta_ny ).x()-(*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+zp*delta_ny ).x(),2 ) );

		//! - Berechnung der Schaufelverperrung \f[	\sigma = \sigma(\nu_{temp})	\f]
		sigma = (*quasiorthogonale)[k][j].nurbsSigma.hpointAt ( ny_temp ).w();

		//! - Einfluss der Schaufelversperrung auf die zu berechnende Fläche \f[	\varphi = 2\pi\sigma	\f]
		phi = 2*M_PI*sigma;

		//! - Massenstromdichte \f[ \Delta \frac{\dot{m}}{A} = \frac{\dot{m}}{A}(\nu)\f] nach initialisiere_nurbsMassenStromDichte().
		//\f[	\Delta \dot{m}_{Dichte} = \dot{m}_{Dichte}(\nu_{temp})	\f]
		delta_massenStromDichte = (*quasiorthogonale)[k][j].nurbsMassenStromDichte.hpointAt ( ny_temp ).w();

		double flaeche2 = 0.0;
		//! - Flächenberechnung für \f$ \xi=konst \f$:
		//! - \f[	\mathrm{d}r_{hi} = r(\nu_{low}+(z_p+1)\Delta\nu)	\f]
		//! - \f[	\mathrm{d}r_{lo} = r(\nu_{low}+z_p\Delta\nu)	\f]
		//! - \f[	\mathrm{d}z_{hi} = z(\nu_{low}+(z_p+1)\Delta\nu)	\f]
		//! - \f[	\mathrm{d}z_{lo} = z(\nu_{low}+z_p\Delta\nu)	\f]
		double dr_hi = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+ ( zp+1 ) *delta_ny ).y();
		double dr_lo = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+zp*delta_ny ).y();
		double dz_hi = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+ ( zp+1 ) *delta_ny ).x();
		double dz_lo = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+zp*delta_ny ).x();

		//! - \f[	\sin\epsilon = \sin\epsilon(\nu_{temp})	\f]
		double sineps = fabs ( sin ( (*quasiorthogonale)[k][j].nurbsEpsilonQO.hpointAt ( ny_temp ).w() ) );
		//! - \f[	\cos\epsilon = \cos\epsilon(\nu_{temp})	\f]
		double coseps = fabs ( cos ( (*quasiorthogonale)[k][j].nurbsEpsilonQO.hpointAt ( ny_temp ).w() ) );
		double r = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_temp ).y();

		//! - \f[	A(z_p) = \varphi \cdot r \cdot \left( (\mathrm{d}r_{hi}-\mathrm{d}r_{lo})\cos\epsilon - (\mathrm{d}z_{hi}-\mathrm{d}z_{lo})\sin\epsilon \right)	\f]
		flaeche2 = phi*r* ( ( dr_hi-dr_lo ) *coseps- ( dz_hi-dz_lo ) *sineps );

		if ( datenObjekt->debug >1 )
		{
			cout <<"rho*w_m = " << delta_massenStromDichte <<endl;
			cout <<"phi = "<< phi<<endl;
			cout <<"r = "<<r<<endl;
			cout <<"dr_hi-dr_lo = "<< ( dr_hi-dr_lo ) <<endl;
			cout <<"coseps = " << coseps << endl;
			cout <<"dz_hi-dz_lo = "<< ( dz_hi-dz_lo ) <<endl;
			cout <<"sineps = " << sineps << endl;
			cout <<"epsilon = " << (*quasiorthogonale)[k][j].nurbsEpsilonQO.hpointAt ( ny_temp ).w() << "\t[°]: "<<(*quasiorthogonale)[k][j].nurbsEpsilonQO.hpointAt ( ny_temp ).w() /M_PI*180 << endl;
			cout<<"ny_hi: "<<ny_hi<<endl;
			cout<<"ny_lo: "<<ny_lo<<endl;
			cout<<"ny_temp: "<<ny_temp<<endl;
			cout <<"["<<j<<"]["<<k<<"]::Fläche = "<<flaeche2<< endl<<endl;
		}
		flaeche = flaeche + flaeche2;

		//! - Aufsummierung über die Zwischenpunkte: \f[	\dot{m}_{R\ddot{o}hre} = \sum_0^{z_p} \Delta\frac{\dot{m}}{A}(z_p)\cdot A(z_p)	\f]
		massenStrom_Roehre = massenStrom_Roehre + delta_massenStromDichte*flaeche2;


	}
	(*quasiorthogonale)[k][j].flaeche = flaeche;

	return massenStrom_Roehre;
}





void massenStrom::initialisiere_nurbsMassenStromDichte ( int j, int k )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	double wert_temp;
	NurbsCurve_2Dd kurve;

	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
	{
		//! \f[	\frac{\dot{m}}{A} = \rho \cdot w_m	\f]
		wert_temp = (*knoten)[k][i][j].dichte *(*knoten)[k][i][j].w_m;
		if ( datenObjekt->debug >1 )
		{
			cout << "["<<i<<"]["<<j<<"]["<<k<<"]";
			cout << "\tMassenStromDichte temporärer Wert: " << wert_temp << endl;
			cout << "\tdichte: " << (*knoten)[k][i][j].dichte << endl;
			cout << "\t\tp: " << (*knoten)[k][i][j].p << endl;
			cout << "\t\tR: " << datenObjekt->R << endl;
			cout << "\t\tT: " << (*knoten)[k][i][j].T << endl;
			cout << "\tw_m: " << (*knoten)[k][i][j].w_m << endl;
		}
		punkte[i] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,wert_temp ) ;
	}
	kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
	(*quasiorthogonale)[k][j].nurbsMassenStromDichte = kurve;
}

//+++++++++++++++ Methoden von Andrea Bader ++++++++++++++++++++++++++++++++++

void massenStrom::setzeMassenStrom ( int j, int k )
{
	double massenStrom_temp = 0.0;

	//! Übertragen des Massenstroms vom vorherigen Gitter, wenn 1. QO im neuen Gitter
	if ( k>0 && j==0 )
	{
		(*quasiorthogonale)[k][j].massenStrom = (*quasiorthogonale)[k-1][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].massenStrom;
	}
	//! Sonst Aufsummierung über berechneMassenStromRoehre_sinPsi()
	else
	{
		for ( int i=0; i<datenObjekt->anzahlStromlinien-1; i++ )
		{
			massenStrom_temp = massenStrom_temp + berechneMassenStromRoehre_sinPsi ( (*knoten)[k][i][j].ny,(*knoten)[k][i+1][j].ny,j,k );
		}

		(*quasiorthogonale)[k][j].massenStrom = massenStrom_temp;
	}

	if ( datenObjekt->debug >0)
	{
		cout << "MassenStrom ["<<j<<"]["<<k<<"] = " << (*quasiorthogonale)[k][j].massenStrom << endl;
	}
}


double massenStrom::berechneMassenStromRoehre_sinPsi ( double ny_lo, double ny_hi, int j, int k )
{
	double delta_ny = 0.0;
	double delta_q = 0.0;
	double phi = 2*M_PI;
	double massenStromDichteSin = 0.0;
	double massenStrom_Roehre_teil = 0.0;
	double massenStrom_Roehre = 0.0;
	double ny_temp = 0.0;
	double sigma = 1.0;
	double r = 0.0;

	//! \f$ \Delta \nu = \frac{\nu_{hi} - \nu_{lo}}{z_p} \f$ mit zp = Anzahl der Zwischenpunkte
	delta_ny = ( ny_hi-ny_lo ) /datenObjekt->anzahlZwischenPunkte;

	//! Iteration über alle Zwischenpunkte \f$ z_p \f$:
	for ( int zp=0; zp<datenObjekt->anzahlZwischenPunkte; zp++ )
	{
		//! \f[ \nu_{temp} = \nu_{lo} + \frac{z_p\cdot \Delta \nu + \Delta \nu \cdot (z_p+1)}{2} \f]
		ny_temp = ny_lo+ ( zp*delta_ny+ ( zp+1 ) *delta_ny ) /2;

		//! - Aktuelle Koordinaten aus der NurbsQO
		//! \f[ \Delta q = \sqrt{(y_1-y_2)^2+(x_1-x_2)^2} \f]
		//! \f[ y_1 = y(\nu_{lo}+(z_p+1)\cdot \Delta \nu) \f]
		//! \f[ y_2 = y(\nu_{lo}+z_p \cdot \Delta \nu) \f]
		//! \f[ x_1 = x(\nu_{lo}+(z_p+1)\cdot \Delta \nu) \f]
		//! \f[ x_2 = x(\nu_{lo}+z_p \cdot \Delta \nu) \f]
		delta_q = sqrt 	( 	pow2 ( (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+ ( zp+1 )	*delta_ny ).y()
								  -(*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+zp*delta_ny ).y() )
				          + pow2 ( (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+ ( zp+1 )*delta_ny ).x()
						          -(*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_lo+zp*delta_ny ).x())
		                );

		sigma = (*quasiorthogonale)[k][j].nurbsSigma.hpointAt ( ny_temp ).w();
		phi = 2*M_PI*sigma;
		massenStromDichteSin = fabs((*quasiorthogonale)[k][j].nurbsMassenStromDichteSin.hpointAt ( ny_temp ).w());
		r = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_temp ).y();
		//! \f[ \dot{m}_{R\ddot{o}re,Teil} = \frac{\dot{m}}{A}\cdot \sin \phi \cdot r \cdot \Delta q\f]
		massenStrom_Roehre_teil = massenStromDichteSin * phi * r *delta_q;

		if ( datenObjekt->debug >1 )
		{
			cout <<"rho*w_m*sinPsi = " << massenStromDichteSin <<endl;
			cout <<"phi = "<< phi <<endl;
			cout <<"r = "<<r<<endl;
		}
		//! - Aufsummierung über alle Stromröhren
		massenStrom_Roehre = massenStrom_Roehre +  massenStrom_Roehre_teil;
	}
	return massenStrom_Roehre;
}


void massenStrom::initialisiere_nurbsMassenStromDichte_sinPsi ( int j, int k )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	double wert_temp;
	NurbsCurve_2Dd kurve;

	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
	{
		wert_temp = (*knoten)[k][i][j].dichte *(*knoten)[k][i][j].w_m * (*knoten)[k][i][j].sin_psi;
		if ( datenObjekt->debug >1 )
		{
			cout << "["<<i<<"]["<<j<<"]["<<k<<"]";
			cout << "\tMassenStromDichte temporaerer Wert: " << wert_temp << endl;
			cout << "\tdichte : " << (*knoten)[k][i][j].dichte << endl;
			cout << "\tsinPsi    : " << (*knoten)[k][i][j].sin_psi << endl;
			cout << "\tw_m: " << (*knoten)[k][i][j].w_m << endl;
		}

		punkte[i] = HPoint2Dd ( (*knoten)[k][i][j].z, (*knoten)[k][i][j].r,wert_temp ) ;

	}
	kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
	(*quasiorthogonale)[k][j].nurbsMassenStromDichteSin = kurve;
}


void massenStrom::initialisiere_nurbsMassenStromDichte_sinPsi_neu ( int j, int k )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	double wert_temp;
	NurbsCurve_2Dd kurve;

	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
	{
		wert_temp = (*knoten)[k][i][j].dichte *(*knoten)[k][i][j].w_m_neu * (*knoten)[k][i][j].sin_psi ;
		if ( datenObjekt->debug >1 )
		{
			cout << "["<<i<<"]["<<j<<"]["<<k<<"]";
			cout << "\tMassenStromDichte temporaerer Wert: " << wert_temp << endl;
			cout << "\tdichte : " << (*knoten)[k][i][j].dichte << endl;
			cout << "\tsinPsi    : " << (*knoten)[k][i][j].sin_psi << endl;
			cout << "\tw_m_neu: " << (*knoten)[k][i][j].w_m_neu << endl;
		}

		punkte[i] = HPoint2Dd ( (*knoten)[k][i][j].z, (*knoten)[k][i][j].r,wert_temp ) ;
	}
	kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
	(*quasiorthogonale)[k][j].nurbsMassenStromDichteSin = kurve;
}


void massenStrom::anpassenFaktorMassenStromRoehre()
{//! Berechnet den Massenstromfaktor nach Flächenverteilung, damit Stromlinien nicht mehr hochgezogen werden

	for ( int i=0; i<datenObjekt->anzahlStromlinien-1; i++ )
	{
		double mFaktor = 0.0;

		int k = 0;
		int j = 0;
		//! \f[ \textrm{mFaktor} = \frac{r(i+1)^2-r(i)^2}{r(i_{max})^2-r(i=0)^2} \f]
		mFaktor = 	 (pow2((*knoten)[k][i+1][j].r) -  pow2((*knoten)[k][i][j].r))/
					 (pow2((*knoten)[k][datenObjekt->anzahlStromlinien-1][j].r) -  pow2((*knoten)[k][0][j].r) )  ;

		datenObjekt->faktorMassenStromRoehre[i] = mFaktor;
	}
}


void massenStrom::massenstrom_aus_schluckziffer_opt()
{
	//! Setze i auf mittlere Stromlinie
	int i = datenObjekt->anzahlStromlinien/2;
	//! \f[ d_{ref} = 2 \cdot r_{Rotor,HK} \f]
	double d_ref = 2 * (*knoten)[datenObjekt->rotorHK_k][i][datenObjekt->rotorHK_j].r;
	//! \f[ u_{ref} = \omega \cdot \frac{d_{ref}}{2} \f]
	double u_ref = datenObjekt->omega[datenObjekt->rotorHK_k] * d_ref/2;
	double massenstrom ;
	double schluckziffer_opt = datenObjekt -> schluckziffer_opt;
	double dichte_tot = datenObjekt->eintrittstotaldichte;
	//! \f[ \dot{m} = \textrm{schluckzifferopt} \cdot u_{ref} \cdot d_{ref}^2 \cdot \rho_{tot}\f]
	massenstrom = schluckziffer_opt * u_ref * pow2(d_ref)* dichte_tot;

	datenObjekt->massenStromVorgabe = massenstrom ;
	cout<<  "      Schluckziffer optimal = " << schluckziffer_opt<< " und  eta_opt = " << datenObjekt->eta_opt << endl;
	cout << "      Massenstrom in der Mitte aus Schluckziffer_opt = " << massenstrom << endl;

}





void massenStrom::berechneAnfangsmassenstrom ()
{
	//! An der Rotor Eintrittskante soll Zuströmung mit fester Inzidenz gewährleistet werden.
	//! Voraussetzung: Drallfreie Zuströmung

	//gemitteltes w_m auf Rotoreintrittskante
	int k_RotorVK = datenObjekt->rotorVK_k;
	int j_RotorVK = datenObjekt->rotorVK_j;

	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
	{	//w_u = u, da c_u = 0;
		double w_u_temp = datenObjekt->omega[k_RotorVK] * (*knoten)[k_RotorVK][i][j_RotorVK].r;
		double inzidenz = 0.0; //Falschanströmung in Grad vorgegeben
		double beta = (*knoten)[k_RotorVK][i][j_RotorVK].beta; //normales beta
		double cot_beta_test = cot ( beta + (inzidenz  *M_PI/180.0) );
		double w_m_temp = fabs(w_u_temp / cot_beta_test );

		(*knoten)[k_RotorVK][i][j_RotorVK].w_m = w_m_temp;

		//Dichte berechnen
		double dichte_temp = 0.0;
		double dichte_tot = 0.0;
		double w_temp = 0.0;
		double kappa = (*knoten)[0][i][0].kappa;

		w_temp = sqrt ( pow2 ( w_m_temp ) + pow2 ( w_u_temp) );

		//dichte bezogen auf eintrittswerte
		//! \f[ \rho_{tot,abs} = \frac{p_{tot,abs}}{R} \cdot T_{tot,abs} \f]
		dichte_tot = (*knoten)[0][i][0].p_tot_abs/(datenObjekt->R *(*knoten)[0][i][0].T_tot_abs);
		//! \f[ \rho = \rho_{tot,abs} \cdot \left(\frac{T_{tot,abs}-\frac{w_{temp}}{2c_p}}{T_{tot,abs}}\right)^\frac{1}{\kappa-1} \f]
		dichte_temp = dichte_tot * pow ((  ((*knoten)[0][i][0].T_tot_abs - (pow2(w_temp)
				/( 2 * (*knoten)[0][i][0].cp ) ) )/ (*knoten)[0][i][0].T_tot_abs ), ( 1 /(kappa - 1 )));

		(*knoten)[k_RotorVK][i][j_RotorVK].dichte = dichte_temp;
	}

	initialisiere_nurbsMassenStromDichte_sinPsi(j_RotorVK, k_RotorVK);
	setzeMassenStrom(j_RotorVK, k_RotorVK);
	double massenStromRotor = (*quasiorthogonale)[k_RotorVK][j_RotorVK].massenStrom ;
	cout<< " berechneter Massenstrom: "<< massenStromRotor << endl;
	datenObjekt->massenStromVorgabe = massenStromRotor;

	//initale wm Belegung auf der Rotorvorderkante wiederherstellen
	for ( int i=0; i < datenObjekt->anzahlStromlinien; i++ )
	{
		(*knoten)[k_RotorVK][i][j_RotorVK].w_m = (*knoten)[k_RotorVK][i][j_RotorVK-1].w_m ;
	}
}





double massenStrom::berechnereduziertenVolumenstrom ()
{
	double eintrittstotaldichte = datenObjekt->eintrittstotaldichte;
	double v_red = 0.0;
	double t_ref = 293.15;
	double t_eintritt = 0.0;

	for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
		{
			t_eintritt = t_eintritt + datenObjekt->faktorMassenStromRoehre[i]
			             * ((*knoten)[0][i][0].T_tot_abs + (*knoten)[0][i+1][0].T_tot_abs)/2;
		}
	v_red = datenObjekt->massenStromVorgabe/eintrittstotaldichte * sqrt(t_ref/t_eintritt)  ;

	return (v_red);
}

