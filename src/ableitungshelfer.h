/***************************************************************************
 *   Copyright (C)  2009-2010 by Andrea Bader (bader.andrea@gmx.de)        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author  Andrea Bader
*/

#include "daten.h"

using namespace std;


//! Ableitungshelferklasse
/*!
	Eine statische Klasse (es wird kein Objekt dieses Klasse erzeugt)
	mit deren Methoden die verschiedenen Differenzenverfahren zur
	Berechnung der Ableitungen berechnet werden.
	Der Aufruf enthält den Übergabeparamenter der abzuleitenden
	Knoteneigenschaft.

	\author Andrea Bader
*/

class ableitungshelfer
{
private:

	static daten* bekommeDaten();


public:

	//! Erste Ableitung in Richtung der Quasiorthogonalen!
		/*!
		Berechnet die erste Ableitung entlang einer Quasiorthogonalen mit Differenzenverfahren
		2. Ordnung bei nicht konstanter Gitterweite und gibt den berechneten Wert zurück.

		\param k aktuelles Gitter
		\param j aktuelle Quasiorthogonale
		\param i aktuelle Stromlinie
		\param eigen  (EKnotenEigenschaft) abzuleitende Knoteneigenschaft

		\author Andrea Bader
		*/
	static double bildeErsteAbleitung_dq(int k, int j, int i, EKnotenEigenschaft eigen);


	//! Erste Ableitung in Richtung der Stromlinie!
		/*!
		Berechnet die erste Ableitung entlang einer Stromlinie mit Differenzenverfahren
		2. Ordnung bei nicht konstanter Gitterweite und gibt den berechneten Wert zurück.

		\param k aktuelles Gitter
		\param j aktuelle Quasiorthogonale
		\param i aktuelle Stromlinie
		\param eigen  (EKnotenEigenschaft) abzuleitende Knoteneigenschaft

		\author Andrea Bader
		*/
		static double bildeErsteAbleitung_dm(int k, int j, int i, EKnotenEigenschaft eigen);


	//! Zweite Ableitung in Richtung der Stromlinie!
		/*!
		Berechnet die zweite Ableitung entlang einer Stromlinie mit Differenzenverfahren
		2. Ordnung bei nicht konstanter Gitterweite und gibt den berechneten Wert zurück.

		\param k aktuelles Gitter
		\param j aktuelle Quasiorthogonale
		\param i aktuelle Stromlinie
		\param eigen  (EKnotenEigenschaft) abzuleitende Knoteneigenschaft

		\author Andrea Bader
		*/
	static double bildeZweiteAbleitung_dm (int k, int j, int i, EKnotenEigenschaft eigen);


	//! Erste Ableitung in Richtung der Quasiorthogonalen!
		/*!
		Berechnet die erste Ableitung entlang einer Quasiorthogonalen mit
		Vorwärtsdifferenzen 1.Ordnung und gibt den berechneten Wert zurück.

		\param k aktuelles Gitter
		\param j aktuelle Quasiorthogonale
		\param i aktuelle Stromlinie
		\param eigen  (EKnotenEigenschaft) abzuleitende Knoteneigenschaft

		\author Andrea Bader
		*/
	static double bildeEinfachVorwaertsAbleitung_dq(int k, int j, int i, EKnotenEigenschaft eigen);


	//! Erste Ableitung in Richtung der Stromlinien!
		/*!
		Berechnet die erste Ableitung entlang einer Stromlinie mit
		Rückwärtsdifferenzen 1.Ordnung und gibt den berechneten Wert zurück.
		Auf der 1. QO erfolgt die Berechnung mit Vorwärtsdifferenz 1. Ordnung.

		\param k aktuelles Gitter
		\param j aktuelle Quasiorthogonale
		\param i aktuelle Stromlinie
		\param eigen  (EKnotenEigenschaft) abzuleitende Knoteneigenschaft

		\author Andrea Bader
		 */
	static double bildeEinfachRueckwaertsAbleitung_dm(int k, int j, int i, EKnotenEigenschaft eigen);


};


