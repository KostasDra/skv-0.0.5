/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef WIRKUNGSGRAD_H
	#define WIRKUNGSGRAD_H



#include "daten.h"

//! Klasse für die Berechnung der Wirkungsgrade.
/*!
Diese Klasse ist für die Berechnung der verschiedenen Wirkungsgrade zuständig.
\author Oliver Borm
*/
class wirkungsgrad
{
	private:
		daten			*datenObjekt;
		v3Knoten		*knoten;
		v2Quasiorthogonale	*quasiorthogonale;
		vStromlinie		*stromlinie;
	public:
		wirkungsgrad();


		//! Berechnung des isentropen Wirkungsgrades
		/*!
		    \author Oliver Borm
		*/
		void wirkungsgrad_isentrop ( );


		//! Berechnung des polytropen Wirkungsgrades
		/*!
		    \author Oliver Borm
		*/
		void wirkungsgrad_polytrop ( );


		//! Berechnung des massengemittelten isentropen Wirkungsgrades
		/*!
		    \author Oliver Borm
		*/
		void wirkungsgrad_isentrop_m ( );


		//! Berechnung des massengemittelten polytropen Wirkungsgrades
		/*!
		    \author Oliver Borm
		*/
		void wirkungsgrad_polytrop_m ( );

//++++++++++++++++++++++++ Methoden von Andrea Bader ++++++++++++++++++++++++++++++++++++++++


		//! Berechnung und setzen der Entropie
		/*!
			Entropiedifferenz wird aus einem vorgegebenen
			polytropen Wirkungsgrad auf Basis der
			Temperatur berechnet und gesetzt.

		    \author Andrea Bader
		*/
		void setze_entropie_aus_eta_polytrop ( );


		//! Berechnung und setzen des Bestpunktes
		/*!
			Berechnet und setzt den Wirkungsgrad und die Schluckziffer
			des Bestpunktes für die aktuelle Umfangsgeschwindigkeit aus
			Messwertkorrelationen.

		    \author Andrea Bader
		*/
		void setze_optimalwerte_fuer_u ( );


		//! Berechnung und setzen des Wirkungsgrades
		/*!
			Berechnet und setzt den Wirkungsgrad für den aktuellen
			Betriebspunkt aus Korrelationen auf Basis der aktuellen
			Schluckziffer in Bezug auf den Optimalpunkt.

		    \author Andrea Bader
		*/
		void setze_eta_betriebspunkt ( );
};

#endif
