/***************************************************************************
 *   Copyright (C) 2008-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef SKELETTFLAECHE3D_H
	#define SKELETTFLAECHE3D_H



#include <nurbs++/nurbsS.h>
#include <cmath>
#include <string>



#include "daten.h"



using namespace PLib ;
using namespace std;


//! SkelettFlaeche3D-Klasse
/*!
Beinhaltet die Methoden zur Berechnung der 3D Skelettfläche.
\author Oliver Borm
*/
class SkelettFlaeche3D
{
	private:
		daten*	datenObjekt;
		NurbsSurfaced SkelettFlaeche ;
		Point3Dd SchnittPunkt ;
		int degree ;
		double parameter[2] ;
		string workdir;

	public:
		double betas[2];
		
		/*!
		    \param Punkte_Skelett Punktematrix aus diskreten Punkten der Skelettfläche
		    \param deg Grad der NURBS
		    \param workdirInput Verzeichnis der Quelldateien
		    \author Oliver Borm
		*/
		SkelettFlaeche3D ( Matrix_Point3Dd Punkte_Skelett, int deg, string workdirInput );

		
		//! Erzeugt die Skelettfläche als 3D NURBS Fläche
		/*!
		    Benutzt die globalInterp Funktion der NURBS++ Bibliothek um die gegebenen Punkte zu einer Fläche zu interpolieren.
		    \param Punkte_Skelett Punktematrix aus diskreten Punkten der Skelettfläche
		    \author Oliver Borm
		*/
		inline void erzeugeSkelettFlaeche3D ( Matrix_Point3Dd Punkte_Skelett );

		
		//! Berechnet die Winkel \f$ \beta_{ru} \f$ und \f$ \beta_{zu} \f$
		/*!
		    \param uSF Parameter \f$ u \f$ der NURBS-Fläche
		    \param vSF Parameter \f$ v \f$ der NURBS-Fläche
		    \return Array mit den Winkeln \f$ \beta_{ru} \f$ und \f$ \beta_{zu} \f$
		    \author Oliver Borm
		*/
		double* berechneBetaRu_BetaZu ( double uSF, double vSF );

		
		//! Berechnet den Schnittpunkt mit der NURBS-Fläche
		/*!
		    Gibt die Parameter des Punktes auf der NURBS-Fläche zurück, der am nächsten zu dem gegebenen Punkt liegt.
		    \param radius Radiale Koordinate des Punktes
		    \param axialePosition Axiale Koordinate des Punktes
		    \return Array mit den Parametern des Schnittpunktes
		    \author Oliver Borm
		*/
		double* berechneSchnittpunkt ( double radius, double axialePosition );

		
		//! Skalarprodukt von zwei Vektoren
		/*!
		    Nimmt die Koordinaten der Punkte und berechnet daraus das Skalarprodukt der Pseudovektoren. 
		    \param punktA Koordinaten des 1. Pseudovektors
		    \param punktB Koordinaten des 2. Pseudovektors
		    \return Skalarprodukt
		    \author Oliver Borm
		*/
		inline double dotS ( Point3Dd punktA, Point3Dd punktB );

		
		//! Winkel zwischen zwei Vektoren
		/*!
		    Nimmt die Koordinaten der Punkte und berechnet daraus den Winkel zwischen den Pseudovektoren.
		    \param punktA Koordinaten des 1. Pseudovektors
		    \param punktB Koordinaten des 2. Pseudovektors
		    \return Winkel
		    \author Oliver Borm
		*/
		inline double winkel ( Point3Dd punktA, Point3Dd punktB );

		
		//! Ausgabe der Skelettfläche
		/*!
		    Gibt die Skelettfläche als .wrl-Datei im VRML97-format aus.
		    \param k aktuelles Gitter
		    \param workdirInput Verzeichnis der Quelldateien
		    \author Oliver Borm
		*/
		void ausgabe2 ( string workdir, int k );
};





inline void SkelettFlaeche3D::erzeugeSkelettFlaeche3D ( Matrix_Point3Dd Punkte_Skelett )
{
	SkelettFlaeche.globalInterp ( Punkte_Skelett,degree,degree ) ;
}





inline double SkelettFlaeche3D::dotS ( Point3Dd punktA, Point3Dd punktB )
{
	return punktA.x() *punktB.x() + punktA.y() *punktB.y() + punktA.z() *punktB.z() ;
}





inline double SkelettFlaeche3D::winkel ( Point3Dd punktA, Point3Dd punktB )
{
	return acos ( dotS ( punktA,punktB ) / ( punktA.norm() *punktB.norm() ) ) ;
}


#endif
