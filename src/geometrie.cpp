/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm
*/



#include "geometrie.h"



geometrie::geometrie ( )
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	stromlinie 		= &datenObjekt->stromlinie;
	stromlinieGitter	= &datenObjekt->stromlinieGitter;
	quasiorthogonale	= &datenObjekt->quasiorthogonale;

	minDistError 		= datenObjekt->minDistError;
	minDistsizeSearch 	= datenObjekt->minDistsizeSearch;
	minDistsep 		= datenObjekt->minDistsep;
	minDistmaxiter 		= datenObjekt->minDistmaxiter;
	minDistuMin 		= datenObjekt->minDistuMin;
	minDistuMax 		= datenObjekt->minDistuMax;
}





void geometrie::aktualisiereGeometrie ()
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].r = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( (*knoten)[k][i][j].ny ).y();
				(*knoten)[k][i][j].z = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( (*knoten)[k][i][j].ny ).x();
			}
		}
	}
}





void geometrie::bekomme_ny ()
{
	double temp;
	double ny_temp;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			ny_temp = 0.0;

			for ( int i=1; i<datenObjekt->anzahlStromlinien-1; i++ )
			{
				temp = 0.0;

				temp = (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.minDist2 ( Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ),ny_temp,minDistError,minDistsizeSearch,minDistsep,minDistmaxiter,minDistuMin,minDistuMax ) ;
				//Definition 'minDist2()':http://libnurbs.sourceforge.net/docs/classPLib_1_1ParaCurve.html#a10

				//! Alle Knotenpunkte werden mit dem \f$ \nu \f$ des aktuellen Knotens beschrieben.
				(*knoten)[k][i][j].ny = ny_temp;
				(*knoten)[k][i][j].ny_alt = ny_temp;

				if ( datenObjekt->debug >1 )
				{
					cout << "ny[" <<i<<"]["<<j<<"]["<<k<<"]: " << ny_temp << endl;
					cout << "r[" <<i<<"]["<<j<<"]["<<k<<"]: " << (*knoten)[k][i][j].r << endl;
					cout << "z[" <<i<<"]["<<j<<"]["<<k<<"]: " << (*knoten)[k][i][j].z << endl;
					cout << "QO["<<i<<"]["<<j<<"]["<<k<<"]: " << (*quasiorthogonale)[k][j].nurbsQuasiOrthogonale.pointAt ( ny_temp ) << endl;
					cout << endl;
				}
			}

			(*knoten)[k][0][j].ny_alt = 0.0;
			(*knoten)[k][0][j].ny = 0.0;
			(*knoten)[k][datenObjekt->anzahlStromlinien-1][j].ny_alt = 1.0;
			(*knoten)[k][datenObjekt->anzahlStromlinien-1][j].ny = 1.0;
		}
	}
}





void geometrie::bekomme_my ()
{
	double temp=0.0;
	double my_temp=0.0;

	for ( int i=0; i< datenObjekt->anzahlStromlinien; i++ )
	{
		temp = 0.0;
		my_temp = 0.0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
			{
				if ( k>0 && j==0 )
				{
					(*knoten)[k][i][0].my = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].my;
				}
				else
				{
					temp = (*stromlinie)[i].nurbsStromlinie.minDist2 ( Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ),my_temp,minDistError,minDistsizeSearch,minDistsep,minDistmaxiter,minDistuMin,minDistuMax ) ;
					//! Alle Knotenpunkte werden mit dem \f$ \mu \f$ des aktuellen Knotens beschrieben.
					(*knoten)[k][i][j].my = my_temp;
				}

				if ( datenObjekt->debug >1 )
				{
					cout << "my[" <<i<<"]["<<j<<"]["<<k<<"]: " << my_temp << endl;
					cout << "r[" <<i<<"]["<<j<<"]["<<k<<"]: " << (*knoten)[k][i][j].r << endl;
					cout << "z[" <<i<<"]["<<j<<"]["<<k<<"]: " << (*knoten)[k][i][j].z << endl;
					cout << "SL["<<i<<"]["<<j<<"]["<<k<<"]: " << (*stromlinie)[i].nurbsStromlinie.pointAt ( my_temp ) << endl;
					cout << endl;
				}
			}
		}
	}
	bekomme_myG ();
}





void geometrie::bekomme_myG ()
{
	double temp=0.0;
	double my_temp=0.0;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
		{
			temp = 0.0;
			my_temp = 0.0;
			for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
			{
				temp = (*stromlinieGitter)[k][i].nurbsGitterStromlinie.minDist2 ( Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ),my_temp,minDistError,minDistsizeSearch,minDistsep,minDistmaxiter,minDistuMin,minDistuMax ) ;
				(*knoten)[k][i][j].myG = my_temp;
			}
		}
	}
}





void geometrie::bekomme_ny_neu ()
{
	//Relaxation
	double relaxfac = datenObjekt->relaxfac;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=1; i<datenObjekt->anzahlStromlinien-1; i++ )
			{
				//! \f[ \nu = \textrm{relaxfac}\cdot \nu + (1-\textrm{relaxfac} \cdot \nu_{alt} ) \f]
				(*knoten)[k][i][j].ny = relaxfac*(*knoten)[k][i][j].ny + ( 1.0-relaxfac ) * (*knoten)[k][i][j].ny_alt;
			}
		}
	}
}





void geometrie::initialisiereGeometrie_H_sigma ()
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	NurbsCurve_2Dd kurve;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				punkte[i] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].sigma );
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*quasiorthogonale)[k][j].nurbsSigma = kurve;
		}
	}
}





void geometrie::initialisiereGeometrie_H_beta_zu ()
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	NurbsCurve_2Dd kurve;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				punkte[i] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].beta_zu ) ;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven ) ;
			(*quasiorthogonale)[k][j].nurbsBeta_zu = kurve;
		}
	}
}





void geometrie::initialisiereGeometrie_H_beta_ru ()
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	NurbsCurve_2Dd kurve;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				punkte[i] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].beta_ru ) ;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven ) ;
			(*quasiorthogonale)[k][j].nurbsBeta_ru = kurve;
		}
	}
}





void geometrie::initialisiereGeometrie_H_entropie ()
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	NurbsCurve_2Dd kurve;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				punkte[i] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].entropie ) ;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven ) ;
			(*quasiorthogonale)[k][j].nurbsEntropie = kurve;
		}
	}
}





void geometrie::initialisiereGeometrie_nurbsQO ()
{
	Vector_Point2Dd punkte_QO ( datenObjekt->anzahlStromlinien ) ;
	NurbsCurve_2Dd kurve_QO;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				if ( datenObjekt->debug >1 )
				{
					cout << "r: "<<(*knoten)[k][i][j].r << endl;
					cout << "z: "<<(*knoten)[k][i][j].z << endl;
					cout << "#QO("<<i<<","<<j<<","<<k<<"): " << j+1<<"/"<<datenObjekt->anzahlQuasiOrthogonale[k] << endl<<endl;
				}
				punkte_QO[i] = Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ) ;
			}
			kurve_QO.globalInterp ( punkte_QO,datenObjekt->gradNurbsKurven );
			(*quasiorthogonale)[k][j].nurbsQuasiOrthogonale = kurve_QO;
		}
	}
}





void geometrie::aktualisiereGeometrie_nurbsQO ()
{
	Vector_Point2Dd punkte_QO ( datenObjekt->anzahlStromlinien ) ;
	NurbsCurve_2Dd kurve_QO;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				punkte_QO[i] = Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ) ;
			}
			kurve_QO.globalInterp ( punkte_QO,datenObjekt->gradNurbsKurven );
			(*quasiorthogonale)[k][j].nurbsQuasiOrthogonale = kurve_QO;
		}
	}
}





void geometrie::aktualisiere_nurbsSinEpsilon ()
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	NurbsCurve_2Dd kurve;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				punkte[i] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].sin_epsilon ) ;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*quasiorthogonale)[k][j].nurbsSinEpsilon = kurve;
		}
	}
}





void geometrie::aktualisiere_nurbsCotBetaQO ()
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlStromlinien ) ;
	NurbsCurve_2Dd kurve;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				punkte[i] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].cot_beta ) ;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*quasiorthogonale)[k][j].nurbsCotBetaQO = kurve;
		}
	}
}





void geometrie::initialisiereGeometrie_nurbsSL ()
{
	Vector_Point2Dd punkte_SL ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve_SL;
	int n;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ((k <= 0) || (j!=0))
				{
					punkte_SL[n] = Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ) ;
					n++;
				}
			}
		}
		kurve_SL.globalInterp ( punkte_SL,datenObjekt->gradNurbsKurven );
		(*stromlinie)[i].nurbsStromlinie = kurve_SL;
	}
	initialisiereGeometrie_nurbsGitterSL ();

	aktualisiere_nurbsEntropieSL ();
	aktualisiere_nurbsSigmaSL ();
	aktualisiere_nurbsSinEpsilon ();
	aktualisiere_nurbsCosEpsilon ();
	aktualisiere_nurbsCotBetaSL ();
	aktualisiere_nurbsCotBetaQO ();
}





void geometrie::initialisiereGeometrie_nurbsGitterSL ()
{
	Vector_Point2Dd punkte_SL;
	NurbsCurve_2Dd kurve_SL;
	int n;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkte_SL.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkte_SL[n] = Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ) ;
				n++;
			}
			kurve_SL.globalInterp ( punkte_SL,datenObjekt->gradNurbsKurven );
			(*stromlinieGitter)[k][i].nurbsGitterStromlinie = kurve_SL;
		}
	}
	aktualisiere_nurbsEntropieSL ();
	aktualisiere_nurbsSigmaSL ();
	aktualisiere_nurbsSinEpsilon ();
	aktualisiere_nurbsCosEpsilon ();
	aktualisiere_nurbsCotBetaSL ();
	aktualisiere_nurbsCotBetaQO ();
}





void geometrie::aktualisiereGeometrie_nurbsSL ()
{
	Vector_Point2Dd punkte_SL ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve_SL;
	int n;

	for ( int i=1;i<datenObjekt->anzahlStromlinien-1;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ((k <= 0) || (j!=0))
				{
					punkte_SL[n] = Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ) ;
					n++;
				}
			}
		}
		kurve_SL.globalInterp ( punkte_SL,datenObjekt->gradNurbsKurven );
		(*stromlinie)[i].nurbsStromlinie = kurve_SL;
	}
	aktualisiereGeometrie_nurbsGitterSL ();

	aktualisiere_nurbsEntropieSL ();
	aktualisiere_nurbsSigmaSL ();
	aktualisiere_nurbsSinEpsilon ();
	aktualisiere_nurbsCosEpsilon ();
	aktualisiere_nurbsCotBetaSL ();
	aktualisiere_nurbsCotBetaQO ();
}





void geometrie::aktualisiereGeometrie_nurbsGitterSL ()
{
	Vector_Point2Dd punkte_SL;
	NurbsCurve_2Dd kurve_SL;
	int n;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkte_SL.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkte_SL[n] = Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ) ;
				n++;
			}
			kurve_SL.globalInterp ( punkte_SL,datenObjekt->gradNurbsKurven );
			(*stromlinie)[i].nurbsStromlinie = kurve_SL;
		}
	}
	aktualisiere_nurbsEntropieSL ();
	aktualisiere_nurbsSigmaSL ();
	aktualisiere_nurbsSinEpsilon ();
	aktualisiere_nurbsCosEpsilon ();
	aktualisiere_nurbsCotBetaSL ();
	aktualisiere_nurbsCotBetaQO ();
}





void geometrie::aktualisiereGeometrie_approx_nurbsSL ()
{
	Vector_Point2Dd punkte_SL ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve_SL;
	int n;

	for ( int i=1;i<datenObjekt->anzahlStromlinien-1;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ((k <= 0) || (j!=0))
				{
					punkte_SL[n] = Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ) ;
					n++;
				}
			}
		}
		if ( i!=0 && i!= ( datenObjekt->anzahlStromlinien-1 ) )
			{kurve_SL.globalApproxErrBnd ( punkte_SL,datenObjekt->gradNurbsKurven,datenObjekt->approxError );}
		else
			{kurve_SL.globalInterp ( punkte_SL,datenObjekt->gradNurbsKurven );}
		(*stromlinie)[i].nurbsStromlinie = kurve_SL;
	}
	aktualisiereGeometrie_approx_nurbsGitterSL ();

	aktualisiere_nurbsEntropieSL ();
	aktualisiere_nurbsSigmaSL ();
	aktualisiere_nurbsSinEpsilon ();
	aktualisiere_nurbsCosEpsilon ();
	aktualisiere_nurbsCotBetaSL ();
	aktualisiere_nurbsCotBetaQO ();
}





void geometrie::aktualisiereGeometrie_approx_nurbsGitterSL ()
{
	Vector_Point2Dd punkte_SL;
	NurbsCurve_2Dd kurve_SL;
	int n;

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkte_SL.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkte_SL[n] = Point2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r ) ;
				n++;
			}
			if ( i!=0 && i!= ( datenObjekt->anzahlStromlinien-1 ) )
				{kurve_SL.globalApproxErrBnd ( punkte_SL, datenObjekt->gradNurbsKurven, datenObjekt->approxError );}

			else
				{kurve_SL.globalInterp ( punkte_SL,datenObjekt->gradNurbsKurven );}
			(*stromlinieGitter)[k][i].nurbsGitterStromlinie = kurve_SL;
		}
	}
	aktualisiere_nurbsEntropieSL ();
	aktualisiere_nurbsSigmaSL ( );
	aktualisiere_nurbsSinEpsilon ( );
	aktualisiere_nurbsCosEpsilon ( );
	aktualisiere_nurbsCotBetaSL ( );
	aktualisiere_nurbsCotBetaQO ( );
}





void geometrie::aktualisiere_nurbsEntropieSL ( )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve;
	int n;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ((k <= 0) || (j!=0))
				{
					punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].entropie ) ;
					n++;
				}
			}
		}
		kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
		(*stromlinie)[i].nurbsEntropieSL = kurve;
	}

	//Gitter
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkte.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].entropie ) ;
				n++;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*stromlinieGitter)[k][i].nurbsEntropieGitterSL = kurve;
		}
	}
}





void geometrie::aktualisiere_nurbsSigmaSL ( )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve;
	int n;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ((k <= 0) || (j!=0))
				{
					punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].sigma ) ;
					n++;
				}
			}
		}
		kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
		(*stromlinie)[i].nurbsSigmaSL = kurve;
	}

	//Gitter
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkte.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].sigma ) ;
				n++;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*stromlinieGitter)[k][i].nurbsSigmaGitterSL = kurve;
		}
	}
}






void geometrie::aktualisiere_nurbsCosEpsilon ()
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve;
	int n;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ((k <= 0) || (j!=0))
				{
					punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].cos_epsilon ) ;
					n++;
				}
			}
		}
		kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
		(*stromlinie)[i].nurbsCosEpsilon = kurve;
	}

	//Gitter
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkte.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].cos_epsilon ) ;
				n++;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*stromlinieGitter)[k][i].nurbsCosEpsilonGitter = kurve;
		}
	}
}





void geometrie::aktualisiere_nurbsCotBetaSL ( )
{
	Vector_HPoint2Dd punkte ( datenObjekt->anzahlUniqueQO ) ;
	NurbsCurve_2Dd kurve;
	int n;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		n=0;
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
		{
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				if ((k <= 0) || (j!=0))
				{
					punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].cot_beta ) ;
					n++;
				}
			}
		}
		kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
		(*stromlinie)[i].nurbsCotBetaSL = kurve;
	}

	//Gitter
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		punkte.resize ( datenObjekt->anzahlQuasiOrthogonale[k] );
		for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
		{
			n=0;
			for ( int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++ )
			{
				punkte[n] = HPoint2Dd ( (*knoten)[k][i][j].z,(*knoten)[k][i][j].r,(*knoten)[k][i][j].cot_beta ) ;
				n++;
			}
			kurve.globalInterpH ( punkte,datenObjekt->gradNurbsKurven );
			(*stromlinieGitter)[k][i].nurbsCotBetaGitterSL = kurve;
		}
	}
}





void geometrie::aktualisiereGeometrie_H_sigma ()
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].sigma = (*quasiorthogonale)[k][j].nurbsSigma.hpointAt ( (*knoten)[k][i][j].ny ).w();
			}
		}
	}
}





void geometrie::aktualisiereGeometrie_H_beta_zu ()
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].beta_zu = (*quasiorthogonale)[k][j].nurbsBeta_zu.hpointAt ( (*knoten)[k][i][j].ny ).w();
			}
		}
	}
}






void geometrie::aktualisiereGeometrie_H_beta_ru ()
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].beta_ru = (*quasiorthogonale)[k][j].nurbsBeta_ru.hpointAt ( (*knoten)[k][i][j].ny ).w();
			}
		}
	}
}





void geometrie::aktualisiereGeometrie_H_entropie ()
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].entropie = (*quasiorthogonale)[k][j].nurbsEntropie.hpointAt ( (*knoten)[k][i][j].ny ).w();
			}
		}
	}
}
