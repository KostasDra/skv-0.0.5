/***************************************************************************
 *   Copyright (C) 2010 Andrea Bader (bader.andrea@gmx.de)                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*!
    @author Andrea Bader
 */

#ifndef KENNFELD_MANAGER_H
#define KENNFELD_MANAGER_H

#include "daten.h"
#include "vector"

using namespace std;


/*!
 	Hilfsdatentype der Kennfeldergebnisse
 */
struct kennfeld_resultat
{
	double m_red;
	double pi_tot;
	double m_vorgabe;
	double u;
	double eta_poly;
	double phi;

	string to_string()
	{
		char line[80];
		sprintf(line,
				"%f; %f; %f; %f; %f; %f",
				u, m_vorgabe, pi_tot, m_red, eta_poly, phi);
		string str(line);

		//Punkte durch Kommas ersetzt -> für Excel
		size_t position = str.find( "." ); // sucht nach erstem Punkt
		while ( position != string::npos )
		{
			str.replace( position, 1, "," ); //durch Komma ersetzt
			position = str.find( ".", position + 1 );
		}

		return str;
	}
};

/*!
	Zustände für die Realisierung des Kennfeldmanager. Intern ist der
	Manager immer in einem dieser Zustände und macht abhängig vom
	aktuellen Zustand und dem eintreffenden Ereignis (Fehler oder Ok)
	den entsprechenden Übergang zum neuen Zustand.
 */
enum zustand_kennlinie {
	ZK_mitte,                  //!< ZK_mitte
	ZK_suchLaufeLinks,         //!< ZK_suchLaufeLinks

	ZK_nachRechts,             //!< ZK_nachRechts
	ZK_sucheSchluckGrenzePlus, //!< ZK_sucheSchluckGrenzePlus
	ZK_sucheSchluckGrenzeMinus,//!< ZK_sucheSchluckGrenzeMinus

	ZK_nachLinks,              //!< ZK_nachLinks
	ZK_suchePumpGrenzePlus,    //!< ZK_suchePumpGrenzePlus
	ZK_suchePumpGrenzeMinus    //!< ZK_suchePumpGrenzeMinus
};


//! Kennfeldmanagerklasse
/*!
    Mit Hilfe der Kennfeldmanagerklasse wird die Berechnung eines Kennfeldes gesteuert und
    die Ausgabe der Kennfeldpunkte generiert.

    \author Andrea Bader
*/
class kennfeld_manager
{
public:

	//! Konstruktor
	/*!
		Baut ein Objekt des Kennfeldmanager mit allen notwendigen
		Initalisierungen.

	    \author Andrea Bader
	*/
	kennfeld_manager();


	//! Berechnung fortsetzen?
	/*!
		Diese Methode frägt ab, ob noch weitere Betriebspunkte einer Kennlinie
		oder eine weitere Kennlinie zu berechnen sind.

	    \author Andrea Bader
	*/
	bool hatWeiterePunkte();


	//! Zustandsübergänge bei Fehler
	/*!
	    Diese Methode steuert die Zustandsübergänge beim Auftreten eines Fehlers und
		passt die Massenstromvorgabe entsprechend an.

		\param loeser_result_code  aktueller Fehlerzustande

	    \author Andrea Bader
	*/
	void fehlerMakieren(loeser_result_code rc);


	//! Zustandsübergänge bei erfolgreicher Berechnung
	/*!
		Diese Methode steuert die Zustandsübergänge bei der erfolgreichen Berechnung
		und passt die Massenstromvorgabe entsprechend an.

	    \author Andrea Bader
	*/
	void meldeOK();


	//! Speichern der Berechungsergebnisse!
	/*!
	    Die Resultate der aktuellen Berechnung werden aus dem datenObjekt geholt
	    und in den Ausgabestring gelegt.

	    \param i aktuelle Stromlinie
	    \param j aktuelle Quasiorthogonale
	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	void ablegenKennfeldPunkt();


	//! Einsortieren der Ergebnisse Ausgabestring!
	/*!
		Jedes neue Berechnungsergebnis auf einer Kennlinie wird entsprechend
		des Massenstroms in aufsteigender Reihenfolge in den Ausgabestring eingefügt.
		Diese aufsteigende Sortierung erleichtert die Weiterverarbeitung in Excel.

		\param kennfeld_resultat aktuelles Berechnungsergebniss

	    \author Andrea Bader
	*/
	void einfuegenInSortierteListe(kennfeld_resultat neues_element);


	//! Ausgabe der Kennfeldberechnung
	/*!
		Der sortierte Ausgabestring wird nach der Berechnung einer kompleten Kennlinie
		in dieser Methode in die Ausgabedatei geschrieben.

	    \author Andrea Bader
	*/
	void ausgebenListe();


//! ----------- private Methoden ---------------------------------
private:
	//! Betriebspunkte
	/*!
		Gibt zurück, ob noch weiter Massenstromvorgaben zu berechnen sind.

	    \author Andrea Bader
	*/
	bool hatWeitereMassenstromWerte();


	//! Massenstromrückgabe
	/*!
		Gibt den aktuellen Massenstrom zurück.

	    \author Andrea Bader
	*/
	double aktuellerMassenstrom();


	//! Kennlinie
	/*!
		Gibt zurück, ob noch weitere Kennlinien (Umfangsgeschwindigkeiten)
		zu berechnen sind.

	    \author Andrea Bader
	*/
	bool hatWeitereUmfangsGeschw();


	//! Umfangsgeschwindigkeitrückgabe
	/*!
		Gibt die aktuelle Umfangsgeschwindigkeit zurück.

	    \author Andrea Bader
	*/
	double aktuelleUmfangsGeschw();


	//! Ausgeben des Zustandes als Text
	/*!
		Zur Überwachung der Kennfeldberechnung können die Zustandsübergänge
		als Text ausgegeben werden.

		\param der zustand
		\return der text

		\author Andrea Bader
	*/
	string zustandString(zustand_kennlinie z);


//! -------------- Eigenschaften -----------------------
	bool weitere;					// Speichert lokal die Information ob weitere Zustände vorhanden sind.
	int indexUmfangGeschw;				// Laufindex über die Liste der Umfangsgeschwindigkeiten

	double mitteMassenstrom;			// der nach dem ersten Lauf auf einer neuen Kennlinie ermittelte Massenstrom
	double deltaMassenstrom;			// Schrittweite der Massenstromänderung
	double massenstrom;				// der im aktuellen Zustand vorgegebene Massenstromwert
	unsigned int suchZaehler;			// Zähler der für die Grenzenannäherung eingesetzt wird

	zustand_kennlinie zustand;			// der aktuelle Zustand
	vector<kennfeld_resultat> vErgebnisse;		// Feld zum Ablegen der Resultate, die nach kompletter
							//   Kennlinie nach Sortierung ausgegeben werden

	std::ofstream *kennfeldDatei;			// Datei zum Ausgeben der berechneten Kennlinienpunkte
};

#endif
