/***************************************************************************
 *   Copyright (C) 2009-2010 Andrea Bader (bader.andrea@gmx.de)            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Andrea Bader
*/

#include <time.h>
#include "loeser_denton.h"
#include "ausgabe.h"

#include "ableitungshelfer.h"

loeser_denton::loeser_denton(geometrie *geometrieObjektEingabe)
	: dglDateiAusgabe(daten::getInstance()->getworkdir())
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	quasiorthogonale	= &datenObjekt->quasiorthogonale;
	geometrieObjekt 	= geometrieObjektEingabe;

	mittlereSL =  datenObjekt->anzahlStromlinien / 2;
	d_wm_2.resize(datenObjekt->anzahlStromlinien, 0.0);

	// error Statistik
	error_max   = 0.0;
	error_max_i = 0;
	error_max_j = 0;
	error_max_massIter = 0;

	zwangsbelgungsWarnung = false;
}



void loeser_denton::schleife ( )
{

	//! Start der Stoppuhr
	time_t start,end;
	double dif;
	time (&start);


	unsigned int errorOutputIndex = 5;	// alle x mal wird die Fehlerausgabe der iter-schleife rausgeschrieben
	double last_error_max = 1000.0; 	// in Prozent, mit einem sehr grossen Startfehler vorbelegt

	initialisiere();
	//! Schleife bis zum Erreichen der maximalen Anzahl an Durchläufen:
	for (int iter=0; iter < datenObjekt->anzahlDurchlaeufe ; iter++)
	{
		

		// Fehlerverlauf:  Iterationszaehler
		if ((iter % errorOutputIndex) == 0) // jeder Durchlauf der äußeren Schleife wird rausgeschreiben, kann hochgesetzt werden
		{
			//BB: funktioniert nicht
// 			dglDateiAusgabe.getVerlaufStream() << "iter: " << setw(3) << iter << "  ";
		}

		// DGL Ausgabe: Iterationszähler
		if (datenObjekt->dglAusgabe)
		{
			dglDateiAusgabe.getStream() << endl;
			dglDateiAusgabe.getStream() << " Äußere Schleife Zähler " << iter << endl;
		}

		//--------- Mittlere Schleife ---------------------------------------
		for ( int k=0;k<datenObjekt->anzahlGitter;k++ )  //laufe über alle Gitter
		{
			schleife_ueber_QOn(k);  //laufe über alle QO

			// Fehlerverlauf
			//BB: funktioniert nicht
// 			if ((iter % errorOutputIndex) == 0) {	erstelleFehlerverlaufAusgabe(); }
		}

		// --------- nach Mittlerer Schleife --------------------------------

		datenObjekt->konvergenz_Gefahr(error_max);

		//! - Konvergenzproblem bei Kennfeldberechnung -> break
		if (datenObjekt->rechneKennfeld && datenObjekt->loeser_resultCode > LRESULT_unknown )
		{
			break;
		}

		//! - Anpassen der Stromlinienlage
		stromlinienObjekt.anpassenStromlinienlage();

		//! - Konsolenausgabe zum Überwachen der Konvergenz im Normalfall
		if (!datenObjekt->rechneKennfeld && datenObjekt->debug >= 0)
		{
			cout <<" Iteration : " << iter << "\t error_max[%]: "<< error_max <<
					",\t massIter=" << setw(3) << error_max_massIter << endl;
		}

		//! - Abbruchkriterium: konvergente Lösung -> break
		if ( abs(error_max) < datenObjekt->errorMaxToleranz && iter >= 5 ) // 0.01 Prozent und mindestens 5 mal
		{
// 			dglDateiAusgabe.getVerlaufStream() << "iter: " << setw(3) << iter << "  ";

			//BB deaktiviert. funktioniert nicht
// 			erstelleFehlerverlaufAusgabe();
			datenObjekt->loeser_resultCode = LRESULT_success;
			break;
		}
		//! - Abbruchkriterium: Maximale Zeit überschritten -> break
		time (&end);
		if (difftime (end,start) > datenObjekt->maxTime)
		{
			DEBUG_IMMER("Achtung -> Abbruch da maximale Zeit überschritten wurde!");
			break;
		}

		//--------------- Zurücksetzten  der Fehlervergangenheit ------------------
		last_error_max = error_max;
		error_max = 0.0; error_max_i = 0; error_max_j = 0; error_max_massIter = 0;
		zwangsbelgungsWarnung = false;
		//-------------------------------------------------------------------------
		
		aktualisiere();
		
		if(iter == (datenObjekt->anzahlDurchlaeufe-1))
			{
				DEBUG_IMMER("Achtung -> Abbruch da maxiamle Anzahl der Iterationen erreicht");
			}
			
	}


	//! Stopuhrausgabe
	time (&end);
	dif = difftime (end,start);
	if ( datenObjekt->debug >= 0)
	{
		printf ("Die Berechnung dauerte %.2f Sekunden.\n", dif );
	}
	

	//! Sollte bei konvergierter Lösung noch eine Zwangsbelegung von w_m vorliegen, erfolgt Warnung
	if (zwangsbelgungsWarnung&& datenObjekt->debug >= 0)
	{
		cout << "Warnung Zwangsbelgung von w_m vorhanden." << endl;
	}

}


void loeser_denton::initialisiere()
{
	datenObjekt->setzeQOTyp();
	datenObjekt->setzeUOderOmega();
	stromlinienObjekt.relaxFaktorWilkinson();
	geschwindigkeitObjekt.initialisiere_w_m();


	ableitungObjekt.setze_Geometrieableitungen();
	ableitungObjekt.setze_dm_Ableitung();
	massenStromObjekt.anpassenFaktorMassenStromRoehre();//! - Hochziehen der Stromlinien unterbinden
	dichteObjekt.setze_eintrittstotaldichte ();


	if(datenObjekt->rechneKennfeld)
	{
		wirkungsgradObjekt.setze_optimalwerte_fuer_u();
	}
	if ( datenObjekt->berechneMassenstromVorgabe )
	{
		if(datenObjekt->rechneKennfeld)
		{
			massenStromObjekt.massenstrom_aus_schluckziffer_opt();
		}
		else{
		//! - Massenstromvorgabe aus inzidenzfreier Anströmung an RotorVK berechnet
		massenStromObjekt.berechneAnfangsmassenstrom();
		}
	}
	else
	{
		if (datenObjekt->debug >= 0)
		{
			cout << "      Eingelesene Massenstromvorgabe: " << datenObjekt->massenStromVorgabe << endl;
		}
	}	
	
	geschwindigkeitObjekt.setze_deviation();
	geschwindigkeitObjekt.setzeSchaufelTransparenz();

	//! - Ausgabe des Intialengitters in vorher.cgns zu Kontrollzwecken
	if (!datenObjekt->rechneKennfeld)
	{//! - Erzeugen eins Ausgabeobjekts, um Methode anwenden zu können
		ausgabe ausgabeObjekt;
// 		ausgabeObjekt.ausgabeVorher();
	}
	else //-> kennfeldberechnung
	{
		wirkungsgradObjekt.setze_eta_betriebspunkt();
	}
}


void loeser_denton::aktualisiere()
{
	//! - Relaxation
	geometrieObjekt->bekomme_ny_neu ();  //hier ist keine Nurbsmethode enthalten

	//! - Aktualisieren der Geometrie
	geometrieObjekt->aktualisiereGeometrie (); 			// Aktualisierung der r,z-Werte aus den neuen ny-Werten
	geometrieObjekt->aktualisiereGeometrie_nurbsSL (); 	// aktualisiert nurbs SL;
	// belegt knoteneigenschaften sigma, beta_ru, beta_zu und entropie neu.

	//! - Setzt alle Geometrieableitungen: sinEpsilon, cosEpsilon, sinGamma, cosGamma, Krümmung (1/R_k)
	ableitungObjekt.setze_Geometrieableitungen();
	ableitungObjekt.setze_dm_Ableitung();
	geschwindigkeitObjekt.setze_deviation();
	wirkungsgradObjekt.setze_entropie_aus_eta_polytrop();
}


void loeser_denton::integrale_Daten ( )
{
	druckObjekt.setze_p_tot_rel_m ();
	druckObjekt.setze_Pi_tot_rel ();
	druckObjekt.setze_Pi_tot_rel_m ();
	druckObjekt.setze_Pi_tot_abs();
	druckObjekt.setze_Pi_abs();
	datenObjekt->v_red = massenStromObjekt.berechnereduziertenVolumenstrom();
	datenObjekt->Pi_tot = druckObjekt.berechne_Pi_tot_m();
	datenObjekt->Pi_stat = druckObjekt.berechne_Pi_abs_m();

	geschwindigkeitObjekt.setze_deHaller();
	epsilonObjekt.setze_Stroemungswinkel();

	//zur Ausgabe sollen nach den Laufrad die relativ Geschwindigkeit = absolut Geschwindigkeit gesetzt werden
	//dazu wird folgende Methode aufgerufen, an der Berechnung ändert sich nichts, nur zur Ausgabe.
	geschwindigkeitObjekt.setzeRelativGeschwindigkeitnachRotor();


}

//BB: Schleife abgeändert, da alte Schleife w_m am Gitterübergang doppelt berechnete
void loeser_denton::schleife_ueber_QOn(int k)
{
	for (int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++)
	{
		DEBUG_2(endl << "++++++++++++++++++++++++ QO: j = " << j << "  +++++++++++++++++++++" << endl );

		//! - Transportterme (Drall, Geschwindigkeitsdreieck und Enthalpie)
		updateTransportterme(k, j);
		//! - Zustandsgleichung (Dichte, Druck, Temp.)
		updateZustandsgroessen(k, j);
		if (j==0 && k>0)
		{
			for (int i=0; i<datenObjekt->anzahlStromlinien; i++)
			{
				//! - Gitterübertrag von w_m
				(*knoten)[k][i][j].w_m = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].w_m;
			}
		}
		else
		{
			//! - dgl_schleife()
			dgl_schleife(k, j);
			//! - Innere Schleife: schleife_auf_QO()
			schleife_auf_QO(k, j);
		}
	}
}

// void loeser_denton::schleife_ueber_QOn(int k)
// {
// 	for (int j=0; j<datenObjekt->anzahlQuasiOrthogonale[k]; j++)
// 	{
// 		DEBUG_2(endl << "++++++++++++++++++++++++ QO: j = " << j << "  +++++++++++++++++++++" << endl );
// 
// 		// Transportterme (Drall, Geschwindigkeitsdreieck und Enthalpie)
// 		updateTransportterme(k, j);
// 		// Zustandsgleichung (Dichte, Druck, Temp.)
// 		updateZustandsgroessen(k, j);
// 		// DGL
// 		dgl_schleife(k, j);
// 
// 		// Innere Schleife
// 		schleife_auf_QO(k, j);
// 	}
// }


void loeser_denton::updateTransportterme(int k, int j)
{
	for (int i=0; i < datenObjekt->anzahlStromlinien; i++)
	{
		geschwindigkeitObjekt.bestimmeDrall( k, j,  i);
		geschwindigkeitObjekt.updateGeschwindigkeitsDreieck(k, j, i);
		enthalpieObjekt.setze_enthalpien(k,j,i);
	}
}


void loeser_denton::updateZustandsgroessen (int k, int j)
{
	for (int i=0; i < datenObjekt->anzahlStromlinien; i++)
	{
		//! - Totalenthalpie notwendig -> intern berechnet
		(*knoten)[k][i][j].T = tempObjekt.berechne_T( i,j,k );
                
		//! - Berechne Schallgeschwindigkeit a
		(*knoten)[k][i][j].a = geschwindigkeitObjekt.berechne_schallGeschwindigkeit ( i,j,k );

		//! - p aus Entropiedifferenz und T_tot, T ; p_tot auch belegt
		(*knoten)[k][i][j].p = druckObjekt.berechne_p( i,j,k );

		//! - Berechne rho  -> rho = p/(R*T) -> p vorher berechnen!
		(*knoten)[k][i][j].dichte= dichteObjekt.berechne_dichte ( i,j,k );

		DEBUG_3(
		      " === updateZustandsgroessen === " << endl
		   << " a = " << (*knoten)[k][i][j].a << endl
		   << " p = " << (*knoten)[k][i][j].p << endl
		   << " dichte = " << (*knoten)[k][i][j].dichte  );
	}
}



void loeser_denton::dgl_schleife(int k, int j)
{
	// ===================== Beschriftung ================================
	if (datenObjekt->dglAusgabe)
	{
		// Beschriftung fuer DGL ausgabe in Datei -- siehe Klasse DGLAusgabe
		dglDateiAusgabe.getStream() << endl
				<< "k- j-i | "
				<< setw(15) << "DGL"			// 100%
				<< " | "
				<< setw(10) << "sk %" 			// a
				<< setw(10) << "rGGW %" 		// c
				<< setw(10) << "S-Kraft %" 		// d
				<< setw(10) << "R-Kraft %"		// e
				<< " | "
				<< setw(10) << "sk_dwm %"		// a2
				<< setw(10) << "sk_Rk %"		// a1
				<< " | "
				<< setw(10) << "dht_dq %"		// c1
				<< setw(10) << "-T*ds_dq %"		// c2
				<< setw(15)	<< "-f*drcu_dq_2 %"	// c3
				<< " | "
				<< setw(15) << "Kruemmung"
				<< setw(15) << "wmdwm_dm"
				<< endl;
	}
	// ===================================================================


	for (int i=0; i< datenObjekt->anzahlStromlinien-1; i++)
	{

		if (datenObjekt->dglFormulierung == DGL_denton)
		{
			(*knoten)[k][i][j].gradient = dgl_denton(k, j, i);
		}

		else if (datenObjekt->dglFormulierung == DGL_benetschik)
		{
			(*knoten)[k][i][j].gradient = dgl_benetschik( k,j,i );
		}
		else
		{
			cout << "Fataler Fehler, keine dglFormulierung gewählt." << endl;
		}
	}
}

double loeser_denton::dgl_denton (int k, int j, int i)
{
	Knoten& bk = (*knoten)[k][i][j];  //aktueller Berechnungsknoten

	double dgl, sk_dwm, sk_Rk , radialesGGW, schaufelkraft,reibungskraft, sk ;
	double dq = sqrt ( pow2( (*knoten)[k][i+1][j].r - bk.r  ) + pow2( (*knoten)[k][i+1][j].z - bk.z  ) );

	double tanalpha 	= bk.tan_alpha;
	double sinpsi   	= bk.sin_psi;
	double cospsi  		= bk.cos_psi;
	double kruemmung	= bk.kruemmung;
	double T 		= bk.T;
 	double r 		= bk.r;
 	double wm 		= bk.w_m;


	//! - \f$ w_m \cdot \frac{\partial w_m}{\partial m} \f$ Ableitungen in m-Richtung -> update in aktualisiere der äußeren Schleife
	double wmdwm_dm 	= bk.wmdwm_dm;
	double drcu_dm 		= bk.rcu_dm;
	//! - \f$ \frac{\partial s}{\partial m} \f$ Direkte Berechnung, da Entropie nur in äußerer Schleife aktualisiert wird
	double ds_dm = ableitungshelfer::bildeEinfachRueckwaertsAbleitung_dm(k,j,i,KE_entropie);

	//! - \f$ \frac{\partial s}{\partial q} \f$ Ableitungen in q-Richtung -> direkte Berechnung
	double ds_dq  = ableitungshelfer::bildeEinfachVorwaertsAbleitung_dq(k,j,i,KE_entropie);
	double dht_dq = ableitungshelfer::bildeEinfachVorwaertsAbleitung_dq(k,j,i, KE_totalenthalpie);
	double drcu_dq_2 = ableitungshelfer::bildeEinfachVorwaertsAbleitung_dq(k,j,i,KE_rc_u_2);

//========================================================================================================================
	//! \f[ sk_{\partial w_m} = w_m \cdot \frac{\partial w_m}{\partial m} \cdot \cos \psi \f]
 	sk_dwm = wmdwm_dm * cospsi;
	//! \f[ sk_{Rk} = w_m^2 \cdot Kr\ddot{u}mmung \cdot \sin \psi\f]
 	sk_Rk  = pow2 (wm) * kruemmung * sinpsi;
	//! \f[ sk = sk_{\partial w_m} + sk_{Rk} \f]
 	sk     = sk_dwm + sk_Rk;
	//! - Radiales GGW =\f[ \frac{\partial h_t}{\partial q} - T\cdot\frac{\partial s}{\partial q} - \frac{1}{2 r^2} \frac{\partial (rc_u)}{\partial q_2} \f]
	radialesGGW = dht_dq - T * ds_dq - 1/(2*pow2(r)) * drcu_dq_2;

	//! - Beschaufelter Raum, ohne Vorderkante und Hinterkante:
	if (!(*knoten)[k][i][j].unbeschaufelt && !(*quasiorthogonale)[k][j].vorderkante &&!(*quasiorthogonale)[k][j].hinterkante )
	{
		//! \f[ F_S = \tan \alpha \frac{w_m}{r} \frac{\partial (rc_u)}{\partial m} \f]
		schaufelkraft = tanalpha * wm/r * drcu_dm ;  //Schaufelkraft hat geringen Einfluß!
	}
	else
	{
		schaufelkraft = 0.0;
	}

	//! - Reibungskraft hat geringen Einfluß: \f$ F_{R} = \cos \psi \cdot T \cdot \frac{\partial s}{\partial m} \f$
	reibungskraft = cospsi * T * ds_dm;

//=======================================================================================================================
	//! - DGL Ergebnis = \f$ sk + radiales GGW + F_S + F_R \f$
	double dgl_ergebnis = sk + radialesGGW + schaufelkraft + reibungskraft ;

	//! - Erweitern, zur passenden Weiterverarbeitung: \f$ dgl = 2 \cdot \partial q \cdot \f$ DGL Ergebnis
	dgl = 2 * dq * dgl_ergebnis;

//========================================================================================================================


	//! - Ausgabe der DGL Terme in Ausgabefile DGL.txt
	if(datenObjekt->dglAusgabe)
	{
		dglDateiAusgabe.getStream().precision (2);    					// 5 stellen nach dem Komma
		dglDateiAusgabe.getStream().setf(ios::fixed,ios::floatfield); 	// nicht e-hoch

		double skalierung = dgl_ergebnis / 100;
		if (dgl_ergebnis == 0) {
			skalierung = 1.0;
		}

		dglDateiAusgabe.getStream()
					<< k << "-" << setw(2) << j << "-" << i << " | "
					<< setw(15) << dgl_ergebnis					// 100%
					<< " | "
					<< setw(10) << sk 			/ skalierung	// a
					<< setw(10) << radialesGGW 	/ skalierung	// c
					<< setw(10) << schaufelkraft 	/ skalierung	// d
					<< setw(10) << reibungskraft/ skalierung	//e
					<< " | "
					<< setw(10) << sk_dwm      	/ skalierung	// a2
					<< setw(10) << sk_Rk	   	/ skalierung	// a1
					<< " | "
					<< setw(10) << dht_dq      	/ skalierung 				// c1
					<< setw(10) << - T * ds_dq	/ skalierung 				// c2
					<< setw(15)	<< - 1/(2*pow2(r)) * drcu_dq_2 / skalierung	// c3
					<< " | "
					<< setw(15) << kruemmung	//
					<< setw(15) << wmdwm_dm		//
					<< endl;
	}

	return dgl;
}


double loeser_denton::dgl_benetschik (int k, int j, int i)
{
	double dgl, sk_dwm, sk_Rk , radialesGGW, schaufelkraft,reibungskraft, sk ;
	double dq = sqrt ( pow2( (*knoten)[k][i+1][j].r - (*knoten)[k][i][j].r  ) +
					   pow2( (*knoten)[k][i+1][j].z - (*knoten)[k][i][j].z  ) );

	double nq_nm 	= (*knoten)[k][i][j].tan_alpha/(*knoten)[k][i][j].cot_beta;
	double sinpsi 	= (*knoten)[k][i][j].sin_psi ;
	double cospsi 	= (*knoten)[k][i][j].cos_psi;
	double kruemmung= (*knoten)[k][i][j].kruemmung;
	double T 		= (*knoten)[k][i][j].T;
	double r 		= (*knoten)[k][i][j].r;
	double wm 		= (*knoten)[k][i][j].w_m;
	double omega 	= datenObjekt->omega[k];

	//! - \f$ w_m \cdot \frac{\partial w_m}{\partial m} \f$ Ableitungen in m-Richtung -> update in aktualisiere der äußeren Schleife
	double wmdwm_dm = (*knoten)[k][i][j].wmdwm_dm;
	double drcu_dm = (*knoten)[k][i][j].rcu_dm;
	double drcu_2_dm = (*knoten)[k][i][j].rcu_2_dm;
	//! - \f$ \frac{\partial s}{\partial m} \f$ Direkte Berechnung, da Entropie nur in äußerer Schleife aktualisiert wird
	double ds_dm = ableitungshelfer::bildeEinfachRueckwaertsAbleitung_dm(k,j,i,KE_entropie);

	//! - \f$ \frac{\partial s}{\partial q} \f$ Ableitungen in q-Richtung -> direkte Berechnung
	double ds_dq  = ableitungshelfer::bildeEinfachVorwaertsAbleitung_dq(k,j,i,KE_entropie);
	double dht_dq = ableitungshelfer::bildeEinfachVorwaertsAbleitung_dq(k,j,i, KE_totalenthalpie);
	double drcu_dq_2 = ableitungshelfer::bildeEinfachVorwaertsAbleitung_dq(k,j,i,KE_rc_u_2);

 	//========================================================================================================================
	//! \f[ sk_{\partial w_m} = w_m \cdot \frac{\partial w_m}{\partial m} \cdot \cos \psi \f]
 	sk_dwm = wmdwm_dm * cospsi;
	//! \f[ sk_{Rk} = w_m^2 \cdot Kr\ddot{u}mmung \cdot \sin \psi\f]
 	sk_Rk  = pow2 (wm) * kruemmung * sinpsi;
	//! \f[ sk = sk_{\partial w_m} + sk_{Rk} \f]
 	sk     = sk_dwm + sk_Rk;
	//! - Radiales GGW =\f[ \frac{\partial h_t}{\partial q} - T\cdot\frac{\partial s}{\partial q} - \frac{1}{2 r^2} \frac{\partial (rc_u)}{\partial q_2} \f]
 	radialesGGW = dht_dq - T * ds_dq - 1/(2*pow2(r)) * drcu_dq_2;


 	//wirkungsbereich funktioniert so
 	if ((*knoten)[k][i][j].unbeschaufelt || (*quasiorthogonale)[k][j].vorderkante || (*quasiorthogonale)[k][j].hinterkante )
 	{
 		nq_nm = 0.0;
 	}
 	//! - Schaufelkraft:
 	//! \f[ F_S = \frac{\tan \alpha}{\cot \beta} \frac{1}{2 r^2} \frac{\partial (rc_{u2})}{\partial m} - \omega  \frac{\partial (rc_{u})}{\partial m} \f]
	schaufelkraft = nq_nm * (  1/(2*pow2(r)) * drcu_2_dm  -    omega * drcu_dm );
	//! - Reibungskraft:
 	//! \f[ F_R = \frac{\tan \alpha}{\cot \beta} \cdot T \frac{\partial s}{\partial m}\f]
 	reibungskraft = nq_nm * T * ds_dm;

	 //=======================================================================================================================
	//! - DGL Ergebnis = \f$ sk + radiales GGW + F_S + F_R \f$
	double dgl_ergebnis = sk + radialesGGW + schaufelkraft + reibungskraft ;

	//! - Erweitern, zur passenden Weiterverarbeitung: \f$ dgl = 2 \cdot \partial q \cdot \f$ DGL Ergebnis
	dgl = 2 * dq * dgl_ergebnis;

	//========================================================================================================================

	if(datenObjekt->dglAusgabe)
	{
		// Ausgabe der DGL-Terme in DGL.txt
		dglDateiAusgabe.getStream().precision (2);    					// 5 stellen nach dem Komma
		dglDateiAusgabe.getStream().setf(ios::fixed,ios::floatfield); 	// nicht e-hoch

		double skalierung = dgl_ergebnis / 100;
		if (dgl_ergebnis == 0) {
			skalierung = 1.0;
		}

		dglDateiAusgabe.getStream()
					<< k << "-" << setw(2) << j << "-" << i << " | "
					<< setw(15) << dgl_ergebnis					// 100%
					<< " | "
					<< setw(10) << sk 			/ skalierung	// a
					<< setw(10) << radialesGGW 	/ skalierung	// c
					<< setw(10) << schaufelkraft 	/ skalierung	// d
					<< setw(10) << reibungskraft/ skalierung	//e
					<< " | "
					<< setw(10) << sk_dwm      	/ skalierung	// a2
					<< setw(10) << sk_Rk	   	/ skalierung	// a1
					<< " | "
					<< setw(10) << dht_dq      	/ skalierung 				// c1
					<< setw(10) << - T * ds_dq	/ skalierung 				// c2
					<< setw(15)	<< - 1/(2*pow2(r)) * drcu_dq_2 / skalierung	// c3
					<< " | "
					<< setw(15) << kruemmung	//
					<< setw(15) << wmdwm_dm		//
					<< endl;
	}


	return dgl;
}





void loeser_denton::schleife_auf_QO(int k, int j)
{
	double mf_ratio;

	for (int i=0; i<datenObjekt->anzahlStromlinien; i++)
	{
		//! - Schreibe auf die Arbeitsvariable "w_m_neu";
		(*knoten)[k][i][j].w_m_neu = (*knoten)[k][i][j].w_m;
	}

	//! - dwm^2 auf die mittelere SL skalierte, später vereinfachter Zugriff!
	// gradient entspricht dwm^2
	for (int i=mittlereSL; i < datenObjekt->anzahlStromlinien -1; i++ )  // mitte bis gehaeuse
	{
		d_wm_2[i+1] = d_wm_2[i] + (*knoten)[k][i][j].gradient;
	}
	for (int i=mittlereSL; i>0; i-- ) // mitte bis nabe
	{
		d_wm_2[i-1] = d_wm_2[i] - (*knoten)[k][i-1][j].gradient;
	}

	// ================= Massenstromiteration ======================
	//! - Massenstromiteration
	int massIter=0;
	for (massIter=0; massIter < datenObjekt->anzahlMassDurchlaufe; massIter++)
	{
		berechne_wm_auf_QO( j, k );

		bool massenStromOk = massenStromLoeser(j, k, mf_ratio);
		if (massenStromOk)	// massenstrom entspricht massenstrom_vorgabe
		{ break; }
	}
	// =============================================================

	for (int i=0; i< datenObjekt->anzahlStromlinien; i++)
	{
		//! - Anpassung der w_m Verteilung falls Massenstromlöser verfrüht abbricht.
		(*knoten)[k][i][j].w_m_neu = (*knoten)[k][i][j].w_m_neu / mf_ratio;

		DEBUG_3(" Stelle [k][i][j]  " << "["<<k <<"]["<<i<<"]["<<j<<"]" <<	" w_m_neu = " << (*knoten)[k][i][j].w_m_neu  );
	}


	//! - Durchschnittlicher Fehler bestimmen -> Vorbereitung für Dämpfung
	double avg_error = mittlererFehler(k, j, massIter);

	for (int i=0; i<datenObjekt->anzahlStromlinien; i++)
	{
		double w_m_gedaempft =
				(*knoten)[k][i][j].w_m * (1.0 + datenObjekt->damp_wm * avg_error) +
				datenObjekt->relaxfac * ((*knoten)[k][i][j].w_m_neu - (*knoten)[k][i][j].w_m);

		(*knoten)[k][i][j].w_m = w_m_gedaempft;

		DEBUG_2(" Stelle [k][i][j]  " << "["<<k <<"]["<<i<<"]["<<j<<"]" <<	" w_m = " << (*knoten)[k][i][j].w_m <<" gedämpft");
	}
}





bool loeser_denton::massenStromLoeser ( int j, int k, double& mf_ratio )
{
	massenStromObjekt.initialisiere_nurbsMassenStromDichte_sinPsi_neu(j,k);
	massenStromObjekt.setzeMassenStrom(j,k);

	mf_ratio = (*quasiorthogonale)[k][j].massenStrom / datenObjekt->massenStromVorgabe;

	//! - Wenn w_m die Massenstromvorgabe nicht erfüllt -> w_m auf mittlerer Stromlinie anpassen!
	if ((mf_ratio <= ( 1-datenObjekt->toleranz )) || (mf_ratio >= ( 1+datenObjekt->toleranz )) )
	{
		double wm_alt = (*knoten)[k][mittlereSL][j].w_m_neu;
		double wm_temp = wm_alt/(mf_ratio*mf_ratio);
		double daempfung = datenObjekt->damp_wm;

		(*knoten)[k][mittlereSL][j].w_m_neu = wm_alt +  daempfung * (wm_temp - wm_alt);

		DEBUG_3("angepasstes wm-Mitte = " << (*knoten)[k][mittlereSL][j].w_m_neu);

		return false;
	}
	else
	{
		return true;
	}
}

void loeser_denton::berechne_wm_auf_QO(int j, int k)
{
	double w_m_neu = 0.0;
	double w_m_mitte = (*knoten)[k][mittlereSL][j].w_m_neu;

	vector<bool> zwangsbelegungFlags(datenObjekt->anzahlStromlinien, false);
	bool zwangsbelegung = false;

	for (int i=0; i<datenObjekt->anzahlStromlinien; i++)
	{
		//! - d_wm_2 [i] entspricht dwm^2 im Unterschied zur mittleren SL
		w_m_neu = sqrt (d_wm_2 [i] + pow2 (w_m_mitte));

		if (isnan (w_m_neu))  // true, wenn w_m_neu = not a number, wegen einer neg. Wurzel
		{
			w_m_neu = 0.05;   // belegung notwendig, aber wird durch w_min überschrieben (kleiner als w_min)
			zwangsbelegung = true;
			zwangsbelegungFlags[i] = true;
			zwangsbelgungsWarnung = true;
		}

		// Rückströmung verbieten ->w_min = 0.05 * w_m (mitte)
		w_m_neu = max ( w_m_neu, w_m_mitte*0.05);
		(*knoten)[k][i][j].w_m_neu = w_m_neu;

		DEBUG_2(" w_m_neu (i+1)= " << w_m_neu );
	}

	//! - Falls eine Zwangsbelegung aufgetreten ist, wird für die zwangsbelegten Knoten wm aus einer Gerade approximiert.
	if (zwangsbelegung)
	{
		int i_start = -1;
		int i_ende  = -1;

		for (int i=0; i<datenObjekt->anzahlStromlinien; i++)
		{
			if (zwangsbelegungFlags[i] == false) { continue; }
			// else: sonst weitermachen
			if (i_start < 0) { i_start = i; }
			if (i_ende  < i) { i_ende = i; }
		}

		if ( (i_ende - i_start) == 0)
		{
			// fertig, da nur ein wm zwangsbelegt - keine zwischenpunkte -> Gerade unnötig
			return;
		}

		i_ende = i_ende + 1; // erstes nicht zwangsbelegtes Element! Anker!

		// fehler abfangen
		if (i_ende >= datenObjekt->anzahlStromlinien - 1) {
			return;
		}

		Vector_HPoint2Dd punkte (2) ;  //Kontrollpunkte
		NurbsCurve_2Dd kurve;		   //Ausgleichsgerade

		punkte[0] = HPoint2Dd( (*knoten)[k][i_start][j].z, (*knoten)[k][i_start][j].r, (*knoten)[k][i_start][j].w_m_neu);
		punkte[1] = HPoint2Dd( (*knoten)[k][i_ende][j].z,  (*knoten)[k][i_ende][j].r,  (*knoten)[k][i_ende ][j].w_m_neu);
		kurve.globalInterpH ( punkte, 1);	// grad = 1, da nur zwei Kontrollpunkte vorhanden

		Knoten& startKnoten = (*knoten)[k][i_start][j];
		Knoten& endeKnoten  = (*knoten)[k][i_ende ][j];

		// ny aus pythagoras (minDist nicht zielfuehrend)
		double dz = startKnoten.z - endeKnoten.z;
		double dr = startKnoten.r - endeKnoten.r;
		double lenTotal = sqrt(pow2(dz) + pow2(dr));

		// iteration ueber alle Zwangsbelegungen
		for (int i_approx=i_start + 1; i_approx < i_ende ; i_approx++)
		{
			Knoten& workKnoten = (*knoten)[k][i_approx][j];
			double ny_temp;

			// ny_temp ist Länge des Vektors
 			dz = startKnoten.z - workKnoten.z;
 			dr = startKnoten.r - workKnoten.r;
 			double len = sqrt(pow2(dz) + pow2(dr));

 			ny_temp = len / lenTotal;

 			double wm_approx = kurve.hpointAt(ny_temp).w();

 			DEBUG_3("Zwangsbelegung --> wm_approx = "<< wm_approx );
 			(*knoten)[k][i_approx][j].w_m_neu = wm_approx;
		}
	}
}


double loeser_denton::mittlererFehler(int k, int j, int massIter)
{
	double sum_error = 0.0;

	for (int i=0; i<datenObjekt->anzahlStromlinien; i++)
	{
		//! Gewichtung des Fehlers nach Denton-Code
		double faktor = 1.0;
		if (i==0 || i==datenObjekt->anzahlStromlinien-1)  //! Obere und untere Stromlinie werden nur zur Hälfe gewichtet.
		{ faktor = 0.5; }
		else
		{ faktor = 1.0; }

		double error = ((*knoten)[k][i][j].w_m_neu / (*knoten)[k][i][j].w_m) - 1.0;
		sum_error = sum_error + error * faktor;

		//! error_max in Prozent
		if ( fabs(error)*100 > fabs(error_max) ) {
			error_max   = error * 100;
			error_max_i = i;
			error_max_j = j;
			error_max_massIter = massIter;
		}

		//! Error in Konteneigenschaft übertragen (für cgns-ausgabefile)
		(*knoten)[k][i][j].error = error;
	}
	double avg_error = sum_error / (datenObjekt->anzahlStromlinien-1);
	return avg_error;
}


void loeser_denton::erstelleFehlerverlaufAusgabe()
{
	if (datenObjekt->dglAusgabe)
	{
		//Fehlerverluaf Ausgabe
		dglDateiAusgabe.getStream().precision (5);    					// 5 stellen nach dem Komma
		dglDateiAusgabe.getStream().setf(ios::fixed,ios::floatfield); 	// nicht e-hoch
	}
	dglDateiAusgabe.getVerlaufStream() <<
			"  error_max: "  << setw(25) << error_max <<
			" , bei j=" << setw(3) << error_max_j <<
			" , i=" <<  setw(3) << error_max_i <<
			" , massIter=" << setw(3) << error_max_massIter <<
			endl;
}




void loeser_denton::zuruecksetzen ( )
{
	DEBUG_IMMER(endl<<"************************ Auf Anfangswert setzen ****************************************"<<endl);

	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				(*knoten)[k][i][j].r = (*knoten)[k][i][j].r_org;
				(*knoten)[k][i][j].z = (*knoten)[k][i][j].z_org;
				(*knoten)[k][i][j].beta_ru = (*knoten)[k][i][j].betaru_org;
				(*knoten)[k][i][j].beta_zu = (*knoten)[k][i][j].betazu_org;
				(*knoten)[k][i][j].sigma = (*knoten)[k][i][j].sigma_org;
				(*knoten)[k][i][j].entropie = (*knoten)[k][i][j].entropie_org;
			}

		}
	}
	datenObjekt->zuruecksetzenLaufzeitDaten();
}
