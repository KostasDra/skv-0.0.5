/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm
*/



#include "temperatur.h"
#include <cmath>


temperatur::temperatur()
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	quasiorthogonale 	= &datenObjekt->quasiorthogonale;
}

double temperatur::berechne_T ( int i, int j, int k )
{
	if ( datenObjekt->debug >1 )
	{
		cout << "T(i,j,k) = " << (*knoten)[k][i][j-1].T * exp ( ( (*knoten)[k][i][j].entropie-(*knoten)[k][i][j-1].entropie + datenObjekt->R *log ( (*knoten)[k][i][j].p /(*knoten)[k][i][j-1].p ) ) /(*knoten)[k][i][j].cp ) << endl;
		cout << "T(i,j-1,k) = " <<(*knoten)[k][i][j-1].T << endl;
		cout << "delta_entropie = " <<(*knoten)[k][i][j].entropie-(*knoten)[k][i][j-1].entropie << endl;
		cout << "R = " <<datenObjekt->R << endl;
		cout << "cp = " <<(*knoten)[k][i][j].cp << endl;
		cout << "p_j/p_j-1 = " <<(*knoten)[k][i][j].p /(*knoten)[k][i][j-1].p << endl;
		cout << "log (p) = " <<log ( (*knoten)[k][i][j].p /(*knoten)[k][i][j-1].p ) << endl;

		cout << "e^A = " <<exp ( ( (*knoten)[k][i][j].entropie-(*knoten)[k][i][j-1].entropie +datenObjekt->R *log ( (*knoten)[k][i][j].p /(*knoten)[k][i][j-1].p ) ) /(*knoten)[k][i][j].cp ) << endl;
	}

	//! \f[	T(i,j,k) = T(i,j-1,k)\cdot \exp\left(\frac{s(i,j,k)-s(i,j-1,k)+R\cdot\log\frac{p(i,j,k)}{p(i,j-1,k)}}{c_p}\right)	\f]
        //if (datenObjekt->backend
	double T_value = (*knoten)[k][i][j-1].T * exp ( ( (*knoten)[k][i][j].entropie-(*knoten)[k][i][j-1].entropie + datenObjekt->R *log ( (*knoten)[k][i][j].p / (*knoten)[k][i][j-1].p ) ) /(*knoten)[k][i][j].cp );

/*	if ( std::isinf(T_value) || std::isnan(T_value))
	{
	T_value = 293.15;
	//cout << "T = " << T_value << endl;
	}
	else
	{
*/
	(*knoten)[k][i][j].update_cp(T_value);
	//cout << "____________________________________________" << endl;
	//cout << "(*knoten)[" << k << "][" << i << "][" << j << "].T = " << T_value << endl;
	//cout << "p_j = " << (*knoten)[k][i][j].p  << "\t" << "p_j-1 =" << (*knoten)[k][i][j-1].p << endl;
	//cout << "p_j/p_j-1 = " << (*knoten)[k][i][j].p /(*knoten)[k][i][j-1].p << endl;
	//cout << "log [p(j)/p(j-1)] = " <<log ( (*knoten)[k][i][j].p /(*knoten)[k][i][j-1].p ) << endl;

	return T_value;
//	}
}


void temperatur::setze_T_tot_abs ( )
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
			{
				if ( j > 0 || k > 0 )
				{
					(*knoten)[k][i][j].T_tot_abs = berechne_T_tot_abs ( i,j,k );
				}
			}
		}
	}
}



void temperatur::setze_T_tot_abs_m ( )
{
	double Tm;
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			Tm = 0.0;
			for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
			{
				//! \f[	T_m = \sum_i f(i)\cdot\dot{m}_{Vorgabe}\frac{T_{tot,abs}(i,j,k)+T_{tot,abs}(i+1,j,k)}{2}	\f]
				Tm += datenObjekt->massenStromVorgabe *datenObjekt->faktorMassenStromRoehre[i]
					* ( (*knoten)[k][i][j].T_tot_abs + (*knoten)[k][i+1][j].T_tot_abs ) * 0.5;
			}
			//! \f[	T_{tot,abs,m} = \frac{T_m}{\dot{m}_{Vorgabe}}	\f]
			//! Wobei \f$ f(i) \f$ der Faktor für die Massenstromröhre ist, siehe daten.faktorMassenStromRoehre .
			(*quasiorthogonale)[k][j].T_tot_abs_m = Tm/datenObjekt->massenStromVorgabe;
		}
	}
}


/*
void temperatur::umrechnen ( )
{
	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
	{
		// o.b.: funktioniert eigentlich nur, falls omega[0]==0, da ansonsten c und w unbekannt sind; bzw. auch w und beta
		if ( abs(datenObjekt->omega[0]) > 0 )
		{
// 			(*knoten)[0][i][0].T_tot_rel = (*knoten)[0][i][0].T_tot_abs-0.5*( pow2( (*knoten)[0][i][0].c ) - pow2( (*knoten)[0][i][0].w ))/((*knoten)[0][i][0].cp);

			(*knoten)[0][i][0].T_tot_rel = (*knoten)[0][i][0].T_tot_abs-0.5*( pow2( (*knoten)[0][i][0].r * datenObjekt->omega[0] ) + 2.0 * datenObjekt->omega[0] * (*knoten)[0][i][0].r * (*knoten)[0][i][0].w_m * cot( (*knoten)[0][i][0].beta ) )/((*knoten)[0][i][0].cp);
		}
		else
		{
			(*knoten)[0][i][0].T_tot_rel = (*knoten)[0][i][0].T_tot_abs; //falls omega == 0
		}
	}
}
*/