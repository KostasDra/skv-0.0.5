/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option); any later version.                                  *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DRUCK_H
	#define DRUCK_H

#include <cmath>

#include "daten.h"
#include "geschwindigkeit.h"


//! Klasse für die Druckberechnung.
/*!
Diese Klasse ist für die Berechnung der verschiedenen Drücke zuständig.
\author Oliver Borm
*/
class druck
{
	private:
		daten 			*datenObjekt;
		geschwindigkeit 	geschwindigkeitObjekt;
		v3Knoten		*knoten;
		v2Quasiorthogonale	*quasiorthogonale;
		vStromlinie		*stromlinie;
	public:
		druck();


		//! Berechnung des relativen Totaldrucks
		/*!
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Oliver Borm
		*/
		double berechne_p_tot_rel ( int i, int j, int k );

		//! Setzt relativen Totaldruck
		/*!
		    \author Oliver Borm
		*/
		void setze_p_tot_rel ( );

		//! Berechnung des massengemittelten relativen Totaldrucks
		/*!
		    \warning Der relative Totaldruck muss zuerst berechnet werden!
		    \author Oliver Borm
		*/
		void setze_p_tot_rel_m ( );

		
		//! Berechnung des massengemittelten relativen Totaldruckverhältnisses
		/*! 
		    \warning Der massengemittelte relative Totaldruck muss zuerst berechnet werden!

		    \author Oliver Borm
		*/
		void setze_Pi_tot_rel_m ( );


		//! Berechnung des relativen Totaldruckverhältnisses
		/*!
		    \warning Der relative Totaldruck muss zuerst berechnet werden!

		    \author Oliver Borm
		*/
		void setze_Pi_tot_rel ( );

		
		//! Setzt den absoluten Totaldruck.
		/*!
		    \author Oliver Borm
		*/
		void setze_p_tot_abs ( );



		//! Setzt den massengemittelten absoluten Totaldruck.
		/*!
		    \warning Der relative Totaldruck muss zuerst berechnet werden!
		    \author Oliver Borm
		*/
		void setze_p_tot_abs_m ( );



		//! Setzen des massengemittelten absoluten Totaldruckverhältnisses.
		/*!
		    \warning Der massengemittelte relative Totaldruck muss zuerst berechnet werden!
		    \author Oliver Borm
		*/
		void setze_Pi_tot_abs_m ( );



		//! Setzen des absoluten Totaldruckverhältnisses entlang der Stromlinien.
		/*!
		    \warning Der relative Totaldruck muss zuerst berechnet werden!
		    \author Oliver Borm
		*/
		void setze_Pi_tot_abs ( );



		//! Berechnung des kritischen Drucks.
		/*!
		    Berechnet den Wert des statischen Drucks bei \f$ Ma=1 \f$:
		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Oliver Borm
		*/
		double berechne_p_krit ( int i, int j, int k );



		//! Zurücksetzen der Intervallgrenzen des Bisektionsverfahren zur Bestimmung des Drucks.
		/*!
		    \author Oliver Borm
		*/
		void zuruecksetzen_p_alt ( );



//		//! Bestimmung des relativen Totaldrucks aus dem absoluten Totaldruck.
//		/*!
//		    Sollte das allerste Gitter des Rechennetzes bereits rotieren,
// 		    berechnet diese Funktion den relativen Totaldrucks aus dem angegebenen
// 		    absoluten Totaldruck.
// 
// 		    \warning Gültig nur an der ersten Quasiorthogonalen des ersten Gitters!
// 
// 		    \author Florian Mayer
// 		*/
// 		void umrechnen ( );



		//! Berechnung des absoluten Totaldrucks.
		/*!
		    Berechnet den Wert des absoluten Totaldrucks:
		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Florian Mayer
		*/
		double berechne_p_tot_abs ( int i, int j, int k );

		
//######################## Andrea Bader #################################################

		//! Setzen des absoluten Druckverhältnisses
		/*!
			Das Druckverhältnis zwischen Austritt und Eintritt wird auf
			jeder Stromlinie berechnet und gesetzt.

		    \author Andrea Bader
		*/
		void setze_Pi_abs ( );


		//! Berechnung des massenstromgemittelten statischen Druckverhältnisses
		/*!
			Das gesamte Druckverhältnis wird massenstromgemittelt aus
			den einzelnen Druckverhältnissen der Stromlinien berechnet.

			\warning Das Drucverhältniss auf jeder SL muss bereits berechnet
					 sein (setze_Pi_abs ( ))

		    \author Andrea Bader
		*/
		double berechne_Pi_abs_m ( );


		//! Setzen des absoluten Totaldruckverhältnisses
		/*!
			Das absolute Totaldruckverhältnis zwischen Austritt und Eintritt
			wird auf jeder Stromlinie berechnet und gesetzt.

		    \author Andrea Bader
		*/
// 		void setze_Pi_tot_abs ( );


		//! Berechnung des massenstromgemittelten Totaldruckverhältnisses
		/*!
			Das gesamte Totaldruckverhältnis wird massenstromgemittelt aus
			den einzelnen Totaldruckverhältnissen der Stromlinien berechnet.

			\warning Das Totaldrucverhältniss auf jeder SL muss bereits berechnet
					 sein (setze_Pi_tot_abs ( ))

		    \author Andrea Bader
		*/
		double berechne_Pi_tot_m ();

		//! Berechnung des Drucks
		/*!
			Der statische Druck wird aus der Temperatur und dem Entropieunterschied
			berechnet, die Methode zur Berechnung des Totaldrucks wird aufgerufen
			und gesetzt.

		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter

		    \author Andrea Bader
		*/
		double berechne_p (int i, int j, int k);


		//! Berechnung des Totaldrucks
		/*!
			Der Totaldruck wird aus der Totaltemperatur und dem Entropieunterschied
			berechnet.

		    \param i aktuelle Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter

		    \author Andrea Bader
		*/
		double berechne_p_tot( int i, int j, int k );



};

#endif
