#!/usr/bin/env python
# -*- coding: utf-8 -*-

#***************************************************************************
#*   Copyright (C) 2008-2010 by Oliver Borm                                *
#*   oli.borm@web.de                                                       *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 3 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program; if not, write to the                         *
#*   Free Software Foundation, Inc.,                                       *
#*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************

# Author: Oliver Borm
# Date: January 2010

import sys
import os
import getopt
import vtk
import subprocess


### Reading Blocks from vtm Files
def readLegacyVTKFile(path):
	vtkReader = vtk.vtkStructuredGridReader()
	vtkReader.ReadAllScalarsOn()
	vtkReader.SetFileName(path)
	vtkData = vtkReader.GetOutput()
	vtkData.Update()
	return vtkData


def usage():
	print "Usage: scm2vtmb [options] \n"
	print "Options:"
	
	print "  -h, --help"
	print "\t\tShow this help and close programm"
	print "  -p PROJECTDIRECTORY, --projectdir PROJECTDIRECTORY"
	print "\t\tSetting of project directory (optional),"


try:
	opts, args = getopt.getopt(sys.argv[1:], "hp:n:", ["help", "projektdir=","name="])
except getopt.GetoptError:
	usage()
	sys.exit(2)

projektdir = os.getcwd()
name = "scm1"

for o, a in opts:
	if o in ("-p", "--projectdir"):
		projektdir = a
	elif o in ("-n", "--name"):
		name = a
	elif o in ("-h", "--help"):
		usage()
		sys.exit()

Pfad = projektdir

ArbeitsVerzeichnis = os.path.dirname(projektdir)
Projekt = os.path.basename(projektdir)

## Herausschreiben
VTKPfad = Pfad + "/"
MultiBlockAusgabeDatei = Pfad + "/" + name + ".vtmb"

if not os.path.exists(VTKPfad): os.mkdir(VTKPfad)
os.chdir(VTKPfad)

kommando = "cgns_to_vtk " + name + ".cgns"
dummy = subprocess.call(kommando,shell=True)

domainenListe = os.listdir(VTKPfad)

domains = []
for n in range(len(domainenListe)):
	pyFileName, pyExt = os.path.splitext(domainenListe[n])
	if pyExt == ".vtk":
		domains.append(pyFileName)

domains.sort()
numberBlocks = len(domains)

## generate struct Grid for each block
multiDataGrid = vtk.vtkMultiBlockDataSet()

for n in xrange(numberBlocks):
	VTKDateiName = domains[n]
	VTKDatei = VTKPfad + "/" + VTKDateiName + ".vtk"
	block = readLegacyVTKFile(VTKDatei)
	multiDataGrid.SetBlock(n, block)
	if os.path.exists(VTKDatei): os.remove(VTKDatei)

multiDataWriter = vtk.vtkXMLPMultiBlockDataWriter()
multiDataWriter.SetFileName(MultiBlockAusgabeDatei)
multiDataWriter.SetInput(multiDataGrid)
multiDataWriter.Write()

