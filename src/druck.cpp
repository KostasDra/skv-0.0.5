/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm
*/

#include "druck.h"

druck::druck()
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	quasiorthogonale 	= &datenObjekt->quasiorthogonale;
	stromlinie		= &datenObjekt->stromlinie;
}

double druck::berechne_p_tot_rel ( int i, int j, int k )
{
	//!	\f[	p_{tot,rel} = p \cdot \left( 1+\frac{(\kappa-1)}{2} \left(\frac{w}{a}\right)^2 \right)^\frac{\kappa}{\kappa-1}	\f]
	//! Mit \f$ \kappa = \kappa(i,j,k) \f$.
	// 	double kappa = (*knoten)[k][i][j].kappa;
	return (*knoten)[k][i][j].p *pow ( 1.0 + 0.5*( (*knoten)[k][i][j].kappa-1.0 )*
		pow2 ( (*knoten)[k][i][j].w / (*knoten)[k][i][j].a ), ( (*knoten)[k][i][j].kappa/ ( (*knoten)[k][i][j].kappa-1.0 ) ) );
}

void druck::setze_p_tot_rel ( )
{
	//! Schleife über alle Knotenpunkte: 
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
			{
				//! \f$	p_{tot,rel} =	\f$ druck.berechne_p_tot_rel()
				(*knoten)[k][i][j].p_tot_rel = berechne_p_tot_rel ( i,j,k );
			}
		}
	}
}

void druck::setze_p_tot_rel_m ( )
{
	double pm;
	//! Schleife über alle Gitter, QO und Stromröhren(!)
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			pm = 0.0;
			for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
			{
				//! Massenstromgemittelte Summe der reativen Totaldrücke aller Stromröhren
				//! \f[	pm = \sum \dot{m}_{R\ddot{o}hre}\cdot \frac{p_{tot,rel}(i)-p_{tot,rel}(i+1)}{2}	\f]
				pm = pm + datenObjekt->massenStromVorgabe *datenObjekt->faktorMassenStromRoehre[i]
					* ( (*knoten)[k][i][j].p_tot_rel + (*knoten)[k][i+1][j].p_tot_rel) /2;
			}
			(*quasiorthogonale)[k][j].p_tot_rel_m = pm/datenObjekt->massenStromVorgabe;
			//! \f[	p_{tot,rel,m} = \frac{pm}{\dot{m}_{Vorgabe}}	\f]
		}
	}
}

void druck::setze_Pi_tot_rel_m ( )
{
	//! Schleife über alle Gitter und QO
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			//! \f[	\Pi_{tot,rel,m} = \frac{p_{tot,rel,m}(j,k)}{p_{tot,rel,m}(0,0)}	\f]
			(*quasiorthogonale)[k][j].Pi_tot_rel_m = (*quasiorthogonale)[k][j].p_tot_rel_m / (*quasiorthogonale)[0][0].p_tot_rel_m;
		}
	}
}



void druck::setze_Pi_tot_rel ( )
{
	int j,k;
	double p_tot_rel_0;
	double p_tot_rel;
	k = datenObjekt->anzahlGitter-1;
	j = datenObjekt->anzahlQuasiOrthogonale[k]-1;

	setze_p_tot_rel();
	//! Schleife über alle Stromlinien:
	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		p_tot_rel = (*knoten)[k][i][j].p_tot_rel;
		p_tot_rel_0 = (*knoten)[0][i][0].p_tot_rel;
		(*stromlinie)[i].Pi_tot_rel = p_tot_rel/p_tot_rel_0;
		//! \f[	\Pi_{tot,rel} = \frac{p_{tot,rel}(k_{max},j_{max})}{p_{tot,rel}(k=0,j=0)}	\f]
	}
}

void druck::setze_p_tot_abs ( )
{
	//! Schleife über alle Knotenpunkte:
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
			{
				//! \f$	p_{tot,abs} =	\f$ druck.berechne_p_tot_abs()
				(*knoten)[k][i][j].p_tot_abs = berechne_p_tot_abs ( i,j,k );
			}
		}
	}
}


void druck::setze_p_tot_abs_m ( )
{
	double pm;
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			pm = 0.0;
			for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
			{
				//! \f[	p_m = \sum_i \dot{m}_{Vorgabe} \cdot f(i)\cdot \frac{p_{tot,abs}(i,j,k)+p_{tot,abs}(i+1,j,k)}{2}	\f]
				pm = pm + datenObjekt->massenStromVorgabe *datenObjekt->faktorMassenStromRoehre[i]
					* ( (*knoten)[k][i][j].p_tot_abs + (*knoten)[k][i+1][j].p_tot_abs ) * 0.5;
			}
			//! \f[	p_{tot,abs,m} = \frac{p_m}{\dot{m}_{Vorgabe}}	\f]
			(*quasiorthogonale)[k][j].p_tot_abs_m = pm/datenObjekt->massenStromVorgabe;
		}
		//! Mit \f$ f(i) \f$ als Faktor der MAssenstromröhre nach daten.faktorMassenStromRoehre.
	}
}


void druck::setze_Pi_tot_abs_m ( )
{
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			//! \f[	\Pi_{tot,abs,m} = \frac{p_{tot,abs,m}(j,k)}{p_{tot,abs,m}(0,0)}	\f]
			(*quasiorthogonale)[k][j].Pi_tot_abs_m = (*quasiorthogonale)[k][j].p_tot_abs_m / (*quasiorthogonale)[0][0].p_tot_abs_m;
		}
	}
}

void druck::setze_Pi_tot_abs ( )
{
	int k = datenObjekt->anzahlGitter-1;
	int j = datenObjekt->anzahlQuasiOrthogonale[k]-1;

	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		//! \f[	\Pi_{tot,abs} = \frac{p_{tot,abs}(i,j,k)}{p_{tot,abs}(i,0,0)}	\f]
		(*stromlinie)[i].Pi_tot_abs = (*knoten)[k][i][j].p_tot_abs/(*knoten)[0][i][0].p_tot_abs;
	}
}


double druck::berechne_p_krit ( int i, int j, int k )
{
	//!	\f[	p_{krit} = p_{tot,rel} \cdot \left( \frac{2}{\kappa+1} \right)^\frac{\kappa}{\kappa-1}	\f]
	//! Mit \f$ \kappa = \kappa(i,j,k) \f$.
// 	double kappa = (*knoten)[k][i][j].kappa;
	return ( berechne_p_tot_rel ( i,j,k ) * pow ( 2/ ( (*knoten)[k][i][j].kappa+1 ),(*knoten)[k][i][j].kappa/ ( (*knoten)[k][i][j].kappa-1 ) ) );
}

void druck::zuruecksetzen_p_alt ( )
{
	//! Schleife über alle Gitter und Quasiorthogonalen:
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			//! \f$ p_{alt,+} = 0 \f$, \f$ p_{alt,-}=0 \f$
			(*quasiorthogonale)[k][j].p_alt_pos = 0;
			(*quasiorthogonale)[k][j].p_alt_neg = 0;
		}
	}
}

// void druck::umrechnen ( )
// {
// // man kann davon ausgehen, dass Geschwindigkeiten vorhanden sind
// 	for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
// 	{
// 		if ( abs(datenObjekt->omega[0]) > 0.0 )
// 		{
// // 			(*knoten)[0][i][0].p_tot_rel = (*knoten)[0][i][0].p_tot_abs * pow ( (*knoten)[0][i][0].T_tot_rel / (*knoten)[0][i][0].T_tot_abs, ( (*knoten)[0][i][0].kappa / ( (*knoten)[0][i][0].kappa-1 ) ) );
// 			(*knoten)[0][i][0].p_tot_rel = (*knoten)[0][i][0].p * pow( 1.0 + 0.5*( (*knoten)[0][i][0].kappa - 1.0 ) * pow2( (*knoten)[0][i][0].w / (*knoten)[0][i][0].a ) , ( (*knoten)[0][i][0].kappa )/( (*knoten)[0][i][0].kappa - 1.0 ) );
// 		}
// 		else
// 		{
// 			(*knoten)[0][i][0].p_tot_rel = (*knoten)[0][i][0].p_tot_abs; //falls omega == 0
// 		}
// 	}
// }

double druck::berechne_p_tot_abs ( int i, int j, int k )
{
	if ( j == 0 && k == 0 )
	{
		return (*knoten)[k][i][j].p_tot_abs;
	}
	else
	{
		//!	\f[	p_{tot,abs} = p \cdot \left( 1+\frac{\kappa-1}{2} \left( \frac{c}{a} \right)^2 \right)^\frac{\kappa}{\kappa-1}	\f]
		//! Mit \f$ \kappa = \kappa(i,j,k) \f$.
		return (*knoten)[k][i][j].p * pow ( 1.0 + ( (*knoten)[k][i][j].kappa-1 ) * 0.5 *
			pow2 ( geschwindigkeitObjekt.berechne_c(i,j,k) /
				(*knoten)[k][i][j].a), ( (*knoten)[k][i][j].kappa/ ( (*knoten)[k][i][j].kappa-1 ) ) );
	}
}


//##################### Andrea Bader #####################################################################


void druck::setze_Pi_abs ( )
{
	int j,k;
	double p_austritt;
	double p_eintritt;
	k = datenObjekt->anzahlGitter-1;
	j = datenObjekt->anzahlQuasiOrthogonale[k]-1;

	//! Schleife über alle Stromlinien:
	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
	{
		p_austritt = (*knoten)[k][i][j].p;
		p_eintritt = (*knoten)[0][i][0].p;
		//! \f$ \Pi_{abs} = \frac{p_A}{p_E} \f$
		(*stromlinie)[i].Pi_abs = p_austritt/p_eintritt;
	}
}

double druck::berechne_Pi_abs_m ()
{
	double pi_abs_m = 0.0;
		double pi_abs_sum = 0.0;
		//! Schleife über alle Stromröhren:
		for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
		{
			double Pi_Unten = (*stromlinie)[i].Pi_abs;
			double Pi_Oben = (*stromlinie)[i+1].Pi_abs;
			pi_abs_sum = pi_abs_sum + datenObjekt->massenStromVorgabe *datenObjekt->faktorMassenStromRoehre[i]
			            * (Pi_Unten + Pi_Oben)/2;
		}
		//! \f[ \Pi_{abs,m} = \sum \dot{m}_{R\ddot{o}hre}\cdot\frac{\Pi_{abs,unten}+\Pi_{abs,oben}}{2} \f]
		pi_abs_m = pi_abs_sum /datenObjekt->massenStromVorgabe;

		return pi_abs_m;
}

// void druck::setze_Pi_tot_abs ( )
// {
// 	int j,k;
// 	double p_tot_abs_0;
// 	double p_tot_abs;
// 	k = datenObjekt->anzahlGitter-1;
// 	j = datenObjekt->anzahlQuasiOrthogonale[k]-1;
// 
// 	for ( int i=0;i<datenObjekt->anzahlStromlinien;i++ )
// 	{
// 		p_tot_abs = (*knoten)[k][i][j].p_tot_abs;
// 		p_tot_abs_0 = (*knoten)[0][i][0].p_tot_abs;
// 		(*stromlinie)[i].Pi_tot_abs = p_tot_abs/p_tot_abs_0;
// 	}
// }


double druck::berechne_Pi_tot_m ()
{
	double pi_tot_m = 0.0;
	double pi_tot_sum = 0.0;
	//! Schleife über alle Stromröhren:
	for ( int i=0;i<datenObjekt->anzahlStromlinien-1;i++ )
	{
		double Pi_tot_Unten = (*stromlinie)[i].Pi_tot_abs;
		double Pi_tot_Oben = (*stromlinie)[i+1].Pi_tot_abs;
		pi_tot_sum = pi_tot_sum + datenObjekt->massenStromVorgabe *datenObjekt->faktorMassenStromRoehre[i]
		            * (Pi_tot_Unten + Pi_tot_Oben)/2;
	}
	//! \f[ \Pi_{tot,m} = \sum \dot{m}_{R\ddot{o}hre}\cdot\frac{\Pi_{tot,abs,unten}+\Pi_{tot,abs,oben}}{2} \f]
	pi_tot_m = pi_tot_sum /datenObjekt->massenStromVorgabe;

	return pi_tot_m;
}



double druck::berechne_p( int i, int j, int k )
{
    (*knoten)[k][i][j].update_cp((*knoten)[k][i][j].T);
	double kappa = (*knoten)[k][i][j].kappa;
	double p_wert;
	//! Für allererste Quasiorthogonale:
	if ( j== 0 && k == 0 )  //auf 1.QO ist T_tot eingelesen
	{
		//! \f[ p = p_{tot,abs} \cdot \left( \frac{T}{T_{tot,abs}} \right)^{\frac{\kappa}{\kappa-1}} \f]
		p_wert = (*knoten)[0][i][0].p_tot_abs *
				pow (((*knoten)[0][i][0].T / (*knoten)[0][i][0].T_tot_abs) , ( kappa /(kappa - 1 )));
	}
	//! Gitterübertrag für erste Quasiorthogonalen nachfolgender Gitter:
	else if (j == 0 && k > 0) //Gitterübertrag -> Überträgt Totalenthalpie der letzten QO im vorherigen Gitter
	{
		p_wert = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].p;
		//! \f[ p(i,0,k) = p(i,j_{max},k-1) \f]
		//Totaldruck dirket übertragen
		//! \f[ p_{tot,abs}(i,0,k) = p_{tot,abs}(i,j_{max},k-1) \f]
		(*knoten)[k][i][j].p_tot_abs = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].p_tot_abs;
	}
	//! Für sonstige Quasiorthogonale:
	else
	{
		//! \f[ p(j) = p(j-1) \cdot \left( \frac{T(j)}{T(j-1)} \right)^\frac{\kappa}{\kappa-1} \cdot \exp\left(-\frac{s(j)-s(j-1)}{R}\right) \f]
		p_wert= (*knoten)[k][i][j-1].p * ( pow (((*knoten)[k][i][j].T / (*knoten)[k][i][j-1].T), ( kappa /(kappa - 1 ))))
													*   exp (-( (*knoten)[k][i][j].entropie-(*knoten)[k][i][j-1].entropie) / datenObjekt->R) ;

		//Totaldruck belegen:
		(*knoten)[k][i][j].p_tot_abs = berechne_p_tot( i,j,k );

		if (isnan (p_wert))  //true, wenn p_wert = nan wird -> DEBUG
		{
			datenObjekt->loeser_resultCode = LRESULT_error;

			if (!datenObjekt->rechneKennfeld)
			{
				cout <<" Stop!!!!  p = nan " << endl;
 				NAN_ASSERT(p_wert);
			}
		}
	}
	return p_wert;
}


double druck::berechne_p_tot( int i, int j, int k )
{
	double p_tot = 0.0;
	double kappa = (*knoten)[k][i][j].kappa;
	//! \f[ p_{tot}(j) = p_{tot,abs}(j-1) \cdot \left( \frac{T_{tot,abs}(j)}{T_{tot,abs}(j-1)} \right)^\frac{\kappa}{\kappa-1} \cdot \exp\left(-\frac{s(j)-s(j-1)}{R}\right) \f]
	p_tot = (*knoten)[k][i][j-1].p_tot_abs * ( pow (((*knoten)[k][i][j].T_tot_abs / (*knoten)[k][i][j-1].T_tot_abs), ( kappa /(kappa - 1 ))))
										*   exp (-( (*knoten)[k][i][j].entropie-(*knoten)[k][i][j-1].entropie) / datenObjekt->R) ;

	return p_tot;
}