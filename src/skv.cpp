/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2008-2009 by Florian Mayer (ilumine@gmx.de)             *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*! \mainpage Dokumentation für das Stromlinienkrümmungsverfahren SKV 0.0.5

\section Einleitung
Die Berechnung durch das Stromlinienkrümmungsverfahren (SKV) erfolgt in den Schnittpunkten von
Quasiorthogonale und Stromlinie auf einer in die r-z-Ebene hinein gedrehten S2m-Stromfläche.
Die Ergebnisse der Berechnung erhält man aber auf der allgemeinen dreidimensionalen mittleren S2-Fläche.
Die S2m-Stromfläche entspricht in etwa der Skelettfläche einer Schaufel.
Die Schnittkurve einer geometrisch definierten S3-Fläche und der S2m-Stromfläche bezeichnet man als Quasiorthogonale,
da diese Kurve quasi orthogonal zur Strömung steht.
Als Stromlinie bezeichnet man eine Schnittkurve einer S1-Stromfläche und der S2m-Stromfläche.

Ausgehend von den reibungsfreien Erhaltungsgleichungen der Fluidmechanik (Eulergleichungen) erhalten wir die Grundgleichung für das SKV.
Der Kern ist eine gewöhnliche Differentialgleichung (DGL) für eine gewählte Quasiorthogonale. 

Da man in Turbomaschinen immer einen stehenden und rotierenden Teil hat,
ist es einfacher die Grundgleichungen im jeweiligen Relativsystem zu lösen, anstatt im Absolutsystem.
Im Stator fällt das Relativsystem mit dem Absolutsystem zusammen, da dort \f$ \omega = 0 \f$ ist.
In der dreidimensionalen Impulsgleichung ersetzen wir deshalb die Absolutgeschwindigkeit und -beschleunigung
durch die Relativgeschwindigkeit und -beschleunigung,
so wie aus der Technischen Mechanik bekannt. Dadurch treten zusätzliche Beschleunigungsterme auf.
Dies sind die Führungsbeschleunigung translatorisch, Führungsbeschleunigung rotatorisch normal,
Führungsbeschleunigung rotatorisch tangential, Coriolisbeschleunigung und Relativbeschleunigung.

Die Kontinuitätsgleichung formt man durch Definition einer Schichtdicke in \f$ \varphi \f$-Richtung,
welche die Versperrung durch die Schaufeln berücksichtigt, und die Gibb'sche Gleichung um.

Da man in der Herleitung Reibungsfreiheit vorausgesetzt hat, stimmen die Winkel am Ende jeder Schaufelreihe,
welche sich aus den berechneten Geschwindigkeiten ergeben, zwischen Rechnung und Wirklichkeit nicht überein.
Dies würde zu einer Falschanströmung der jeweils folgenden Schaufelreihe führen. Deshalb gibt man sich die Entropie,
welche bei der Durchströmung entsteht, vor. Die Entropie erhält man aus Versuchen, empirischen Korrelationen oder NS3D Rechnungen.
Wobei sich die meisten Korrelationen nur für Duct Flow Methoden eignen.
Bei den durch NS3D Rechnungen unterstützten Berechnungen nimmt man an,
dass sich die Entropieerzeugung im Laufe der Geometrieoptimierung nicht wesentlich ändert.
Durch die Vorgabe der Entropie ist das Gleichungssystem aber überbestimmt,
da man bei der Herleitung eine reibungsfreie Strömung vorausgesetzt hat, deshalb bleibt eine DGL,
die Impulsgleichung senkrecht zu den Quasiorthogonalen, unberücksichtigt.
Das Ergebnis dieser DGL wäre die Entropiedifferenz, diese ist  in diesem Fall natürlich Null.

Mit der Rothalpie, dem zweiten Hauptsatz der Thermodynamik und der verbliebenen Impulsgleichung kann das
Gleichungssystem zur Bestimmung des Druckes entlang einer Quasiorthogonalen iterativ gelöst werden.
Prinzipiell kann jede andere Strömungsgröße entlang der Quasiorthogonalen gelöst werden.

Aufgrund der extrem kurzen Rechenzeit des SKV's im Vergleich zu einer dreidimensionalen Navier-Stokes-Rechnung (NS3D),
wird dieses Verfahren auch heutzutage noch in der Auslegung von Turbomaschinen in jeder großen Firma oder Institution verwendet,
wie eine ausgiebige Literaturrecherche ergeben hat. Diese Programme werden von den Firmen sehr geschützt behandelt,
da in diesen die jahrzehntelange Erfahrung der Firmen steckt. Sie sind deshalb nicht frei verfügbar.
Mit einem SKV ist es überhaupt erst möglich instationäre Prozesse, wie das Hochfahren einer Gasturbine, zu berechnen.
Das vorliegende Programm beschränkt sich aber auf stationäre Vorgänge.

*/

/*!
        @author Andrea Bader, Balint Balassa, Oliver Borm, Florian Mayer, Steve Walter, Konstantinos Drakopoulos
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sstream>
#include <getopt.h>

#include <sys/time.h>
#include "daten.h"
#include "einlesen.h"
#include "geometrie.h"
#include "loeser_expeu.h"
#include "loeser_denton.h"
#include "kennfeld_manager.h"
#include "ausgabe.h"
#include "realFluid.h"

using namespace std;

loeser& erzeugeLoeser(loeser_type auswahl, geometrie& geometrieObjekt);
double cpu0, cpu1;

void intro()
{
    cout << "skv 0.0.5 Copyright (C) 2005-2010  A. Bader, B. Balassa, O. Borm, F. Mayer, S. Walter and K. Drakopoulos" << endl;
    cout << "\tThis program comes with ABSOLUTELY NO WARRANTY." << endl; //; for details type `show w'." << endl;
    cout << "\tLicense GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>" << endl;
    cout << "\tThis is free software, and you are welcome to redistribute it" << endl;
    cout << "\tunder certain conditions." << endl; //; type `show c' for details." << endl;
}
void fluids()
{
    cout<<"The fluids available in CoolProp are specified below:"<<endl;
    cout<<endl;
    cout<<"	1-Butene	Methanol	R21	"<<endl;
    cout<<"	Acetone     MethylLinoleate	R218	"<<endl;
    cout<<"	Air         MethylLinolenate	R22	"<<endl;
    cout<<"	Ammonia     MethylOleate	R227EA	"<<endl;
    cout<<"	Argon   	MethylPalmitate	R23	"<<endl;
    cout<<"	Benzene     MethylStearate	R236EA	"<<endl;
    cout<<"	CarbonDioxide	Neon	R236FA	"<<endl;
    cout<<"	CarbonMonoxide	Neopentane	R245fa	"<<endl;
    cout<<"	CarbonylSulfide	Nitrogen	R32	"<<endl;
    cout<<"	CycloHexane	NitrousOxide	R365MFC	"<<endl;
    cout<<"	CycloPropane	OrthoDeuterium	R404A	"<<endl;
    cout<<"	Cyclopentane	OrthoHydrogen	R407C	"<<endl;
    cout<<"	D4          Oxygen	R41	"<<endl;
    cout<<"	D5          ParaDeuterium	R410A	"<<endl;
    cout<<"	D6          ParaHydrogen	R507A	"<<endl;
    cout<<"	Deuterium	Propylene	RC318	"<<endl;
    cout<<"	DimethylCarbonate	Propyne	SES36	"<<endl;
    cout<<"	DimethylEther	R11	SulfurDioxide	"<<endl;
    cout<<"	Ethane      R113	SulfurHexafluoride	"<<endl;
    cout<<"	Ethanol 	R114	Toluene	"<<endl;
    cout<<"	EthylBenzene	R116	Water	"<<endl;
    cout<<"	Ethylene	R12	Xenon	"<<endl;
    cout<<"	Fluorine	R123	cis-2-Butene	"<<endl;
    cout<<"	HFE143m     R1233zd(E)	m-Xylene	"<<endl;
    cout<<"	Helium  	R1234yf	n-Butane	"<<endl;
    cout<<"	Hydrogen	R1234ze(E)	n-Decane	"<<endl;
    cout<<"	HydrogenSulfide	R1234ze(Z)	n-Dodecane	"<<endl;
    cout<<"	IsoButane	R124	n-Heptane	"<<endl;
    cout<<"	IsoButene	R125	n-Hexane	"<<endl;
    cout<<"	Isohexane	R13	n-Nonane	"<<endl;
    cout<<"	Isopentane	R134a	n-Octane	"<<endl;
    cout<<"	Krypton	R14	n-Pentane	"<<endl;
    cout<<"	MD2M        R141b	n-Propane	"<<endl;
    cout<<"	MD3M        R142b	n-Undecane	"<<endl;
    cout<<"	MD4M    	R143a	o-Xylene	"<<endl;
    cout<<"	MDM         R152A	p-Xylene	"<<endl;
    cout<<"	MM      	R161	trans-2-Butene	"<<endl;
    cout<<"	Methane	"<<endl;
    cout << endl;
    
    cout<< "The binary mixtures available are: "<<endl;
    	cout << "R125           	R143a" << endl;
	cout << "R125           	R32" << endl;
	cout << "R125           	R134a" << endl;
	cout << "R143a          	R134a" << endl;
	cout << "R152a          	R134a" << endl;
	cout << "R32            	R134a" << endl;
	cout << "Argon          	Helium" << endl;
	cout << "Argon          	IsoButane" << endl;
	cout << "Argon          	HydrogenSulfide" << endl;
	cout << "Argon          	Isopentane" << endl;
	cout << "CarbonDioxide  	Hydrogen" << endl;
	cout << "CarbonDioxide  	n-Heptane" << endl;
	cout << "CarbonDioxide  	Methane" << endl;
	cout << "CarbonDioxide  	Ethane" << endl;
	cout << "CarbonDioxide  	Propane" << endl;
	cout << "CarbonDioxide  	Helium" << endl;
	cout << "CarbonDioxide  	IsoButane" << endl;
	cout << "CarbonDioxide  	HydrogenSulfide" << endl;
	cout << "CarbonDioxide  	Isopentane" << endl;
	cout << "CarbonMonoxide 	Methane" << endl;
	cout << "CarbonMonoxide 	Ethane" << endl;
	cout << "CarbonMonoxide 	Propane" << endl;
	cout << "CarbonMonoxide 	Helium" << endl;
	cout << "CarbonMonoxide 	IsoButane" << endl;
	cout << "CarbonMonoxide 	HydrogenSulfide" << endl;
	cout << "CarbonMonoxide 	Isopentane" << endl;
	cout << "Ethane         	Propane" << endl;
	cout << "Ethane         	Argon" << endl;
	cout << "Ethane         	Helium" << endl;
	cout << "Ethane         	IsoButane" << endl;
	cout << "Ethane         	Nitrogen" << endl;
	cout << "Ethane         	Water" << endl;
	cout << "Ethane         	Oxygen" << endl;
	cout << "Ethane         	HydrogenSulfide" << endl;
	cout << "Ethane         	Isopentane" << endl;
	cout << "Helium         	IsoButane" << endl;
	cout << "Helium         	Nitrogen" << endl;
	cout << "Helium         	Water" << endl;
	cout << "Helium         	Oxygen" << endl;
	cout << "Helium         	HydrogenSulfide" << endl;
	cout << "Helium         	Isopentane" << endl;
	cout << "Hydrogen       	n-Heptane" << endl;
	cout << "Hydrogen       	CarbonMonoxide" << endl;
	cout << "Hydrogen       	Methane" << endl;
	cout << "Hydrogen       	Ethane" << endl;
	cout << "Hydrogen       	Propane" << endl;
	cout << "Hydrogen       	Argon" << endl;
	cout << "Hydrogen       	Helium" << endl;
	cout << "Hydrogen       	IsoButane" << endl;
	cout << "Hydrogen       	Nitrogen" << endl;
	cout << "Hydrogen       	Water" << endl;
	cout << "Hydrogen       	Oxygen" << endl;
	cout << "Hydrogen       	HydrogenSulfide" << endl;
	cout << "Hydrogen       	Isopentane" << endl;
	cout << "HydrogenSulfide	 Isopentane" << endl;
	cout << "IsoButane      	Nitrogen" << endl;
	cout << "IsoButane      	Water" << endl;
	cout << "IsoButane      	Oxygen" << endl;
	cout << "IsoButane      	HydrogenSulfide" << endl;
	cout << "IsoButane      	Isopentane" << endl;
	cout << "Methane        	Ethane" << endl;
	cout << "Methane        	Propane" << endl;
	cout << "Methane        	Argon" << endl;
	cout << "Methane        	Helium" << endl;
	cout << "Methane        	IsoButane" << endl;
	cout << "Methane        	Nitrogen" << endl;
	cout << "Methane        	Water" << endl;
	cout << "Methane        	Oxygen" << endl;
	cout << "Methane        	HydrogenSulfide" << endl;
	cout << "Methane        	Isopentane" << endl;
	cout << "Nitrogen       	HydrogenSulfide" << endl;
	cout << "Nitrogen       	Isopentane" << endl;
	cout << "Oxygen         	HydrogenSulfide" << endl;
	cout << "Oxygen         	Isopentane" << endl;
	cout << "Propane        	Argon" << endl;
	cout << "Propane        	Helium" << endl;
	cout << "Propane        	IsoButane" << endl;
	cout << "Propane        	Nitrogen" << endl;
	cout << "Propane        	Water" << endl;
	cout << "Propane        	Oxygen" << endl;
	cout << "Propane        	HydrogenSulfide" << endl;
	cout << "Propane        	Isopentane" << endl;
	cout << "Water          	HydrogenSulfide" << endl;
	cout << "Water          	Isopentane" << endl;
	cout << "n-Butane       	n-Pentane" << endl;
	cout << "n-Butane       	n-Hexane" << endl;
	cout << "n-Butane       	n-Octane" << endl;
	cout << "n-Butane       	n-Nonane" << endl;
	cout << "n-Butane       	n-Decane" << endl;
	cout << "n-Butane       	CarbonDioxide" << endl;
	cout << "n-Butane       	Hydrogen" << endl;
	cout << "n-Butane       	n-Heptane" << endl;
	cout << "n-Butane       	CarbonMonoxide" << endl;
	cout << "n-Butane       	Methane" << endl;
	cout << "n-Butane       	Ethane" << endl;
	cout << "n-Butane       	Propane" << endl;
	cout << "n-Butane       	Argon" << endl;
	cout << "n-Butane       	Helium" << endl;
	cout << "n-Butane       	IsoButane" << endl;
	cout << "n-Butane       	Nitrogen" << endl;
	cout << "n-Butane       	Water" << endl;
	cout << "n-Butane       	Oxygen" << endl;
	cout << "n-Butane       	HydrogenSulfide" << endl;
	cout << "n-Butane       	Isopentane" << endl;
	cout << "n-Decane       	CarbonDioxide" << endl;
	cout << "n-Decane       	Hydrogen" << endl;
	cout << "n-Decane       	n-Heptane" << endl;
	cout << "n-Decane       	CarbonMonoxide" << endl;
	cout << "n-Decane       	Methane" << endl;
	cout << "n-Decane       	Ethane" << endl;
	cout << "n-Decane       	Propane" << endl;
	cout << "n-Decane       	Argon" << endl;
	cout << "n-Decane       	Helium" << endl;
	cout << "n-Decane       	IsoButane" << endl;
	cout << "n-Decane       	Nitrogen" << endl;
	cout << "n-Decane       	Water" << endl;
	cout << "n-Decane       	Oxygen" << endl;
	cout << "n-Decane       	HydrogenSulfide" << endl;
	cout << "n-Decane       	Isopentane" << endl;
	cout << "n-Heptane      	CarbonMonoxide" << endl;
	cout << "n-Heptane      	Methane" << endl;
	cout << "n-Heptane      	Ethane" << endl;
	cout << "n-Heptane      	Propane" << endl;
	cout << "n-Heptane      	Argon" << endl;
	cout << "n-Heptane      	Helium" << endl;
	cout << "n-Heptane      	IsoButane" << endl;
	cout << "n-Heptane      	Nitrogen" << endl;
	cout << "n-Heptane      	Water" << endl;
	cout << "n-Heptane      	Oxygen" << endl;
	cout << "n-Heptane      	HydrogenSulfide" << endl;
	cout << "n-Heptane      	Isopentane" << endl;
	cout << "n-Hexane       	n-Octane" << endl;
	cout << "n-Hexane       	n-Nonane" << endl;
	cout << "n-Hexane       	n-Decane" << endl;
	cout << "n-Hexane       	CarbonDioxide" << endl;
	cout << "n-Hexane       	Hydrogen" << endl;
	cout << "n-Hexane       	n-Heptane" << endl;
	cout << "n-Hexane       	CarbonMonoxide" << endl;
	cout << "n-Hexane       	Methane" << endl;
	cout << "n-Hexane       	Ethane" << endl;
	cout << "n-Hexane       	Propane" << endl;
	cout << "n-Hexane       	Argon" << endl;
	cout << "n-Hexane       	Helium" << endl;
	cout << "n-Hexane       	IsoButane" << endl;
	cout << "n-Hexane       	Nitrogen" << endl;
	cout << "n-Hexane       	Water" << endl;
	cout << "n-Hexane       	Oxygen" << endl;
	cout << "n-Hexane       	HydrogenSulfide" << endl;
	cout << "n-Hexane       	Isopentane" << endl;
	cout << "n-Nonane       	n-Decane" << endl;
	cout << "n-Nonane       	CarbonDioxide" << endl;
	cout << "n-Nonane       	Hydrogen" << endl;
	cout << "n-Nonane       	n-Heptane" << endl;
	cout << "n-Nonane       	CarbonMonoxide" << endl;
	cout << "n-Nonane       	Methane" << endl;
	cout << "n-Nonane       	Ethane" << endl;
	cout << "n-Nonane       	Propane" << endl;
	cout << "n-Nonane       	Argon" << endl;
	cout << "n-Nonane       	Helium" << endl;
	cout << "n-Nonane       	IsoButane" << endl;
	cout << "n-Nonane       	Nitrogen" << endl;
	cout << "n-Nonane       	Water" << endl;
	cout << "n-Nonane       	Oxygen" << endl;
	cout << "n-Nonane       	HydrogenSulfide" << endl;
	cout << "n-Nonane       	Isopentane" << endl;
	cout << "n-Octane       	n-Nonane" << endl;
	cout << "n-Octane       	n-Decane" << endl;
	cout << "n-Octane       	CarbonDioxide" << endl;
	cout << "n-Octane       	Hydrogen" << endl;
	cout << "n-Octane       	n-Heptane" << endl;
	cout << "n-Octane       	CarbonMonoxide" << endl;
	cout << "n-Octane       	Methane" << endl;
	cout << "n-Octane       	Ethane" << endl;
	cout << "n-Octane       	Propane" << endl;
	cout << "n-Octane       	Argon" << endl;
	cout << "n-Octane       	Helium" << endl;
	cout << "n-Octane       	IsoButane" << endl;
	cout << "n-Octane       	Nitrogen" << endl;
	cout << "n-Octane       	Water" << endl;
	cout << "n-Octane       	Oxygen" << endl;
	cout << "n-Octane       	HydrogenSulfide" << endl;
	cout << "n-Octane       	Isopentane" << endl;
	cout << "n-Pentane      	n-Hexane" << endl;
	cout << "n-Pentane      	n-Octane" << endl;
	cout << "n-Pentane      	n-Nonane" << endl;
	cout << "n-Pentane      	n-Decane" << endl;
	cout << "n-Pentane      	CarbonDioxide" << endl;
	cout << "n-Pentane      	Hydrogen" << endl;
	cout << "n-Pentane      	n-Heptane" << endl;
	cout << "n-Pentane      	CarbonMonoxide" << endl;
	cout << "n-Pentane      	Methane" << endl;
	cout << "n-Pentane      	Ethane" << endl;
	cout << "n-Pentane      	Propane" << endl;
	cout << "n-Pentane      	Argon" << endl;
	cout << "n-Pentane      	Helium" << endl;
	cout << "n-Pentane      	IsoButane" << endl;
	cout << "n-Pentane      	Nitrogen" << endl;
	cout << "n-Pentane      	Water" << endl;
	cout << "n-Pentane      	Oxygen" << endl;
	cout << "n-Pentane      	HydrogenSulfide" << endl;
	cout << "n-Pentane      	Isopentane" << endl;
	cout << "Argon          	Nitrogen" << endl;
	cout << "Argon          	Water" << endl;
	cout << "Argon          	Oxygen" << endl;
	cout << "CarbonDioxide  	CarbonMonoxide" << endl;
	cout << "CarbonDioxide  	Argon" << endl;
	cout << "CarbonDioxide  	Nitrogen" << endl;
	cout << "CarbonDioxide  	Water" << endl;
	cout << "CarbonDioxide  	Oxygen" << endl;
	cout << "CarbonMonoxide 	Argon" << endl;
	cout << "CarbonMonoxide 	Nitrogen" << endl;
	cout << "CarbonMonoxide 	Water" << endl;
	cout << "CarbonMonoxide 	Oxygen" << endl;
	cout << "Nitrogen       	Water" << endl;
	cout << "Nitrogen       	Oxygen" << endl;
	cout << "Water          	Oxygen" << endl;
	cout << "Ethanol        	Water" << endl;
	cout << "R245fa         	R134a" << endl;
	cout << "R1234ze(E)     	R32" << endl;
	cout << "R32            	R1234yf" << endl;	
}
//! Main skv Function
/*!
Startet das Stromlinienkrümmungsverfahren Programm.
\author Andrea Bader, Balint Balassa, Oliver Borm, Florian Mayer, Steve Walter, Konstantinos Drakopoulos
\param -p Option für Pfadangabe. Ein Argument wird benötigt (/path/to/working/dir/).
\param -d debug Option. -1 für still.
\param -f Format der Eingabedatei. Ein Argument wird benötigt ("t" für text oder "c" für cgns).
\param -h Zeige Hilfe.
\param -r Relaxfactor für Verschiebung der Stromlinien bei Denton und Benetschik.
\param -s Solver. 0 für explizit Euler, 1 für Denton, 2 für Benetschik Differentialgleichung.
\param -t Zeit in Sekunden, nach der die Berechnung beendet wird und die Ergebnisse geschrieben werden sollen.
\return EXIT_SUCCESS
*/
int main ( int argc, char *argv[] )
{

    daten *datenObjekt = daten::getInstance();
    realFluid *fluidObj =realFluid::getFluid();
    
    cpu0 = (double)clock()/CLOCKS_PER_SEC;
    int c;		//!< Integer Zahl zur Bestimmung der Kommandozeilen Argumente.
    // Behandeln der Kommandozeilen Argumente:
    while(1)
    {
        // Parameter für Argumente
        static struct option long_options[] =
        {
        {"format",  required_argument, 0, 'f'},
        {"path",  required_argument, 0, 'p'},
        {"debug",  required_argument, 0, 'd'},
        {"solver",  required_argument, 0, 's'},
        {"ideal", required_argument, 0, 'i'},
        {"fluid", required_argument, 0, 'l'},
        {"time",  required_argument, 0, 't'},
        {"relax",  required_argument, 0, 'r'},
        {"help",  no_argument, 0, 'h'},
        {0, 0, 0, 0}
    };
        string a;
        int option_index = 0;
        c = getopt_long (argc, argv, "f:p:d:s:i:l:t:r:h", long_options, &option_index);

        if (c == -1)	// Falls keine Argumente eingegeben
            break;
        switch (c)
        {
        case 'f':	// format
            a = optarg;
            if (a=="t" || a=="text")
            {
                datenObjekt->inputFormat = 2;
            }
            if (a=="c" || a=="cgns")
            {
                datenObjekt->inputFormat = 1;
            }
            break;
        case 'p':	// path
            datenObjekt->setworkdir(optarg);
            break;
        case 'd':	// debug
            datenObjekt->debug = atoi(optarg);
            break;
        case 's':	// solver
            if (atoi(optarg) == 0){datenObjekt->loeserAuswahl = LSR_expeu;}
            else
            {
                datenObjekt->loeserAuswahl = LSR_denton;
                if (atoi(optarg) == 1){datenObjekt->dglFormulierung = DGL_denton;}
                if (atoi(optarg) == 2){datenObjekt->dglFormulierung = DGL_benetschik;}
            }
            break;
        case 'i':
            datenObjekt->ideal=atoi(optarg);
            break;
        case 'l':
            if (optarg=="h" || optarg=="help"){
                fluids();
            }else{
            fluidObj->readFluid(optarg);
            datenObjekt->mol = fluidObj->myFluid->molar_mass();
            datenObjekt->R = datenObjekt->Rm / datenObjekt->mol;
            }
            break;
        case 't':	// maximum time
            datenObjekt->maxTime = atoi(optarg);
            break;
        case 'r':	// relaxfactor
            datenObjekt->relaxfac = atof(optarg);
            datenObjekt->wil = false;
            break;
        case 'h':	// help
            if ((optarg=="l") or (optarg=="fluid")){
                fluids();
            }else{
                intro();
                cout << endl << endl << "USAGE: skv [OPTION]" << endl << endl;
                cout << "Options:" << endl;
                cout << "\t-f, --format\tUse specified format. One required argument: <t, text, c, cgns>" << endl;
                cout << "\t-p, --path\tUse specified path. One required argument: <./path/to/working/dir/>" << endl;
                cout << "\t-d, --debug\tUse sepcified debug level. One required argument: <-1,0,1,2...>" << endl;
                cout << "\t-s, --solver\tUse sepcified solver. One required argument: 0 for explicit Euler, 1 for Denton, 2 for Benetschik" << endl;
                cout << "\t-i, --ideal\tSet fluid to be real(false) or ideal(true). Default is ideal" << endl; 
                cout << "\t-l, --fluid\tUse specified fluid: Type '-l h' for a list of available fluids in CoolProp."<<endl;
                cout << "\t-t, --time\tQuit Calculation after sepcified time and write results. One required argument: <time in seconds>. Default is " << datenObjekt->maxTime << endl;
                cout << "\t-r, --relax\tUse sepcified relax factor. One required argument: <relaxfaktor [0,1]>" << endl;
                cout << "\t-h, --help\tDisplay this help" << endl;
                cout << endl;
                exit (0);
            }
        default:
            exit (0);
            break;
        }
    }

    if ( datenObjekt->debug >-1 )	// falls Debugmodus nicht silent, Intro anzeigen.
    {
        intro();
    }

    //! Initialisiere Ein-, Ausgabe- und Geometrieobjekte.
    geometrie 	geometrieObjekt;
    ausgabe 	ausgabeObjekt;
    einlesen 	eingabeObjekt;

    //! Einlesen der Eingabedaten (Text- oder cgns-Datei)
    eingabeObjekt.einlesenDaten ();
    loeser& loeserObjekt = erzeugeLoeser(datenObjekt->loeserAuswahl, geometrieObjekt);

    //! Kennfeldobjekt wird erst nach gesetzten workdirectory erstellt
    kennfeld_manager kennfeld;

    //! Originalgitter wird gespeichert
    datenObjekt->origingrid2();

    // Für Kennfeldberechnungen:
    if (datenObjekt->rechneKennfeld)
    {
        //Initialisierung u,m für den ersten Betriebspunkt der Kennfeldberechnung
        kennfeld.hatWeiterePunkte();
    }

    do
    {
        loeserObjekt.zuruecksetzen();

        //! Nurbs initialisieren
        DEBUG_0("Die Nurbskurven werden initialisiert ");
        geometrieObjekt.initialisiereGeometrie_nurbsQO();
        geometrieObjekt.initialisiereGeometrie_nurbsSL();
        //! Initialisiere "Entropie"
        geometrieObjekt.initialisiereGeometrie_H_entropie();
        //! Initialisiere "Schaufelversperrung sigma"
        geometrieObjekt.initialisiereGeometrie_H_sigma();
        //! Initialisiere "beta_ru", "beta_zu"
        geometrieObjekt.initialisiereGeometrie_H_beta_ru();
        geometrieObjekt.initialisiereGeometrie_H_beta_zu();
        //! Ny setzen
        geometrieObjekt.bekomme_ny();
        //! My setzen
        geometrieObjekt.bekomme_my();
        //realFluid::getFluid()->myFluid->update(PT_INPUTS,)
        
        realFluid::getFluid()->is_initialising = false;
        //! Schleife starten
        DEBUG_0("Berechnung gestartet! Bitte warten");
        DEBUG_0("Eingelesenes Gitter nach vorher.cgns: ");
        loeserObjekt.schleife();

        //! Berechnungserfolg überprüfen
        if ( datenObjekt->loeser_resultCode == LRESULT_success || ! datenObjekt->rechneKennfeld )
        {
            loeserObjekt.integrale_Daten();
            if (datenObjekt->rechneKennfeld) { kennfeld.ablegenKennfeldPunkt(); }
        }
        //! Konvergenzproblem behandeln
        else if (datenObjekt->rechneKennfeld)
        {
            kennfeld.fehlerMakieren(datenObjekt->loeser_resultCode);
        }

    } while (datenObjekt->rechneKennfeld && kennfeld.hatWeiterePunkte() );

    //! Ausgabe schreiben
    ausgabeObjekt.ausgabeDaten();

    //! Aufräumen des 'loeserObjekts'
    delete(&loeserObjekt);
    cpu1 = (double)clock()/CLOCKS_PER_SEC;
    cout <<"\n"<< cpu1 - cpu0 << endl;
    //! Exit
    DEBUG_IMMER(" Die Berechnung wurde beeendet. ");
    return EXIT_SUCCESS;
}


/*!
 * Je nach Wunsch (Denton, Druck-DGL in Löser-Expeu) wird hier das richtige
 * Loeser Objekt erzeugt.
 * <p>
 * Dieses extern erzeugte Objekt muss nach der Berechnung durch den Aufruf
 * von delete explizit aufgeräumt werden.
 *
 * \param auswahl
 * \param geometrieObjekt
 * \return Löserobjekt
 */
loeser& erzeugeLoeser(loeser_type auswahl, geometrie& geometrieObjekt)
{
    loeser* erzeugterLsr;
    switch (auswahl)
    {
    case LSR_denton:
        erzeugterLsr = new loeser_denton( &geometrieObjekt );
        break;
    case LSR_expeu:
        erzeugterLsr = new loeser_expeu( &geometrieObjekt );
        break;
    }
    return *erzeugterLsr;
}


