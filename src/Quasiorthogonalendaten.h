/***************************************************************************
 *   Copyright (C) 2008-2009 by Florian Mayer (ilumine@gmx.de)             *
 *                 2009 by Oliver Borm (oli.borm@web.de)                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef QUASIORTHOGONALENDATEN_H
	#define QUASIORTHOGONALENDATEN_H



#include <nurbs++/nurbs.h>



using namespace PLib;

enum QOType {QOT_Stator, QOT_Kanal, QOT_Rotor};

//! Quasiorthogonalen-Datenklasse
/*!
Beinhaltet die NURBS-Kurven der Quasiorthogonalen mit ihren gespeicherten Werten.
\author Oliver Borm, Florian Mayer
*/
struct Quasiorthogonalendaten
{
	double p_tot_abs_m;	//!< Absoluter Totaldruck
	double T_tot_abs_m;	//!< Absolute Totaltemperatur
	double Pi_tot_abs_m;	//!< Absolutes Totaldruckverhältnis

	double p_tot_rel_m;	//!< Relativer Totaldruck
	double Pi_tot_rel_m;	//!< Relatives Totaldruckverhältnis

	double machzahl_gemittelt;
	double flaeche;		//!< Fläche
	double massenStrom;	//!< Auf jeder QO
	//   int Kante; // 1=Vorderkante, -1=Hinterkante, 0=nicht definiert (schaufelfrei oder Schaufelmitte)

	double p_alt_pos; //!< An der Nabe jeder QO
	double p_alt_neg; //!< an der Nabe jeder QO

	QOType typ;  //rotor, stator, kanal
	bool eintritt;
	bool austritt;
	bool vorderkante;
	bool hinterkante;

	bool rotor() {
		if (typ == QOT_Rotor) { return true; }
		return false;
	}
	bool kanal() {
			if (typ == QOT_Kanal) { return true; }
			return false;
	}
	bool stator() {
			if (typ == QOT_Stator) { return true; }
			return false;
	}
	std::string typeToString()
	{
		if ( stator() )
			return "stator";

		else if ( kanal() )
			return "kanal";

		else if ( rotor() )
			return "rotor";
		else
			return "<fehler>";
	}


	NurbsCurve_2Dd nurbsQuasiOrthogonale;
	NurbsCurve_2Dd nurbsEntropie;
	NurbsCurve_2Dd nurbsSigma;
	NurbsCurve_2Dd nurbsBeta_zu;
	NurbsCurve_2Dd nurbsBeta_ru;
	NurbsCurve_2Dd nurbsMassenStromDichte;
	NurbsCurve_2Dd nurbsMassenStromDichteSin;

	NurbsCurve_2Dd nurbsSinEpsilon;
	NurbsCurve_2Dd nurbsCosEpsilon;
	NurbsCurve_2Dd nurbsEpsilonQO;
	NurbsCurve_2Dd nurbsCotBetaQO;

 	NurbsCurve_2Dd nurbsDrallQuadrat;
};

#endif
