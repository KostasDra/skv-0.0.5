/***************************************************************************
 *   Copyright (C) 2010 Andrea Bader (bader.andrea@gmx.de)                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
 * Kennfeld Manager verwaltet die Kennfeldberechnung.
 *
 * @author Andrea Bader
 */

#include "kennfeld_manager.h"


kennfeld_manager::kennfeld_manager()
 : indexUmfangGeschw(0)
 , zustand(ZK_mitte)
{
	weitere = true;

	mitteMassenstrom = 0.0;  //vorgelegung mit 0.0 erforderlich, um Methode in loeser_denton::schleife() aufzurufen
	deltaMassenstrom = daten::getInstance()->massenstromSchrittweite;
	suchZaehler = 0;

	// Erzeugen der Ausgabedatei
	kennfeldDatei = new std::ofstream( ( daten::getInstance()->getworkdir() + "Kennfeld.csv" ).c_str() );

	// Schreiben der CSV-Kopfzeile
	(*kennfeldDatei) << "u; m_vorgabe; pi_tot; V_red; eta_poly; phi" << endl;
}


bool kennfeld_manager::hatWeiterePunkte()
{
	bool rv = false;

	if (hatWeitereMassenstromWerte())
	{
		rv = true;
	}
	else	// auf die nächste Kennlinie wechseln!
	{
		rv = hatWeitereUmfangsGeschw();
	}

	if (rv)
	{
		// schreibe Umfangsgeschwindigkeit ins datenObjekt
		daten::getInstance()->u_ref = aktuelleUmfangsGeschw();

		// zurücksetzen omega -> wird wegen Umrechnungsmethode gebraucht.
		for (unsigned int k=0; k < daten::getInstance()->omega.size(); k++)
		{ daten::getInstance()->omega[k] = 0.0; };

		// schreibe Massenstrom ins datenObjekt
		daten::getInstance()->massenStromVorgabe = aktuellerMassenstrom();
	}

	return rv;
}





void kennfeld_manager::fehlerMakieren(loeser_result_code rc)
{
	zustand_kennlinie naechsterZustand = zustand;

	switch (zustand)
	{
	case ZK_mitte:
		massenstrom = daten::getInstance()->massenStromVorgabe - deltaMassenstrom;
		naechsterZustand = ZK_suchLaufeLinks;
		weitere = true;
		break;

	case ZK_nachRechts:
		naechsterZustand = ZK_sucheSchluckGrenzeMinus;
		++suchZaehler;
		massenstrom  = massenstrom - deltaMassenstrom / (2*suchZaehler);
		weitere = true;
		break;

	case ZK_sucheSchluckGrenzeMinus:
	case ZK_sucheSchluckGrenzePlus:
		++suchZaehler;
		if ( suchZaehler <= daten::getInstance()->kennlinieGrenzenSuchZaehler) {
			naechsterZustand = ZK_sucheSchluckGrenzeMinus;
			massenstrom = massenstrom - deltaMassenstrom / (2*suchZaehler);
			weitere = true;
		} else {
			naechsterZustand = ZK_nachLinks;
			suchZaehler = 0;
			massenstrom = mitteMassenstrom - deltaMassenstrom;
			weitere = true;
			DEBUG_IMMER("    --> Schluckgrenze erreicht.");
		}
		break;

	case ZK_nachLinks:
		if (daten::getInstance()->pumpgrenzeSuchen)	{
			naechsterZustand = ZK_suchePumpGrenzePlus;
			++suchZaehler;
			massenstrom  = massenstrom + deltaMassenstrom / (2*suchZaehler);
			weitere = true;
		} else {
			naechsterZustand = ZK_mitte;
			suchZaehler = 0;
			weitere = false;
		}
		break;

	case ZK_suchePumpGrenzeMinus:
	case ZK_suchePumpGrenzePlus:
		++suchZaehler;
		if (suchZaehler <= daten::getInstance()->kennlinieGrenzenSuchZaehler) {
			naechsterZustand = ZK_suchePumpGrenzePlus;
			massenstrom = massenstrom + deltaMassenstrom / (2*suchZaehler);
			weitere = true;
		} else {
			naechsterZustand = ZK_mitte;
			suchZaehler = 0;
			weitere = false;
			DEBUG_IMMER("    --> Pumpgrenze erreicht.");
		}
		break;

	case ZK_suchLaufeLinks:
		massenstrom = massenstrom - deltaMassenstrom;
		if (massenstrom < 0.0) {
			naechsterZustand = ZK_mitte;
			weitere = false;
		} else {
			naechsterZustand = ZK_suchLaufeLinks;
			weitere = true;
		}
		break;
	}

	DEBUG_IMMER( "    [ Fehler bei Zustandswechsel: " << zustandString(zustand) <<
			" -> " << zustandString(naechsterZustand) << "] " );
	zustand = naechsterZustand;
}


void kennfeld_manager::meldeOK()
{
	zustand_kennlinie neuerZustand = zustand;
	bool rv = false;

	switch (zustand)
	{
	case ZK_nachLinks:
		rv = true;
		massenstrom = massenstrom - deltaMassenstrom;
		break;

	case ZK_nachRechts:
		rv = true;
		massenstrom = massenstrom + deltaMassenstrom;
		break;

	case ZK_mitte:
		// Übernehemen des berechneten Massenstroms, falls in der Mitte (da mit 0.0 vorbelegt)
		mitteMassenstrom = daten::getInstance()->massenStromVorgabe;

		if (deltaMassenstrom > 0.0) {
			neuerZustand = ZK_nachRechts;
			massenstrom = mitteMassenstrom + deltaMassenstrom;
			rv = true;
		} else { // sonderbehandlung wenn nur ein kennlinienpunkt (deltaMassenstrom==0)
			neuerZustand = ZK_mitte;
			massenstrom = 0.0;
			rv = false;
		}

		break;

	case ZK_suchLaufeLinks:
		neuerZustand = ZK_nachLinks;
		massenstrom = massenstrom - deltaMassenstrom;
		rv = true;
		if (massenstrom < 0.0)  //negativen Massenstrom abfangen
		{
			neuerZustand = ZK_mitte;
			suchZaehler = 0;
			rv = false;
		}
		break;

	case ZK_sucheSchluckGrenzeMinus:
	case ZK_sucheSchluckGrenzePlus:
		++suchZaehler;
		if (suchZaehler <= daten::getInstance()->kennlinieGrenzenSuchZaehler) {
			neuerZustand = ZK_sucheSchluckGrenzePlus;
			massenstrom = massenstrom + deltaMassenstrom / (2*suchZaehler);
			rv = true;
		} else {
			neuerZustand = ZK_nachLinks;
			massenstrom = mitteMassenstrom - deltaMassenstrom;
			suchZaehler = 0;
			rv = true;
			DEBUG_IMMER("    --> Schluckgrenze erreicht.");
		}
		break;

	case ZK_suchePumpGrenzePlus:
	case ZK_suchePumpGrenzeMinus:
		++suchZaehler;
		if (suchZaehler <= daten::getInstance()->kennlinieGrenzenSuchZaehler) {
			neuerZustand = ZK_suchePumpGrenzeMinus;
			massenstrom = massenstrom - deltaMassenstrom / (2*suchZaehler);
			rv = true;
		} else {
			neuerZustand = ZK_mitte;
			suchZaehler = 0;
			rv = false;
			DEBUG_IMMER("    --> Pumpgrenze erreicht.");
		}
		break;
	}

	DEBUG_IMMER(
			"  [ OK -> Zustandswechsel: " << zustandString(zustand) <<
			" -> " << zustandString(neuerZustand) << " ]" );

	weitere = rv;
	zustand = neuerZustand;
}


void kennfeld_manager::ablegenKennfeldPunkt()
{
	kennfeld_resultat ergPunkt;

	// holen der Ergebnisse
	ergPunkt.m_red 		= daten::getInstance()->v_red;
	ergPunkt.pi_tot 	= daten::getInstance()->Pi_tot;
	ergPunkt.u 			= aktuelleUmfangsGeschw();
	ergPunkt.m_vorgabe 	= daten::getInstance()->massenStromVorgabe;
	ergPunkt.eta_poly   = daten::getInstance()->eta_polytrop_vorgabe;
	ergPunkt.phi        = daten::getInstance()->schluckziffer_aktuell;

	meldeOK();

	DEBUG_IMMER("     Ablegen der Kennfeld Ergebnisse: -> Totaldruckverhältnis: " << ergPunkt.pi_tot<<endl<<
			    "                                                 Wirkungsgrad: "<< daten::getInstance()->eta_polytrop_vorgabe <<
			    "     bei aktuelle Schluckziffer: "<< daten::getInstance()->schluckziffer_aktuell);

	einfuegenInSortierteListe(ergPunkt);
}




void kennfeld_manager::einfuegenInSortierteListe(kennfeld_resultat neues_element)
{ //Ergebnisse für eine Umfangsgeschindigkeit werden nach aufsteigenden Massenströmen sortiert -> erleitert excel Diagramm
	vector<kennfeld_resultat>::iterator it;

	//einordnen
	for (it=vErgebnisse.begin(); it < vErgebnisse.end(); it++)
	{
		if ((*it).m_red > neues_element.m_red) {
			break;
		}
	}

	// an der gefundenen Stelle einfügen
	vErgebnisse.insert(it, neues_element);
}


void kennfeld_manager::ausgebenListe()
{
	DEBUG_1("--------------------------------" << endl << "berechneter Massenstrombereich");

	vector<kennfeld_resultat>::iterator it;
	for (it=vErgebnisse.begin(); it < vErgebnisse.end(); it++)
	{
		DEBUG_1((*it).m_red);
		(*kennfeldDatei) << (*it).to_string() << endl;
	}
	(*kennfeldDatei) << endl;
	DEBUG_1("--------------------------------"<< endl);
}


bool kennfeld_manager::hatWeitereMassenstromWerte()
{
	return weitere;
}

double kennfeld_manager::aktuellerMassenstrom()
{
	if (zustand == ZK_mitte)
	{
		daten::getInstance()->berechneMassenstromVorgabe = true;
		massenstrom = 0.0;
	}
	else
	{
		daten::getInstance()->berechneMassenstromVorgabe = false;
	}

	// negativen massenstrom abfangen -- falls sucheLaufeLinks fehlschlägt.
	if (massenstrom < 0.0)
	{
		massenstrom = 0.0;
	}
	return massenstrom;
}

bool kennfeld_manager::hatWeitereUmfangsGeschw()
{
	ausgebenListe();
	vErgebnisse.clear();
	size_t size = daten::getInstance()->vUmfangsGeschwindigkeiten.size();

	if ((unsigned int) indexUmfangGeschw >= (size-1) ) //falls das schon die letzte war
	{
		return false;
	}
	else  // sprung auf nächste Kennlinie
	{
		daten::getInstance()->berechneMassenstromVorgabe = true;

		indexUmfangGeschw++;
		return true;
	}
}

double kennfeld_manager::aktuelleUmfangsGeschw()
{
	return daten::getInstance()->vUmfangsGeschwindigkeiten[indexUmfangGeschw];
}



string kennfeld_manager::zustandString(zustand_kennlinie z)
{
	string rs = "<empty>";
	switch(z)
	{
	case ZK_mitte:
		rs = "mitte"; break;
	case ZK_suchLaufeLinks:
		rs = "suchLaufeLinks"; break;
	case ZK_nachRechts:
		rs = "nachRechts"; break;
	case ZK_sucheSchluckGrenzePlus:
		rs = "sucheSchluckGrenzePlus"; break;
	case ZK_sucheSchluckGrenzeMinus:
		rs = "sucheSchluckGrenzeMinus"; break;
	case ZK_nachLinks:
		rs = "nachLinks"; break;
	case ZK_suchePumpGrenzePlus:
		rs = "suchePumpGrenzePlus"; break;
	case ZK_suchePumpGrenzeMinus:
		rs = "suchePumpGrenzeMinus"; break;
	};

	return rs;
}

