%% ==============================================================
%% SKV Manual von Oliver Borm, Andrea Bader, Balint Balassa, Florian Mayer  
%% steht unter einer Creative Commons Namensnennung - Weitergabe 
%% unter gleichen Bedingungen 3.0 Deutschland Lizenz.
%% http://creativecommons.org/licenses/by-sa/3.0/de/
%% ==============================================================

%% ====================================================
%%  **********           Kapitel 5          **********
%% ====================================================

\chapter{Numerische Methoden für den Druck-Löser}
\label{kap05}

In diesem Kapitel werden die eingesetzten numerischen Verfahren genauer erläutert, deshalb ist ein grundsätzliches Verständnis von objektorientierter Programmierung hilfreich. Für die Programmierung mit C++ ist \cite{schildt:2004} sehr zu empfehlen.
Nichtsdestotrotz wird im folgenden versucht einige prägnante Begriffe zu erläutern:

\begin{itemize}
\item Eine \textbf{Klasse} ist eine Sammlung von Variablen - häufig mit unterschiedlichen Datentypen - kombiniert mit einer Gruppe verwandter Methoden. \textbf{Methoden} manipulieren die Variablen einer Klasse. Methoden übernehmen bei Bedarf Parameter und liefern einen Wert zurück. 
\item Der \textbf{Konstruktor} ist eine spezielle Methode eventuell mit Übergabeparametern aber ohne Rückgabewert mit demselben Namen wie die Klasse selbst. Mit einem Konstruktor wird die Instanz einer Klasse initialisiert.
\item Die Instanz einer Klasse nennt man \textbf{Objekt}. 
\item \textbf{Destruktoren} führen Aufräumarbeiten nach Abbau des Objektes aus und geben reservierten Speicherplatz wieder frei. Destruktoren übernehmen keine Parameter und liefern keinen Wert zurück.
\item Ein \textbf{Zeiger} ist eine Variable, welche eine Speicheradresse enthält.
\end{itemize}


Prinzipiell kann man natürlich in jeder Schleife eine andere Indizierung vornehmen, da die Zählvariablen nur lokale Gültigkeit besitzen. Um aber einen gewissen Überblick zu behalten wurden den folgenden drei Indizes entsprechende Bedeutung zugewiesen:
\begin{itemize}
\item i - Anzahl der Stromlinien
\item k - Anzahl der Gitter
\item j - Anzahl der Quasiorthogonalen in einem Gitter
\end{itemize}

\newpage

\section{Hauptmethode}
In der Hauptmethode 
%\anref{skv:cpp} 
wird der Zeiger auf das Datenobjekt angelegt, in welchem alle erforderlichen Daten gespeichert sind. Es werden noch weitere Objekte angelegt, um dann deren Methoden nutzen zu können. Danach werden die Eingabedaten eingelesen, die \nurbs Kurven initialisiert, die aktuellen $\nu$-Werte gesucht und der Löser gestartet. Nach dem Beenden der Lösungsiterationen werden die abgeleiteten Größen berechnet und die Ergebnisse herausgeschrieben.

Die Funktionsweise der einzelnen Methoden wird in den nachfolgenden Abschnitten genauer erläutert.

\section{Datenverwaltung}
Die Dateiverwaltung geschieht zentral in der Klasse \verb|Daten|. Um auf die Daten zugreifen zu können, wird immer ein Zeiger auf ein Datenobjekt übergeben. Alle Variablen sind als \verb|private| deklariert, damit sind diese Variablen für Methoden außerhalb der Klasse nicht sichtbar. Man hat also eine Datenkapselung und kann somit vermeiden, dass ein anderes Programm auf die Daten zugreift. Um jetzt aber doch auf die Daten gewollt zugreifen zu können, sind  entsprechende ,,\textit{setze}'' und ,,\textit{bekomme}'' Methoden vorhanden. Diese Methoden weisen der jeweiligen Variable einen Wert zu oder geben den Variablenwert zurück.

Um die Größe von Feldern erst während der Laufzeit festlegen zu können, muss man in C++ mit Zeigern arbeiten, was im Gegensatz zu Java sehr unkomfortabel ist. Die Zeiger müssen erzeugt und wieder gelöscht  werden, was einen nicht unerheblichen Schreibaufwand bedarf.

\section{Ableitungen}
\label{kap02:3}
Die Ableitungen im Strömungsfeld werden mit dem zentralen Differenzenverfahren $\mathcal{O}(2)$ ermittelt. An den Rändern versagt aber das zentrale Differenzenverfahren, so dass dort mit einem stromauf bzw. stromab Verfahren gearbeitet wird.
Es ist zu unterscheiden entlang welcher Richtung die Ableitung gebildet werden soll.
$F_{i,j,k}$ steht für einen beliebigen Funktionswert an der Stelle $i,j,k$ .

\subsection{Erste Ableitung - Stromlinie - Meridianrichtung}
\label{kap02:3:1}
Die erste Ableitung entlang der Meridianrichtung auf einer Stromlinie im Strömungsfeld ist gegeben durch:
\begin{equation}
\label{gl0002_1_1}
F_{i,j,k}^\prime = \dfrac{F_{i,j+1,k}-F_{i,j-1,k}}{\sqrt{\left(z_{i,j+1,k}-z_{i,j-1,k} \right) ^2 + \left(r_{i,j+1,k}-r_{i,j-1,k} \right) ^2}}
\end{equation} 
Die erste Ableitung entlang der Meridianrichtung auf einer Stromlinie an den Rändern ist gegeben durch:
\begin{equation}
\label{gl0002_1_2}
F_{i,j,k}^\prime = \pm \dfrac{- F_{i,j\pm2,k} + 4 F_{i,j\pm1,k} - 3 F_{i,j,k}}{ \sqrt{\left(z_{i,j\pm2,k}-z_{i,j,k} \right) ^2 + \left(r_{i,j\pm2,k}-r_{i,j,k} \right) ^2}}
\end{equation} 
Das ,,$+$'' steht für eine stromauf Diskretisierung für den vorderen Rand und das ,,$-$'' für eine stromab Diskretisierung für den hinteren Rand. 
Es ist zu beachten, dass alle Methoden die Übergabereihenfolge $(i,j,k)$ der Indizes haben. Doch die Felder wurden mit der Reihenfolge $[i][k][j]$ initialisiert. 

\subsection{Erste Ableitung - Stromlinie - z-Koordinate}
Die anderen möglichen Ableitungen unterscheiden sich nur in der Indizierung. Der Vollständigkeit wegen sollen sie trotzdem angegeben werden.
Die erste Ableitung entlang der z-Koordinate auf einer Stromlinie im Strömungsfeld ist gegeben durch:
\begin{equation}
\label{gl0002_1_3}
F_{i,j,k}^\prime = \dfrac{F_{i,j+1,k}-F_{i,j-1,k}}{z_{i,j+1,k}-z_{i,j-1,k}}
\end{equation} 
Die erste Ableitung entlang der z-Koordinate auf einer Stromlinie an den Rändern ist gegeben durch:
\begin{equation}
\label{gl0002_1_4}
F_{i,j,k}^\prime = \pm \dfrac{- F_{i,j\pm2,k} + 4 F_{i,j\pm1,k} - 3 F_{i,j,k}}{ z_{i,j\pm2,k}-z_{i,j,k}}
\end{equation} 
Das Vorzeichen ist entsprechend den Hinweisen in \kapref{kap02:3:1} zu wählen. 

\subsection{Erste Ableitung - Quasiorthogonale - z-Koordinate}
Die erste Ableitung entlang der z-Koordinate auf einer Quasiorthogonalen im Strömungsfeld ist gegeben durch:
\begin{equation}
\label{gl0002_1_5}
F_{i,j,k}^\prime = \dfrac{F_{i+1,j,k}-F_{i-1,j,k}}{z_{i+1,j,k}-z_{i-1,j,k}}
\end{equation} 
Die erste Ableitung entlang der z-Koordinate auf einer Quasiorthogonalen an den Rändern ist gegeben durch:
\begin{equation}
\label{gl0002_1_6}
F_{i,j,k}^\prime = \pm \dfrac{- F_{i\pm2,j,k} + 4 F_{i\pm1,j,k} - 3 F_{i,j,k}}{ z_{i\pm2,j,k}-z_{i,j,k}}
\end{equation} 
Das Vorzeichen ist entsprechend den Hinweisen in \kapref{kap02:3:1} zu wählen. 

\subsection{Erste Ableitung - Quasiorthogonale - r-Koordinate}
Die erste Ableitung entlang der r-Koordinate auf einer Quasiorthogonalen im Strömungsfeld ist gegeben durch:
\begin{equation}
\label{gl0002_1_7}
F_{i,j,k}^\prime = \dfrac{F_{i+1,j,k}-F_{i-1,j,k}}{r_{i+1,j,k}-r_{i-1,j,k}}
\end{equation} 
Die erste Ableitung entlang der r-Koordinate auf einer Quasiorthogonalen an den Rändern ist gegeben durch:
\begin{equation}
\label{gl0002_1_8}
F_{i,j,k}^\prime = \pm \dfrac{- F_{i\pm2,j,k} + 4 F_{i\pm1,j,k} - 3 F_{i,j,k}}{ r_{i\pm2,j,k}-r_{i,j,k}}
\end{equation} 
Das Vorzeichen ist entsprechend den Hinweisen in \kapref{kap02:3:1} zu wählen. 

\section{Winkel}

\subsection{Winkelbestimmung}

Da jede Arkusfunktion nur auf einen $\pi$ breiten Streifen abgebildet wird, ist es nicht ohne weiteres möglich einen Winkel eineindeutig zu bestimmen.

Diesem Umstand wurde insofern Rechnung getragen, dass $\ud r$ und $\ud z$ separat übergeben werden. An Hand der jeweiligen Vorzeichen kann ein Winkel eineindeutig durch folgende Fallunterscheidungen bestimmt werden.

\begin{tabular}{|c|c|c|}
\hline
$\ud r$ & $\ud z$ & Rückgabewert \\
\hline
$> 0$ & $> 0$ & $\arctan\left(\dfrac{\ud r}{\ud z}\right)$ \\
\hline
$< 0$ & $> 0$ & $\arctan\left(\dfrac{\ud r}{\ud z}\right) + \pi$  \\
\hline
$< 0$ & $< 0$ & $\arctan\left(\dfrac{\ud r}{\ud z}\right) - \pi$ \\
\hline
$> 0$ & $< 0$ & $\arctan\left(\dfrac{\ud r}{\ud z}\right)$ \\
\hline
\end{tabular} 

\subsection{Winkel - \texorpdfstring{$\varepsilon$}{epsilon}}
%\subsection{Winkel - $\varepsilon$}

Wie aus \abref{a0002_03} ersichtlich, ist
\begin{equation}
\label{gl0002_3_1}
\varepsilon = \arctan\left( \dfrac{\ud r}{\ud z}\right) 
\end{equation} 
Wobei diese erste Ableitung entlang einer Stromlinie gebildet wird. 

\begin{figure}[htb]
\makebox[\textwidth]{\includegraphics[width=0.8\textwidth]{Quasi-Orthogonale}}
\caption{Quasiorthogonale \label{a0002_03}}
\end{figure}

\subsection{Winkel - \texorpdfstring{$\gamma$}{gamma}}
%\subsection{Winkel - $\gamma$}

Die Berechnung von $\gamma$ erfolgt mit
\begin{equation}
\label{gl0002_3_2}
\gamma = \arctan\left( \dfrac{\ud r}{\ud z}\right) 
\end{equation}
Hier ist die erste Ableitung entlang einer Quasiorthogonalen zu berechnen. 

\subsection{Winkel - \texorpdfstring{$\beta$}{beta}}
%\subsection{Winkel - $\beta$}

Der Winkel $\beta$ wird mit Hilfe von \glref{gl0100_1_28ab} wie folgt berechnet, da in C++ keine Methoden für die Behandlung des Kotanges vorhanden sind.
\begin{equation}
\label{gl0002_3_3}
\beta = \mathrm{arccot} \left( \sin \varepsilon \cdot \cot \beta_{ru} + \cos \varepsilon \cdot \cot \beta_{zu} \right)
% BB: laut Quellcode arccot version benutzen!
%\beta = \arctan\left( \dfrac{1}{\frac{\sin\varepsilon}{\tan\beta_{ru}}+ \frac{\cos\varepsilon}{\tan\beta_{zu} } } \right) 
\end{equation} 

Falls $\beta = \frac{\pi}{2}$,  geht $\Delta z \rightarrow 0$. Durch numerische Unsauberkeiten, kann es leicht zu einem Vorzeichenwechsel kommen. Dann liefert die C++ Standardmethode für $\beta$ nicht $\frac{\pi}{2}$ sondern $-\frac{\pi}{2}$ zurück. 
Diesem Umstand wurde insofern Rechnung getragen, dass $\beta = \beta + \pi$ gelten soll, falls $\beta < 0$. Damit hat $\beta$ einen Gültigkeitsbereich von $0$ bis  $\pi$.


\subsection{Krümmungsradius und Felder mit Winkelfunktionen}
Der reziproke Krümmungsradius wird nach \glref{gl0100_1_31} mit einer ersten Ableitung auf einer Stromlinie entlang der Meridianrichtung berechnet.

Das Feld wird für den $\cot\beta$ wie folgt berechnet
\begin{equation}
\label{gl0002_3_4}
\cot\beta = \dfrac{1}{\tan\beta}
\end{equation} 

% In \anref{epsilon:6} und \anref{epsilon:7} werden die $\cos\varepsilon$ und $\sin\varepsilon$ Felder berechnet und zugewiesen.

\section{Geometrie}

\subsection{\texorpdfstring{\nurbs}{NURBS} Kurven}
%\subsection{\nurbs Kurven}

Mit Hilfe von \nurbs Kurven kann man geometrische Kurven im Raum beschreiben und physikalische Größen auf diesen interpolieren.
Dabei steht die Abkürzung \nurbs für \textit{$\mathcal{N}$on $\mathcal{U}$niform $\mathcal{R}$ational $\mathcal{B}$-$\mathcal{S}$pline} und stellt eine Verallgemeinerung von B\'ezierkurven und B-Splines dar. 
Für weiterführende Erläuterungen sei auf \cite{piegl:1997} verwiesen. 
Für die Implementierung wird die Programmbibliothek NURBS++ \cite{nurbs:2002} verwendet. 

Für die Beschreibung einfacher geometrischer Kurven, wie den Quasiorthogonalen, verwendet man 3D Punkte. Ein Punkt im 3D-Raum ist definiert als $P(x,y,z)$. Dem gegenüber wird für die Interpolation einer Größe auf dieser Kurve ein 4D Punkt benötigt, welcher als $P(x,y,z,q)$ definiert wird, wobei $q$ ein beliebiger skalarer Wert sein kann, bspw. die Temperatur.
Wenn die geometrischen Koordinaten der Punkte im 3D und 4D, mit denen die \nurbs Kurven erzeugt werden, übereinstimmen, dann liefern die Kurven beim gleichen $\nu$ Wert auch identische geometrische Koordinaten zurück. Die NURBS++ Programmbibliothek ist nur in der Lage kartesische Koordinaten zu bearbeiten.

\underline{Hinweis:} Die Programmbibliothek ist nur im Quelltext erhältlich und wird seit dem Jahre 2002 nicht mehr gepflegt. Vor deren Benutzung muss sie deshalb kompiliert werden. Moderne Kompilierer orientieren sich zusehends am ISO/IEC 14882:2003 Standard von C++. Der Quelltext der Programmbibliothek weicht aber von diesem ISO-Standard ab, deshalb muss der Kode korrigiert werden. 
Entsprechende Korrekturdateien stehen auf der Netzseite zur Verfügung.

\subsection{Initialisierung der Geometrie}
\label{kap02:5:2}
Um die Quasiorthogonalen beschreiben zu können, werden \nurbs Kurven, welche durch 3D Punkte beschrieben werden, erzeugt.
Die Vorgehensweise ist bei 3D und 4D Kurven prinzipiell identisch. Für jede Quasiorthogonale wird  ein Vektor mit den diskreten Punkten, welche als r- und z-Koordinate eingelesen wurden, erzeugt. Dieser Vektor wird für die Interpolation der \nurbs Kurve benötigt. Als x-Koordinate wird der $r$ Wert eingelesen. Die y-Koordinate ist immer Null und bei der z-Koordinate ändert sich nichts. Die gewählte Interpolationsmethode benötigt mindestens (Grad + 1) = 4 Punkte zur Lösung des Gleichungssystems, da die \nurbs Kurven den Grad 3 haben.
% Die Initialisierung der \nurbs Kurven, welche durch 4D Punkte beschrieben werden. Der einzige Unterschied besteht darin, dass zusätzlich ein Wert je Punkt eingelesen werden muss.

\subsection{Aktualisierung der Geometrie}
Nach jeder großen Iterationsschleife verändert sich die Lage der Stromlinien und damit die $\nu$ Werte auf den einzelnen Quasiorthogonalen. Deshalb müssen vor der nächsten Iteration alle mittels \nurbs Kurven interpolierte Werte neu in die einzelnen Felder eingelesen werden. Dies geschieht für die einfache Geometrie und für die anderen Werte wie $\beta_{ru}$, $\beta_{zu}$, $\sigma$ und $s$.

\subsection{Berechnung der Startwerte für \texorpdfstring{$\nu$}{nu}}
%\subsection{Berechnung der Startwerte für $\nu$}

Es werden wie erwähnt nur r- und z-Koordinaten eingelesen. Für die weitere Berechnung werden aber auch die $\nu$ Werte benötigt. Damit diese nicht noch zusätzlich eingelesen werden müssen, werden sie berechnet. Die dabei verwendete Methode (\verb|minDist2|)  berechnet eigentlich den Minimalabstand zwischen dem übergebenen Punkt und der \nurbs Kurve. Die $\nu$ Koordinate auf der \nurbs Kurve bei welcher der Abstand zum Punkt minimal wird, ist in der Variablen \verb|nu_temp| gespeichert. Der eigentlich berechnete Abstand, welcher in der Variablen \verb|temp| gespeichert wird, interessiert nicht weiter, denn er sollte identisch Null sein, da mit dem übergebenem Punkt die \nurbs Kurve initialisiert wird.

\section{Massenstromintegration}
\label{sec:massenstrom}

\subsection{Interpolation der Massenstromdichte}
Die Interpolation der Massenstromdichte $\frac{\dot{m}}{A}$ geschieht mit Hilfe einer \nurbs Kurve.
Es wird dazu die Dichte und die Geschwindigkeit, welche exakt senkrecht zur Quasiorthogonalen zeigt, nach \glref{gl0002_4_1} ermittelt. Dies ist der Wert der 4D Punkte. 
\begin{equation}
\label{gl0002_4_1}
\dfrac{\dot{m}}{A} = \varrho w_m \sin\left(\gamma - \varepsilon \right) 
\end{equation} 
Die Interpolation erfolgt dann analog zu \kapref{kap02:5:2}. 

\subsection{Massenstrom je Stromröhre}
\label{kap02:6:2}
Für die Berechnung des Massenstromes wird die Quasiorthogonale zwischen den beiden Stromlinien in eine bestimmte \textit{Anzahl Zwischenpunkte} aufgeteilt und anschließend nach folgender Gleichung berechnet
\begin{equation}
\label{gl0002_4_2}
\dot{m}_{Rohr} = \sum_0^{z_p} \Delta\frac{\dot{m}}{A}(z_p)\cdot A(z_p)
%\dot{m}_{\textit{Röhre}} = \sum_{zp} \left[\varrho w_m \sin\left(\gamma - \varepsilon \right) \right] \: \varphi \: \Delta l  \:\: r
\end{equation} 
Desweiteren sind 
\begin{equation}
\label{gl0002_4_3}
A(z_p) = \varphi \cdot r \cdot \left( (\mathrm{d}r_{hi}-\mathrm{d}r_{lo})\cos\varepsilon - (\mathrm{d}z_{hi}-\mathrm{d}z_{lo})\sin\varepsilon \right)
% \Delta\nu =  \dfrac{\nu_{i}+\nu_{i+1}}{\textit{Anzahl Zwischenpunkte}}
\end{equation} 
und 
\begin{equation}
\label{gl0002_4_4}
\Delta \frac{\dot{m}}{A} = \rho \cdot w_m
% \nu_{temp} =  \nu_{i}+ zp \Delta\nu + \dfrac{\Delta\nu}{2}
\end{equation}
gegeben.

Die einzelnen Komponenten der \glref{gl0002_4_3} bestimmen sich wie folgt

% \begin{equation}
% \label{gl0002_4_5}
% \left[\varrho w_m \sin\left(\gamma - \varepsilon \right) \right] = \dfrac{1}{2}
% \left[\dfrac{\dot{m}}{A}\bigg(\nu_i + zp \: \Delta\nu \bigg) + \dfrac{\dot{m}}{A}\bigg( \nu_i + \left(zp+1\right) \: \Delta\nu \bigg)\right] = \dfrac{\dot{m}}{A}\bigg(\nu_{temp} \bigg) \qquad \:
% \end{equation}

\begin{equation}
\label{gl0002_4_6}
\varphi = 2 \pi \: \sigma\left(\nu_{temp} \right)
\end{equation}

\begin{equation}
\label{gl0002_4_7}
\mathrm{d}x_{hi} = x(\nu_{low}+(z_p+1)\Delta\nu)
\end{equation}

\begin{equation}
\label{gl0002_4_7a}
\mathrm{d}x_{lo} = x(\nu_{low}+z_p\Delta\nu)
\end{equation}

% \begin{equation}
% \label{gl0002_4_7}
% \Delta l = \sqrt{\left( \Delta r\right)^2 + \left( \Delta z \right)^2}
% \end{equation}

\subsection{Massenstrom je Quasiorthogonale}
Den Massenstrom auf einer Quasiorthogonalen erhält man durch Aufsummation der einzelnen Massenströme je Stromröhre. 

\section{Löser}

\subsection{Iterationsschleifen}
Die Lösung des Problems erfolgt, wie in \kapref{kap01} beschrieben. In \ref{nsd} ist der grundsätzliche Ablauf des Verfahrens dargestellt. Die benötigten Methoden werden in den einzelnen Schleifen aufgerufen. Der Name einer Variablen sollte niemals identisch mit dem einer Klasse sein. Sich wiederholende Aufrufe mehrerer Methoden werden in eigene Methoden ausgegliedert. Dazu gehören:
\begin{itemize}
\item Berechnung des Massenstromes und Finden eines neuen Druckwertes an der Nabe
\item Zurücksetzen der alten Druckwerte auf Null
\item Berechnung von abgeleiteten Werten
\end{itemize}

\subsection{Berechnung von \texorpdfstring{$\frac{\ud p}{\ud \nu}$}{dpdn}}
% \subsection{Berechnung von $\frac{\ud p}{\ud \nu}$}

Es wird $\frac{\ud p}{\ud \nu}$ berechnet. Falls $\sigma = 0$ ist, geht man davon aus, dass man sich im schaufelfreien Raum befindet. Dadurch vereinfacht sich \glref{gl0001_1_1} zu 
\newline \glref{gl0100_0_24}, denn es gilt $\frac{\partial p}{\partial \varphi} = 0$. Wenn man durch $\sigma$ auch aerodynamisch bedingte Versperrungen berücksichtigen will, dann gilt obige Annahme natürlich nicht und man würde \glref{gl0001_1_1} für $\sigma > 0$, d.h. den allgemeinen Fall, lösen.

\subsection{Berechnung des Druckes}
\label{kap02:2}

Das Verfahren versagt, wenn $Ma_{krit} = 1$ ist, dann werden die Nenner der Vorfaktoren von \glref{gl0001_1_1} und \glref{gl0100_0_24} zu Null. Die kritische Machzahl bestimmt sich im beschaufelten Raum zu

\begin{equation}
\label{gl0002_2_0a}
Ma_{krit} = \sqrt{\left(\dfrac{w_m}{a}\right)^{2} \left( 1+\cot^{2}\beta \right)}
\end{equation}

Im schaufelfreien Raum wird die kritische Machzahl durch die Meridionalmachzahl wie folgt bestimmt

\begin{equation}
\label{gl0002_2_0b}
Ma_{krit} = \dfrac{w_m}{a}
\end{equation}

Falls $Ma_{krit} = 1$ ist, dann wird der Druck nicht durch \glref{gl0001_1_1} bzw. \glref{gl0100_0_24} bestimmt. Stattdessen wird der kritische Druck gesetzt, unter der Voraussetzung, dass der Totaldruck zwischen den beiden Quasiorthogonalen auf der nächsten Stromlinie identisch ist. Die Berechnung erfolgt dann mit \glref{gl0001_4_6}.

Falls aber $Ma_{krit} \neq 1$ ist, muss \glref{gl0001_1_1} bzw. \glref{gl0100_0_24} gelöst werden. Dies geschieht mit einem expliziten Eulerverfahren nach \cite{oertel:2003}. Der Druckwert auf der nächsten Stromlinie berechnet sich damit zu:
\begin{equation}
\label{gl0002_2_1}
p_{i+1} = p_i + \dfrac{\ud p}{\ud \nu} \Delta \nu
\end{equation} 

\section{Bisektionsverfahren}
\label{sec:bisektion}
Das Bisektionsverfahren sucht die Nullstelle einer Funktion. Es sind zwei Startwerte notwendig: $x_{pos}$ mit $f(x_{pos}) > 0$ und $x_{neg}$ mit $f(x_{neg}) < 0$. Damit ist gewährleistet, dass zwischen $x_{pos}$ und $x_{neg}$ mindestens eine Nullstelle liegt. Der neue $x$-Wert berechnet sich dann zu:
\begin{equation}
\label{gl0002_2_2}
x_{neu} = \dfrac{x_{pos}+x_{neg}}{2}
\end{equation} 
Jetzt wird der neue Funktionswert an der Stelle $x_{neu}$ berechnet. Abhängig, ob $f(x_{neu}) \gtrless 0$ ist, wird entsprechend $x_{pos}$ oder $x_{neg}$ aktualisiert. Der nächste $x$-Wert berechnet sich wieder mit \glref{gl0001_5_1}. Dieses Verfahren konvergiert bei der Vorgabe richtiger Startwerte immer.

\subsection{Bestimmung der neuen Lage der Stromlinien}
\label{kap02:8:1}
Durch die Vorgabe, wieviel Prozent des Massenstromes durch eine Stromröhre gehen sollen, gibt es auf der Quasiorthogonalen nur einen Punkt, bei dem dies identisch erfüllt ist. Es soll die Nullstelle folgender Funktion gesucht werden:
\begin{equation}
\label{gl0002_2_3}
f(\nu_i) = \dfrac{\dot{m}_{\mbox{Vorgabe}}}{\mbox{Anzahl Stromlinien}-1}-\dot{m}_{\mbox{Röhre}}
\end{equation}  
Als Startwerte werden jedesmal $\nu_{neg} = 0$ und $\nu_{pos} = 1$ festgelegt. Die Berechnung eines neuen $\nu$ Wertes erfolgt dann mit \glref{gl0002_2_2}.

\subsection{Bestimmung eines neuen Druckwertes an der Nabe}

\begin{figure}[htb]
\makebox[\textwidth]{\includegraphics[width=0.8\textwidth]{diagramme_1}}
\caption{Massenstromverläufe \label{ab0100_01}}
\end{figure}

In \abref{ab0100_01} ist der Massenstromverlauf, welcher sich berechnet, über dem Verhältnis von statischem Druck zu Totaldruck aufgetragen. Von rot über orange nach blau steigt der vorgegebene Massenstrom $\dot{m}_{Vorgabe}$ stetig an. Diese Kurve hat für physikalisch wichtige Fälle zwei Nullstellen. Diese Nullstellen zu finden ist nicht ganz einfach, denn es gibt im allgemeinen immer eine Unter- und Überschalllösung. Man kann also nicht von Beginn an sagen, welches die Startwerte sind. Das Hauptproblem besteht darin, die richtigen Startwerte zu finden. 
Es soll eine Nullstelle folgender Funktion gefunden werden. 
\begin{equation}
\label{gl0002_2_4}
f(p_{i=0}) = \dot{m}_{Vorgabe}-\dot{m}
\end{equation}  
Die gefundene Nullstelle ist abhängig vom ersten Startwert. Ist der Druck größer als der kritische Druck, dann wird nach der Unterschalllösung gesucht, ansonsten nach der Überschalllösung. 
Ist $f(p_{i=0}) > 0$ und $p_{neg} = 0$, dann ist $p_{pos} = p_{i=0}$ und $p_{neu}$ errechnet sich mit
\begin{equation}
\label{gl0002_2_5}
p_{neu} = \dfrac{p_{pos}+p_{krit}}{2}
\end{equation}
Dies wird solange fortgesetzt, bis $f(p_{i=0}) < 0$ erreicht ist. Denn dann wird $p_{neg} = p_{i=0}$ gesetzt und man hat zwei geeignete Startwerte um den neuen Druckwert analog zu \glref{gl0002_2_2} zu bestimmen.

Wenn $f(p_{i=0}) < 0$ und $p_{pos} = 0$, dann ist zusätzlich zu unterscheiden, ob man die Unter- oder Überschalllösung sucht. Für die Unterschalllösung ist $p_{neg} = p_{i=0}$ und ein geeigneter zweiter Startwert wird wie folgt ermittelt:
\begin{equation}
\label{gl0002_2_6}
p_{neu} = \dfrac{p_{neg}+p_{tot}}{2}
\end{equation}
Für die Überschalllösung gilt
\begin{equation}
\label{gl0002_2_7}
p_{neu} = \dfrac{p_{neg}}{2}
\end{equation}
Dies wird solange fortgeführt, bis $f(p_{i=0}) > 0$ erreicht ist. Denn dann wird $p_{pos} = p_{i=0}$ gesetzt und man hat zwei geeignete Startwerte um den neuen Druckwert analog zu \glref{gl0002_2_2} zu bestimmen.

% Die Implementation dieses Bisektionsverfahrens erfolgt in \anref{newton:1}.

\section{Stromlinien}
\subsection{Neue Stromlinien}
Die neue Lage der Stromlinien wird berechnet. Es kommt dabei das unter \kapref{kap02:8:1} beschriebene Bisektionsverfahren zum Einsatz. Der Massenstrom in jeder Stromröhre für eine neuen $\nu$ Wert wird dabei mit dem unter \kapref{kap02:6:2} beschriebenen Algorithmus berechnet. Dies wird solange fortgesetzt bis sich der vorgegebene Massenstrom je Stromröhre einstellt, dieser wird mit Hilfe der Variablen \verb|faktorMassenStrom| bestimmt.

\subsection{Überprüfe Stromlinien}
Es wird die Lage der Stromlinien überprüft. Falls sich diese gegenüber der vorhergehenden Iteration geändert hat, wird die Berechnung fortgesetzt. 


%% ====================================================
%%  **********       ENDE  Kapitel 5        **********
%% ====================================================

