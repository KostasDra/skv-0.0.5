/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm
*/



#include "newton.h"



newton::newton()
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	quasiorthogonale 	= &datenObjekt->quasiorthogonale;
}





double newton::newtonVerfahren_p ( int j, int k )
{
	double deltaP = 0.05;
	
	
	//! Falls \f$ p_{alt,-}=0 \f$:
	//! \f[	p = \frac{p_{alt,+}+p_{krit}}{2}	\f]
	//! Sonst:
	//! \f[	p = \frac{p_{alt,+}+p_{alt,-}}{2}	\f]
	if ( ( datenObjekt->massenStromVorgabe-(*quasiorthogonale)[k][j].massenStrom ) > 0 ) // Falls zu wenig Massenstrom:
	{
		(*quasiorthogonale)[k][j].p_alt_pos = (*knoten)[k][0][j].p;

		if ( (*quasiorthogonale)[k][j].p_alt_neg == 0 )
		{
			return ( (*quasiorthogonale)[k][j].p_alt_pos + (*knoten)[k][0][j].p_krit ) *0.5;
		}
		else
		{
			
			return ( (*quasiorthogonale)[k][j].p_alt_pos + (*quasiorthogonale)[k][j].p_alt_neg ) *0.5;
		}
	}
	else // Falls zu viel Massenstrom:
	{
		(*quasiorthogonale)[k][j].p_alt_neg = (*knoten)[k][0][j].p;
		
		if ( (*knoten)[k][0][j].p > ( (*knoten)[k][0][j].p_krit * (1.0+deltaP) )  && (*quasiorthogonale)[k][j].p_alt_pos == 0 ) 
		{
			return ( (*quasiorthogonale)[k][j].p_alt_neg +(*knoten)[k][0][j].p_tot_rel ) *0.5;
		}

		else if ( (*knoten)[k][0][j].p < ( (*knoten)[k][0][j].p_krit * (1.0-deltaP) )  && (*quasiorthogonale)[k][j].p_alt_pos == 0 )
		{
			return ( (*quasiorthogonale)[k][j].p_alt_neg ) *0.5;
		}
		else
		{
			return ( (*quasiorthogonale)[k][j].p_alt_pos +(*quasiorthogonale)[k][j].p_alt_neg ) *0.5;
		}
	}

}





double newton::newtonVerfahren ( double m_ziel,double m_akt,double ny_neu, int i, int j, int k )
{
	if ( ( m_ziel-m_akt ) > 0 ) //! Falls zu "wenig" Massenstrom in Röhre -> \f$ \nu \f$ muss größer werden:
	{
		//! \f[	\nu_{alt,+} = \nu_{neu}	\f]
		(*knoten)[k][i][j].ny_alt_pos = ny_neu;
		//! \f[	\nu = \frac{\nu_{neu}+\nu_{alt,-}}{2}	\f]
		return ( ny_neu+(*knoten)[k][i][j].ny_alt_neg ) /2;
	}
	else //! Falls zu "viel" Massenstrom in Röhre -> \f$ \nu \f$ muss kleiner werden:
	{
		//! \f[	\nu_{alt,-} = \nu_{neu}	\f]
		(*knoten)[k][i][j].ny_alt_neg = ny_neu;
		//! \f[	\nu = \frac{\nu_{neu}+\nu_{alt,+}}{2}	\f]
		return ( (*knoten)[k][i][j].ny_alt_pos +ny_neu ) /2;
	}
}
