%% ==============================================================
%% SKV Manual von Oliver Borm, Andrea Bader, Balint Balassa, Florian Mayer  
%% steht unter einer Creative Commons Namensnennung - Weitergabe 
%% unter gleichen Bedingungen 3.0 Deutschland Lizenz.
%% http://creativecommons.org/licenses/by-sa/3.0/de/
%% ==============================================================

%% ====================================================
%%  **********           Kapitel 6          **********
%% ====================================================

\chapter{Meridiangeschwindigkeit-Löser}
\label{kap06}

Das Lösungsverfahren nach Denton unterscheidet sich vom Schleifenaufbau her grundlegend von dem Druck-Löser. Es wird jedoch die gleiche Programmstruktur benutzt, was eine Fusion der beiden Codes überhaupt erst möglich gemacht hat.

\begin{itemize}
\item Annahme der Stromlinienlage in der Meridianfläche (z.B. Flächengleiche Linien)
\item Wahl eines Anfangswertes für die Meridiangeschwindigkeit an allen Punkten im Rechengitter
\item Lösung der DGL entlang der Quasiorthogonalen
\item Integration des Durchsatzes. Ist dieser unterschiedlich zum vorgegebenen Durchsatz $\rightarrow$ Iteration der Meridiangeschwindigkeit bis der Durchsatz übereinstimmt.
\end{itemize}

Danach ergibt sich eine neue Lage der Stromlinien. Diese werden aber nur mit einem Dämpfungsfaktor verändert, um die Stabilität des Verfahrens zu gewährleisten. Anschließend wird die DGL erneut gelöst, bis sich die Meridiangeschwindigkeiten nicht mehr ändern, oder andere Abbruchkriterien zum Tragen kommen.


\section{Erläuterung zur Differenzialgleichung nach Denton}
In dieser Formulierung der Differenzialgleichung tritt keine Unstetigkeit für den Schalldurchgang der Relativgeschwindigkeit auf, wodurch sich die supersonische Zuströmung am Laufradeintritt ohne Sonderbehandlung realisieren lässt... Diese Formulierung der Differenzialgleichung basiert auf Denton \cite{denton:1978} und wurde von Casey und Roth \cite{casey:1984} in gleicher Form angewandt. 
... die Differenzialgleichung (nimmt), auf Grundlage einer axialsymmetrischen Strömung $\frac{\partial}{\partial \varphi}=0$, folgende Gestalt an:
%	\frac{\overline{\partial}}{\partial}
\begin{equation}
\label{gl01:01}
\frac{1}{2}\frac{\overline{\partial}w^2_m}{\partial q} = \underbrace{w_m\frac{\overline{\partial}w_m}{\partial m}\cos \psi + \frac{w_m^2}{R_k}\sin \psi}_\textrm{Terme der Stromlinienkrümmung} + \underbrace{\frac{\overline{\partial}h_t}{\partial q}-T\frac{\overline{\partial}s}{\partial q}-\frac{1}{2r^2}\frac{\overline{\partial}(rc_u)^2}{\partial q}}_\textrm{Terme des radialen Gleichgewichts} + F_{R,m} + F_S
\end{equation}

Die „Terme der Stromlinienkrümmung“ fassen die entlang der geneigten Stromlinie wirksame konvektive Beschleunigung und die durch die Stromlinienkrümmung hervorgerufene Normalbeschleunigung zusammen. Die weiteren Terme werden in Anlehnung an Casey \cite{casey:1984} als „Terme des radialen Gleichgewichts“ bezeichnet. Der enthaltene Entropiegradient gliedert sich, wie in Gleichung \ref{gl01:02} wiedergegeben, in einen reversiblen und einen irreversiblen Anteil:

\begin{equation}
\label{gl01:02}
T\frac{\overline{\partial}s}{\partial q} = T\frac{\overline{\partial}s_{rev}}{\partial q} + T\frac{\overline{\partial}s_{irr}}{\partial q}
\end{equation}

Der reversible Teil beschreibt den Wärmeaustausch, der im adiabaten System nicht vorhanden ist, d.h. $\dot{q}=0 $ ist. Der irreversible Anteil beruht dagegen auf Reibungsvorgängen. Aus diesem Grund sind Reibungseffekte in $q$−Richtung bereits in den „Termen des radialen Gleichgewichts“ enthalten. Die Reibungseffekte entlang der Stromlinie werden durch folgende Reibungskraft

\begin{equation}
\label{gl01:03}
F_{R,m} = \cos \psi T \frac{\overline{\partial}s}{\partial m}
\end{equation}

nach \cite{casey:2008} im beschaufelten und unbeschaufelten Raum gleichermaßen einbezogen. Aus der Berücksichtigung der Komponente in Umfangsrichtung lässt sich Schaufelkraft mit

\begin{equation}
\label{gl01:04}
F_S = \tan \alpha \frac{w_m}{r}\frac{\overline{\partial}(rc_u)}{\partial m}
\end{equation}

modellieren. Im unbeschaufelten Raum gilt dabei $F_S = 0$.


\subsection{Modifikation der Differentialgleichung}
Eine modifizierte Form der Differenzialgleichung nach Denton, unter Berücksichtigung der Gradienten in Umfangsrichtung $\frac{\partial}{\partial \varphi}\neq0$, wurde in detaillierter Vektoranalysis von Benetschik (vgl. \cite{benetschik:2010}) erarbeitet. Diese Modifikation entsteht durch zusätzliche Berücksichtigung der m−Komponente der Bewegungsgleichung und ist in Gleichung \ref{gl01:05} angegeben:

\begin{align}
\label{gl01:05}
\frac{1}{2}\frac{\overline{\partial}w^2_m}{\partial q} = w_m\frac{\overline{\partial}w_m}{\partial m}\cos \psi + \frac{w_m^2}{R_k}\sin \psi + \frac{\overline{\partial}h_t}{\partial q}-T\frac{\overline{\partial}s}{\partial q}-\frac{1}{2r^2}\frac{\overline{\partial}(rc_u)^2}{\partial q} \notag \\
 + \underbrace{\left[ \frac{1}{2r^2}\frac{\overline{\partial}(rc_u)^2}{\partial m}-\omega\frac{\overline{\partial}(rc_u)}{\partial m} \right] \frac{\frac{r\partial \varphi}{\partial q}}{\frac{r\partial \varphi}{\partial m}}}_{\textrm{Schaufelkraft }F_S} + \underbrace{\left[ T\frac{\overline{\partial}s}{\partial m}\right]\frac{\frac{r\partial \varphi}{\partial q}}{\frac{r\partial \varphi}{\partial m}} }_{\textrm{Reibungskraft } F_{R,m}}
\end{align}


Auf der rechten Seite der Gleichung \ref{gl01:05} ist der analytisch begründete Term $ w_m\frac{w_u}{r}\frac{\partial \Phi}{\partial \phi} \sin \psi$ bereits vernachlässigt worden, da die Verwindung der S2-Fläche nicht berechnet werden kann. Die im Vergleich zur Gleichung \ref{gl01:01} abgewandelten Terme werden im Berechnungsverfahren, wie in Gleichung \ref{gl01:05} gekennzeichnet, analog als „Schaufel“- und „Reibungskraft“ bezeichnet.

\section{Algorithmus}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Empfohlene Breite für das NSD: 150
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{struktogramm}(145,140)[Nassi-Shneiderman-Diagramm des SKV für Meridiangeschwindigkeit-Löser \label{nsdWM}]
  \assign{Initialisieren der Daten und Parameter, u.a. Meridiangeschwindigkeit $w_m$}
  \until{Bis Meridiangeschwindigkeit sich nicht mehr verändert, die maximale Zeit abgelaufen ist, oder die maximale Anzahl an Iterationen erreicht ist}
    \assign{Außer bei allererster Iteration: aktualisiere Relaxation, Geometrie, Ableitungen, Deviation nach \ref{sec:deviation}, Entropieerhöhung nach \glref{gl01:07d}}
    \while{Für jedes Gitter:}
      \while{Für alle QO des Gitters:}
	\assign{Bestimme Drall $rc_u$ nach \glref{gl01:06}}
	\assign{Update Geschwindigkeiten $c_u,w_u$ und $c$ nach \ref{sec:Geschwindigkeit}}
	\assign{Setze Enthalpien $h_{tot}$ und $h$ nach \ref{sec:Enthalpie}}
	\assign{Berechne Temperaturen $T_{tot}$ und $T$ nach \ref{sec:temperaturen}}
	\assign{Berechne Schallgeschwindigkeit $a$ nach \glref{gl01:18}}
	\assign{Berechne Druck $p$ nach \ref{sec:druck}}
	\assign{Berechne Dichte $\rho$ nach \glref{gl01:17}}
	\assign{Berechne Liste von $\mathrm{d} w_m^2$ Werten für alle SL der QO mit \glref{gl01:01} oder \glref{gl01:05}}
% 	\assign{$w_m$ nach $w_{m,neu}$ schreiben}
	\assign{DGL Ergebnis auf mittlere Stromlinie skalieren}
	\until{Solange Massenstrom nicht OK und maxIter nicht erreicht ist}
	  \assign{Berechne $w_m$ für alle SL der QO mit den $\mathrm{d} w_m^2$ der Liste}
	  \assign{Falls $\dot{m}\neq\dot{m}_\textrm{Vorgabe}$, $w_m$ auf mittlerer SL anpassen}
	\untilend
      \whileend
    \whileend
    \assign{Neue Lage der Stromlinien berechnen: Neue $\nu$ Werte}
    \assign{Aktualisieren der  Geometrie}
  \untilend
  \assign{Setze integrale Daten nach Abschnitt \ref{sec:abgeleiteteZustande}}
  \assign{Ausgabe (und ggf. Berechnung) der Daten nach \kapref{kap03:2:1}}
\end{struktogramm}
% \label{nsd}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	NSD Ende
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---------------------------------------------------------------------
\subsection{Definition und Berechnung der Stromlinienwinkel } \label{geowinkel}
%---------------------------------------------------------------------
Die Winkel zur Beschreibung der Stromlinienkrümmung sind in Bild \ref{Fig:epsilongammapsi} zusammengefasst. Um Polstellen und Doppeldeutigkeiten der Arkuswinkelfunktionen vorzubeugen, werden die benötigten Winkelfunktionen  meist direkt aus den geometrischen 
Zusammenhängen ermittelt. 


% \begin{figure} [htbp]
% \centering
% \includegraphics[angle=0,width=0.5\textwidth,clip]%
%                 {./bilder/epsilongammapsi}
%   \caption{Winkeldefinitionen der Stromlinienwinkel}
%  \label{Fig:epsilongammapsi}
% \end{figure}




\subsubsection{Funktionen des Winkels $\epsilon$ }
%
Der Winkel $\epsilon$ gibt die Neigung der Stromlinie gegenüber der z- Achse an, wobei folgender Zusammenhang gilt: 
\begin{equation}
 \epsilon = \arctan  \frac{\mathrm{d} r}{\mathrm{d} z}  \ .
\label{tanEpsilon}
\end{equation}
%
Seine weiteren Winkelfunktionen ergeben sich zu
%
\begin{equation}
\sin \epsilon = \frac{\mathrm{d} r}{\mathrm{d} m}  \quad \text{und} \quad \cos \epsilon = \frac{\mathrm{d} z}{\mathrm{d} m} \ .
\label{sincosEpsilon}
\end{equation}
%

\subsubsection{Funktionen des Winkels $\gamma$ }

Der Winkel $\gamma$, der die Neigung der Quasiorthogonalen gegenüber der z-Achse angibt, ist über folgende Winkelfunktionen 
%
\begin{equation}
\sin \gamma = \frac{\mathrm{d} r}{\mathrm{d} q}  \quad \text{und} \quad \cos \gamma = \frac{\mathrm{d} z}{\mathrm{d} q} 
\label{sincosGamma}
\end{equation}
bestimmt.

\subsubsection{Funktionen des Winkels $\psi$ }

Zur Übersichtlichkeit ist der Winkel $\psi$ zwischen einer Stromlinie und einer Quasiorthogonalen aus $\gamma$ und $\epsilon$  definiert als:
%
\begin{equation}
\psi = \gamma - \epsilon  \ .
\label{psiDef}
\end{equation}
%
Seine Winkelfunktionen berechnen sich aus Additionstheoremen der Komponentenwinkel wie folgt:
%
\begin{align} 
\sin\psi &= \sin(\gamma - \epsilon) = \sin\gamma \  \cos\epsilon - \cos\gamma \ \sin \epsilon =  
						\frac{\mathrm{d} r}{\mathrm{d} q}  \frac{\mathrm{d} z}{\mathrm{d} m} - \frac{\mathrm{d} z}{\mathrm{d} q} \frac{\mathrm{d} r}{\mathrm{d} m}  \\
\cos\psi &= \cos(\gamma - \epsilon) = \cos\gamma \  \cos\epsilon + \sin\gamma \ \sin \epsilon =  
						\frac{\mathrm{d} z}{\mathrm{d} q}  \frac{\mathrm{d} z}{\mathrm{d} m} + \frac{\mathrm{d} r}{\mathrm{d} q} \frac{\mathrm{d} r}{\mathrm{d} m}		\ .				
\label{sincosPsi}
\end{align}

%---------------------------------------------------------------------
\subsection{Definition und Berechnung der Stromlinienkrümmung} \label{kruemmung}
%---------------------------------------------------------------------
Die Stromlinienkrümmung ist durch die Änderung des  Neigungswinkels  $\epsilon$ entlang der Stromlinie bestimmt, und es gilt:

\begin{equation}
\frac{1}{R_{\mathrm{k}}} =  \frac{\partial \epsilon}{\partial m}  \ .
\label{betaKrüemmung}
\end{equation}

Unter Verwendung von Gleichung (\ref{tanEpsilon}) ergibt sich für die Krümmung:

\begin{align}
\frac{1}{R_{\mathrm{k}}} &= \frac{\mathrm{d} \left(\arctan \frac{\mathrm{d} r}{\mathrm{d} z}\right)}{\mathrm{d} m} = \frac{1}{\mathrm{d} m} \cdot \frac{\mathrm{d}^2r \:\mathrm{d} z - \mathrm{d} r \: \mathrm{d}^2 z}{\underbrace{\mathrm{d} z^2 +\mathrm{d} r^2}_{\mathrm{d} m^2}}   \nonumber \\
\frac{1}{R_{\mathrm{k}}} &= \frac{\mathrm{d}^2r}{\mathrm{d} m^2}\cdot \underbrace{\frac{\mathrm{d} z}{\mathrm{d} m}}_{\cos \epsilon} - \frac{\mathrm{d}^2z}{\mathrm{d} m^2}\cdot\underbrace{\frac{\mathrm{d} r}{\mathrm{d} m}}_{\sin \epsilon} \ .
\end{align}

Für die Berechnung der Krümmung müssen die Koordinaten folglich zweifach in meridionaler Richtung abgeleitet werden.

%---------------------------------------------------------------------
\subsection{Definition und Berechnung der Skelettwinkel } \label {schaufelwinkel}
%---------------------------------------------------------------------
Die in Bild \ref{Fig:betazuru} dargestellten Winkel $\beta_\mathrm{ru}$ und $\beta_\mathrm{zu}$ beschreiben die Lage der $S_{2,\mathrm{m}}$-Fläche gegenüber dem Zylinderkoordinatensystem. Diese Lage ist nur in der Beschaufelung annähernd durch die Skelettwinkel der Schaufeln bestimmt.

%Zur Beschreibung der Schaufelgeometrie dienen die in Bild \ref{Fig:betazuru} dargestellten Winkel $\beta_\mathrm{ru}$ und $\beta_\mathrm{zu}$.

% \begin{figure} [htbp]
% \centering
% \includegraphics[angle=0,width=0.3\textwidth,clip]%
%                 {./bilder/betaruzu}
%   \caption{Winkeldefinitionen $\beta_\mathrm{ru}$ und $\beta_\mathrm{zu}$ }
%  \label{Fig:betazuru}
% \end{figure}


Auf einer Ebene $z= \mathrm{konst.}$ ist $\beta_\mathrm{ru}$ der Winkel zwischen der Umfangsrichtung und der Schaufelskelettfläche. Der Winkel $\beta_\mathrm{zu}$ beschreibt die Lage der Skelettfläche gegenüber der Umfangsrichtung auf einer zylindrischen Tangentialebene ($r= \mathrm{konst.}$).
Der Kotangens dieser Winkel repräsentiert folglich die Differenziale:

\begin{equation}
\cot \beta_\mathrm{ru} = r\frac{\partial\varphi}{\partial r}\bigg|_{z} \quad \text{bzw.} \quad
\cot \beta_\mathrm{zu} = r\frac{\partial\varphi}{\partial z}\bigg|_{r}  \ .
\label{betaruzuDef}
\end{equation}

Die entsprechenden Winkel im $m-, q-$Rechengebiet sind in Bild \ref{Fig:alphabeta} dargestellt und nachfolgend definiert.

% 

\subsubsection{Winkel $\alpha$}
   
Der Winkel $\alpha$ beschreibt die Neigung der $S_{2,\mathrm{m}}$-Fläche gegenüber der $q-$Richtung und folglich nicht den absoluten Strömungswinkel, wie die Bezeichnung nahelegen könnte. Der Tangens des Winkels $\alpha$ ist aus Bild \ref{Fig:alphabeta} als

%  
\begin{equation}
\tan \alpha = r\left(\frac{\partial\varphi}{\partial q} \right) =
						 r\left(\frac{\partial\varphi}{\partial r}\bigg|_{z} \; \frac{\partial r}{\partial q} + 
						 				\frac{\partial\varphi}{\partial z}\bigg|_{r} \; \frac{\partial z}{\partial q} \right) 
\label{alphaDef}
\end{equation}
%

definiert. Unter Verwendung der Gleichungen (\ref{betaruzuDef}) und (\ref{sincosGamma}) ergibt sich $\tan \alpha $ zu:

\begin{equation}
\tan \alpha =  \cot \beta_\mathrm{ru} \sin \gamma +   \cot \beta_\mathrm{zu} \cos \gamma \ .
\label{alphaDefFinal}
\end{equation}

%Dabei ist zu Beachten, dass der Winkel $\alpha$ nicht den absolut Strömungswinkel bezeichnet.

\subsubsection{Winkel $\beta$ }


Aus der Anschauung ergibt sich für den Winkel $\beta$ der folgende Zusammenhang:

\begin{equation}
\cot \beta = r\left(\frac{\partial\varphi}{\partial m} \right) =
						 r\left(\frac{\partial\varphi}{\partial r}\bigg|_{z} \; \frac{\partial r}{\partial m} + 
						 				\frac{\partial\varphi}{\partial z}\bigg|_{r} \; \frac{\partial z}{\partial m} \right) \ .
\label{betaDef}
\end{equation}

Nach den Gleichungen (\ref{betaruzuDef}) und (\ref{sincosEpsilon}) ergibt sich $\cot \beta$ zu: 

\begin{equation}
\cot \beta =   \cot \beta_\mathrm{ru} \sin \epsilon +   \cot \beta_\mathrm{zu} \cos \epsilon \ .
\label{betaDefFinal}
\end{equation}


Der Schaufelskelettwinkel $\beta$ entspricht in der Regel nicht dem Skelettwinkel der Meridianfläche. Vielmehr tritt bei der Schaufelvorderkante  Falschanströmung und an der Schaufelhinterkante  Minderumlenkung auf. Der Strömungswinkel $\beta_{\mathrm{St}}$ muss folglich durch geeignete Korrelationen auf Grundlage des Schaufelwinkels $\beta$ abgeleitet werden. Der Abstand zur Schaufelvorder- bzw. Schaufelhinterkante beeinflusst die notwendige Manipulation des Schaufelwinkels $\beta$ dabei maßgeblich. So ist es in ausreichendem Abstand zu den Kanten durchaus zulässig, von einer vollständig schaufelkongruenten Strömung auszugehen. Um diese Abhängigkeit abzubilden, wird ein Faktor der Vorderkante $f_{\mathrm{VK}}$ definiert, der von 1 auf der Vorderkante quadratisch bis zu dem Bereich, für den schaufelkongruente Strömung anzunehmen ist, auf 0 abnimmt. Der Hinterkantenfaktor $f_{\mathrm{HK}}$ wächst dagegen quadratisch von 0 auf 1 an der Schaufelhinterkante.



\section{Deviation}
\label{sec:deviation}
Für die Berechnung der Deviation gelten die Annahmen aus Abb. \ref{ab06:1a}.

\begin{figure}[htb]
\makebox[\textwidth]{\includegraphics[width=0.7\textwidth]{minderumlenkung}}
\caption{Minderumlenkung\label{ab06:1a}}
\end{figure}

 Deviation $\Delta_{HK}$ berechnet sich mit Hilfe vom Schlupf aus der Korrelation von Wiesner \cite{wiesner:1967}:
Der Schlupffaktor $F_\textrm{Schlupf}$ ergibt sich zu

\begin{equation}
\label{gl01:06a}
F_\textrm{Schlupf} = \frac{c_s}{u} = \frac{\sqrt{\cos\left(90^\circ-\beta\right)}}{Z^{0,7}}
\end{equation}

Wobei $Z$ die Schaufelanzahl ist, und $\beta$ der Schaufelwinkel. Da die Umfangsgeschwindigkeit $u$ gegeben ist, kann die Schlupfgeschwindigkeit $c_s$ berechnet werden. Daraus können die Relativgeschwindigkeiten in Umfangsrichtung mit Minderumlenkung $w_u$ und ohne Minderumlenkung $w_{u,\beta}$ berechnet werden:

\begin{equation}
\label{gl01:06b}
w_u = w_{u,\beta} - c_s = w_m \cdot  \cot\beta - F_\textrm{Schlupf} \cdot u
\end{equation}

Mit

\begin{align}
\label{gl01:06c}
\cot\beta_S &= \left( \frac{w_u}{w_m} \right)	\\
\label{gl01:06d}
\cot\beta &= \left( \frac{w_{u,\beta}}{w_m} \right)
\end{align}

ergibt sich die Deviation zu:

\begin{equation}
\label{gl01:06e}
\Delta_{HK} = \beta_S -\beta
\end{equation}

\section{Entropie}
\label{sec:entropie}
Der Entropieanstieg zwischen zwei Quasiorthogonalen ergibt sich mit Hilfe des polytropen Wirkungsgrades und der Gibbs-Gleichung zu

\begin{equation}
\label{gl01:07d}
\Delta s = (1-\eta_{pol})c_p\ln{\left|\frac{T_j}{T_{j-1}}\right|}
\end{equation}

\section{Bestimmung des Dralls}
``Auf der Eintrittsebene wird die Umfangskomponente der Absolutgeschwindigkeit $c_{u,0}$ vorgegeben. Der Drall ergibt sich als Produkt der Umfangskomponente $c_u$ mit dem Radius $r$.
Im unbeschaufelten Ringraum gilt Drallerhaltung, folglich wird der Drall der vorausgehenden Quasiorthogonalen übernommen. In der Beschaufelung basiert die Drallbestimmung auf dem Relativdrall \cite{casey:2008}, der sich nach folgender Gleichung berechnet:
\begin{equation}
\label{gl01:06}
(rw_u)_j = (f_{VK})_j\cdot(rw_u)_{VK} + \left\{ (rw_m)_j\cdot \cot\left[ \beta_j+\Delta_{HK}(f_{HK})_j \right] \right\} \cdot \left[ 1-(f_{VK})_j \right]
\end{equation}

Der Relativdrall an der Vorderkante $(rw_u)_{VK}$ wird in der Beschaufelung gemäß der Beziehung

\begin{equation}
\label{gl01:07}
(rw_u)_{VK} = (rc_u)_{VK} - \omega\cdot r^2
\end{equation}

berechnet. Im Stator ist die Winkelgeschwindigkeit $\omega=0$ gesetzt, somit geht Gleichung \ref{gl01:06} in eine Formulierung des absoluten Dralls über.
%Das Berechnungsmodel zur Drallberechnung auf Basis der vorausgehenden Quasiorthogonalen ist in Anhang B.5.2 angegeben.

Die Transparenzfaktoren $f_{VK}$ und $f_{HK}$ sind geometrische Größen und werden bei der Initialisierung der Geometrie berechnet.

\section{Geschwindigkeit}
\label{sec:Geschwindigkeit}
Die benötigten Geschwindigkeiten werden folgerndermaßen berechnet:

\begin{align}
c_u &= \frac{rc_u}{r}  \label{gl01:08}\\
w_u &= c_u -\omega\cdot r  \label{gl01:09}\\
c &= \sqrt{w_m^2 - w_u^2} \label{gl01:10}
\end{align}

\subsection{Winkel}
\begin{align}
\cot \alpha_{St} = \frac{c_u}{w_m} \label{gl01:11}\\
\cot \beta_{St} = \frac{w_u}{w_m}  \label{gl01:12}
\end{align}

\section{Enthalpie}
\label{sec:Enthalpie}
Die Totalenthalpie am Eintritt kann durch die Totaltemperatur berechnet werden:

\begin{equation}
\label{gl01:07a}
h_{tot,0} = c_p T_{tot,0}
\end{equation}

An den folgenden Quasiorthogonalen ergibt sie sich zu

\begin{equation}
\label{gl01:07b}
h_{tot,abs,j} = h_{tot,abs,j-1} + \omega \cdot [(rc_u)_j-(rc_u)_{j-1}]
\end{equation}

Es folgt für die statische Enthalpie:

\begin{equation}
\label{gl01:07c}
h = h_{tot,abs} -\frac{c^2}{2}
\end{equation}


\section{Zustandsgrößen}

\subsection{Berechnung der Temperaturen}
\label{sec:temperaturen}
``Im Eintritt ist die Totaltemperatur $T_{t,0}$ vorgegeben. Für die darauffolgenden Quasiorthogonalen wird sie nach der folgenden Gleichung aus der bereits berechneten Totalenthalpie bestimmt:

\begin{equation}
\label{gl01:13}
T_{tot} = \frac{h_{tot}}{c_p}
\end{equation}

Dann ergibt sich für die statische Temperatur:

\begin{equation}
\label{gl01:14}
T_j = T_{tot,abs} - \frac{c^2}{2c_p}
\end{equation}

\subsection{Schallgeschwindigkeit}
Die Schallgeschwindigkeit ergibt sich zu

\begin{equation}
\label{gl01:18}
a = \sqrt{\kappa RT}
\end{equation}

\subsection{Berechnung des Druckes}
\label{sec:druck}
Durch die Isentropenbeziehung ergibt sich der Eintrittsdruck zu

\begin{equation}
\label{gl01:15}
p_0 = p_{tot,0}\left( \frac{T_0}{T_{tot,0}} \right)^\frac{\kappa}{\kappa-1}
\end{equation}

Aus idealem Gasgesetz, Gibbs-Gleichung und Isentropenbeziehung folgt

\begin{equation}
\label{gl01:16}
p_{tot,j} = p_{tot,j-1} \cdot \left( \frac{T_{tot,j}}{T_{tot,j-1}} \right)^\frac{\kappa}{\kappa-1} \cdot e^{-\frac{s_j-s_{j-1}}{R}}
\end{equation}

\subsection{Dichte}

Die Dichte wird mit dem idealen Gasgesetz berechneten

\begin{equation}
\label{gl01:17}
\rho = \frac{p}{RT}
\end{equation}

\section{Massenstrom}
Der Massenstrom durch eine Stromröhre wird wie folgt berechnet:

\begin{equation}
\label{gl01:19}
\dot{m}_\textrm{Röhre} = \sum_{ZP}(2\pi r\sigma \cdot \sin\psi \cdot\rho w_m) \mathrm{d}q
\end{equation}

Um den Gesamtmassenstrom zu erhalten, werden die Teilmassenströme der Stromröhren eifnach aufsummiert.

\section{Verschiebung der Stromlinien}
Um die neue Lage der Stromlinien zu erhalten, wird folgende Funktion mit dem Bisektionsverfahren\footnote{\url{http://de.wikipedia.org/wiki/Bisektion}} gelöst:

\begin{equation}
\label{gl01:20}
f(\nu_{neu}) = f_{\dot{m}_\textrm{Röhre}} \cdot \dot{m}_\textrm{Vorgabe} - \dot{m}_\textrm{Röhre}(\nu_{neu})
\end{equation}

\section{Dämpfungsfaktor}
``Die Lage der Stromlinien beeinflusst die Bestimmung des Krümmungsradius und der übrigen Geometrieableitungen zur Bestimmung der Winkel... Da eine starke Verschiebung der Stromlinien die Konvergenz des Berechnungsprozesses gefährdet, muss die Änderung der Stromlinienlage gedämpft werden. Der Abstand der Quasiorthogonalen beeinflusst den notwendigen Dämpfungsfaktor dabei maßgeblich. Bei eng stehenden Quasiorthogonalen wirkt sich eine lokale Abweichung der Stromlinienlage wesentlich stärker auf die Bestimmung der Krümmung aus als bei Quasiorthogonalen mit größerem Abstand. Wilkinson beschreibt diese Abhängigkeit in \cite{wilkinson:1970} und gibt eine geeignet Bestimmung des notwendigen Dämpfungsfaktors, in Abhängigkeit des Teilungsverhältnisses $\Delta m$ der gesamten Kanalhöhe $b$ zum Abstand der Quasiorthogonalen an. Die Berechnung des optimalen Relaxationsfaktors ist aus \cite{casey:2008} entnommen und berechnet sich zu:

\begin{equation}
\label{gl01:21}
RF_{Wil} = \frac{0,5}{1+\frac{20}{96}\left( \frac{b}{\Delta m} \right)^2}
\end{equation}

Programmintern wird das größte Teilungsverhältnis im Rechengitter ermittelt und der resultierende Dämpfungsfaktor $RF_{Wil}$ berechnet... Sollte der berechnete Relaxationsfaktor unter dem vorgegebenen Wert liegen, wird dieser aus Stabilitätsgründen ersetzt.
Der Relaxationsfaktor gibt den Bruchteil an, zu dem die eigentlich notwendige Verschiebung der Stromlinien die tatsächlich neue Lage der Stromlinie bestimmt. Die gedämpfte Lage ergibt sich folglich zu:
\begin{equation}
\label{gl01:22}
\nu_{neu,gedämpft} = \textrm{RF}\cdot\nu_{neu} + (1-\textrm{RF})\cdot \nu
\end{equation}

%% ====================================================
%%  **********       ENDE  Kapitel 6        **********
%% ====================================================

