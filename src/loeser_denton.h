/***************************************************************************
 *   Copyright (C) 2009-2010 Andrea Bader (bader.andrea@gmx.de)            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Andrea Bader
*/

#ifndef LOESER_DENTON_H_
#define LOESER_DENTON_H_

#include "loeser.h"
#include "geometrie.h"
#include "newton.h"
#include "massenstrom.h"
#include "daten.h"
#include "geschwindigkeit_denton.h"
#include "enthalpie.h"
#include "dichte.h"
#include "druck.h"
#include "temperatur_denton.h"
#include "ableitung.h"
#include "epsilon.h"
#include "stromlinie.h"
#include "wirkungsgrad.h"

#include "laufzeitAusgabe.h"

class loeser_denton
	: public loeser
{
	newton 				newtonObjekt;
	massenStrom 			massenStromObjekt;
	LaufzeitAusgabe 		dglDateiAusgabe;
	geschwindigkeit_denton		geschwindigkeitObjekt;
	enthalpie			enthalpieObjekt;
	dichte				dichteObjekt;
	druck				druckObjekt;
	temperatur_denton		tempObjekt;
	ableitung			ableitungObjekt;
	epsilon				epsilonObjekt;
	stromlinie			stromlinienObjekt;
	wirkungsgrad			wirkungsgradObjekt;


	geometrie			*geometrieObjekt;
	v2Quasiorthogonale		*quasiorthogonale;
	v3Knoten			*knoten;
	daten				*datenObjekt;

	// error statistik
	double error_max;
	int error_max_i;
	int error_max_j;
	int error_max_massIter;

	int mittlereSL;
	vector<double> d_wm_2;

	bool zwangsbelgungsWarnung;

	std::ofstream *verlaufAusgabe;

public:

	//! Konstruktor

	/*!
	    \author Andrea Bader
	*/
	loeser_denton(geometrie *geometrieObjektEingabe);


	//! Destruktor

	/*!
	    \author Andrea Bader
	*/
	virtual ~loeser_denton()
	{
		verlaufAusgabe->close();
	}


	//! Äußere Schleife
	/*! Äußere Schleif wird von der main aufgerufen und bricht bei
		konvergenter Lösung ab. Es erfolgt der Aufruf der mittleren
		Schleife und die Verschiebung der Stromlinien.
		Schnittstellenmethode muss vorhanden sein, da auch in loeser_expeu

	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	virtual void schleife ( );

	//! Initialisierung
	/*! Vor dem Iterationsstart der äußeren Schleife werden weitere
		Initialisierungen durchgeführt.

	    \author Andrea Bader
	*/
	void initialisiere();

	//! Aktualisierung
	/*! Nach Verschiebung der Stromlinien werden die erforderlichen
		Aktualisierungen in dieser Methode gebündelt aufgerufen.

	    \author Andrea Bader
	*/
	void aktualisiere();



	//! Berechnung zusätzlicher Größen
	/*!
		ab todo beschreibung aktualisieren

	    Berechnet zusätzliche Größen, wie Totaldruck,
	    Totaldruckverhältnis und Wirkungsgrad nach
	    konvergenter Berechnung.
	    Schnittstellenmethode muss vorhanden sein, da auch in loeser_expeu

	    \author Andrea Bader
	*/
	virtual void integrale_Daten ( );


	//! Mittlere Schleife
	/*! Mittlere Schleife läuft über alle Quasiorthogonalen und
		berechnet Differentialgleichung und ruft innere Schleife auf.

	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	void schleife_ueber_QOn(int k);


	//! Aktualisieren der Transportterme!
	/*! Methoden zur Berechnung des Dralls, der Geschwindigkeitsdreiecke
		Enthalpiebestimmung werden aufgerufen.

	    \param j aktuelle Quasiorthogonale
	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	void updateTransportterme(int k, int j);


	//! Aktualisieren der Zustandsgrößen!
	/*! Methoden zur Berechnung der Temperatur, Totaltemperatur,
		Schallgeschwindigkeit, Druck und Dichte werden aufgrufen und gespeichert.

	    \param j aktuelle Quasiorthogonale
	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	void updateZustandsgroessen (int k, int j);


	//! Aufruf der Differentialgleichung
	/*! Der berechnete Gradient wird für alle Knoten der
		aktuellen Quasiorthogonalen gespeichert.

		\param dglFormulierung  entscheidet welche Formulierung
								der Differentialgleichung gelöst wird
	    \param j aktuelle Quasiorthogonale
	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	void dgl_schleife(int k, int j);


	//! Loesen der Denton - Differentialgleichung
	/*! Die Differentialgleichung wird für einen Knotenpunkt berechnet
		und 2*DGL-Ergebniss*delta q zurückgegeben.

	    \param i aktuelle Stromlinie
	    \param j aktuelle Quasiorthogonale
	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	double dgl_denton (int k, int j, int i);


	//! Loesen der Benetschik - Differentialgleichung
	/*! Die Differentialgleichung wird für einen Knotenpunkt berechnet
		und 2*DGL-Ergebniss*delta q zurückgegeben.

	    \param i aktuelle Stromlinie
	    \param j aktuelle Quasiorthogonale
	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	double dgl_benetschik (int k, int j, int i);


	//! Innere Schleife
	/*! Kopplung der Ergebnisse der Differentialgleichung mit dem Nivau
		der Meridiangeschwindigkeit aus der Kontiuitätsgleichung.

	    \param j aktuelle Quasiorthogonale
	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	void schleife_auf_QO(int k, int j);


	//! Kopplung zwischen Massenstrom und Meridiangeschwindigkeit!
	/*! Falls Massenstromvorgabe noch nicht erfüllt ist (Rückgabe=false),
		wird die ein neuer Startwert für die Meridiangeschwindigkeit
		in der Mitte berechnet.

	    \param mp_ratio aktuelles Verhältnis aktueller Massenstrom/Vorgabemassenstrom
	    \param j aktuelle Quasiorthogonale
	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	bool massenStromLoeser ( int j, int k, double& mp_ratio );


	//! Berechnung der Meridiangeschwindigkeit auf aktueller Quasiorthogonale!
	/*! Die Meridiangeschwindigkeit wird für jeden Knoten der Quasiorthogoanlen
		unter Verwendung der gespeicherten Gradienten der Differentialgleichung
		auf Basis des wm der mittleren Stromlinie berechnet.

	    \param j aktuelle Quasiorthogonale
	    \param k aktuelles Gitter

	    \author Andrea Bader
	*/
	void berechne_wm_auf_QO(int j, int k);


	//! Berechnung des gemittelten Fehlers
	/*! Der gewichtete mittlere Fehler auf einer Quasiorthogonale wird
		berechnet. Für die Ausgabe werden die einzelnen Fehler und die
		Massenstromitertaionen gespeichert.

	    \param j aktuelle Quasiorthogonale
	    \param k aktuelles Gitter
	    \param massIter

	    \author Andrea Bader
	*/
	double mittlererFehler(int k, int j, int massIter);


	//! Ausgabe des Konvergenzverhaltens
	/*! Aktueller Fehler wird mit der Angabe der Auftretensstelle und der
		benötigten Massenstromiterationen in die Datei Fehlerverlauf.txt
		geschreiben.

	    \author Andrea Bader
	*/
	void erstelleFehlerverlaufAusgabe();


	//! Zuruecksetzen aller Groessen
	/*!
	    Setzt alle erforderlichen Groessen für neuen Zustandpunkt
	    des Kennfeldes auf deren Ursprungswert zurueck.
	    Schnittstellenmethode muss vorhanden sein, da auch in loeser_expeu

	    \author Andrea Bader
	*/
	virtual void zuruecksetzen ( );
};

#endif
