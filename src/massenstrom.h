/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de)              *
 *                 2008-2009 by Florian Mayer (ilumine@gmx.de)             *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm, Florian Mayer
*/



#ifndef MASSENSTROM_H
	#define MASSENSTROM_H



#include <cmath>
#include <nurbs++/nurbs.h>



#include "daten.h"
#include "dichte.h"

//! Massenstromklasse
/*!
Berechnet die Massenströme.
\author Oliver Borm, Florian Mayer
*/
class massenStrom
{
	private:
		daten			*datenObjekt;
		v3Knoten		*knoten;
		v2Quasiorthogonale	*quasiorthogonale;
		dichte			dichteObjekt;

	public:
		massenStrom();

		//! Berechnung des Massenstroms
		/*!
		    Berechnet den durch eine Quasiorthogonale strömenden Massenstrom.
		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Oliver Borm
		*/
		void berechneMassenStrom ( int j, int k );

		
		//! Berechnung des Massenstromes zwischen zwei Stromlinien
		/*!
		    Berechnet den Massenstrom einer Stromröhre.
		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \param ny_lo \f$ \nu \f$-Wert der unteren Stromlinie
		    \param ny_hi \f$ \nu \f$-Wert der oberen Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \return \f$ \dot{m}_{R\ddot{o}hre} \f$
		    \author Oliver Borm, Florian Mayer
		*/
		double berechneMassenStromRoehre ( double ny_lo, double ny_hi, int j, int k );


		//! Initialisierung der Massenstromdichte auf einer Quasiorthogonalen
		/*!
		    Berechnet die Massenstromdichte entlang einer Quasirthogonalen, und initialisiert
		    eine NURBS Kurve die diese Daten enthält.
		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter
		    \author Oliver Borm, Florian Mayer
		*/
		void initialisiere_nurbsMassenStromDichte ( int j, int k );

//+++++++++++++++ Methoden von Andrea Bader ++++++++++++++++++++++++++++++++++

		//! Berechnung und setzen des Massenstroms durch eine Quasiorthogonale
		/*!
		    Berechnet den durch eine Quasiorthogonale strömenden Massenstrom
		    durch aufsummieren der Massenströme durch die einzelnen Stromröhren

		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter

		    \author Andrea Bader
		*/
		void setzeMassenStrom ( int j, int k );


		//! Berechnung des Massenstromes zwischen zwei Stromlinien
		/*!
		    Berechnet den Massenstrom in einer Stromröhre.

		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \param ny_lo Ny-Wert der unteren Stromlinie
		    \param ny_hi Ny-Wert der oberen Stromlinie
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter

		    \author Andrea Bader
		*/
		double berechneMassenStromRoehre_sinPsi ( double ny_lo, double ny_hi, int j, int k );


		//! Initialisierung der Massenstromdichte*sinPsi auf einer Quasiorthogonalen basierende auf w_m
		/*!
		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter

		    \author Andrea Bader
		*/
		void initialisiere_nurbsMassenStromDichte_sinPsi ( int j, int k );


		//! Initialisierung der Massenstromdichte*sinPsi auf einer Quasiorthogonalen basierend auf w_m_neu
		/*!
		    \param datenObjekt Zeiger auf Objekt vom Typ 'daten'
		    \param j aktuelle Quasiorthogonale
		    \param k aktuelles Gitter

		    \author Andrea Bader
		*/
		void initialisiere_nurbsMassenStromDichte_sinPsi_neu ( int j, int k );


		//! Anpassen des Massenstromfaktors der Stromline
		/*!
		    Der Massenstromfaktor durch die Stromlinien wird entsprechend der Flächenverteilung
		    angepasst. Stromlinien auf geringem Radius werden dadurch nicht mehr so stark nach außen
		    gezogen. Der Aufruf dieser Methode kann einfach auskommentiert werden, falls der Effekt nicht
		    gewünscht ist.

		    \author Andrea Bader
		*/
		void anpassenFaktorMassenStromRoehre();


		//! Massenstromvorgabe für Kennfeldberechnung
		/*!
		    Die Massenstromvorgabe in der Mitte der Kennlinie (Optimalpunkt) wird bei der Kennfeldberechnung
		    aus der optimalen Schluckziffer der Korrelationen berechnet.

		    \author Andrea Bader
		*/
		void massenstrom_aus_schluckziffer_opt();


		//! Massenstromvorgabeberechnung aus den Geschwindigkeitsdreiecken an der Rotorvorderkante
		/*!
		    Massenstromvorgabe wird aus den Geschwindigkeitsdreiecken an der Rotorvorderkante berechnet.
		    Drallfrei Zuströmung ist Voraussetzung, eine feste Inzidenz kann angegeben werden. Diese
		    Methode wurde zu Testzwecken erstellt, falls keine durchsetztbare Vorgabe vorhanden ist.

		    \author Andrea Bader
		*/
		void berechneAnfangsmassenstrom ();


		//! Berechnung der Kennzahl "reduzierter Volumenstrom"
		/*!
			Berechnet den Volumenstrom bezogen auf die Eintrittstotaldichte
		    \param Eintrittsdichte

		    \author Andrea Bader
		*/
		double berechnereduziertenVolumenstrom ();
};

#endif
