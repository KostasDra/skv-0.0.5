/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de )             *
 *                 2008      by Florian Mayer (ilumine@gmx.de)             *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm, Florian Mayer
*/

#include "ableitung.h"
#include "ableitungshelfer.h" //ab: zentrale Implementierung der Differenzenverfahren




ableitung::ableitung()
{
	datenObjekt 		= daten::getInstance();
	knoten 			= &datenObjekt->knoten;
	quasiorthogonale 	= &datenObjekt->quasiorthogonale;
	stromlinieGitter 	= &datenObjekt->stromlinieGitter;
}



//================ ABLEITUNGSMETHODEN VON BORM, MAYER ===========================================================

double ableitung::dr_dxi ( int i, int j, int k )
{
	if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 )
	{
		//!	\f[ \frac{\partial r}{\partial \xi} = \frac{r(i,j+1,k)-r(i,j-1,k)}{2} \f]
		return ((*knoten)[k][i][j+1].r - (*knoten)[k][i][j-1].r ) *0.5;
	}

	else if ( j == 0 )	//! bzw. für die erste Quasiorthogonale:
	{
		//!	\f[ \frac{\partial r}{\partial \xi} = \frac{-r(i,j+2,k)+4\cdot r(i,j+1,k)-3\cdot r(i,j,k)}{2} \f]
		return ( -(*knoten)[k][i][j+2].r + 4.0*(*knoten)[k][i][j+1].r - 3.0*(*knoten)[k][i][j].r ) *0.5;
	}

	else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 )	//! und für die letzte Quasiorthogonale:
	{
		//!	\f[ \frac{\partial r}{\partial \xi} = \frac{-r(i,j-2,k)+4\cdot r(i,j-1,k)-3\cdot r(i,j,k)}{2} \f]
		return ( -(*knoten)[k][i][j-2].r +4.0*(*knoten)[k][i][j-1].r - 3.0*(*knoten)[k][i][j].r ) *0.5;
	}
}





double ableitung::dr_deta ( int i, int j, int k )
{
	if ( i != datenObjekt->anzahlStromlinien-1 && i != 0 )
	{
		//!	\f[ \frac{\partial r}{\partial \eta} = \frac{r(i+1,j,k)-r(i-1,j,k)}{2} \f]
		return ( (*knoten)[k][i+1][j].r-(*knoten)[k][i-1][j].r ) *0.5;
	}

	else if ( i == 0 )	//! bzw. für die erste Quasiorthogonale:
	{
		//!	\f[ \frac{\partial r}{\partial \eta} = \frac{-r(i+2,j,k)+4\cdot r(i+1,j,k)-3\cdot r(i,j,k)}{2} \f]
		return ( -(*knoten)[k][i+2][j].r +4.0 *(*knoten)[k][i+1][j].r-3.0 *(*knoten)[k][i][j].r ) *0.5;
	}

	else if ( i == datenObjekt->anzahlStromlinien-1 )	//! und für die letzte Quasiorthogonale:
	{
		//!	\f[ \frac{\partial r}{\partial \eta} = \frac{-r(i-2,j,k)+4\cdot r(i-1,j,k)-3\cdot r(i,j,k)}{2} \f]
		return ( -(*knoten)[k][i-2][j].r +4.0 *(*knoten)[k][i-1][j].r-3.0 *(*knoten)[k][i][j].r ) *0.5;
	}
}





double ableitung::dz_dxi ( int i, int j, int k )
{
	if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 )
	{
		//!	\f[ \frac{\partial z}{\partial \xi} = \frac{z(i,j+1,k)-z(i,j-1,k)}{2} \f]
		return ( (*knoten)[k][i][j+1].z-(*knoten)[k][i][j-1].z ) *0.5;
	}

	else if ( j == 0 )	//! bzw. für die erste Quasiorthogonale:
	{
		//!	\f[ \frac{\partial z}{\partial \xi} = \frac{-z(i,j+2,k)+4\cdot z(i,j+1,k)-3\cdot z(i,j,k)}{2} \f]
		return ( -(*knoten)[k][i][j+2].z +4.0*(*knoten)[k][i][j+1].z -3.0*(*knoten)[k][i][j].z ) *0.5;
	}

	else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 )	//! und für die letzte Quasiorthogonale:
	{
		//!	\f[ \frac{\partial z}{\partial \xi} = \frac{-z(i,j-2,k)+4\cdot z(i,j-1,k)-3\cdot z(i,j,k)}{2} \f]
		return ( -(*knoten)[k][i][j-2].z +4.0*(*knoten)[k][i][j-1].z -3.0*(*knoten)[k][i][j].z ) *0.5;
	}
}





double ableitung::dz_deta ( int i, int j, int k )
{
	if ( i != datenObjekt->anzahlStromlinien-1 && i != 0 )
	{
		//!	\f[ \frac{\partial z}{\partial \eta} = \frac{z(i+1,j,k)-z(i-1,j,k)}{2} \f]
		return ( (*knoten)[k][i+1][j].z-(*knoten)[k][i-1][j].z ) *0.5;
	}

	else if ( i == 0 )	//! bzw. für die erste Quasiorthogonale:
	{
		//!	\f[ \frac{\partial z}{\partial \eta} = \frac{-z(i+2,j,k)+4\cdot z(i+1,j,k)-3\cdot z(i,j,k)}{2} \f]
		return ( -(*knoten)[k][i+2][j].z +4.0*(*knoten)[k][i+1][j].z-3.0*(*knoten)[k][i][j].z ) *0.5;
	}

	else if ( i == datenObjekt->anzahlStromlinien-1 )	//! und für die letzte Quasiorthogonale:
	{
		//!	\f[ \frac{\partial z}{\partial \eta} = \frac{-z(i-2,j,k)+4\cdot z(i-1,j,k)-3\cdot z(i,j,k)}{2} \f]
		return ( -(*knoten)[k][i-2][j].z +4.0*(*knoten)[k][i-1][j].z-3.0*(*knoten)[k][i][j].z ) *0.5;
	}
}





double ableitung::ersteAbleitung_epsilon ( int i, int j, int k )
{
	if ( datenObjekt->ablmod ==0 ) //! Im NURBS Ableitungsmodus werden die Ableitungen von \f$ \varepsilon, r, z \f$ an dem gesuchten Punkt durch die NURBS++ Bibliothek berechnet:
	{
		//!	\f[ \varepsilon' = \frac{\mathrm{d}\varepsilon}{\sqrt{(\mathrm{d}r)^2+(\mathrm{d}z)^2}}\f]

		Vector_HPoint2Dd epsilonDerivative;
		(*stromlinieGitter)[k][i].nurbsEpsilonGitter.deriveAtH ( (*knoten)[k][i][j].myG ,1,epsilonDerivative );
		return ( epsilonDerivative[1].w() /sqrt ( pow2( epsilonDerivative[1].y()) + pow2(epsilonDerivative[1].x()) ) );
	}

	else if ( datenObjekt->ablmod ==1 ) //! Mit Differenzenverfahren:
	{
		if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 )
		{
			//!	\f[ \varepsilon' = \frac{\varepsilon(i,j+1,k)-\varepsilon(i,j-1,k)}{\sqrt{(r(i,j+1,k)-r(i,j-1,k))^2+(z(i,j+1,k)-z(i,j-1,k))^2}} \f]
			double delta_r = (*knoten)[k][i][j+1].r-(*knoten)[k][i][j-1].r;
			double delta_z =  (*knoten)[k][i][j+1].z - (*knoten)[k][i][j-1].z;
			return ( (*knoten)[k][i][j+1].epsilon-(*knoten)[k][i][j-1].epsilon ) /sqrt ( delta_r*delta_r + delta_z*delta_z );
		}

		else if ( j == 0 )	//! bzw. für erste Quasiorthogonale:
		{
			//! \f[ \varepsilon' = \frac{-\varepsilon(i,j+2,k)+4\cdot\varepsilon(i,j+1,k)-3\cdot\varepsilon(i,j,k)}{\sqrt{(r(i,j+2,k)-r(i,j,k))^2+(z(i,j+2,k)-z(i,j,k))^2}} \f]
			return ( -(*knoten)[k][i][j+2].epsilon +4.0*(*knoten)[k][i][j+1].epsilon -3.0*(*knoten)[k][i][j].epsilon ) /sqrt ( pow2(  (*knoten)[k][i][j+2].z -(*knoten)[k][i][j].z ) +pow2(  (*knoten)[k][i][j+2].r -(*knoten)[k][i][j].r ) );
		}

		else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 )	//! und letzte Quasiorthogonale:
		{
			//!	\f[ \varepsilon' = \frac{\varepsilon(i,j-2,k)-4\cdot\varepsilon(i,j-1,k)+3\cdot\varepsilon(i,j,k)}{\sqrt{(r(i,j-2,k)-r(i,j,k))^2+(z(i,j-2,k)-z(i,j,k))^2}} \f]
			return ( (*knoten)[k][i][j-2].epsilon-4.0*(*knoten)[k][i][j-1].epsilon +3.0*(*knoten)[k][i][j].epsilon ) /sqrt ( pow2(  (*knoten)[k][i][j].z -(*knoten)[k][i][j-2].z ) +pow2(  (*knoten)[k][i][j].r -(*knoten)[k][i][j-2].r ) );
		}
	}
}





double ableitung::ersteAbleitung_entropie ( int i, int j, int k )
{
	if ( datenObjekt->ablmod == 0 ) //! Im NURBS Ableitungsmodus werden die Ableitungen von \f$ s, r, z \f$ an dem gesuchten Punkt durch die NURBS++ Bibliothek berechnet:
	{
		//!	\f[ s' = \frac{\mathrm{d}s}{\sqrt{(\mathrm{d}r)^2+(\mathrm{d}z)^2}}\f]
		Vector_HPoint2Dd entropieDerivative;

		(*stromlinieGitter)[k][i].nurbsEntropieGitterSL.deriveAtH ( (*knoten)[k][i][j].myG,1,entropieDerivative );
		return ( entropieDerivative[1].w() /sqrt ( ( pow2(  entropieDerivative[1].y() ) +pow2(  entropieDerivative[1].x() ) ) ) );
	}

	else if ( datenObjekt->ablmod == 1 ) //! Ableitung mit Differenzenmethode:
	{
		if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 )
		{
			//!	\f[ s' = \frac{s(i,j+1,k)-s(i,j-1,k)}{\sqrt{(r(i,j+1,k)-r(i,j-1,k))^2+(z(i,j+1,k)-z(i,j-1,k))^2}} \f]
			return ( (*knoten)[k][i][j+1].entropie-(*knoten)[k][i][j-1].entropie ) /sqrt ( pow2(  (*knoten)[k][i][j+1].z-(*knoten)[k][i][j-1].z ) +pow2(  (*knoten)[k][i][j+1].r-(*knoten)[k][i][j-1].r ) );
		}

		else if ( j == 0 )	//! bzw. für erste Quasiorthogonale:
		{
			//!	\f[ s' = \frac{-s(i,j+2,k)+4\cdot s(i,j+1,k)-3\cdot s(i,j,k)}{\sqrt{(r(i,j+2,k)-r(i,j,k))^2+(z(i,j+2,k)-z(i,j,k))^2}} \f]
			return ( -(*knoten)[k][i][j+2].entropie + 4.0*(*knoten)[k][i][j+1].entropie-3.0*(*knoten)[k][i][j].entropie ) /sqrt ( pow2(  (*knoten)[k][i][j+2].z-(*knoten)[k][i][j].z ) +pow2(  (*knoten)[k][i][j+2].r-(*knoten)[k][i][j].r ) );
		}

		else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 )	//! und letzte Quasiorthogonale:
		{
			//!	\f[ s' = \frac{s(i,j-2,k)-4\cdot s(i,j-1,k)+3\cdot s(i,j,k)}{\sqrt{(r(i,j-2,k)-r(i,j,k))^2+(z(i,j-2,k)-z(i,j,k))^2}} \f]
			return ( (*knoten)[k][i][j-2].entropie-4.0*(*knoten)[k][i][j-1].entropie +3.0*(*knoten)[k][i][j].entropie ) /sqrt ( pow2(  (*knoten)[k][i][j].z-(*knoten)[k][i][j-2].z ) +pow2(  (*knoten)[k][i][j].r-(*knoten)[k][i][j-2].r ) );
		}
	}
}





double ableitung::ersteAbleitung_r_sin_epsilon ( int i, int j, int k )
{

	double dq_deta = 0.0;
	double dq_dxi = 0.0;

	if ( i != datenObjekt->anzahlStromlinien-1 && i != 0 )	//! Die Gleichung für Stromlinien lautet:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{\sin\varepsilon(i+1,j,k)-\sin\varepsilon(i-1,j,k)}{2} \f]
		dq_deta = ( (*knoten)[k][i+1][j].sin_epsilon-(*knoten)[k][i-1][j].sin_epsilon ) *0.5;
	}

	else if ( i == 0 )	//! bzw. für die unterste Stromlinie:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{-\sin\varepsilon(i+2,j,k)+4\cdot\sin\varepsilon(i+1,j,k)-3\cdot\sin\varepsilon(i,j,k)}{2} \f]
		dq_deta = ( -(*knoten)[k][i+2][j].sin_epsilon + 4.0*(*knoten)[k][i+1][j].sin_epsilon- 3.0*(*knoten)[k][i][j].sin_epsilon ) *0.5;
	}

	else if ( i == datenObjekt->anzahlStromlinien-1 )	//! und die oberste Stromlinie:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{\sin\varepsilon(i-2,j,k)-4\cdot\sin\varepsilon(i-1,j,k)+3\cdot\sin\varepsilon(i,j,k)}{2} \f]
		dq_deta = ( -(*knoten)[k][i-2][j].sin_epsilon +4.0*(*knoten)[k][i-1][j].sin_epsilon-3.0*(*knoten)[k][i][j].sin_epsilon ) *0.5;
	}


	if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 )	//! Die Gleichung für Quasiorthogonale lautet:
	{
		//!	\f[ \frac{\partial q}{\partial \xi} = \frac{\sin\varepsilon(i,j+1,k)-\sin\varepsilon(i,j-1,k)}{2} \f]
		dq_dxi = ( (*knoten)[k][i][j+1].sin_epsilon-(*knoten)[k][i][j-1].sin_epsilon ) *0.5;
	}

	else if ( j == 0 )	//! bzw. für die erste Quasiorthogonale
	{
		//!	\f[ \frac{\partial q}{\partial \xi} =  \frac{-\sin\varepsilon(i,j+2,k)+4\cdot\sin\varepsilon(i,j+1,k)-3\cdot\sin\varepsilon(i,j,k)}{2} \f]
		dq_dxi = ( -(*knoten)[k][i][j+2].sin_epsilon +4.0*(*knoten)[k][i][j+1].sin_epsilon-3*(*knoten)[k][i][j].sin_epsilon ) *0.5;
	}

	else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 )	//! und die letzte Quasiorthogonale
	{
		//!	\f[ \frac{\partial q}{\partial \xi} = \frac{\sin\varepsilon(i,j-2,k)-4\cdot\sin\varepsilon(i,j-1,k)+3\cdot\sin\varepsilon(i,j,k)}{2} \f]
		dq_dxi = ( -(*knoten)[k][i][j-2].sin_epsilon +4.0*(*knoten)[k][i][j-1].sin_epsilon-3.0*(*knoten)[k][i][j].sin_epsilon ) *0.5;
	}

	/*!
	Die Ableitung lautet also:

	\f[ \frac{\partial \sin \varepsilon}{\partial r} = \frac{\frac{\partial q}{\partial \eta} \cdot \frac{\partial z}{\partial \xi}(i,j,k) -
	\frac{\partial q}{\partial \xi} \cdot \frac{\partial z}{\partial \eta}(i,j,k)}{\frac{\partial r}{\partial \eta}(i,j,k)
	\cdot \frac{\partial z}{\partial \xi}(i,j,k) - \frac{\partial z}{\partial \eta}(i,j,k) \cdot \frac{\partial r}{\partial \xi}(i,j,k)  } \f]
	*/
	return ( ( dq_deta*dz_dxi (i, j, k)-dq_dxi*dz_deta (i, j, k) ) /(dr_deta (i, j, k) *dz_dxi (i, j, k)-dz_deta (i, j, k) *dr_dxi (i, j, k)) );
}





double ableitung::ersteAbleitung_z_cos_epsilon ( int i, int j, int k )
{
	double dq_deta = 0.0;
	double dq_dxi = 0.0;

	if ( i != datenObjekt->anzahlStromlinien-1 && i != 0 )	//! Für Stromlinien lautet die Gleichung:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{\cos\varepsilon(i+1,j,k)-\cos\varepsilon(i-1,j,k)}{2} \f]
		dq_deta = ( (*knoten)[k][i+1][j].cos_epsilon-(*knoten)[k][i-1][j].cos_epsilon ) *0.5;
	}

	else if ( i == 0 )	//! bzw. für die unterste Stromlinie:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{-\cos\varepsilon(i+2,j,k)+4\cdot\cos\varepsilon(i+1,j,k)-3\cdot\cos\varepsilon(i,j,k)}{2} \f]
		dq_deta = ( -(*knoten)[k][i+2][j].cos_epsilon +4.0*(*knoten)[k][i+1][j].cos_epsilon-3.0*(*knoten)[k][i][j].cos_epsilon ) *0.5;
	}

	else if ( i == datenObjekt->anzahlStromlinien-1 )	//! und die oberste Stromlinie:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{\cos\varepsilon(i-2,j,k)-4\cdot\cos\varepsilon(i-1,j,k)+3\cdot\cos\varepsilon(i,j,k)}{2} \f]
		dq_deta = ( -(*knoten)[k][i-2][j].cos_epsilon +4.0*(*knoten)[k][i-1][j].cos_epsilon-3.0*(*knoten)[k][i][j].cos_epsilon ) *0.5;
	}

	if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 )	//! Für Quasiorthogonale lautet die Gleichung:
	{
		//!	\f[ \frac{\partial q}{\partial \xi} = \frac{\cos\varepsilon(i,j+1,k)-\cos\varepsilon(i,j-1,k)}{2} \f]
		dq_dxi = ( (*knoten)[k][i][j+1].cos_epsilon-(*knoten)[k][i][j-1].cos_epsilon ) *0.5;
	}

	else if ( j == 0 )	//! bzw. für die erste Quasiorthogonale:
	{
		//!	\f[ \frac{\partial q}{\partial \xi} =  \frac{-\cos\varepsilon(i,j+2,k)+4\cdot\cos\varepsilon(i,j+1,k)-3\cdot\cos\varepsilon(i,j,k)}{2} \f]
		dq_dxi = ( -(*knoten)[k][i][j+2].cos_epsilon +4.0*(*knoten)[k][i][j+1].cos_epsilon-3.0*(*knoten)[k][i][j].cos_epsilon ) *0.5;
	}

	else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 )	//! und die letzte Quasiorthogonale:
	{
		//!	\f[ \frac{\partial q}{\partial \xi} = \frac{\cos\varepsilon(i,j-2,k)-4\cdot\cos\varepsilon(i,j-1,k)+3\cdot\cos\varepsilon(i,j,k)}{2} \f]
		dq_dxi = ( -(*knoten)[k][i][j-2].cos_epsilon +4.0*(*knoten)[k][i][j-1].cos_epsilon-3.0*(*knoten)[k][i][j].cos_epsilon ) *0.5;
	}

	/*!
	Die Ableitung lautet also:
	\f[ \frac{\partial \cos \varepsilon}{\partial z} = \frac{\frac{\partial q}{\partial \xi} \cdot \frac{\partial r}{\partial \eta}(i,j,k) -
	\frac{\partial q}{\partial \eta} \cdot \frac{\partial r}{\partial \xi}(i,j,k)}{\frac{\partial r}{\partial \eta}(i,j,k)
	\cdot \frac{\partial z}{\partial \xi}(i,j,k) - \frac{\partial z}{\partial \eta}(i,j,k) \cdot \frac{\partial r}{\partial \xi}(i,j,k)  } \f]
	*/
	return ( dq_dxi*dr_deta (i, j, k)-dq_deta*dr_dxi (i, j, k) ) /(dr_deta (i, j, k) *dz_dxi (i, j, k)-dz_deta (i, j, k) *dr_dxi (i, j, k));
}





double ableitung::ersteAbleitung_z_cot_beta ( int i, int j, int k )
{
	int difver = 0; //CENTRAL
	bool center = false;
	bool down = false;
	bool up = false;
	//1=unbeschaufelt, 0=beschaufelt
	if ( j != 0 && j != datenObjekt->anzahlQuasiOrthogonale[k] - 1 )
	{
		if ( (*knoten)[k][i][j-1].unbeschaufelt ) down=1;
		if ( (*knoten)[k][i][j].unbeschaufelt ) center=1;
		if ( (*knoten)[k][i][j+1].unbeschaufelt ) up=1;
	}

	// nicht klar warum hier 1000000 hart reincodiert worden sind
	if ( ( down && center && !up ) || ( !down && !center && up ) || fabs ( (*knoten)[k][i][j+1].cot_beta ) > HUGE )
	{
		difver = -1; //DOWNstream
	}
	else if ( ( !down && center && up ) || ( down && !center && !up ) || fabs ( (*knoten)[k][i][j-1].cot_beta ) > HUGE )
	{
		difver = 1; //UPstream
	}
	else
	{
		difver = 0;
	}
	//korrigiertes Differenzenverfahren bei Sprüngen beschaufelt<->unbeschaufelt

	double J = dr_deta (i, j, k) *dz_dxi (i, j, k)-dz_deta (i, j, k) *dr_dxi (i, j, k);
	double dq_deta = 0.0;
	double dq_dxi = 0.0;

	if ( i != datenObjekt->anzahlStromlinien-1 && i != 0 )	//! Für Stromlinien lautet die Gleichung:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{\cot\beta(i+1,j,k)-\cot\beta(i-1,j,k)}{2} \f]
		dq_deta = ( (*knoten)[k][i+1][j].cot_beta-(*knoten)[k][i-1][j].cot_beta ) *0.5;
	}

	else if ( i == 0 )	//! bzw. für die unterste Stromlinie:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{-\cot\beta(i+2,j,k)+4\cdot\cot\beta(i+1,j,k)-3\cdot\cot\beta(i,j,k)}{2} \f]
		dq_deta = ( -(*knoten)[k][i+2][j].cot_beta +4*(*knoten)[k][i+1][j].cot_beta-3*(*knoten)[k][i][j].cot_beta ) *0.5;
	}

	else if ( i == datenObjekt->anzahlStromlinien-1 )	//! und die oberste Stromlinie:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{\cot\beta(i-2,j,k)-4\cdot\cot\beta(i-1,j,k)+3\cdot\cot\beta(i,j,k)}{2} \f]
		dq_deta = ( -(*knoten)[k][i-2][j].cot_beta +4*(*knoten)[k][i-1][j].cot_beta-3*(*knoten)[k][i][j].cot_beta ) *0.5;
	}


	if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 && difver==0 )	//! Für Quasiorthogonale lautet die Gleichung:
	{
		//!	\f[ \frac{\partial q}{\partial \xi} = \frac{\cot\beta(i,j+1,k)-\cot\beta(i,j-1,k)}{2} \f]
		dq_dxi = ( (*knoten)[k][i][j+1].cot_beta-(*knoten)[k][i][j-1].cot_beta ) *0.5;
	}

	else if ( j == 0 || difver==1 )	//! bzw. für die erste Quasiorthogonale:
	{
		//!	\f[ \frac{\partial q}{\partial \xi} =  \frac{-\cot\beta(i,j+2,k)+4\cdot\cot\beta(i,j+1,k)-3\cdot\cot\beta(i,j,k)}{2} \f]
		dq_dxi = ( -(*knoten)[k][i][j+2].cot_beta +4*(*knoten)[k][i][j+1].cot_beta-3*(*knoten)[k][i][j].cot_beta ) *0.5;
	}

	else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 || difver==-1 )	//! und die letzte Quasiorthogonale:
	{
		//!	\f[ \frac{\partial q}{\partial \xi} = \frac{\cot\beta(i,j-2,k)-4\cdot\cot\beta(i,j-1,k)+3\cdot\cot\beta(i,j,k)}{2} \f]
		dq_dxi = ( -(*knoten)[k][i][j-2].cot_beta +4*(*knoten)[k][i][j-1].cot_beta-3*(*knoten)[k][i][j].cot_beta ) *0.5;
	}

	/*!
	Die Ableitung lautet also:
	\f[ \frac{\partial \cot \beta}{\partial z} = \frac{\frac{\partial q}{\partial \xi} \cdot \frac{\partial r}{\partial \eta}(i,j,k) -
	\frac{\partial q}{\partial \eta} \cdot \frac{\partial r}{\partial \xi}(i,j,k)}{\frac{\partial r}{\partial \eta}(i,j,k)
	\cdot \frac{\partial z}{\partial \xi}(i,j,k) - \frac{\partial z}{\partial \eta}(i,j,k) \cdot \frac{\partial r}{\partial \xi}(i,j,k)  } \f]
	*/
	return ( dq_dxi*dr_deta (i, j, k)-dq_deta*dr_dxi (i, j, k) ) /(dr_deta (i, j, k) *dz_dxi (i, j, k)-dz_deta (i, j, k) *dr_dxi (i, j, k));
}





double ableitung::ersteAbleitung_r_cot_beta ( int i, int j, int k )
{
	int difver = 0;
	bool center = false;
	bool down = false;
	bool up = false;
	//1=unbeschaufelt, 0=beschaufelt
	if ( j!=0 && j!=datenObjekt->anzahlQuasiOrthogonale[k]-1 )
	{
		if ( (*knoten)[k][i][j-1].unbeschaufelt ) down = true;
		if ( (*knoten)[k][i][j].unbeschaufelt ) center = true;
		if ( (*knoten)[k][i][j+1].unbeschaufelt ) up = true;
	}

	if ( ( down && center && !up ) || ( !down && !center && up ) || fabs ( (*knoten)[k][i][j+1].cot_beta ) >= HUGE )
	{
		difver = -1; //DOWNstream
	}
	else if ( ( !down && center && up ) || ( down && !center && !up ) || fabs ( (*knoten)[k][i][j-1].cot_beta ) >= HUGE )
	{
		difver = 1; //UPstream
	}
	else
	{
		difver = 0;
	}
	//korrigiertes Differenzenverfahren bei Sprüngen beschaufelt<->unbeschaufelt

	//double J = dr_deta (i, j, k) *dz_dxi (i, j, k)-dz_deta (i, j, k) *dr_dxi (i, j, k);
	double dq_deta = 0.0;
	double dq_dxi = 0.0;


	if ( i != datenObjekt->anzahlStromlinien-1 && i != 0 )	//! Für Stromlinien lautet die Gleichung:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{\cot\beta(i+1,j,k)-\cot\beta(i-1,j,k)}{2} \f]
		dq_deta = ( (*knoten)[k][i+1][j].cot_beta-(*knoten)[k][i-1][j].cot_beta ) *0.5;
	}

	else if ( i == 0 )	//! bzw. für die unterste Stromlinie:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{-\cot\beta(i+2,j,k)+4\cdot\cot\beta(i+1,j,k)-3\cdot\cot\beta(i,j,k)}{2} \f]
		dq_deta = ( -(*knoten)[k][i+2][j].cot_beta +4.0*(*knoten)[k][i+1][j].cot_beta-3.0*(*knoten)[k][i][j].cot_beta ) *0.5;
	}

	else if ( i == datenObjekt->anzahlStromlinien-1 )	//! und die oberste Stromlinie:
	{
		//!	\f[ \frac{\partial q}{\partial \eta} = \frac{\cot\beta(i-2,j,k)-4\cdot\cot\beta(i-1,j,k)+3\cdot\cot\beta(i,j,k)}{2} \f]
		dq_deta = ( -(*knoten)[k][i-2][j].cot_beta +4.0*(*knoten)[k][i-1][j].cot_beta-3.0*(*knoten)[k][i][j].cot_beta ) *0.5;
	}


	if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 && difver==0 )	//! Für Quasiorthogonale lautet die Gleichung:
	{
		//!	\f[ \frac{\partial q}{\partial \xi} = \frac{\cot\beta(i,j+1,k)-\cot\beta(i,j-1,k)}{2} \f]
		dq_dxi = ( (*knoten)[k][i][j+1].cot_beta-(*knoten)[k][i][j-1].cot_beta ) *0.5;
	}

	else if ( j == 0 || difver==1 )	//! bzw. für die erste Quasiorthogonale:
	{
		//!	\f[ \frac{\partial q}{\partial \xi} =  \frac{-\cot\beta(i,j+2,k)+4\cdot\cot\beta(i,j+1,k)-3\cdot\cot\beta(i,j,k)}{2} \f]
		dq_dxi = ( -(*knoten)[k][i][j+2].cot_beta +4.0*(*knoten)[k][i][j+1].cot_beta-3.0*(*knoten)[k][i][j].cot_beta ) *0.5;
	}

	else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 || difver==-1 )	//! und die letzte Quasiorthogonale:
	{
		//!	\f[ \frac{\partial q}{\partial \xi} = \frac{\cot\beta(i,j-2,k)-4\cdot\cot\beta(i,j-1,k)+3\cdot\cot\beta(i,j,k)}{2} \f]
		dq_dxi = ( -(*knoten)[k][i][j-2].cot_beta +4.0*(*knoten)[k][i][j-1].cot_beta-3.0*(*knoten)[k][i][j].cot_beta ) *0.5;
	}

	/*!
	Die Ableitung lautet also:
	\f[ \frac{\partial \cot \beta}{\partial r} = \frac{\frac{\partial q}{\partial \eta} \cdot \frac{\partial z}{\partial \xi}(i,j,k) -
	\frac{\partial q}{\partial \xi} \cdot \frac{\partial z}{\partial \eta}(i,j,k)}{\frac{\partial r}{\partial \eta}(i,j,k)
	\cdot \frac{\partial z}{\partial \xi}(i,j,k) - \frac{\partial z}{\partial \eta}(i,j,k) \cdot \frac{\partial r}{\partial \xi}(i,j,k)  } \f]
	*/
	return ( dq_deta*dz_dxi ( i,j,k)-dq_dxi*dz_deta ( i,j,k) ) /(dr_deta (i, j, k) *dz_dxi (i, j, k)-dz_deta (i, j, k) *dr_dxi (i, j, k));
}





double ableitung::ersteAbleitung_sigma ( int i, int j, int k )
{
	int difver = 0;
	bool center = false;
	bool down = false;
	bool up = false;
	//1=unbeschaufelt, 0=beschaufelt
	if ( j!=0 && j!=datenObjekt->anzahlQuasiOrthogonale[k]-1 )
	{
		if ( (*knoten)[k][i][j-1].unbeschaufelt ) down=1;
		if ( (*knoten)[k][i][j].unbeschaufelt ) center=1;
		if ( (*knoten)[k][i][j+1].unbeschaufelt ) up=1;
	}
	if ( ( down && center && !up ) || ( !down && !center && up ) )
	{
		difver = -1; //DOWNstream
	}
	else if ( ( !down && center && up ) || ( down && !center && !up ) )
	{
		difver = 1; //UPstream
	}
	else
	{
		difver = 0;
	}

	if ( datenObjekt->ablmod == 0 )	//! Im NURBS Ableitungsmodus werden die Ableitungen von \f$ \sigma, r, z \f$ an dem gesuchten Punkt durch die NURBS++ Bibliothek berechnet:
	{
		double delta_r = 0.0;
		double delta_z = 0.0;
		Vector_HPoint2Dd sigmaDerivative;

		(*quasiorthogonale)[k][j].nurbsSigma.deriveAtH ( (*knoten)[k][i][j].ny,1,sigmaDerivative );

		delta_r = sigmaDerivative[1].y();
		delta_z = sigmaDerivative[1].x();

		//!	\f[ \sigma' = \frac{\mathrm{d}\sigma}{\sqrt{(\mathrm{d}r)^2+(\mathrm{d}z)^2}}\f]
		return ( sigmaDerivative[1].w() /sqrt ( ( pow2(  delta_r ) +pow2(  delta_z ) ) ) );
	}

	else if ( datenObjekt->ablmod ==1 )	//! Mit Differenzenverfahren:
	{
		if ( j != datenObjekt->anzahlQuasiOrthogonale[k]-1 && j != 0 && difver==0 )
		{
			//!	\f[ \sigma' = \frac{\sigma(i,j+1,k)-\sigma(i,j-1,k)}{\sqrt{(r(i,j+1,k)-r(i,j-1,k))^2+(z(i,j+1,k)-z(i,j-1,k))^2}} \f]
			return ( (*knoten)[k][i][j+1].sigma-(*knoten)[k][i][j-1].sigma ) /sqrt ( pow2(  (*knoten)[k][i][j+1].z-(*knoten)[k][i][j-1].z ) +pow2(  (*knoten)[k][i][j+1].r-(*knoten)[k][i][j-1].r ) );
		}

		else if ( j == 0 || difver==1 )	//! bzw. für erste Quasiorthogonale:
		{
			//!	\f[ \sigma' = \frac{-\sigma(i,j+2,k)+4\cdot\sigma(i,j+1,k)-3\cdot\sigma(i,j,k)}{\sqrt{(r(i,j+2,k)-r(i,j,k))^2+(z(i,j+2,k)-z(i,j,k))^2}} \f]
			return ( -(*knoten)[k][i][j+2].sigma +4*(*knoten)[k][i][j+1].sigma-3*(*knoten)[k][i][j].sigma ) /sqrt ( pow2(  (*knoten)[k][i][j+2].z-(*knoten)[k][i][j].z ) +pow2(  (*knoten)[k][i][j+2].r-(*knoten)[k][i][j].r ) );
		}

		else if ( j == datenObjekt->anzahlQuasiOrthogonale[k]-1 || difver==-1 )	//! und letzte Quasiorthogonale:
		{
			//!	\f[ \sigma' = \frac{\sigma(i,j-2,k)-4\cdot\sigma(i,j-1,k)+3\cdot\sigma(i,j,k)}{\sqrt{(r(i,j-2,k)-r(i,j,k))^2+(z(i,j-2,k)-z(i,j,k))^2}} \f]
			return ( (*knoten)[k][i][j-2].sigma-4.0*(*knoten)[k][i][j-1].sigma +3.0*(*knoten)[k][i][j].sigma ) /sqrt ( pow2(  (*knoten)[k][i][j].z-(*knoten)[k][i][j-2].z ) +pow2(  (*knoten)[k][i][j].r-(*knoten)[k][i][j-2].r ) );
		}
	}
}

//================ ABLEITUNGSMETHODEN VON BADER FÜR LÖSER_DENTON ===========================================================

void ableitung::setze_Geometrieableitungen ( )
{
	//! Schleife über alle Gitter, Quasiorthogonalen und Stromlinien:
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				//! - \f$ \sin \varepsilon =\f$ ableitungshelfer.bildeErsteAbleitung_dm() mit Argumenten k, j, i, KE_r
				double sinepsilon = ableitungshelfer::bildeErsteAbleitung_dm( k, j, i, KE_r);
				(*knoten)[k][i][j].sin_epsilon = sinepsilon ;
				//! - \f$ \cos \varepsilon =\f$ ableitungshelfer.bildeErsteAbleitung_dm() mit Argumenten k, j, i, KE_z
				double cosepsilon = ableitungshelfer::bildeErsteAbleitung_dm( k, j, i, KE_z);
				(*knoten)[k][i][j].cos_epsilon = cosepsilon ;

				//! - \f$ \cos \gamma =\f$ ableitungshelfer.bildeErsteAbleitung_dq() mit Argumenten k, j, i, KE_z
				double cosgamma = ableitungshelfer::bildeErsteAbleitung_dq( k, j, i, KE_z);
				(*knoten)[k][i][j].cos_gamma = cosgamma;
				//! - \f$ \sin \gamma =\f$ ableitungshelfer.bildeErsteAbleitung_dq() mit Argumenten k, j, i, KE_r
				double singamma = ableitungshelfer::bildeErsteAbleitung_dq( k, j, i, KE_r);
				(*knoten)[k][i][j].sin_gamma = singamma;

				//Berechnung aus Aditionstheorem :
				//! - \f$ \sin \psi = \sin \gamma \cdot \cos \varepsilon - \cos \gamma \cdot \sin \varepsilon \f$
				(*knoten)[k][i][j].sin_psi = singamma * cosepsilon - cosgamma * sinepsilon ;
				//! - \f$ \cos \psi = \cos \gamma \cdot \cos \varepsilon + \sin \gamma \cdot \sin \varepsilon \f$
				(*knoten)[k][i][j].cos_psi = cosgamma * cosepsilon + singamma * sinepsilon ;

				//! - Krümmung = \f$ \cos \varepsilon \cdot \f$ [ableitungshelfer.bildeZweiteAbleitung_dm() mit Argumenten ( k, j, i, KE_r)] \f$ - \sin \varepsilon \cdot \f$ [ableitungshelfer.bildeZweiteAbleitung_dm() mit Argumenten ( k, j, i, KE_z)]
				(*knoten)[k][i][j].kruemmung = 	ableitungshelfer::bildeZweiteAbleitung_dm ( k, j, i, KE_r) * cosepsilon -
						ableitungshelfer::bildeZweiteAbleitung_dm ( k, j, i, KE_z) * sinepsilon ;

				//! - \f$ \tan \alpha = \sin \gamma \cdot \cot \beta_{ru} + \cos \gamma \cdot \cot \beta_{zu} \f$
				(*knoten)[k][i][j].tan_alpha = singamma  * cot((*knoten)[k][i][j].beta_ru)  + cosgamma * cot((*knoten)[k][i][j].beta_zu);
				//! - \f$ \cot \beta = \sin \varepsilon \cdot \cot \beta_{ru} + \cos \varepsilon \cdot \beta_{zu} \f$
				(*knoten)[k][i][j].cot_beta = sinepsilon * cot ( (*knoten)[k][i][j].beta_ru ) + cosepsilon * cot ( (*knoten)[k][i][j].beta_zu ) ;
				//! - \f$ \beta = \mathrm{acot} (\cot \beta) \f$
				(*knoten)[k][i][j].beta = acot((*knoten)[k][i][j].cot_beta);
			}
		}
	}
}





void ableitung::setze_dm_Ableitung()
{
	//! Schleife über alle Gitter, Quasiorthogonalen und Stromlinien:
	for ( int k=0;k<datenObjekt->anzahlGitter;k++ )
	{
		for ( int j=0;j<datenObjekt->anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i<datenObjekt->anzahlStromlinien; i++ )
			{
				//! - \f$ w_m\frac{\partial w_m}{\partial m} = \f$ ersteAbleitung_dwm_dm()
				(*knoten)[k][i][j].wmdwm_dm = ersteAbleitung_dwm_dm ( i,j,k );
				//! - \f$ r\frac{\partial c_u}{\partial m} = \f$ ableitungshelfer.bildeEinfachRueckwaertsAbleitung_dm() mit den Argumenten (k,j,i, KE_rc_u)
				(*knoten)[k][i][j].rcu_dm = ableitungshelfer::bildeEinfachRueckwaertsAbleitung_dm(k,j,i, KE_rc_u);

				//für die DGL Formulierung nach Benetschik wird zusätzlich folgende Ableitung benötigt:
				if (datenObjekt->dglFormulierung == DGL_benetschik)
				{
					//! - Für Benetschik: \f$ r\frac{\partial c_{u,2}}{\partial m} = \f$ ableitungshelfer.bildeEinfachRueckwaertsAbleitung_dm() mit den Argumenten (k,j,i, KE_rc_u_2)
					(*knoten)[k][i][j].rcu_2_dm = ableitungshelfer::bildeEinfachRueckwaertsAbleitung_dm(k,j,i,KE_rc_u_2);
				}
			}
		}
	}
}

double ableitung::ersteAbleitung_dwm_dm ( int i, int j, int k )
{
//	//solange die nurbsWmGitterSL nicht wieder initialisiert wird -> rausgenommen
//	if ( datenObjekt->ablmod == 0 ) // nurbs-Ableitungen
//	{
//		Vector_HPoint2Dd WmDerivative;
//		(*stromlinieGitter)[k][i].nurbsWmGitterSL.deriveAtH ( (*knoten)[k][i][j].myG,1,WmDerivative );
//		return ( (*knoten)[k][i][j].w_m * WmDerivative[1].w() /sqrt ( ( pow2(  WmDerivative[1].y() ) +pow2(  WmDerivative[1].x() ) ) ) );
//	}
//	else if ( datenObjekt->ablmod == 1 ) //Ableitung mit Differenzenmethode


	{// Rückwärtsdifferenzbildung wm * dwm_dm diskretisiert
		//! Solange nicht die erste Quasiorthogonale:
		if ( j != 0 )//nicht die erste QO
		{
			//! \f[ w_m\frac{\partial w_m}{\partial m} = \frac{w(j)_m^2-w(j-i)_m^2}{(z(j)-z(j-1))^2+(r(j)-r(j-1))^2}\f]
			return ( 0.5 *
					(pow2((*knoten)[k][i][j].w_m) - pow2((*knoten)[k][i][j-1].w_m ))
					/sqrt ( pow2(  (*knoten)[k][i][j].z-(*knoten)[k][i][j-1].z )
							+pow2(  (*knoten)[k][i][j].r-(*knoten)[k][i][j-1].r )));
		}
		//! Für die erste Quasiorthogonale:
		else if ( j == 0 )  //w_m * Vorwärtsdifferenzbildung
		{
			//! \f$ w_m\frac{\partial w_m}{\partial m} = w_m \cdot \f$ ableitungshelfer.bildeErsteAbleitung_dm() mit den Argumenten (k,j,i, KE_w_m)
			return ((*knoten)[k][i][j].w_m * ableitungshelfer::bildeErsteAbleitung_dm(k,j,i, KE_w_m));
		}
	}
	cout <<" Fehler bei ersteAbleitung_dwm_dm = 0 gesetzt: unbekannter Ableitungsmod " << endl;
	return 0.0;
}

