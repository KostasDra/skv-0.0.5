/***************************************************************************
 *   Copyright (C) 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *   				                                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Andrea Bader
*/



#ifndef GESCHWINDIGKEIT_DENTON_H
	#define GESCHWINDIGKEIT_DENTON_H

#include<cmath>

#include "daten.h"

//! Geschwindigkeitklasse für Denton
/*!
In der Geschwindigkeitklasse werden die benötigten Geschwindigkeiten berechnet.
\author Andrea Bader
*/
class geschwindigkeit_denton
{
	private:
		daten*				datenObjekt;
		v3Knoten*			knoten;
		vStromlinie*		stromlinie;
		v2StromlinieGitter*	stromlinieGitter;
		v2Quasiorthogonale* quasiorthogonale;


	public:
		geschwindigkeit_denton();



		//! Aufruf des Initialisieren der Meridiangeschwindigkeit.
		/*!
			Wenn Rotor im Berechengebiert vorhanden ist,
			erfolgt die initialisierung der Merdiangeschwindigkeit.

			\author Andrea Bader
		*/
		void initialisiere_w_m ();



		//! Initialisieren der Meridiangeschwindigkeit aus der Schluckziffer
		/*!
			Das gesamte Rechengebiet wird mit der gleichen, aus
			der Schluckziffer berechneten Meridiangeschwindigkeit
			initialisiert.

			\author Andrea Bader
		*/
		void wmausSchluckziffer ();



		//! Berechnung der relativen Umfangsgeschwindigkeit
		/*!
			Berechnet den Wert w_u = w_m * cot Beta.

			\param i aktuelle Stromlinie
			\param j aktuelle Quasiorthogonale
			\param k aktuelles Gitter

			\author Andrea Bader
		*/
		double berechne_w_u ( int i, int j, int k );

		//! Berechnung der absoluten Umfangsgeschwindigkeit
		/*!
			Berechnet den Wert c_u = w_u + omega * r.

			\param i aktuelle Stromlinie
			\param j aktuelle Quasiorthogonale
			\param k aktuelles Gitter

			\author Andrea Bader
		*/
		double berechne_c_u ( int i, int j, int k );

		//! Berechnung der Absolutgeschwindigkeit
		/*!
			Berechnet den Wert c = Wurzel (w_m Quadrat * c_u Quadrat).

			\param i aktuelle Stromlinie
			\param j aktuelle Quasiorthogonale
			\param k aktuelles Gitter

			\author Andrea Bader
		*/
		double berechne_c ( int i, int j, int k );

		//! Berechnung der Relativgeschwindigkeit
		/*!
			Berechnet den Wert w = Wurzel (w_m Quadrat * w_u Quadrat).

			\param i aktuelle Stromlinie
			\param j aktuelle Quasiorthogonale
			\param k aktuelles Gitter

			\author Andrea Bader
		*/
		double berechne_w ( int i, int j, int k );



		//! Bestimmen des Dralls
		/*!
			Die Berechnung des Dralls erfolgt in Abhängigkeit des QO-Typs
			(Kanal, Rotor, Stator).

			\param i aktuelle Stromlinie
			\param j aktuelle Quasiorthogonale
			\param k aktuelles Gitter

			\author Andrea Bader
		*/
		void bestimmeDrall(int k, int j, int i);


		//! Setzen der aller Geschwindigkeitskomponenten
		/*!
			Auf Basis des bereits berechneten Dralls werden alle
			Geschwindigkeitskomponenten berechnet.

			\param i aktuelle Stromlinie
			\param j aktuelle Quasiorthogonale
			\param k aktuelles Gitter

			\author Andrea Bader
		*/
		void updateGeschwindigkeitsDreieck(int k, int j, int i);


		//! Steuermethode zum Setzen des Deviationswinkels
		/*!
			In Abhängigikeit des Vorhandenseins von Stator oder Rotor
			wird für den Rotor der Deviationswinkel aus der Wiesner
			Schlupffaktor bestimmt. Für den Stator kann eine fester
			Deviationswinkel vorgeben werden.

			\author Andrea Bader
		*/
		void setze_deviation ();

		//! Deviationswinkel nach Wiesener Schlupffaktor berechnet
		/*!
			Der Deviationswinkel an der Rotorhinterkante wird nach dem
			Schlupffaktor von Wiesner berechnet.

			\param HK_k Gitter der Rotorhinterkante
			\param HK_j Quasiorthogonale der Rotorhinterkante

			\author Andrea Bader
		*/
		double bestimme_schlupf (int HK_j, int HK_k);



		//! Berechnung der Schallgeschwindigkeit
		/*!
			\param i aktuelle Stromlinie
			\param j aktuelle Quasiorthogonale
			\param k aktuelles Gitter

			\author Oliver Borm
		*/
		double berechne_schallGeschwindigkeit ( int i, int j, int k );





		//! Steuermethode zum Setzen der Schaufeltransparenz
		/*!
			In Abhängigikeit des Vorhandenseins von Stator oder Rotor
			wird die Methode zum Belegen der Transparenzfaktoren
			mit den entsprechenden Übergabeparametern aufgerufen.

			\author Andrea Bader
		*/
		void setzeSchaufelTransparenz();


		//! Setzen der Transparenzfaktoren der Vorder- und Hinterkante.
		/*!
			Die Transparenzfunktionen entlang einer Stromlinie werder
			für den beschaufelten Bereich berechnet und gesetzt.

			\author Andrea Bader
		*/
	private:
		//! Setzen der Schaufeltransparenz.
		/*!
			\param k aktuelles Gitter
			\param i aktuelle Stromlinie
			\param jVK Quasiorthogonale der Rotorvorderkante
			\param jHK Quasiorthogonale der Rotorhinterkante
			\param ZVK Schaufelanzahl Vorderkante
			\param ZHK Schaufelanzahl Hinterkante

			\author Andrea Bader
		*/
		void setzeSchaufelTransparenzSL(int k, int i, int jVK, int jHK, int ZVK, int ZHK);


	public:

		//! Setzen der DeHaller Kennzahl
		/*!
			Berechnet und setzt die DeHaller Kennzahl für jede Stromlinie
			innerhalb des Rotors. Nur zu Information/Ausgabezwecken.

			\author Andrea Bader
		*/
		void setze_deHaller();



		//! Setzt relativ Geschwindigkeit nach Rotor = absolut Geschwindigkeit
		/*!
			Zur Ausgabe wird die relativ Geschwindigkeit nach dem Rotor
			gleich der absolut Geschwindigkeit gesetzt. Nur zur Ausgabe,
			keine Beeinflussung der Berechnung.

			\param HK_k Gitter der Rotorhinterkante
			\param HK_j Quasiorthogonale der Rotorhinterkante

			\author Andrea Bader
		*/
		void setzeRelativGeschwindigkeitnachRotor ();


//######################  Nurbs initalisieren ##############################
// Die Methoden zum Erzeugen der gewichteten Nurbskurven sind nicht mehr in Verwendung, dennoch hier belassen,
// falls Ableitungsmethoden wieder auf NurbsAbleitungen umgestellte werden sollen.


		//! Sammelmethode zum aktualisieren aller Nurbsgeschwindigkeitskurven
		/*!
			Gesammelter Aufruf aller Nurbsgeschwindigkeitesmethoden

			\author Andrea Bader
		*/
		void aktualisiere_nurbsGeschwindigkeitskurven ( );


		//! Bestimmung einer Nurbskurve mit den Werten der Meridiangeschwindigkeit entlang einer Stromlinie
		/*!
			Erzeugt bzw. aktualisiert eine Nurbskurve, die zusaetzlich die Werte der
			Meridiangeschwindigkeit entlang einer Stromlinie beeinhaltet.

			\author Andrea Bader
		*/
		void aktualisiere_nurbsWmSL ( );


		//! Bestimmung einer Nurbskurve mit den Werten des Dralls entlang einer Stromlinie
		/*!
			Erzeugt bzw. aktualisiert eine Nurbskurve, die zusaetzlich die Werte des
			Dralls entlang einer Stromlinie beeinhaltet.

			\author Andrea Bader
		*/
		void aktualisiere_nurbsDrallSL ( );


		//! Bestimmung einer Nurbskurve mit den Quadrat des Dralls entlang einer Stromlinie
		/*!
			Erzeugt bzw. aktualisiert eine Nurbskurve, die zusaetzlich die Quadrate des
			Dralls entlang einer Stromlinie beeinhaltet.

			\author Andrea Bader
		*/
		void aktualisiere_nurbsQuadratDrallSL ( );


		//! Bestimmung einer Nurbskurve mit den Quadrat des Dralls entlang einer Quasiorhogonalen
		/*!
			Erzeugt bzw. aktualisiert eine Nurbskurve, die zusaetzlich die Quadrate des
			Dralls entlang einer Quasiorthogonale beeinhaltet.

			\author Andrea Bader
		*/
		void aktualisiere_nurbsQuadratDrallQO ();

//######################  Nurbs initalisieren Ende ##############################

};


#endif
