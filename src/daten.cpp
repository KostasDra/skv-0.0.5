/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm (oli.borm@web.de )             *
 *                 2008 by Florian Mayer (ilumine@gmx.de)                  *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope thied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,           at it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the impl                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm, Florian Mayer, Balint Balassa
*/

#include "daten.h"

daten::daten()
{
	workdir = "";  //vp: Falls argc < 2, "LeerString"
	// Berechnung spezifischer Gaskonstante R
	Rm 			= 8.314472;	//!< 8.314472 [J / (mol K)]
	mol 			= 0.028964;	//!< 0.028964 [kg / mol] für Luft
	R 			= Rm/mol;	//!< \f[	\frac{Rm}{mol}	\f] [J / (kg K)]

	gradNurbsKurven = 3;
	anzahlZwischenPunkte = 10;
	toleranz = 1.0e-5;
	errorMaxToleranz = 0.01;		//1.0;  //Konvergenzkriterium für Denton-Löser % wie error_max
	anzahlDurchlaeufe = 30;// 100;

	cpFt 			= true;
        ideal                   = true;
        err_cout                = false;
	//NASA Koeffizienten für temperaturabhängiges cp
	a11 			= 3.08792717E+00;
	a21 			= 1.24597184E-03;
	a31 			= -4.23718945E-07;
	a41 			= 6.74774789E-11;
	a51 			= -3.97076972E-15;

	a12 			= 3.56839620E+00;
	a22 			= -6.78729429E-04;
	a32 			= 1.55371476E-06;
	a42 			= -3.29937060E-12;
	a52 			= -4.66395387E-13;

	numberSL 		= 0;

	// Debugmodus
	debug 			= 0;		// 2=SPAM, 1=AN, 0=AUS, -1=Silent;

	inputFormat 		= 0;

	//Ausgabeparameter setzen
	zonename = "Block-";
	//Name der CGNS Eingabebedatei
	eingabeDatei = "scm0.cgns";
	//Name der CGNS Ausgabedatei
	ausgabeDatei = "scm1.cgns";
	//Ein Gitter pro Zone?
	//gitterMod = true;


	//Eingabeparameter
	globalVarDatei = "Globale_Variablen";
	gitterDatei = "Gitter_Variablen-%02i";

	//Geometrie Dist
	minDistError 		= 1.0e-6;
	minDistsizeSearch 	= 0.5;
	minDistsep 		= 9;
	minDistmaxiter 		= 100;
	minDistuMin 		= 0.0;
	minDistuMax 		= 1.0;
	// Relaxationsfaktor für neue Stromlinienlage
	relaxfac		= 0.1;
	wil	= true;

	//Loeser Daten
	pFaktor = 0.9;
	loeser_approx = false;
	MkritMin = 0.99;
	MkritMax = 1.01;

	maxTime	= 120; // Sekunden

	// Lösermod
	loeserAuswahl = LSR_expeu;	// LSR_expeu ODER LSR_denton


	//******************* nur Löser Expeu ***********************************************************************
	{
	gammaToleranz = 1.0e-10;  	//nur löser-expeu

	approxError 		= 5;  		//nicht verwendet, nurbs approx
	approxSL 		= false; 	//nicht verwendet, nurbs approx
	approxQO 		= false;	//nicht verwendet, nurbs approx
	loeser_approx 		= false;

	// Variablen für Interpolation zusaetzlicher Stromlinien
	interpSL 		= false;	//true=interp AN,false=interp AUS
	numberSL 		= 0;  		//=0 -> keine zusätzlichen Stromlinien zwischen die eingelesen interpoliert.

	// Ableitungsmodus
	ablmod 			= 1; 		//0=nurbs-Ableitungen, 1=Zentrale Differenz

	// Iterationsstart
	startit 		= false;	//false=Nabe,true=Kanalmitte;

	//Loeser Daten
	p_mindest 		= 1000.0;
	minimalVelocity		= 0.01;

	//Skelettflächen Daten für das Einlesen der Skelettfläche -> nicht löser_denton
	maxFehler 		= 0.00001 ;
	MaxIt 			= 100 ;

	//Stromlinien Daten
	stromlinieToleranz 	= 0.005;
	}


//******************* nur Löser Denton ***********************************************************************
	{
	anzahlMassDurchlaufe 	= 20;
	berechneMassenstromVorgabe = false;//true;

	//Dämpfung der Anpassung der Meridiangeschwinigkeit
	damp_wm			= 0.5;  	//Casey Empfehlung: 0.25

	//DGL Optionen
	dglFormulierung = DGL_denton;//DGL_benetschik;  	// DGL_denton ODER DGL_benetschik
	dglAusgabe 		= false  && !rechneKennfeld;	// Herausschreiben der DGL Ergebnisse in DGL.txt

	//Schaufelanzahl Rotor und Stator
	Z_rotorVK 		= 8;
	Z_statorVK 		= 19;

	schluckziffer_wm 	= 0.3;

	//Vorgabe des polytropen Wirkungsgrads um daraus Entropiegefälle zu berechnen.
	eta_polytrop_vorgabe 	= 1;

	//--------------------------------------------------------------
	// Start: Kennfeldberechung Steuervariablen
	//--------------------------------------------------------------
	{
		rechneKennfeld		= false;

		massenstromSchrittweite = 2.0;
		pumpgrenzeSuchen 	= false; 		// alternative: true, steuert kennfeld_manager
		kennlinieGrenzenSuchZaehler = 3;
		// Kennlinien nach Umfangsgeschwindigkeit auswÃ¤hlen
		double kennlinienListe[] = {			// befuellen der Drehzahlschleife
				250.0,
				300.0,
				350.0,
				400.0,
				450.0,
				475.0,
				500.0,
				525.0,
				550.0 ,
				560.0
		};

		{//Anzahl der Kennlinien variable -> Länge herausfinden -> muss hier stehen, aber NICHT ÄNDERN!!!
			int size = sizeof(kennlinienListe) / sizeof(double); // speicherplatz / speicherplatz pro double
			for(int z=0; z<size; z++) { vUmfangsGeschwindigkeiten.push_back(kennlinienListe[z]);	}
		}
	}
	//--------------------------------------------------------------
	// Ende
	//--------------------------------------------------------------


	//Methoden für Löser-Denton
	initialisiereLaufzeitDaten();
	zuruecksetzenLaufzeitDaten();
	}
	
}





daten* daten::getInstance()
{
	static daten dataInstance;
	return &dataInstance;
};





void daten::initialisiere()
{
	//! Berechne Anzahl an Quasiorthogonalen
	int temp = 0;
	int temp0 = 0;
	for ( int k=0; k< anzahlGitter; k++ )
	{
		temp0 = temp;
		temp = anzahlQuasiOrthogonale[k];
		temp = max ( temp, temp0 );
	}
	MAXanzahlQO = temp ;
        
	//! Berechne Anzahl aller unabhängigen Quasiorthogonalen
	for ( int k=0; k< anzahlGitter; k++ )
	{
		anzahlUniqueQO += anzahlQuasiOrthogonale[k];
		if ( k!=0 ) anzahlUniqueQO--;
	}

	stoss1.resize ( anzahlGitter );
	//! Initialisierung des Containers "Winkelgeschwindigkeit"
	omega.resize ( anzahlGitter );
	//! Initialisierung des Containers "Aufteilungsfaktor Massenstrom"
	faktorMassenStromRoehre.resize ( anzahlStromlinien );

	//! Initialisierung des Containers "Knoten" (3D)
	//3D_kij
	knoten.resize ( anzahlGitter, vector< vector<Knoten> > ( anzahlStromlinien, vector<Knoten>() ) );

	for ( int i=0; i< anzahlStromlinien; i++ )
	{
		for ( int k=0; k< anzahlGitter; k++ )
			knoten[k][i].resize ( anzahlQuasiOrthogonale[k] );
	}

	//! Initialisierung des Containers "Stromliniendaten" (2D und 1D)
	//2D_ik
	stromlinieGitter.resize ( anzahlGitter, vector<Stromliniendaten> ( anzahlStromlinien ) );
	//1D_i
	stromlinie.resize ( anzahlStromlinien );

	//! Initialisierung des Containers "Quasiorthogonalendaten" (2D)
	//2D_kj
	quasiorthogonale.resize ( anzahlGitter, vector<Quasiorthogonalendaten>() );
	for ( int k=0; k< anzahlGitter; k++ )
		quasiorthogonale[k].resize ( anzahlQuasiOrthogonale[k] );
}





void daten::origingrid()
{
	//! Schleife über alle Gitter, Quasiorthogonalen und Stromlinien:
	for ( int k=0;k< anzahlGitter;k++ )
	{
		for ( int j=0;j< anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i< anzahlStromlinien; i++ )
			{
				//! Speichern von \f$	r,z,\beta_{ru}, \beta_{zu}, \sigma, s	\f$
				knoten[k][i][j].r_org = knoten[k][i][j].r;
				knoten[k][i][j].z_org = knoten[k][i][j].z;
				knoten[k][i][j].betaru_org = knoten[k][i][j].beta_ru;
				knoten[k][i][j].betazu_org = knoten[k][i][j].beta_zu;
				knoten[k][i][j].sigma_org = knoten[k][i][j].sigma;
				knoten[k][i][j].entropie_org = knoten[k][i][j].entropie;
			}
		}
	}
}

void daten::origingrid2()
{
	//! Schleife über alle Gitter, Quasiorthogonalen und Stromlinien:
	for ( int k=0;k< anzahlGitter;k++ )
	{
		for ( int j=0;j< anzahlQuasiOrthogonale[k];j++ )
		{
			for ( int i=0; i< anzahlStromlinien; i++ )
			{
				//! Speichern von ursprüglicher Versperrung \f$  \sigma \f$
				knoten[k][i][j].sigma_original = knoten[k][i][j].sigma;
				knoten[k][i][j].unbeschaufelt = ( knoten[k][i][j].sigma_original < ( 1.0 - toleranz ) ) ?  false : true;
			}
		}
	}
	origingrid();
}



//*********** Methoden für Löser Denton ***********************************************

void daten::initialisiereLaufzeitDaten()
{
	//berechnete Größen
	Z_rotorHK = 2 * Z_rotorVK; //zwischen jeder Hauptschaufel Splitter vorhanden
	Z_statorHK = Z_statorVK;
	rotorVK_j=0;
	rotorVK_k=0;
	rotorHK_j=0;
	rotorHK_k=0;

	statorVK_j=0;
	statorVK_k=0;
	statorHK_j=0;
	statorHK_k=0;

	deviationHKrotor = 0.0;
	deviationHKstator = 0.0;

	v_red = 0.0;
	Pi_tot = 0.0;

	eta_opt = 0.0;
	schluckziffer_opt = 0.0;
	schluckziffer_aktuell= 0.0;
}


void daten::zuruecksetzenLaufzeitDaten()
{
	for(int l=0; l<5; l++) { error_history[l] = 1000.0; }
	loeser_resultCode = LRESULT_unknown;
}

void daten::setzeQOTyp()
//! Wird nicht mehr benutzt?!
{  //rotor, stator, kanal in einlesen belegt
	if (!rechneKennfeld && debug >=0)
	{
		cout <<"-------------------------------------------------------------------------------------------------"<<endl;
		cout << "Gitterinformation : " <<endl;
	}
	int tempVK  = 0;
	int tempEin = 0;
	int tempAus = 0;

	for ( int k=0;k< anzahlGitter;k++ )
	{
		for ( int j=0;j< anzahlQuasiOrthogonale[k];j++ )
		{
			// initialisieren mit false
			quasiorthogonale[k][j].vorderkante = false;
			quasiorthogonale[k][j].hinterkante = false;
			// eintritt
			if ( k == 0 && j == 0 )  //allererste QO
			{
				quasiorthogonale[k][j].eintritt = true;
				tempEin = j;
			}
			else {
				quasiorthogonale[k][j].eintritt = false;
			}

			// austritt
			if ( k == anzahlGitter-1 && j == anzahlQuasiOrthogonale[k]-1)  //letze QO überhaupt
			{
				quasiorthogonale[k][j].austritt = true;
				tempAus = j;
			}
			else {
				quasiorthogonale[k][j].austritt = false;
			}

			if (quasiorthogonale[k][j].typ != QOT_Kanal)
			{
				// typenwechsel - aber nur für stator und rotor
				if (j>0 && quasiorthogonale[k][j].typ != quasiorthogonale[k][j-1].typ)
				{
					// vorderkante
					quasiorthogonale[k][j].vorderkante = true;

					tempVK = j;
				}
				else if (j<anzahlQuasiOrthogonale[k]-1 && quasiorthogonale[k][j].typ != quasiorthogonale[k][j+1].typ)
				{
					// hinterkante
					int tempHK = j;
					quasiorthogonale[k][j].hinterkante = true;
					if (quasiorthogonale[k][j].typ == QOT_Rotor)
					{
						rotorVK_j = tempVK;
						rotorHK_j = tempHK;
						rotorVK_k = rotorHK_k = k;
					}
					else if (quasiorthogonale[k][j].typ == QOT_Stator)
					{
						statorVK_j = tempVK;
						statorHK_j = tempHK;
						statorVK_k = statorHK_k = k;
					}
					if (!rechneKennfeld && debug >=0) {
						cout << "Der "<< quasiorthogonale[k][j].typeToString() << " geht von j= " << tempVK << " bis j= " << tempHK << endl;
					}
					tempVK = 0;
				}
				else if (j == anzahlQuasiOrthogonale[k]-1) // wenn auf rotor oder stator kein kanal folgt, HK direkt belegen
				{
					if (quasiorthogonale[k][j].typ == QOT_Rotor) {
						rotorHK_j = j;
						rotorHK_k = k;
					} else if (quasiorthogonale[k][j].typ == QOT_Rotor) {
						statorHK_j = j;
						statorHK_k = k;
					}

					if (!rechneKennfeld && debug >=0) {
						cout << "Der "<< quasiorthogonale[k][j].typeToString() << " geht von j= " << tempVK << " bis j= " << j << endl;
					}
					tempVK = 0;
				}
			}
		}
	}
}

bool daten::konvergenz_Gefahr(double error_max)
{
	double fehlerMittel = 0.0;

	for (int l=0; l<2; l++) //
	{
		// schiebe die alten werte nach vorne
		error_history[l] = error_history[l+1];
	}
	error_history[1] = fabs(error_max) ;
	fehlerMittel = error_history[1]/error_history[0];

	if ( fehlerMittel > 1.5)
	{
		DEBUG_1("Konvergenzproblem error_max Verhaeltnis zu gross =  " << fehlerMittel);
		loeser_resultCode = LRESULT_error;
		return true;
	}
	else
	{
		return false;
	}
}

void daten::setzeUOderOmega()
{
	if (omega[rotorHK_k] == 0) // u eingelesen
	{
		double r = knoten[rotorHK_k][0][rotorHK_j].r;
		double omega_temp = u_ref / r;
		omega[rotorHK_k] = omega_temp;
	}
}

