/***************************************************************************
 *   Copyright (C) 2008-2009 by Florian Mayer (ilumine@gmx.de)             *
 *                 2009-2010 by Andrea Bader (bader.andrea@gmx.de)         *
 *                 2009 by Oliver Borm (oli.borm@web.de)                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Florian Mayer, Balint Balassa
*/


#include <assert.h>
#include "Knoten.h"
#include "daten.h"
#include "realFluid.h"


//Konstruktor setzt Initialwerte für ersten Durchlauf
Knoten::Knoten()
{
	cp		= 1004.5;
	

	beta		= 0.5*M_PI;
	beta_zu		= 0.5*M_PI;
	beta_ru		= 0.5*M_PI;
	beta_St		= 0.5*M_PI;
	alpha_St	= 0.5*M_PI;

// o.b: 2011-06-29: Warum ist das auskommentiert?
// b.b: 2011-08-16: wieder aktiviert
	ny = 0.0;
	ny_alt = 0.0;

	w_m		= 50.0; //nicht 0, falls nicht eingelesen und kein Rotor vorhanden
	c_u		= 0.0;
	rc_u		= 0.0;
	
	p		= 1e4;
	T		= 250;
        
        if (!daten::getInstance()->ideal){
        ptemp           = 6000;
        Ttemp           = 250;
        
        if (p<ptemp){
            p=ptemp;
            daten::getInstance()->err_cout=true;
        }
        if (T<Ttemp){
            T=Ttemp;
            daten::getInstance()->err_cout=true;
        }
        if (daten::getInstance()->err_cout){
            std::cout<<"The real fluid solvers accept pressures of over 6kPa and temperatures over 250K"<<std::endl;
        }
        }
        update_kappa();
        
	p_tot_rel	= 1e4;
	p_krit		= 1e4;

	p_tot_abs	= 1e4;
	T_tot_abs	= 250;

	error		= 0.0;
	massenStromRoehre = 0.0;

	faktorHK	= 0.0;
	faktorVK	= 0.0;
	mySchaufel	= 0.0;
}

// o.b: 2011-06-29: Das ist nicht der Zweck von Knoten.h; dringend umschreiben!
double Knoten::bekommeKnotenWert(EKnotenEigenschaft ke)
{
	switch (ke)
	{
	case KE_r:
		return r;
		break;
	case KE_z:
		return z;
		break;
	case KE_entropie:
		return entropie;
		break;
	case KE_rc_u:
		return rc_u;
		break;
	case KE_rc_u_2:
		return rc_u * rc_u;
		break;
	case KE_w_m:
		return w_m;
		break;
	case KE_totalenthalpie:
		return totalenthalpie;
		break;
	default:
		cout << "verwendetet Knoteneigenschaft nicht definiert! -> Programmabbruch " <<endl;
		assert(0);
	}
}


// Berechnet cp-Wert anhand der Temperatur neu (NASA POLYNOMIALS); nur gültig für Luft
void Knoten::update_cp(double T)
{
	//! \f[	c_p(T) = R \cdot \left( a_1 + a_2 \cdot T + a_3 \cdot T^2 + a_4 \cdot T^3 + a_5 \cdot T^4 \right)	\f]
	//! Die Koeffizienten sind im daten Objekt zu finden.
	if ( daten::getInstance()->cpFt ) {
            if(daten::getInstance()->ideal||realFluid::getFluid()->is_initialising) {
		double a1, a2, a3, a4, a5;
		if (T < 1000.0) {
		a1 = daten::getInstance()->a11;
		a2 = daten::getInstance()->a21;
		a3 = daten::getInstance()->a31;
		a4 = daten::getInstance()->a41;
		a5 = daten::getInstance()->a51;
		}
		else {
		a1 = daten::getInstance()->a12;
		a2 = daten::getInstance()->a22;
		a3 = daten::getInstance()->a32;
		a4 = daten::getInstance()->a42;
		a5 = daten::getInstance()->a52;
		}
		cp = daten::getInstance()->R * (a1 + a2 * T + a3 * pow2(T) + a4 * pow3(T) + a5 * pow4(T));
            }
            else{
                
                realFluid::getFluid()->update(0,p,T);
                
                cp = realFluid::getFluid()->myFluid->cpmass();
                //std::cout<<"The Cp value is: " << cp <<std::endl;
                //std::cout<<"The T value is: " << T <<std::endl;
            }
	}
        //std::cout<<"The Cp value is: " << cp <<std::endl;
	update_kappa();
}

void Knoten::update_kappa()
{
    	//! \f[	\kappa = \frac{c_p}{R}	\f]
    if (!daten::getInstance()->ideal and !realFluid::getFluid()->is_initialising){
        kappa = cp / (realFluid::getFluid()->myFluid->cvmass());
    }else{
	kappa = cp/ ( cp-daten::getInstance()->R );
    }
}
