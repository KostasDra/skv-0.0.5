/***************************************************************************
 *   Copyright (C) 2009-2010 Andrea Bader (bader.andrea@gmx.de)            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Andrea Bader
*/


#include <assert.h>
#include "temperatur_denton.h"
#include <cmath>


temperatur_denton::temperatur_denton()
{
	datenObjekt 		= daten::getInstance();
	knoten 				= &datenObjekt->knoten;
	quasiorthogonale 	= &datenObjekt->quasiorthogonale;
}

double temperatur_denton::berechne_T_tot_abs ( int i, int j, int k )
{
	//! \f[ T_{tot,abs} = \frac{h_{tot}}{c_p} \f]
	return ((*knoten)[k][i][j].totalenthalpie / (*knoten)[k][i][j].cp );
}


double temperatur_denton::berechne_T ( int i, int j, int k )
{
	double T_value;
	if ( j== 0 )
	{
		if ( k == 0 )  	//auf 1.QO ist T_tot eingelesen
		{
			//!\f[ T = T_{tot,abs} - \frac{c^2}{2c_p} \f]
			T_value = (*knoten)[k][i][j].T_tot_abs - ( pow2( (*knoten)[k][i][j].c )/( 2 * (*knoten)[k][i][j].cp));
		}
		else  		   //1.QO auf nachfolgendem Gitter -> übertragen
		{
			T_value = (*knoten)[k-1][i][datenObjekt->anzahlQuasiOrthogonale[k-1]-1].T ;
		}
	}
	else  			   //T_tot erst berechnen
	{
		double T_tot_abs = berechne_T_tot_abs ( i,j,k );
		(*knoten)[k][i][j].T_tot_abs = T_tot_abs;
		T_value = T_tot_abs - ( pow2( (*knoten)[k][i][j].c )/( 2 * (*knoten)[k][i][j].cp));
	}

	DEBUG_2(	" T = " << T_value << endl<<
				" c = " << (*knoten)[k][i][j].c  << endl<<
				" T_tot_abs = " << (*knoten)[k][i][j].T_tot_abs );

	
	//Fehlercode für Kennfeldberechnung -> Fehler melden
	if (T_value <= 0.0)
	{
		datenObjekt->loeser_resultCode = LRESULT_error;

		if (!datenObjekt->rechneKennfeld)
		{
			assert(T_value >= 0.0); //Programmabbruch nahe der Schluckgrenze, T < 0.0  durch starke Schwankung
		}
	}



	return T_value;
}






