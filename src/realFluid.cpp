/* 
 * File:   realFluid.cpp
 * Author: kostas
 * 
 * Created on April 7, 2015, 12:20 AM
 */

#include "realFluid.h"

realFluid::realFluid() {
    /*backendName = "HEOS";
    fluidNamev.push_back("Air");
    fractions.push_back(1.0);
    setFluid(backendName,fluidNamev);*/
    is_initialising=true;
}

void realFluid::update(const int j, double X, double Y){
    
    if(j==0)
    {
        if (X<1e4){X=1e4;}
        if (Y<250){Y=250;}
        myFluid->update(PT_INPUTS,X,Y);

    }else if(j==1)
    {
        myFluid->update(HmassT_INPUTS,X,Y);
    }
}


/*void realFluid::setFluid(string &mybackend, std::vector<string> &fluidN){
    myFluid.reset(AbstractState::factory(mybackend,fluidN));
}*/

void realFluid::readFluid(const string &path){
    ifstream file;
    string temp0;
    double temp1;
    
    file.open(path.c_str());
    if(file.is_open()){
        std::getline(file,backendName);
        while(!file.eof()){
            file >> temp1;
            file >> temp0;
            fractions.push_back(temp1);
            fluidNamev.push_back(temp0);
        }
    }else{
        std::cout << "Could not open file!! \n";
    }
    fractions.pop_back();
    fluidNamev.pop_back();
    /*std::string name;
    for (int i=0; i<fluidNamev.size();i++){
        name.append(fluidNamev[i]);
        name.append("&");
    }
    name.erase(name.length()-1,1);
    std::cout << fluidNamev.size() << std::endl;*/
    //myFluid.reset(AbstractState::factory(backendName, fluidNamev));
    shared_ptr<AbstractState> temp(AbstractState::factory(backendName,fluidNamev));
    myFluid = temp;
    if(fractions.size()>1){myFluid->set_mole_fractions(fractions);}
    //setFluid(backendName, fluidNamev);
    //myFluid->update(PT_INPUTS,1e5,300);
    std::cout << "Fluid set successfully " << myFluid->T()<< std::endl;
}

realFluid* realFluid::getFluid(){
    static realFluid myInstance;
    return &myInstance;
}

realFluid::realFluid(const realFluid& orig) {
}

realFluid::~realFluid() {
}

