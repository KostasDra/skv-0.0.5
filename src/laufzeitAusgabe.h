/***************************************************************************
 *   Copyright (C)  2009-2010 by Andrea Bader (bader.andrea@gmx.de)	   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


/**
 * LaufzeitAusgabe --
 *
 * Hilfsklasse zur Steuerung der zusätzlichen Ausgabefiles:
 *	- DGL Ausgabe
 *  - Fehlerverlauf
 *
 * \author Andrea Bader
 */


#ifndef _LAUFZEITAUSGABE_H_
#define _LAUFZEITAUSGABE_H_


class LaufzeitAusgabe
{
public:
	LaufzeitAusgabe(std::string workingDir)
		: pfadDglAusgabe(workingDir + "DGL.txt")
		, pfadFehlerAusgabe( workingDir + "Fehlerverlauf.txt" )
	{
		// Keine weiteren Initalisierungen.
		// Passieren bei erster Verwendung (siehe getStream()).
	}

	ofstream& getStream( )
	{
		if (dglos == NULL)
		{
			dglos = new std::ofstream( pfadDglAusgabe.c_str() );
			dglos->precision ( 9 );
			(*dglos) << fixed << "DGL Ausgabe" << endl;
		}

		return *dglos;
	}

	ofstream& getVerlaufStream()
	{
		if (verlauf_os == NULL)
		{
			verlauf_os = new std::ofstream( pfadFehlerAusgabe.c_str() );
		}

		return *verlauf_os;
	}

	~LaufzeitAusgabe()
	{
		if (dglos != NULL) {
			dglos->close();
		}

		if (verlauf_os != NULL) {
			verlauf_os->close();
		}
	}

private:
	string pfadDglAusgabe;
	string pfadFehlerAusgabe;

	std::ofstream *dglos;
	std::ofstream *verlauf_os;
};

#endif
