/***************************************************************************
 *   Copyright (C) 2005-2009 by Oliver Borm                                *
 *   oli.borm@web.de                                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/**
@author Oliver Borm
*/

#ifndef AUSGABE_H
	#define AUSGABE_H

#include <fstream>
#include <string>
#include <nurbs++/nurbs.h>

#include "daten.h"
#include "CGNSformat.h"

using namespace std;
//! Ausgabeklasse
/*!
In der Ausgabeklasse werden die Ausgabewerte ausgegeben.
\author Oliver Borm
*/

class ausgabe
{
	private:
		CGNSformat 		data;
		daten 			*datenObjekt;
		v2Quasiorthogonale	*quasiorthogonale;
		vStromlinie		*stromlinie;
		

	public:
		ausgabe();
		
		//! Datenausgabe
		/*!
		    Mit Hilfe dieser Funktion werden alle gewollten
		    Daten der Berechnung ausgegeben.
		*/
		void ausgabeDaten ();
};




#endif
